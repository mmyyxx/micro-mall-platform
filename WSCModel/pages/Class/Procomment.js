// pages/Class/ProductDetail.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //屏幕高度
    PageHeight: app.globalData.systemInfo.windowHeight,
    //屏幕宽度
    PictureHeight: app.globalData.systemInfo.windowWidth,
    //按钮宽度
    BtnWidth: app.globalData.systemInfo.windowWidth / 2 - 48,
    //滚动条
    scrollTop: 0,
    //商品主键
    ProductGUID: '',
    //商品信息
    ProductInfo: null,
    //是否显示限时
    IsLimitShow: false,
    //明细图片
    DetailPictures: [],
    //计时器控件
    LimitTimeComponents: null,
    //商品选择是否显示
    IsChooseShow: false,
    //方式：0.加入购物车 1.立即购买
    BuyType: 0,
    //是否是众筹
    IsZC: false,
    //已众筹数量
    IsZCCount: 0,
    //是否销售完
    IsSaleEnd: false,
    //点赞数量
    ZanCount: '0',
    //是否点赞
    IsZan: '1',
    //商品星级
    starts:4
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.zc) {
      that.setData({
        IsZC: options.zc == 'true' ? true : false
      })
      if (that.data.IsZC) {
        util.postMethods('GetZCCount', {
          productguid: options.code
        }, function (data) {
          if (data.IsOK) {
            that.setData({
              IsZCCount: parseInt(data.Data)
            })
          }
        })
      }
    }
    //获取需要显示的商品主键
    if (options.code) {
      that.setData({
        ProductGUID: options.code
      })
      util.postMethods('GetProductInfoByGUID', {
        guid: options.code
      }, function (data) {
        if (data.IsOK) {
          var newProductInfo = JSON.parse(data.Data);
          //初始化详情图
          var pictures = [];
          pictures.push(newProductInfo.DetailPictureList[0]);
          that.setData({
            ProductInfo: newProductInfo,
            DetailPictures: pictures
          })
          if (newProductInfo.IsLimitTimeProduct == 0) {
            that.setData({
              IsLimitShow: true
            })
            that.setData({
              LimitTimeComponents: that.selectComponent("#LimitTime")
            })
            that.data.LimitTimeComponents.TimerShow(newProductInfo.LimitEndTime);
            var totalSecond = parseInt((parseInt(newProductInfo.LimitEndTime.replace("/Date(", "").replace(")/", "")) - Date.now()) / 1000);
            if (totalSecond <= 0) {
              that.setData({
                IsSaleEnd: true
              });
            }
          }
        }
      })
    }

    //点赞情况
    util.postMethods('GetZanCountByGUID', {
      guid: options.code,
      openId: app.globalData.openid
    }, function (data) {
      if (data.IsOK) {
        that.setData({
          ZanCount: data.Data,
          IsZan: data.msg
        })
      }
    })
  },
  //点赞
  BtnZan: function () {
    var that = this
    //点赞情况
    util.postMethods('UserZan', {
      guid: that.data.ProductGUID,
      openId: app.globalData.openid
    }, function (data) {
      if (data.IsOK) {
        //点赞情况
        util.postMethods('GetZanCountByGUID', {
          guid: that.data.ProductGUID,
          openId: app.globalData.openid
        }, function (result) {
          if (result.IsOK) {
            that.setData({
              ZanCount: result.Data,
              IsZan: result.msg
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    if (this.data.LimitTimeComponents != null) {
      this.data.LimitTimeComponents.TimeEnd()
    }
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    if (this.data.LimitTimeComponents != null) {
      this.data.LimitTimeComponents.TimeEnd()
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //滚动到底部触发事件  
  ShowScrollLower: function (e) {
    let that = this;
    var pictures = that.data.ProductInfo.DetailPictureList;
    var showpicrures = that.data.DetailPictures;
    if (showpicrures.length < pictures.length) {
      showpicrures.push(pictures[showpicrures.length]);
      that.setData({
        DetailPictures: showpicrures
      });
    }
  },
  //返回主页面
  PageReLaunch: function (e) {
    wx.switchTab({
      url: '/pages/' + e.currentTarget.id + '/' + e.currentTarget.id
    })
  },
  //返回顶部
  ReturnTop: function () {
    this.setData({
      scrollTop: 0
    });
  },
  //加入购物车
  AddUserShop: function () {
    this.setData({
      IsChooseShow: true,
      BuyType: 0
    });
  },
  //立即购买
  UserBuy: function () {
    this.setData({
      IsChooseShow: true,
      BuyType: 1
    });
  },
  //关闭商品选择
  CloseView: function () {
    this.setData({
      IsChooseShow: false
    });
  },
  //确认商品选择
  BtnOK: function () {
    //是否可以购买
    if (this.data.IsLimitShow && !app.globalData.IsMember) {
      wx.showModal({
        content: "该商品只有会员可以购买！",
        confirmColor: "#F3ABC6",
        confirmText: "注册会员",
        success: function (res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/Setting/MemberRegister'
            })
          }
        }
      })
      return;
    }
    //商品选择控件
    var ProductChoose = this.selectComponent("#ProductChoose")
    //类别选择
    var selectItems = ProductChoose.data.SelectClassItem
    //选择数量
    var productNum = ProductChoose.data.Count;
    for (var m = 0; m < selectItems.length; m++) {
      if (selectItems[m] == '') {
        wx.showModal({
          content: "请选择商品属性！",
          showCancel: false,
          confirmColor: "#E3101E",
          confirmText: "确定"
        })
        return;
      }
    }
    if (this.data.BuyType == 0) {
      //加入购物车
      if (this.data.ProductInfo.IsGroupProduct == 0) {
        //参与拼团
        wx.navigateTo({
          url: '/pages/Shop/ParticipateGroup?code=' + selectItems[selectItems.length - 1]
        })
      } else {
        //加入购物车
        //获取已加入购物车的数据
        var userShopList = [];
        var list = wx.getStorageSync('UserShopList')
        if (list) {
          userShopList = list;
        }
        for (var m = 0; m < userShopList.length; m++) {
          //已经存在购物车中
          if (selectItems[selectItems.length - 1] == userShopList[m]) {
            var value = wx.getStorageSync(selectItems[selectItems.length - 1])
            wx.setStorage({
              key: selectItems[selectItems.length - 1],
              data: value + productNum
            })
            wx.showToast({
              title: '加入成功',
              image: '/resources/Icon/Toast_Success.png',
              mask: true
            })
            this.setData({
              IsChooseShow: false
            });
            return;
          }
        }
        //加入购物车
        userShopList.push(selectItems[selectItems.length - 1]);
        wx.setStorage({
          key: 'UserShopList',
          data: userShopList
        })
        wx.setStorage({
          key: selectItems[selectItems.length - 1],
          data: productNum
        })
        wx.showToast({
          title: '加入成功',
          image: '/resources/Icon/Toast_Success.png',
          mask: true
        })
        this.setData({
          IsChooseShow: false
        });
      }
    } else {
      //立即购买
      for (var m = 0; m < selectItems.length; m++) {
        if (selectItems[m] == '') {
          wx.showModal({
            content: "请选择商品属性！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
          return;
        }
      }
      if (this.data.ProductInfo.IsGroupProduct == 0) {
        //发起拼团
        wx.navigateTo({
          url: '/pages/Shop/Order?group=true&&code=' + selectItems[selectItems.length - 1] + "&&num=" + productNum
        })
      } else {
        //立即购买
        wx.navigateTo({
          url: '/pages/Shop/Order?code=' + selectItems[selectItems.length - 1] + "&&num=" + productNum
        })
      }
    }
  }
})