// pages/Class/evaluate.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [{
      GroupPeople: 0,
      IsLimit: 1,
      NewPrice: 75,
      OldPrice: 78,
      ProductDescribe: null,
      ProductSale: 0,
      ProductUnit: "瓶",
      ProductZan: 0,
      Stock: 0,
    }],
    showMessage: false,
    typeNumber: [],
    eval: [],
    OrderGUID: null,
    OrderInfo: null,
    m_des: 0,
    m_du: 0,
    m_wuliu: 0,
    n_name: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    util.postMethods('GetOrderDetail', {
      guid: options.id
    }, function (data) {
      if (data.IsOK) {
        data = JSON.parse(data.Data);
        that.setData({
          list: data.OrderProductList,
          OrderGUID: options.id,
          OrderInfo :data
        })
        for (var i = 0, j = data.OrderProductList.length; i < j; i++) {
          that.data.typeNumber.push('');
          that.data.eval.push('');
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //类别选择
  saleTypeClick: function (e) {
    this.data.typeNumber[e.currentTarget.dataset.index] = e.currentTarget.dataset.key;
    this.setData({
      typeNumber: this.data.typeNumber
    })
  },
  bindinput: function (e) {
    this.data.eval[e.currentTarget.dataset.index] = e.detail.value;
  },
  //是否匿名
  unameSelect:function(e){
    this.setData({
      n_name: !this.data.n_name
    })
  },
  //描述评分
  mDes:function(e){
    let m_mber = e.currentTarget.dataset.mdes;
    this.setData({
      m_des: m_mber
    })
  },
  //物流服务
  mWu:function(e){
    let m_wl = e.currentTarget.dataset.wuliu;
    this.setData({
      m_wuliu: m_wl
    })
  },
  //服务态度
  mFuWu: function (e) {
    let m_fuwu = e.currentTarget.dataset.wufu;
    this.setData({
      m_du: m_fuwu
    })
  },
  submitEvalute:function(e){
    var that = this;
    console.log(that.data.list);

    var codeid = '';
    var typeNumber='';
    var evals ='';
    var productParam = '';
    var productCount = '';
    var productName = '';

    for(var i=0,j=that.data.typeNumber.length;i<j;i++){
      var product = that.data.list[i];
      if (!that.data.typeNumber[i]){
        wx.showModal({
          content: "请评价第" + util.cnumber(i+1)+"个商品",showCancel: false,confirmColor: "#E30C1A",confirmText: "确定"
        })
        return;
      }
      codeid += product.GUID + ';';
      productParam += product.ProductParam + ';';
      typeNumber += that.data.typeNumber[i] + ';';
      evals += that.data.eval[i] + ';';
      productCount += product.ProductCount + ';';
      productName += product.ProductName+';';
    }
    if (that.data.m_des == 0) {
      wx.showModal({
        content: "请给描述打分", showCancel: false, confirmColor: "#E30C1A", confirmText: "确定"
      })
      return;
    }
    if (that.data.m_wuliu == 0) {
      wx.showModal({
        content: "请给物流打分", showCancel: false, confirmColor: "#E30C1A", confirmText: "确定"
      })
      return;
    }
    if (that.data.m_du == 0) {
      wx.showModal({
        content: "请给服务打分", showCancel: false, confirmColor: "#E30C1A", confirmText: "确定"
      })
      return;
    }
    util.postMethods('SavePingJia', {
      OrderGUID: that.data.OrderGUID,
      OrderNumber: that.data.OrderInfo.OrderNumber,
      openid: app.globalData.openid,
      m_des: that.data.m_des,
      m_du: that.data.m_du,
      m_wuliu: that.data.m_wuliu,
      n_name: that.data.n_name,
      codeid: codeid,
      eval: evals,
      typeNumber: typeNumber,
      productParam: productParam,
      productCount: productCount,
      productName: productName
    }, function (data) {
      console.log(data);
      if (data.IsOK) {
        that.setData({
          showMessage: true
        })
        setTimeout(function () {
          that.setData({
            showMessage: false
          })
          wx.switchTab({
            url: '/pages/Setting/Setting',
          })
        }, 3000)
      }
      else {
        wx.showModal({
          content: data.msg, showCancel: false, confirmColor: "#E30C1A", confirmText: "确定"
        })
      }
    })
  }
})