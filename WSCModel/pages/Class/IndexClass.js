// pages/Class/Class.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //分类数据
    ClassModelList: [],
    //选中分类
    ChooseItem: '',
    //选中分类子分类
    ChildCategoryList: null,
    //列表高度
    ListHeight: app.globalData.systemInfo.windowHeight,
    //列表控件
    ListComponent: null
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: options.title
    })
    var that = this;
    util.postMethods('GetClassData', {
      appId: app.globalData.appId
    }, function(data) {
      if (data) {
        that.setData({
          ClassModelList: data,
          ChildCategoryList: data[0].ChildCategoryList,
          ChooseItem: data[0].CategoryCode,
          ListComponent: that.selectComponent("#List")
        })
        that.CategoryChoose(options.code)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //选中分类
  CategoryChoose: function(code) {
    for(let calssmodel in this.data.ClassModelList){
      if(this.data.ClassModelList[calssmodel].CategoryCode == code){
        var newList = this.data.ClassModelList[calssmodel].ChildCategoryList
        this.setData({
          //选中分类
          ChooseItem: code,
          ChildCategoryList: newList
        })
        this.BtnSearch()
        return
      }
    }
    this.setData({
      //选中分类
      ChooseItem: code
    })
    this.BtnSearch()
  },
  //搜索
  BtnSearch: function() {
    if (this.data.ChildCategoryList.length > 0) {
      this.data.ListComponent.ShowChildCategory(this.data.ChildCategoryList)
    } else {
      this.data.ListComponent.SearchProducts({
        ChooseItem: this.data.ChooseItem,
        SearchStr: '',
        Orderstr: '',
        Ordertype: 0
      }, true)
    }
  },
  //搜索页面
  navigateToSearch: function () {
    wx.navigateTo({ url: '/pages/Class/ProductSearch' })
  }
})