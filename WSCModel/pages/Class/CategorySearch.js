// pages/Class/CategorySearch.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //列表高度
    ListHeight: app.globalData.systemInfo.windowHeight - 50,
    //搜索条件
    Searchstr: '',
    //选中分类
    //限时
    IsLimit: false,
    //是否是众筹
    IsZC: false,
    //排序默认值
    sort_index: 0,
    ChooseItem: 'Colligate',
    classid:'',
    Ordertype: 0,
    //是否已经点击了价格排序字段
    onPrice:true,
    //价格降序
    upDown:false,

  },
  //输入框事件，每输入一个字符，就会触发一次  
  bindKeywordInput: function(e) {
    this.setData({
      Searchstr: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.code) {
      this.data.classid = options.code;
      console.log(options.code);
    }
    if (options.title){
      wx.setNavigationBarTitle({
        title: options.title
      })
    }
    var newIsLimit = this.data.IsLimit
    if (options.islimit) {
      newIsLimit = options.islimit
    }
    this.selectComponent("#List").SearchProducts({
      ChooseItem: this.data.classid,
      SearchStr: this.data.Searchstr,
      Orderstr: '',
      Ordertype: 0,
      IsLimit: this.data.IsLimit,
      IsZC: this.data.IsZC
    }, true)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //搜索
  BtnSearch: function() {
    this.selectComponent("#List").SearchProducts({
      ChooseItem: this.data.classid,
      SearchStr: this.data.Searchstr,
      Orderstr: this.data.ChooseItem,
      Ordertype: this.data.Ordertype,
      IsLimit: this.data.IsLimit,
      IsZC: this.data.IsZC
    }, true)
  },
  //排序方式
  sortPro:function(e){
    let z_index=e.currentTarget.dataset.index;
    let ordertype = '0';
    if (z_index == 2 && this.data.onPrice){
      this.data.onPrice = !this.data.onPrice;
    }
    else if (z_index != 2 && this.data.onPrice==false){ 
      this.data.onPrice=true;
      this.data.upDown=false;
    }
    if (z_index=='1')ordertype='1';
    this.setData({
      sort_index: z_index,
      onPrice: this.data.onPrice,
      upDown: this.data.upDown,
      ChooseItem: e.currentTarget.id,
      Ordertype: ordertype
    })
    this.BtnSearch();
  },
  //价格排序
  priceChange: function (e) {
    let z_index = e.currentTarget.dataset.index;
    this.data.Ordertype = this.data.upDown ? '0' : '1';
    this.setData({
      upDown: !this.data.upDown
    })
    this.BtnSearch();
  }
})