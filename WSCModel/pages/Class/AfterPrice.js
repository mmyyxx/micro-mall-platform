// pages/Class/AfterPrice.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    OrderInfo:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options)
    util.postMethods('GetOrderDetail2', {
      guid: options.id
    }, function (data) {
      if (data.IsOK) {
        var json = JSON.parse(data.Data);
        console.log(json);
        that.setData({
          OrderInfo: json,
          list: json.OrderProductList
        })
      }
    })
  },
  formSubmit:function(e){
    var that = this;
    console.log(e);
    util.postMethods('SaveAfterSale', {
      orderid: that.data.OrderInfo.GUID,
      type:'4',
      remark: e.detail.value.remark,
      adduser: app.globalData.openid,
      storeid:''
    }, function (data) {
      if (data.IsOK) {
        var json = JSON.parse(data.Data);
        wx.redirectTo({
          url: '/pages/Class/AfterPriceResult?status=0',
        })
      }
      else{
        wx.showModal({
          showCancel:false,
          content: data.msg
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})