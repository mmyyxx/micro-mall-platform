// pages/Class/ProductDetail.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //屏幕高度
    PageHeight: app.globalData.systemInfo.windowHeight,
    //屏幕宽度
    PictureHeight: app.globalData.systemInfo.windowWidth,
    //按钮宽度
    BtnWidth: app.globalData.systemInfo.windowWidth / 2 - 48,
    //滚动条
    scrollTop: 0,
    //商品主键
    ProductGUID: '',
    //商品信息
    ProductInfo: null,
    //是否显示限时
    islimit: '1',
    //明细图片
    DetailPictures: [],
    //计时器控件
    LimitTimeComponents: null,
    //商品选择是否显示
    IsChooseShow: false,
    //方式：0.加入购物车 1.立即购买
    BuyType: 0,
    //是否是众筹
    IsZC: false,
    //已众筹数量
    IsZCCount: 0,
    //点赞数量
    ZanCount: '0',
    //是否点赞
    IsZan: '1',
    // 参团弹窗是否显示
    tuan: false,
    // 遮罩
    maskState: false,
    // 规则弹窗
    guizeState: false,
    //是否结束
    IsSaleEnd: false,
    countDownDay: '00',
    countDownHour: '00',
    countDownMinute: '00',
    countDownSecond: '00',
    //计时器
    PageInterval: null,
    IsOpen: false,
    IsBot:false,
    BosType:0,
    Count:1,
    SelectClassItem: null,
    MinCount: 1,
    ShowPrice: null,
    ClassStock: 0,
    ClassStockNum: 1,
    IsChooseItem: false,
    num3:0,
    newpeople:'',
  },
  botTan:function(e){
    this.setData({
      IsBot: true,
      BosType: e.currentTarget.dataset.index
    })
  },
  closeBotTan:function(){
    this.setData({
      IsBot: false,
      BosType: 0
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.newpeople){
      that.data.newpeople = options.newpeople;
    }
    //获取需要显示的商品主键
    if (options.code) {
      that.setData({
        ProductGUID: options.code
      })
      var limit = '1';
      util.postMethods('GetProductInfoByGUID', {
        guid: options.code,
        islimit: limit
      }, function (data) {
        if (data.IsOK) {
          var newProductInfo = JSON.parse(data.Data);
          console.log(newProductInfo)
          //初始化详情图
          var pictures = [];
          pictures.push(newProductInfo.DetailPictureList[0]);
          if (newProductInfo.LimitEndTime)limit = '0';
          that.setData({
            ProductInfo: newProductInfo,
            islimit: limit
          })
          if (that.data.ProductInfo) {
            var newProductInfo = that.data.ProductInfo
            var newMinCount = that.data.MinCount
            var newCount = that.data.Count
            if (newProductInfo.PackingSpecification) {
              newMinCount = parseInt(newProductInfo.PackingSpecification)
              newCount = parseInt(newProductInfo.PackingSpecification)
            }
            //初始化选中值
            var newSelectClassItem = [];
            for (var m = 0; m < newProductInfo.ProductClassList.length; m++) {
              newSelectClassItem.push('');
            }
            that.setData({
              SelectClassItem: newSelectClassItem,
              MinCount: newMinCount,
              Count: newCount,
              ShowPrice: newProductInfo.DiscountPrice,
              DetailPictures: pictures,
            })
          }
          if (limit == 0) {
            that.TimerShow(newProductInfo.LimitEndTime);
            var totalSecond = parseInt((parseInt(newProductInfo.LimitEndTime.replace("/Date(", "").replace(")/", "")) - Date.now()) / 1000);
            var num1 = parseInt(newProductInfo.Remark);
            var num2 = parseInt(newProductInfo.Remark2);
            var num3 = (num1 - num2);
            if (totalSecond <= 0 || num1-num2<=0) {
              that.setData({
                IsSaleEnd: true,
                islimit:'1'
              });
            }
            else {
              that.setData({
                num3: num3
              });
            }
          }
        }
      })
    }

    //点赞情况
    // util.postMethods('GetZanCountByGUID', {
    //   guid: options.code,
    //   openId: app.globalData.openid
    // }, function (data) {
    //   if (data.IsOK) {
    //     that.setData({
    //       ZanCount: data.Data,
    //       IsZan: data.msg
    //     })
    //   }
    // })

    wx.getStorage({
      key: 'UserShopList',
      success: function (res) {
        if (res.data == null || res.data.length == 0) {
          that.setData({ shoplistCount: '0' })
        }
        else
          that.setData({ shoplistCount: res.data.length })
      },
      fail: function (res) {
        that.setData({ shoplistCount: '0' })
      }
    })
  },
  //点赞
  BtnZan: function () {
    var that = this
    //点赞情况
    util.postMethods('UserZan', {
      guid: that.data.ProductGUID,
      openId: app.globalData.openid
    }, function (data) {
      if (data.IsOK) {
        //点赞情况
        util.postMethods('GetZanCountByGUID', {
          guid: that.data.ProductGUID,
          openId: app.globalData.openid
        }, function (result) {
          if (result.IsOK) {
            that.setData({
              ZanCount: result.Data,
              IsZan: result.msg
            })
          }
        })
      }
    })
  },
  // 关闭参团按钮
  close_tuan: function () {
    this.setData({
      tuan: false
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    if (this.data.LimitTimeComponents != null) {
      this.data.LimitTimeComponents.TimeEnd()
    }
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    if (this.data.LimitTimeComponents != null) {
      this.data.LimitTimeComponents.TimeEnd()
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //滚动到底部触发事件  
  ShowScrollLower: function (e) {
    let that = this;
    var pictures = that.data.ProductInfo.DetailPictureList;
    var showpicrures = that.data.DetailPictures;
    if (showpicrures.length < pictures.length) {
      showpicrures.push(pictures[showpicrures.length]);
      that.setData({
        DetailPictures: showpicrures
      });
    }
  },
  //返回主页面
  PageReLaunch: function (e) {
    wx.switchTab({
      url: '/pages/' + e.currentTarget.id + '/' + e.currentTarget.id
    })
  },
  //返回顶部
  ReturnTop: function () {
    this.setData({
      scrollTop: 0
    });
  },
  //加入购物车
  AddUserShop: function () {
    //判断是否是商城会员
    this.data.BuyType=0;
    this.BtnOK();
  },
  //立即购买
  UserBuy: function () {
    this.data.BuyType = 1;
    this.BtnOK();
  },
  //关闭商品选择
  CloseView: function () {
    this.setData({
      IsChooseShow: false
    });
  },
  //确认商品选择
  BtnOK: function () {
    //是否可以购买
    if (!app.globalData.IsMember) {
      this.setData({
        IsOpen: true
      });
      this.selectComponent("#openMember").setPrice(app.globalData.oldmemberprice, app.globalData.newmemberprice);
      return;
    }
    //类别选择
    var selectItems = this.data.SelectClassItem
    //选择数量
    var productNum = this.data.Count;
    for (var m = 0; m < selectItems.length; m++) {
      if (selectItems[m] == '') {
        wx.showModal({
          content: "请选择商品属性！",
          showCancel: false,
          confirmColor: "#E3101E",
          confirmText: "确定"
        })
        return;
      }
    }
    if (this.data.BuyType == 0) {
      //加入购物车
      if (this.data.ProductInfo.IsGroupProduct == 0) {
        //参与拼团
        wx.navigateTo({
          url: '/pages/Shop/ParticipateGroup?code=' + selectItems[selectItems.length - 1]
        })
      } else {
        //加入购物车
        //获取已加入购物车的数据
        var userShopList = [];
        var list = wx.getStorageSync('UserShopList')
        if (list) {
          userShopList = list;
        }
        for (var m = 0; m < userShopList.length; m++) {
          //已经存在购物车中
          if (selectItems[selectItems.length - 1] == userShopList[m]) {
            var value = wx.getStorageSync(selectItems[selectItems.length - 1]);
            if (this.data.newpeople || this.data.islimit == '0') {
              wx.showToast({
                title: '该商品已经在购物车中!',
                icon:'none',
                mask: true
              })
              return;
            }
            wx.setStorage({
              key: selectItems[selectItems.length - 1],
              data: value + productNum
            })
            wx.showToast({
              title: '加入成功',
              image: '/resources/Icon/Toast_Success.png',
              mask: true
            })
            this.setData({
              IsChooseShow: false
            });
            return;
          }
        }
        //加入购物车
        userShopList.push(selectItems[selectItems.length - 1]);
        wx.setStorage({
          key: 'UserShopList',
          data: userShopList
        })
        wx.setStorage({
          key: selectItems[selectItems.length - 1],
          data: productNum
        })
        wx.showToast({
          title: '加入成功',
          image: '/resources/Icon/Toast_Success.png',
          mask: true
        })
        this.setData({
          shoplistCount: userShopList.length,
          IsChooseShow: false
        });
      }
    } else {
      //立即购买
      for (var m = 0; m < selectItems.length; m++) {
        if (selectItems[m] == '') {
          wx.showModal({
            content: "请选择商品属性！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
          return;
        }
      }
      if (this.data.ProductInfo.IsGroupProduct == 0) {
        //发起拼团
        wx.navigateTo({
          url: '/pages/Shop/Order?group=true&&code=' + selectItems[selectItems.length - 1] + "&&num=" + productNum
        })
      } else {
        //立即购买
        wx.navigateTo({
          url: '/pages/Shop/Order?code=' + selectItems[selectItems.length - 1] + "&&num=" + productNum
        })
      }
    }
  },
  go_evaluate: function () {
    let that = this;
    wx.navigateTo({
      url: '/pages/Class/Procomment?code=' + that.data.ProductGUID
    })
  },
  //结束
  detached: function () {
    this.TimeEnd()
  },
  //结束
  TimeEnd: function () {
    if (this.data.PageInterval != null) {
      clearInterval(this.data.PageInterval);
    }
  },
  //倒计时
  TimerShow: function (endtime) {
    var totalSecond = parseInt((parseInt(endtime.replace("/Date(", "").replace(")/", "")) - Date.now()) / 1000);
    if (totalSecond <= 0) {
      this.setData({
        IsSaleEnd: true
      });
      return;
    }
    var interval = setInterval(function () {
      // 秒数  
      var second = totalSecond;
      // 天数位  
      var day = Math.floor(second / 3600 / 24);
      var dayStr = day.toString();
      if (dayStr.length == 1) dayStr = '0' + dayStr;

      // 小时位  
      var hr = Math.floor((second - day * 3600 * 24) / 3600);
      var hrStr = hr.toString();
      if (hrStr.length == 1) hrStr = '0' + hrStr;

      // 分钟位  
      var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
      var minStr = min.toString();
      if (minStr.length == 1) minStr = '0' + minStr;

      // 秒位  
      var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
      var secStr = sec.toString();
      if (secStr.length == 1) secStr = '0' + secStr;

      this.setData({
        countDownDay: dayStr,
        countDownHour: hrStr,
        countDownMinute: minStr,
        countDownSecond: secStr,
      });
      totalSecond--;
      if (totalSecond < 0) {
        clearInterval(interval);
        this.setData({
          countDownDay: '00',
          countDownHour: '00',
          countDownMinute: '00',
          countDownSecond: '00',
          IsSaleEnd: true
        });
      }
    }.bind(this), 1000);

    this.setData({
      PageInterval: interval
    });
  },
  closeOpenMember: function () {
    this.setData({
      IsOpen: false
    });
  },
  ClassItemClick:function(e){
    if (e.currentTarget.dataset.classshow){
      return;
    }
    if (e.currentTarget.dataset.stock < e.currentTarget.dataset.stocknum && e.currentTarget.dataset.price > 0) {
      wx.showModal({
        content: '已售罄！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return
    }
    //初始化数量
    var newSelectClassItem = this.data.SelectClassItem
    newSelectClassItem[e.currentTarget.dataset.groupindex] = e.currentTarget.id
    for (var m = e.currentTarget.dataset.groupindex + 1; m < newSelectClassItem.length; m++) {
      newSelectClassItem[m] = ''
    }
    this.setData({
      IsChooseItem: e.currentTarget.dataset.price > 0,
      Count: this.data.MinCount,
      SelectClassItem: newSelectClassItem,
      ClassStock: e.currentTarget.dataset.stock,
      ClassStockNum: e.currentTarget.dataset.stocknum
    })
    this.ProductClassShow()
  },
  //商品类别操作
  ProductClassShow: function () {
    var newProductInfo = this.data.ProductInfo;
    var dic = newProductInfo.ClassDic;
    var newSelectClassItem = this.data.SelectClassItem;
    var newShowPrice = this.data.ShowPrice;
    var typeinfo = '';
    for (var m = 0; m < newProductInfo.ProductClassList.length; m++) {
      for (var ii = 0, ij = newProductInfo.ProductClassList[m].ClassItemList.length; ii < ij; ii++) {
        //计算选择的规格
        if (newSelectClassItem[m] == newProductInfo.ProductClassList[m].ClassItemList[ii].ClassGUID){
          typeinfo += newProductInfo.ProductClassList[m].ClassType + ':' + newProductInfo.ProductClassList[m].ClassItemList[ii].ClassValue+',';
          break;
        }
      }
      var chooselist = dic[newSelectClassItem[m - 1]];
      if (!chooselist) {
        for (var num = 0; num < newProductInfo.ProductClassList[0].ClassItemList.length; num++) {
          if (newProductInfo.ProductClassList[0].ClassItemList[num].ClassGUID == newSelectClassItem[newSelectClassItem.length - 1]) {
            newShowPrice = app.globalData.IsMember ? newProductInfo.ProductClassList[0].ClassItemList[num].ClassPrice : newProductInfo.ProductClassList[0].ClassItemList[num].ClassOldPrice
          }
        }
        continue;
      }
      for (var n = 0; n < newProductInfo.ProductClassList[m].ClassItemList.length; n++) {
        newProductInfo.ProductClassList[m].ClassItemList[n].ClassShow = true;
        for (var x = 0; x < chooselist.length; x++) {
          if (newProductInfo.ProductClassList[m].ClassItemList[n].ClassGUID == chooselist[x]) {
            newProductInfo.ProductClassList[m].ClassItemList[n].ClassShow = false;
            if (newProductInfo.ProductClassList[m].ClassItemList[n].ClassGUID == newSelectClassItem[newSelectClassItem.length - 1]) {
              newShowPrice = app.globalData.IsMember ? newProductInfo.ProductClassList[m].ClassItemList[n].ClassPrice : newProductInfo.ProductClassList[m].ClassItemList[n].ClassOldPrice
            }
          }
        }
      }
    }
    if (typeinfo) typeinfo = typeinfo.substring(0, typeinfo.length-1);
    this.setData({
      SelectClassItem: newSelectClassItem,
      ProductInfo: newProductInfo,
      ShowPrice: newShowPrice,
      typeinfo: typeinfo
    });
  },
  //减事件
  BtnReduce: function () {
    if (this.data.Count > this.data.MinCount) {
      this.setData({
        Count: this.data.Count - 1
      });
    }
  },
  //加事件
  BtnAdd: function () {
    if (this.data.newpeople || this.data.islimit=='0'){
      return;
    }
    var limitcount = this.data.ProductInfo.Remark;
    if (this.data.ProductInfo.Remark2) {
      limitcount = this.data.ProductInfo.Remark - this.data.ProductInfo.Remark2;
    }
    if (this.data.ProductInfo.IsGroupProduct == 0) {
      wx.showModal({
        content: '团购商品限购数量！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return
    }
    if (!this.data.IsChooseItem) {
      wx.showModal({
        content: '请选择要购买的商品！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return
    }
    // else if (limitcount < this.data.Count + 1) {
    //   wx.showModal({
    //     content: '商品限购' + limitcount + '组！',
    //     showCancel: false,
    //     confirmColor: "#F3ABC6",
    //     confirmText: "确定"
    //   })
    //   return
    // }
    else if (this.data.ClassStock < (this.data.Count + 1) * this.data.ClassStockNum) {
      wx.showModal({
        content: '库存不足！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return
    }
    this.setData({
      Count: this.data.Count + 1
    });
  },
  goHome:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  }
})