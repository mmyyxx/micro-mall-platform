// pages/Class/addressMain.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    typeNumber:'',
    list: [],
    username: '',
    usertel: '',
    address:''
  },

  userNameInput: function (e) {
    this.data.username = e.detail.value;
  },
  userTelInput: function (e) {
    this.data.usertel = e.detail.value;
  },
  userAddressInput:function(e){
    this.data.address = e.detail.value;
  },
  saveinfo:function(e){
    if (!this.data.username) {
      wx.showToast({
        title: '请输入取货人',
        icon: 'none',
        duration: 1000
      })
      return;
    }
    if (!this.data.usertel) {
      wx.showToast({
        title: '请输入手机号码',
        icon: 'none',
        duration: 1000
      })
      return;
    }
    if (!this.data.address) {
      wx.showToast({
        title: '请输入自提地址',
        icon: 'none',
        duration: 1000
      })
      return;
    }
    var that = this;
    util.postMethods('SaveUserQuHuo', {
      wxopenId: app.globalData.openid,
      username: that.data.username,
      usertel: that.data.usertel,
      address: that.data.address,
      selectid: that.data.typeNumber
    }, function (data) {
      if (data.IsOK) {
        var pages = getCurrentPages();
        var prevPage = pages[pages.length - 2];
        for (var i = 0, j = that.data.list.length;i<j;i++){
          if (that.data.typeNumber == that.data.list[i].GUID) {
            prevPage.setData({
              Adress: that.data.list[i],
              username: that.data.username,
              usertel: that.data.usertel
            })
            break;
          }
        }
        wx.navigateBack();
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    util.postMethods('GetAdressList', {
      adresstype: 1,
      wxopenId: app.globalData.openid,
      appguId: app.globalData.appId
    }, function (data) {
      if (data) {
        util.postMethods('GetUser', {
          openid: app.globalData.openid
        }, function (data2) {
          var typeguid = data[0].GUID; 
          if (data2) {
            var json = JSON.parse(data2.Data);
            that.setData({
              username:json.username,
              usertel: json.usertel,
              address:json.useraddress
            })
            if (json.selectid){
              typeguid = json.selectid;
            }
          }
          that.setData({
            typeNumber: typeguid,
            list: data
          })
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  checkAddress:function(e){
    var address = '';
    var that = this;
    for (var i = 0, j = that.data.list.length; i < j; i++) {
      if (e.currentTarget.dataset.index == that.data.list[i].GUID) {
        address = that.data.list[i].AdressPlace
        break;
      }
    }
    this.setData({
      typeNumber: e.currentTarget.dataset.index,
      address: address
    })
  }
})