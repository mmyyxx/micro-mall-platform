// pages/Class/ProductSearch.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //选择项
    ChooseItem: 'Colligate',
    //列表高度
    LisrHeight: app.globalData.systemInfo.windowHeight - 80,
    //搜索条件
    Searchstr: '',
    //排序方式
    Ordertype: 0,
    //列表控件
    ListComponent: null,
    //是否查询限时商品
    IsLimit: false,
    //团购
    IsGroup: false
  },
  //输入框事件，每输入一个字符，就会触发一次  
  bindKeywordInput: function (e) {
    this.setData({
      Searchstr: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var newIsLimit = false
    var newIsGroup = false
    if (options.islimit == 'true') {
      newIsLimit = true
      wx.setNavigationBarTitle({
        title: '限时商品'
      })
    }
    if (options.isgroup == 'true') {
      newIsGroup = true
      newIsLimit = true
      wx.setNavigationBarTitle({
        title: '团购商品'
      })
    }
    this.setData({
      ListComponent: this.selectComponent("#List"),
      IsLimit: newIsLimit,
      IsGroup: newIsGroup
    })
    this.BtnSearch()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //选项选择
  ItemChoose: function (e) {
    var newOrdertype = this.data.Ordertype
    if (this.data.ChooseItem == e.currentTarget.id) {
      newOrdertype = 1 - newOrdertype
    }
    else {
      newOrdertype = 0
    }
    this.setData({
      ChooseItem: e.currentTarget.id,
      Ordertype: newOrdertype
    })
    this.BtnSearch()
  },
  //搜索
  BtnSearch: function () {
    this.data.ListComponent.SearchProducts({
      ChooseItem: '',
      SearchStr: this.data.Searchstr,
      Orderstr: this.data.ChooseItem,
      Ordertype: this.data.Ordertype,
      IsLimit: this.data.IsLimit,
      IsGroup: this.data.IsGroup
    }, true)
  }
})