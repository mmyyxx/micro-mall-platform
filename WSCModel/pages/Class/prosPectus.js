// pages/platform/prosPectus.js
let app = getApp();
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    let that=this;
  },
  bindGetUserInfo: function (e) {
    console.log(e.detail)
    if (e.detail.errMsg =='getUserInfo:fail auth deny'){
      wx.showToast({
        title: '授权失败,请先授权!',
        icon: "none",
        duration: 1500
      })
      return;
    }
    if (e.detail.userInfo) {
      util.postMethods('GetUserInfo', {
        openid: app.globalData.openid,
        successData: e.detail
      }, function (result) {
        app.globalData.userInfo = e.detail.userInfo
        app.globalData.IsMember = result.IsOK;
        wx.reLaunch({
          url: '/pages/index/index',
        })  
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})