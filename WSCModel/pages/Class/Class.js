// pages/Class/Class.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //分类数据
    ClassModelList: [],
    //选中分类
    ChooseItem: '',
    //选中分类子分类
    ChildCategoryList: null,
    //列表高度
    ListHeight: app.globalData.systemInfo.windowHeight,
    //屏幕宽度
    PageWidth: app.globalData.systemInfo.windowWidth - 90,
    //列表控件
    ListComponent: null
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    util.postMethods('GetClassData', {
      appId: app.globalData.appId
    }, function (data) {
      if (data) {
        console.log(app.globalData.currentclass)
        var code = data[0].CategoryCode;
        var list = data[0].ChildCategoryList;
        if (app.globalData.currentclass) {
          code = app.globalData.currentclass;
          for (var i = 0, j = data.length; i < j; i++) {
            if (code == data[i].CategoryCode) {
              list = data[i].ChildCategoryList;
              break;
            }
          }
          app.globalData.currentclass = '';
        }
        that.setData({
          ClassModelList: data,
          ChildCategoryList: list,
          ChooseItem: code,
          ListComponent: that.selectComponent("#List")
        })
        that.BtnSearch()
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //选中分类
  CategoryChoose: function(e) {
    var newList = this.data.ClassModelList[e.currentTarget.dataset.index].ChildCategoryList
    this.setData({
      //选中分类
      ChooseItem: e.currentTarget.id,
      ChildCategoryList: newList
    })
    this.BtnSearch()
  },
  //搜索
  BtnSearch: function() {
    if (this.data.ChildCategoryList.length > 0) {
      this.data.ListComponent.ShowChildCategory(this.data.ChildCategoryList)
    } else {
      this.data.ListComponent.SearchProducts({
        ChooseItem: this.data.ChooseItem,
        SearchStr: '',
        Orderstr: '',
        Ordertype: 0
      }, true)
    }
  },
  //搜索页面
  navigateToSearch: function () {
    wx.navigateTo({ url: '/pages/Class/ProductSearch' })
  }
})