// pages/Class/LimitProduct.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: [],
    CategoryId: null,
    ShowMsg: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.code) {
      this.setData({
        CategoryId: options.code
      })
      this.GetProductList(true)
    }
    if (options.title) {
      this.setData({
        ShowMsg: '团购'
      })
      wx.setNavigationBarTitle({
        title: options.title
      })
    }
    else{
      this.setData({
        ShowMsg: '秒杀'
      })
    }
  },
  //获取分类商品
  GetProductList: function(isFirst) {
    var that = this
    util.postMethods('GetCategoryProduct', {
      guid: this.data.CategoryId
    }, function(data) {
      that.setData({
        List: data
      })
      if (!isFirst) {
        //结束下拉
        wx.stopPullDownRefresh();
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.GetProductList(false)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //秒杀买
  BtnBuyProduct: function(e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  }
})