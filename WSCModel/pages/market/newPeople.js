// pages/market/newPeople.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    banindex: 0,
    modelList: null,
    modeldataList: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadData(0);
  },
  loadData(idx) {
    var that = this;
    //获取模块数据
    util.postMethods('GetMoelListOther2', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      type: '12'
    }, function (data) {
      if (data) {
        console.log(data)
        var newlist = [];
        var list = [];
        var hasmodel2 = false;
        for (var m = 0; m < data.length; m++) {
          if (m >= 2) list.push(data[m]);
          else newlist.push(data[m]);
        }
        that.setData({
          modelList: newlist,
          list: list[idx].ProductList,
          modeldataList: list
        })
        //结束下拉
      }
    })
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /*huatab切换 */
  hua_tab: function (e) {
    var index = e.currentTarget.dataset.index;
    this.setData({
      banindex: index,
      list: this.data.modeldataList[index].ProductList
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})