// pages/market/AssembleList.js
const app = getApp()
const util = require('../../utils/util.js')
var timer;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //轮播图设置
    SwiperConfig: null,
    SwiperImgs: [{ 'imageurl': '1', "image": "http://htgl.hmcys.cn:8088/ModelList/b55cb68f02364da3b04b7ac2d5108917.jpg" }, { 'imageurl': '1', "image": "http://htgl.hmcys.cn:8088/ModelList/a720a616dbe34c018547469cad7a6b5a.jpg" }, { 'imageurl': '1', "image": "http://htgl.hmcys.cn:8088/ModelList/ac632880b55b496f8eb2658c6abec14c.jpg" }, { 'imageurl': '1', "image": "https://wsc.hmcys.cn:855/Upload/smallshop/20190820/20190820115304_2828.png" }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获取轮播图设置
    util.postMethods('GetSwiperConfig', {
      appId: app.globalData.appId
    }, function (data) {
      if (data) {
        that.setData({
          SwiperConfig: data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})