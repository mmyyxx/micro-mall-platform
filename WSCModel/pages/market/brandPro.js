// pages/market/brandPro.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: 0,
    banindex: 0,
    modelList: null,
    list:null,
    alllist: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.type)
      this.data.type = options.type;
    this.loadData(0);
  },

  loadData(idx) {
    var that = this;
    //获取模块数据
    console.log(that.data.type)
    util.postMethods('GetMoelListOther3', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      type: '10',
      modelurl: that.data.type,
    }, function (data) {
      data = JSON.parse(data);
      if (data) {
        console.log(data)
        var newlist = [];
        var list = [];
        var alllist = [];
        var hasmodel2 = false;
        for (var m = 0; m < data[1].ProductList.length; m++) {
          list[m] = [];
          for (var n = 0; n < data[2].ProductList.length; n++) {
            if (data[1].ProductList[m].productName == data[2].ProductList[n].ProductDescribes){
              list[m].push(data[2].ProductList[n]);
              alllist.push(data[2].ProductList[n]);
            }
          }
        }
        var temlist= alllist;
        console.log(temlist);
        if (that.data.banindex!=0)
          temlist = list[that.data.banindex-1];
        //结束下拉
        that.setData({
          list:list,
          modelList: data,
          temlist: temlist,
          alllist: alllist
        });
      }
    })
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /*huatab切换 */
  hua_tab: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var temlist = [];
    if (index != 0)
      temlist = that.data.list[index - 1];
    else{
      temlist = that.data.alllist;
    }
    console.log(index)
    console.log(temlist)
    this.setData({
      banindex: index,
      temlist: temlist
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})