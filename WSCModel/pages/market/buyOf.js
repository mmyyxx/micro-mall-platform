const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modelList:null,
    IsOpen: false
  },
  opDesXianShi:function(){
   
    this.setData({
      IsOpen: true
    });
  },
  closeDesXianShi: function () {
    this.setData({
      IsOpen: false
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadData();
  },
  loadData(){
    var that = this;
    //获取模块数据
    util.postMethods('GetMoelListOther', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      type:'fresh'
    }, function (data) {
      if (data) {
        console.log(data)
        var newlist = [];
        var hasmodel2 = false;
        for (var m = 0; m < data.length; m++) {
          newlist.push(data[m])
        }
        that.setData({
          modelList: newlist
        })
        //结束下拉
      }
    })
  },
  navigateToSearch: function () {
    wx.navigateTo({
      url: '/pages/Class/CategorySearch'
    })
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})