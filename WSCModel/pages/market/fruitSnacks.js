// pages/market/fruitSnacks.js
const app = getApp()
const util = require('../../utils/util.js')
var timer;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //轮播图设置
    SwiperConfig: null,
    SwiperImgs: null,
    modelList: null,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获取轮播图设置
    util.postMethods('GetSwiperConfigType', {
      appId: app.globalData.appId,
      type:'fruits'
    }, function (data) {
      if (data) {
        that.setData({
          SwiperConfig: data
        })
      }
    })
  },
  PageListShow: function () {
    var that = this;
    //获取模块数据
    util.postMethods('GetMoelListOther', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      type:'fruits'
    }, function (data) {
      data = JSON.parse(data);
      console.log(data)
      if (data) {
        var newlist = [];
        var hasmodel2 = false;
        for (var m = 1; m < data.length; m++) {
          newlist.push(data[m])
        }
        var __height = that.data.ViewWidth * data[0].ModelURL;
        that.setData({
          modelList: newlist,
          SwiperImgs: data[0],
          vheight: __height
        })

        //结束下拉
        wx.stopPullDownRefresh();
      }
    })
  },
  toClass:function(e){
    console.log(e)
    //app.globalData.currentclass = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: '/pages/Class/CategorySearch?code=' + e.currentTarget.dataset.url,
    })
  },
  toPage: function (e) {
    var code = e.currentTarget.dataset.url;
    wx.redirectTo({
      url: code
    })
  },
  //搜索页面
  navigateToSearch: function () {
    wx.navigateTo({
      url: '/pages/Class/CategorySearch'
      //url: '/pages/Class/ProductSearch'
    })
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.PageListShow()
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.PageListShow()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})