const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    banindex: 2,
    modelList: null,
    modeldataList: null,
    typeList:null,
    type: '2'
  },
  /*huatab切换 */
  hua_tab: function (e) {
    var index = e.currentTarget.dataset.index;
    this.data.type = index;
    this.loadData(0);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    if (options.type)
      this.data.type = options.type;

    // util.postMethods('GetMoelListOther3', {
    //   windowWidth: app.globalData.systemInfo.windowWidth,
    //   appId: app.globalData.appId,
    //   type: '20',
    //   modelurl: '1'
    // }, function (data) {
    //   data = JSON.parse(data);
    //   if (data) {
    //     console.log(data)
    //     that.setData({
    //       typeList: data[0].ProductList
    //     })
    //     //结束下拉
    //   }
    // })
    this.loadData(0);
  },
  loadData(idx) {
    var that = this;
    //获取模块数据
    console.log(that.data.type)
    if (that.data.type<5){
      that.setData({
        isshow:true
      });
    }
    util.postMethods('GetMoelListOther3', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      type: '20',
      modelurl: that.data.type
    }, function (data) {
      data = JSON.parse(data);
      if (data) {
        var newlist = [];
        var list = [];
        var hasmodel2 = false;
        for (var m = 0; m < data[0].ProductList.length; m++) {
          if (that.data.type < 5) {
            if (m > 1) list.push(data[0].ProductList[m]);
            else newlist.push(data[0].ProductList[m]);
          }
          else {
            if (m > 0) list.push(data[0].ProductList[m]);
            else newlist.push(data[0].ProductList[m]);
          }
        }
        console.log(list);
        that.setData({
          modelList: newlist,
          list: list,
          banindex: that.data.type
        })
        wx.setNavigationBarTitle({
          title: data[0].ModelTitle
        })
        //结束下拉
      }
    })
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    loadData(0);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})