const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    banindex: 0,
    modelList:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.loadData(0);
  },
  loadData: function (page) {
    var that = this;
    //获取模块数据
    util.postMethods('GetModelFreshGood', {
      appId: app.globalData.appId,
      type: 'fresh',
      top:2,
      orderinfo:'modelList4'
    }, function (data) {
      if (data) {
        console.log(data)
        var newlist = [];
        for (var m = 0; m < data.length; m++) {
          newlist.push(data[m])
        }
        that.setData({
          modelList: newlist
        })
        //结束下拉
      }
    })
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /*huatab切换 */
  hua_tab: function (e) {
    this.setData({
      banindex: e.currentTarget.dataset.index
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.loadData(0);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})