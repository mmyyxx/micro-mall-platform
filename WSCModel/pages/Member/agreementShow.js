// pages/Member/agreementShow.js
const app = getApp()
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  yes_agreement: function() {
    try {
      util.postMethods('MemberPay', {
        appId: app.globalData.appId,
        openId: app.globalData.openid
      }, function(e) {
        var data = JSON.parse(e)
        if (e.IsOK) {
          wx.showModal({
            content: e.msg,
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function () {
              wx.navigateBack()
            }
          })
        } else {
          wx.requestPayment({
            timeStamp: data.timeStamp,
            nonceStr: data.nonceStr,
            package: data.package,
            signType: data.signType,
            paySign: data.paySign,
            success: function(res) {
              wx.showModal({
                content: "支付成功,请稍后刷新页面",
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function() {
                  wx.reLaunch({
                    url: '/pages/index/index',
                  })
                }
              })
            },
            fail: function(res) {
              wx.showModal({
                content: "支付失败！",
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function() {}
              })
            }
          })
        }
      })
    } catch (e) {
      wx.showModal({
        content: "支付失败！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        success: function() {}
      })
    }
  },
  no_agreement: function() {
    wx.navigateBack();
  }
})