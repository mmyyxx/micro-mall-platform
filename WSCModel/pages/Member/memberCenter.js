// pages/Member/memberCenter.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    IsShow:false,
    oldprice:0,
    memberprice:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      oldprice: app.globalData.oldmemberprice,
      memberprice: app.globalData.newmemberprice,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  openAgreement:function(){
    if (app.globalData.IsMember) {
      wx.showModal({
        content: '您已经是会员,无需开通!',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        success: function () {
        }
      })
      return;
    }
    this.setData({
      IsShow: true
    });
  },
  openMember:function(){
    this.setData({
      IsOpen: true
    });
  },
  closeAgreement:function(){
    this.setData({
      IsShow: false
    });
  },
  closeOpenMember: function () {
    this.setData({
      IsOpen: false
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})