const app = getApp()
const util = require('../../utils/util.js')
var timer;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rushIndex: 0,
    modelList: null,
    type: '30'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  loadData(idx) {
    var that = this;
    //获取模块数据
    util.postMethods('GetMoelListTimer', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      type: that.data.type
    }, function (data) {
      data = JSON.parse(data);
      if (data) {
        var list = data[1].ProductList;
        for (var i = 0, j = list.length;i<j;i++){
          if (!list[i].ProductDescribes) list[i].ProductDescribes="";
          var num1 = parseFloat(list[i].num1);
          var num2 = 0;
          if (list[i].num2) num2 = parseFloat(list[i].num2);
          list[i].perset = (num2)/num1*100;
        }
        console.log(list);

        that.setTimeHour(list);

        var hasmodel2 = false;
        that.setData({
          image: data[0].ProductList[0].image,
          modelList: list
        })
        console.log(list)
        wx.stopPullDownRefresh()
      }
    })
  },
  setTimeHour: function (data) {
    var that = this;
    var hours = [];
    var mins = [];
    var secs = [];
    var days = [];
    var hastimer = false;
    for (var i = 0, j = data.length; i < j; i++) {
      var ms = 0;
      if (!data[i].ms){
        var end_date = new Date(data[i].Timer1.replace(/-/g, "/"));
        var start_date = new Date();
        ms = end_date.getTime() - start_date.getTime();
        data[i].ms = ms;
      }
      else ms = data[i].ms;
      if (ms <= 0) {
        hours[i] = '0';
        mins[i] = '0';
        secs[i] = '0';
        days[i] = '';
        if (timer != null)
          clearTimeout(timer);
        continue;
      }
      hastimer = true;
      let second = Math.floor(ms / 1000);
      // 小时位
      let hr = Math.floor(second / 3600);
      // 分钟位
      let min = Math.floor((second - hr * 3600) / 60);
      // 秒位
      let sec = second % 60; // equal to => var sec = second % 60;
      var day = Math.floor(ms / (1000 * 60 * 60 * 24));
      hr = hr - 24 * day;
      if (hr <= 9) hr = '0' + hr;
      if (min <= 9) min = '0' + min;
      if (sec <= 9) sec = '0' + sec;
      hours[i] = hr;
      mins[i] = min;
      secs[i] = sec;
      days[i] = day;

      data[i].ms = data[i].ms-1000;
    }
    that.setData({
      hours:hours,
      mins:mins,
      secs:secs,
      days:days
    })
    if (hastimer)
      timer = setTimeout(function () {
        that.setTimeHour(data);
      }, 1000);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  selectRush:function(e){
    this.setData({
      rushIndex: e.currentTarget.dataset.index
    })
  },

  //商品跳转
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?islimit=0&code=' + e.currentTarget.id
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadData(0);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    if (timer != null)
      clearTimeout(timer);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    if (timer != null)
      clearTimeout(timer);
    this.loadData(0);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})