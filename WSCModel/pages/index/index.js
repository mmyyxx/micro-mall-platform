//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')
var timer;
Page({
  data: {
    //滚动字幕
    MoveText: '',
    //模块列表
    modelList: null,
    //页面高度
    ViewHeight: app.globalData.systemInfo.windowHeight,
    //页面宽度
    ViewWidth: app.globalData.systemInfo.windowWidth,
    //轮播图数据navigateTo
    SwiperImgs: null,
    //轮播图设置
    SwiperConfig: null,
    //菜单
    TitleMenuIndex: 1,
    //显示
    IsShow: true,
    //砸金蛋
    IsGoldEgg: false,
    height: 0,
    //超值拼团倒计时
    hours: '00',
    mins: '00',
    secs: '00',
    days: '',
    likeselected: '1',
    likeimg: [],
    showtype: '1',
    IsOpen: false,
    percent:80,
    showpingtuan:false,
    wait: false
  },
  //数据加载
  PageListShow: function() {
    var that = this;

    util.postMethods('GetHome', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      orderinfo:'0'
    }, function (data) {
      if (data) {
        console.log(data)
        that.setData({
          SwiperImgs: data[0],
          menulist:data[1]
        })
      }
    })

    //获取模块数据
    util.postMethods('GetModelList', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId
    }, function(data) {
      if (data) {
        var newlist = [];
        var hasmodel2 = false;
        for (var m = 0; m < data.length; m++) {
          newlist.push(data[m])
          if (data[m].ModelName == 'modelList2') {
            var end_date = new Date(data[m].ModelURL.replace(/-/g, "/"));
            var start_date = new Date();
            var ms = end_date.getTime() - start_date.getTime();
            that.setTimeHour(ms);
          }
        }
        var __height = that.data.ViewWidth * data[1].ModelURL;
        that.setData({
          modelList: newlist
        })

        //结束下拉
        wx.stopPullDownRefresh();
      }
    })
  },
  setTimeHour: function(ms) {
    var that = this
    if (ms <= 0) {
      that.setData({
        hours: '00',
        mins: '00',
        secs: '00',
        day: ''
      });
      if (timer != null)
        clearTimeout(timer);
      return;
    }
    let second = Math.floor(ms / 1000);
    // 小时位
    let hr = Math.floor(second / 3600);
    // 分钟位
    let min = Math.floor((second - hr * 3600) / 60);
    // 秒位
    let sec = second % 60; // equal to => var sec = second % 60;
    var day = Math.floor(ms / (1000 * 60 * 60 * 24));
    hr = hr - 24 * day;
    if (hr <= 9) hr = '0' + hr;
    if (min <= 9) min = '0' + min;
    if (sec <= 9) sec = '0' + sec;
    that.setData({
      hours: hr,
      mins: min,
      secs: sec,
      days: day
    });
    timer = setTimeout(function() {
      that.setTimeHour(ms - 1000);
    }, 1000);
  },
  onLoad: function(options) {
    this.data.likeimg[0] = '/resources/Icon/slike1.png';
    this.data.likeimg[1] = '/resources/Icon/like2.png';
    this.data.likeimg[2] = '/resources/Icon/like3.png';
    this.data.likeimg[3] = '/resources/Icon/like4.png';
    if (options.code) {
      wx.navigateTo({
        url: '/pages/Shop/Order?code=' + options.code + "&&num=1&&group=true&&groupnum=" + options.groupnum
      })
    }
    var that = this;
    that.setData({
      likeimg: that.data.likeimg
    })
    this.PageListShow()

    util.postMethods('getDataType', {
      type: "MemberPrice"
    }, function (data) {
      if (data) {
        console.log(data)
        for (var m = 0; m < data.data.length; m++) {
          if (data.data[m].ItemName == 'NewPrice') {
            app.globalData.newmemberprice = data.data[m].ItemValue;
          }
          else if (data.data[m].ItemName == 'OldPrice')
            app.globalData.oldmemberprice = data.data[m].ItemValue;
        }
      }
    })
  },
  //监听页面初次渲染完成
  onReady: function() {
  },
  //监听页面显示
  onShow: function() {
    var that = this;
    if (app.globalData.openid) {
      util.postMethods('GetMemberInfo', {
        openid: app.globalData.openid
      }, function (data) {
        console.log(data)
        if (data.IsOK) {
          app.globalData.IsMember = data.IsOK;
          var userdata = JSON.parse(data.Data);
          app.globalData.iseventdraw = userdata.eventdraw;
          app.globalData.score = userdata.integral;
        } else {
        }
      })
    }
  },
  //监听页面隐藏
  onHide: function() {

  },
  //监听页面卸载
  onUnload: function() {

  },
  //页面相关事件处理函数--监听用户下拉动作
  onPullDownRefresh: function() {
    this.PageListShow()
    //加在会员信息
    util.postMethods('GetMemberInfo', {
      openid: app.globalData.openid
    }, function (data) {
      console.log(data)
      if (data.IsOK) {
        app.globalData.IsMember = data.IsOK;
        var userdata = JSON.parse(data.Data);
        app.globalData.iseventdraw = userdata.eventdraw;
        app.globalData.score = userdata.integral;
      } else {
      }
    })
  },
  //页面上拉触底事件的处理函数
  onReachBottom: function() {

  },
  //用户点击右上角转发
  onShareAppMessage: function(res) {
    var that = this
    // 转发成功
    wx.showModal({
      content: '分享成功！',
      showCancel: false,
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function() {
      }
    })
  },
  //页面滚动触发事件的处理函数
  onPageScroll: function() {

  },
  //当前是 tab 页时，点击 tab 时触发
  onTabItemTap(item) {

  },
  //跳转
  navigateTo: function(e) {
    if (e.currentTarget.dataset.url) {
      wx.navigateTo({
        url: e.currentTarget.dataset.url
      })
    } else if (e.currentTarget.id) {
      wx.navigateTo({
        url: '/pages/Class/IndexClass?code=' + e.currentTarget.id + '&&title=' + e.currentTarget.dataset.title
      })
    }
  },
  //搜索页面
  navigateToSearch: function() {
    wx.navigateTo({
      url: '/pages/Class/CategorySearch'
      //url: '/pages/Class/ProductSearch'
    })
  },
  //扫一扫
  BtnScanCode: function() {
    // 只允许从相机扫码
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        util.postMethods('GetProductGUID', {
          sku: res.result,
          guid: app.globalData.appId,
          openId: app.globalData.openid
        }, function(data) {
          if (data.IsOK) {
            if (data.msg) {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            } else {
              wx.navigateTo({
                url: '../Class/ProductDetail?code=' + data.Data
              })
            }
          } else {
            wx.showModal({
              content: data.msg,
              showCancel: false,
              confirmColor: "#F3ABC6",
              confirmText: "确定"
            })
          }
        })
      }
    })
  },
  //我的二维码
  BtnQRCode: function () {
    if (!app.globalData.IsMember) {
      this.setData({
        IsOpen: true
      });
      this.selectComponent("#openMember").setPrice(app.globalData.oldmemberprice, app.globalData.newmemberprice);
      return;
    }
    wx.navigateTo({
      url: '/pages/Setting/QRCode'
    })
  },
  //八格跳转
  closePingTuan: function (e) {
    this.setData({
      showpingtuan: false
    })
  },
  BtnBGNavigateTo: function(e) {
    var title = e.currentTarget.dataset.title;
    console.log(title)
    if (title =='超值拼团'){
      this.setData({
        showpingtuan:true
      })
      return;
    }
    if(title=='限时秒杀'){
      //判断是否是商城会员
      if (!app.globalData.IsMember) {
        this.setData({
          IsOpen: true
        });
        this.selectComponent("#openMember").setPrice(app.globalData.oldmemberprice, app.globalData.newmemberprice);
        return;
      }
    }
    if (title == '鲜食预定') {
      this.setData({
        wait: true
      });
      return;
    }
    if (e.currentTarget.dataset.newurl) {
      wx.navigateTo({
        url: e.currentTarget.dataset.newurl
      })
    } 
  },
  //商品跳转
  BtnProductNavigateTo: function (e) {
    var title = e.currentTarget.dataset.title;
    if (title == '新人专享') {
      //判断是否是商城会员
      if (!app.globalData.IsMember) {
        this.setData({
          IsOpen: true
        });
        this.selectComponent("#openMember").setPrice(app.globalData.oldmemberprice, app.globalData.newmemberprice);
        return;
      }
      if (app.globalData.iseventdraw == 1) {
        wx.showModal({
          content: '抱歉！您已参与过首单体验，无法再次体验！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定",
          success: function () {
          }
        })
        return;
      }

      wx.navigateTo({
        url: '/pages/Class/ProductDetail?newpeople=1&code=' + e.currentTarget.id
      })
      return;
    }
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  //公司简介
  BtnCompany: function(e) {
    wx.navigateTo({
      url: '/pages/index/ImageShow?url=' + e.currentTarget.dataset.url
    })
  },
  //菜单点击
  BtnTitleMenu: function(e) {
    var id = e.currentTarget.id;
    if (id == 2) { //鲜食
      wx.navigateTo({
        url: '/pages/market/buyOf',
      })
    } else if (id == 3) { //手机
      wx.navigateTo({
        url: '/pages/market/MobilePhone',
      })
    } else if (id == 4) { //数码
      wx.navigateTo({
        url: '/pages/market/digiTal',
      })
    } else if (id == 5) { //零食
      wx.navigateTo({
        url: '/pages/market/fruitSnacks',
      })
    }
  },
  //砸金蛋
  GoldEggs: function() {
    this.setData({
      IsShow: true
    })
  },
  //砸金蛋
  BtnEggClose: function() {
    this.setData({
      IsShow: false
    })
  },
  //关闭砸金蛋
  BtnCloseEgg: function() {
    this.setData({
      IsGoldEgg: false
    })
  },
  BtnShowLike: function(e) {
    var that = this;
    that.data.likeselected = e.currentTarget.dataset.type;
    var i = parseInt(that.data.likeselected);

    that.data.likeimg[0] = '/resources/Icon/like1.png';
    that.data.likeimg[1] = '/resources/Icon/like2.png';
    that.data.likeimg[2] = '/resources/Icon/like3.png';
    that.data.likeimg[3] = '/resources/Icon/like4.png';
    that.data.likeimg[i - 1] = '/resources/Icon/slike' + (i) + '.png';
    that.setData({
      likeselected: that.data.likeselected,
      likeimg: that.data.likeimg,
      showtype: that.data.likeselected
    });
  },
  closeOpenMember: function () {
    this.setData({
      IsOpen: false
    });
  },
  closeWait:function(){
    this.setData({
      wait: false
    });
  }
})