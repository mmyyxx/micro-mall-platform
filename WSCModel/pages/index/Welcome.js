// pages/index/Welcome.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ImageUrl: '',
    NavigateUrl: '',
    ImageUrlBack: '',
    ShowTime: 5,
    IsTimeOut: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    //获取模块数据
    util.postMethods('GetModelList', {
      windowWidth: app.globalData.systemInfo.windowWidth,
      appId: app.globalData.appId,
      orderinfo:0
    }, function(data) {
      if (data) {
        that.setData({
          ImageUrl: data[0].ProductList[0].image,
          NavigateUrl: data[0].ProductList[0].imageurl2,
          ImageUrlBack: data[0].ProductList[0].image2,
          ShowTime: parseInt(data[0].ProductList[0].imageurl) ? parseInt(data[0].ProductList[0].imageurl):5
        })
        that.selectComponent("#welcome").ShowWelcome()
      }
    }, function(result) {
      if (result.errMsg == 'request:fail timeout') {
        that.setData({
          IsTimeOut: true
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //关闭页面
  CloseView: function(e) {
    this.setData({
      isShow: false
    })
  }
})