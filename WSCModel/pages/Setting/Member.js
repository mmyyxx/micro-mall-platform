// pages/Setting/Member.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    CardHeight: 200,
    MemberInfo: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.IsMember) {
      wx.showModal({
        content: '请先注册会员！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        complete: function() {
          wx.navigateBack({
            delta: 1
          })
        }
      })
    } else {
      var that = this
      //获取会员信息
      util.postMethods('GetMemberInfo', {
        openid: app.globalData.openid
      }, function(data) {
        if (data.IsOK) {
          that.setData({
            MemberInfo: JSON.parse(data.Data),
            CardHeight: app.globalData.systemInfo.windowWidth * 0.4
          })
        } else {
          wx.showModal({
            content: '会员信息不存在！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            complete: function() {
              wx.navigateBack({
                delta: 1
              })
            }
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //余额明细
  BtnDetail: function() {
    wx.navigateTo({
      url: '/pages/Setting/MemberDetail'
    })
  },
  //修改用户信息
  BtnEditMember: function() {
    wx.navigateTo({
      url: '/pages/Setting/MemberRegister?isedit=true'
    })
  }
})