// pages/Setting/MyGroup.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //订单列表
    GroupOrderList: null,
    //列表高度
    ScrollHeight: app.globalData.systemInfo.windowHeight - 30,
    //计时器
    IntervalList: []
  },
  //页面加载
  GroupOrderShow: function(isFirst) {
    var that = this;
    util.postMethods('GetMyGroupOrder', {
      openid: app.globalData.openid
    }, function(data) {
      if (data.IsOK) {
        var newGroupOrderList = JSON.parse(data.Data)
        for (var m = 0; m < newGroupOrderList.length; m++) {
          newGroupOrderList[m].GroupPeopleImg = JSON.parse(newGroupOrderList[m].GroupPeopleImg)
          if (newGroupOrderList[m].OrderStatus == 1) {
            //待支付
            that.TimerShow(newGroupOrderList[m].CreateTime, m)
          } else if (newGroupOrderList[m].OrderStatus == 2) {
            //待成团
            that.TimerShow(newGroupOrderList[m].LimitEndTime, m)
          }
        }
        that.setData({
          GroupOrderList: newGroupOrderList
        })
      }
      if (!isFirst) {
        //停止下拉刷新
        wx.stopPullDownRefresh()
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.GroupOrderShow(true)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    this.ClearIntervalList();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    this.ClearIntervalList();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.GroupOrderShow(false)
  },
  //滚动上拉触顶事件
  ScrollUpper: function() {
    wx.startPullDownRefresh()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    if (res.from == 'button') {
      // 来自页面内转发按钮
      return {
        title: '拼团抢购-' + res.target.dataset.name,
        path: '/pages/index/index?code=' + res.target.dataset.guid + "&&num=1&&group=true&&groupnum=" + res.target.dataset.number,
        imageUrl: res.target.dataset.url,
        success: function(res) {
          // 转发成功
          wx.showModal({
            content: '邀请成功！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        },
        fail: function(res) {
          // 转发失败
          wx.showModal({
            content: '邀请失败！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        }
      }
    }
  },
  //停止计时器
  ClearIntervalList: function() {
    for (var m = 0; m < this.data.IntervalList.length; m++) {
      clearInterval(this.data.IntervalList[m]);
    }
  },
  //倒计时
  TimerShow: function(endtime, index) {
    var totalSecond = parseInt((parseInt(endtime.replace("/Date(", "").replace(")/", "")) - Date.now()) / 1000);
    if (totalSecond <= 0) {
      return;
    }
    var newIntervalList = this.data.IntervalList;
    var interval = setInterval(function() {
      // 秒数  
      var second = totalSecond;

      // 天数位  
      var day = Math.floor(second / 3600 / 24);
      var dayStr = day.toString();
      if (dayStr.length == 1) dayStr = '0' + dayStr;

      // 小时位  
      var hr = Math.floor((second - day * 3600 * 24) / 3600);
      var hrStr = hr.toString();
      if (hrStr.length == 1) hrStr = '0' + hrStr;

      // 分钟位  
      var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
      var minStr = min.toString();
      if (minStr.length == 1) minStr = '0' + minStr;

      // 秒位  
      var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
      var secStr = sec.toString();
      if (secStr.length == 1) secStr = '0' + secStr;

      totalSecond--;

      if (totalSecond < 0) {
        clearInterval(interval);
        dayStr = '00';
        hrStr = '00';
        minStr = '00';
        secStr = '00';
      }
      this.SetTimeData(dayStr, hrStr, minStr, secStr, index);
    }.bind(this), 1000);

    newIntervalList.push(interval);

    this.setData({
      IntervalList: newIntervalList
    })
  },
  //设置倒计时
  SetTimeData: function(dayStr, hrStr, minStr, secStr, index) {
    var newGroupOrderList = this.data.GroupOrderList;
    newGroupOrderList[index].dayStr = dayStr;
    newGroupOrderList[index].hrStr = hrStr;
    newGroupOrderList[index].minStr = minStr;
    newGroupOrderList[index].secStr = secStr;
    this.setData({
      GroupOrderList: newGroupOrderList
    })
  },
  //立即支付
  PayOrder: function(e) {
    var that = this
    util.WxPayByOrder(app.globalData.appId, app.globalData.openid, e.currentTarget.id, function(res) {
      wx.showModal({
        content: '支付成功！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        success: function() {
          that.GroupOrderShow(true)
        }
      })
    });
  },
})