// pages/Setting/OrderDetail.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //订单主键
    OrderGUID: null,
    //订单信息
    OrderInfo: null,
    //限时显示
    IsShowTime: 0,
    countDownDay: '00',
    countDownHour: '00',
    countDownMinute: '00',
    countDownSecond: '00',
    //下单时间
    CreateTime: '无记录',
    //成团时间
    RegimentTime: '无记录',
    //发货时间
    DeliverTime: '无记录',
    //成交时间
    CompleteTime: '无记录',
    //计时器
    PageInterval: null,
    //众筹人数
    IsZCCount: 0
  },
  //时间格式化
  DateTimeFormat: function(datevalue) {
    //下单时间
    if (datevalue) {
      var datenumber = parseInt(datevalue.replace("/Date(", "").replace(")/", ""));
      var newdate = new Date(datenumber);
      var datearray = [];
      //年
      datearray.push(newdate.getFullYear() + "-");
      //月
      var month = (newdate.getMonth() + 1).toString() + "-";
      datearray.push(month.length == 2 ? '0' + month : month);
      //日
      var day = newdate.getDate().toString();
      datearray.push(day.length == 1 ? '0' + day : day);
      datearray.push('  ');
      //时
      var hour = newdate.getHours().toString();
      datearray.push(hour.length == 1 ? '0' + hour + ':' : hour + ':');
      //分
      var minutes = newdate.getMinutes().toString();
      datearray.push(minutes.length == 1 ? '0' + minutes + ':' : minutes + ':');
      //秒
      var seconds = newdate.getSeconds().toString();
      datearray.push(seconds.length == 1 ? '0' + seconds : seconds);
      var datestr = '';
      for (var m = 0; m < datearray.length; m++) {
        datestr += datearray[m];
      }
      return datestr;
    } else {
      return '无记录';
    }
  },
  //页面显示
  OrderInfoShow: function() {
    if (this.data.OrderInfo.OrderStatus != 1) {
      this.setData({
        IsShowTime: 0
      })
    }
    if (this.data.OrderInfo.OrderStatus == 1) {
      this.TimerShow(this.data.OrderInfo.OrderUseTime)
      this.setData({
        IsShowTime: 3
      })
    } else if (this.data.OrderInfo.OrderStatus == 2 && this.data.OrderInfo.IsGroupOrder && !this.data.OrderInfo.GroupResult) {
      //待成团
      this.TimerShow(this.data.OrderInfo.OrderProductList[0].LimitEndTime)
      this.setData({
        IsShowTime: 1
      })
    } else if (this.data.OrderInfo.OrderStatus == 2 && !this.data.OrderInfo.IsGroupOrder && this.data.OrderInfo.OrderProductList[0].IsLimitTimeProduct == 0) {
      //众筹
      this.TimerShow(this.data.OrderInfo.OrderProductList[0].LimitEndTime)
      this.setData({
        IsShowTime: 1
      })
    } else if (this.data.OrderInfo.OrderStatus == 3) {
      //待收货
      this.TimerShow(this.data.OrderInfo.DeliveryTime)
      this.setData({
        IsShowTime: 2
      })
    }
    this.setData({
      //下单时间
      CreateTime: this.DateTimeFormat(this.data.OrderInfo.CreateTime),
      //成团时间
      RegimentTime: this.DateTimeFormat(this.data.OrderInfo.RegimentTime),
      //发货时间
      DeliverTime: this.DateTimeFormat(this.data.OrderInfo.DeliverTime),
      //成交时间
      CompleteTime: this.DateTimeFormat(this.data.OrderInfo.CompleteTime)
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    if (options.code) {
      util.postMethods('GetOrderDetail', {
        guid: options.code
      }, function(data) {
        if (data.IsOK) {
          var json = JSON.parse(data.Data);
          var total = 0;
          for (var i = 0, j = json.OrderProductList.length; i < j; i++) {
            var item = json.OrderProductList[i];
            total += item.DiscountPrice * item.ProductCount;
          }
          that.setData({
            OrderInfo: json,
            OrderGUID: options.code,
            total: total.toFixed(2)
          })
          that.OrderInfoShow();
          // util.postMethods('GetZCCount', {
          //   productguid: that.data.OrderInfo.OrderProductList[0].ProductGUID
          // }, function(data) {
          //   if (data.IsOK) {
          //     that.setData({
          //       IsZCCount: parseInt(data.Data)
          //     })
          //   }
          // })
        }
      })
    }

    //判断是否能抽奖
    // util.postMethods('GetPrizeCount', {
    //   openId: app.globalData.openid
    // }, function (result) {
    //   if (result.IsOK) {
    //     wx.navigateTo({
    //       url: '/pages/Shop/LuckDraw'
    //     })
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    if (this.data.PageInterval != null) {
      clearInterval(this.data.PageInterval);
    }
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
    if (this.data.PageInterval != null) {
      clearInterval(this.data.PageInterval);
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function(res) {
    if (res.from == 'button') {
      // 来自页面内转发按钮
      return {
        title: '拼团抢购-' + this.data.OrderInfo.OrderProductList[0].ProductName,
        path: '/pages/index/index?code=' + this.data.OrderInfo.OrderProductList[0].GUID + "&&num=1&&group=true&&groupnum=" + this.data.OrderInfo.GroupNumber,
        imageUrl: this.data.OrderInfo.OrderProductList[0].ProductMainPicture,
        success: function(res) {
          // 转发成功
          wx.showModal({
            content: '邀请成功！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        },
        fail: function(res) {
          // 转发失败
          wx.showModal({
            content: '邀请失败！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        }
      }
    } else {
      return {
        success: function(res) {
          // 转发成功
          wx.showModal({
            content: '分享成功！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function() {
              util.postMethods('ShareApp', {
                openid: app.globalData.openid,
                appguid: app.globalData.appId,
                sharetype: 2
              }, function(data) {

              })
            }
          })
        },
        fail: function(res) {
          // 转发失败
          wx.showModal({
            content: '分享失败！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        }
      }
    }
  },
  //倒计时
  TimerShow: function(endtime) {
    var totalSecond = parseInt((parseInt(endtime.replace("/Date(", "").replace(")/", "")) - Date.now()) / 1000);
    if (totalSecond <= 0) {
      return;
    }
    var interval = setInterval(function() {
      // 秒数  
      var second = totalSecond;

      // 天数位  
      var day = Math.floor(second / 3600 / 24);
      var dayStr = day.toString();
      if (dayStr.length == 1) dayStr = '0' + dayStr;

      // 小时位  
      var hr = Math.floor((second - day * 3600 * 24) / 3600);
      var hrStr = hr.toString();
      if (hrStr.length == 1) hrStr = '0' + hrStr;

      // 分钟位  
      var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
      var minStr = min.toString();
      if (minStr.length == 1) minStr = '0' + minStr;

      // 秒位  
      var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
      var secStr = sec.toString();
      if (secStr.length == 1) secStr = '0' + secStr;

      this.setData({
        countDownDay: dayStr,
        countDownHour: hrStr,
        countDownMinute: minStr,
        countDownSecond: secStr,
      });
      totalSecond--;
      if (totalSecond < 0) {
        clearInterval(interval);
        this.setData({
          countDownDay: '00',
          countDownHour: '00',
          countDownMinute: '00',
          countDownSecond: '00'
        });
      }
    }.bind(this), 1000);

    this.setData({
      PageInterval: interval
    });
  },
  //取消订单
  CancellationOrder: function(e) {
    var that = this
    wx.showModal({
      content: '确认取消订单？',
      confirmColor: "#DB1840",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          util.postMethods('CancellationOrder', {
            wxopenId: app.globalData.openid,
            guid: that.data.OrderGUID
          }, function(data) {
            if (data.IsOK) {
              wx.showModal({
                content: '取消成功！',
                showCancel: false,
                confirmColor: "#DB1840",
                confirmText: "确定",
                success: function() {
                  var options = {};
                  options.code = that.data.OrderGUID;
                  that.onLoad(options);
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  },
  PayOrder: function(e) {
    var that = this;
    util.WxPayByOrder(app.globalData.appId, app.globalData.openid, that.data.OrderGUID, function(res) {
      wx.showModal({
        content: '支付成功！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        success: function() {
          var options = {};
          options.code = that.data.OrderGUID;
          that.onLoad(options);
        }
      })
    });
  },
  BuyAgain: function(e) {
    var that = this;
    var ChooseItems = [];
    console.log(that.data.OrderInfo);
    for (var l = 0, m = that.data.OrderInfo.OrderProductList.length; l < m; l++) {
      var item = that.data.OrderInfo.OrderProductList[l];
      ChooseItems.push(item.GUID);
      wx.setStorage({
        key: item.GUID,
        data: item.ProductCount,
      })
    }
    wx.setStorage({
      key: "OrderList",
      data: ChooseItems
    })
    wx.navigateTo({
      url: '/pages/Shop/Order'
    })
  },
  ConfirmationReceipt: function (e) {
    var that = this
    wx.showModal({
      content: '是否确认收货？',
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function (res) {
        if (res.confirm) {
          util.postMethods('ConfirmOrder', {
            wxopenId: app.globalData.openid,
            guid: that.data.OrderGUID
          }, function (data) {
            if (data.IsOK) {
              wx.showModal({
                content: '确认成功！',
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function () {
                  var options = {};
                  options.code = that.data.OrderGUID;
                  that.onLoad(options);
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  }, 
  DeleteOrder: function (e) {
    var that = this
    wx.showModal({
      content: '确认删除订单？',
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function (res) {
        if (res.confirm) {
          util.postMethods('DeleteOrder', {
            wxopenId: app.globalData.openid,
            guid: that.data.OrderGUID
          }, function (data) {
            if (data.IsOK) {
              wx.showModal({
                content: '删除成功！',
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function () {
                  wx.navigateBack();
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  },
  ConfirmationEvalu: function (e) {
    wx.navigateTo({
      url: '/pages/Class/evaluate?id=' + this.data.OrderGUID
    })
  },
  //退款
  RefundDetail: function(e) {
    var list = ['仅退款', '退款并退货', '仅换货'];
    var that = this;
    var newdetailguid = e.currentTarget.id;
    wx.showActionSheet({
      itemList: list,
      success: function(e) {
        util.postMethods('SetAfterSale', {
          detailguid: newdetailguid,
          remark: list[e.tapIndex]
        }, function(data) {
          if (data.IsOK) {
            clearInterval(that.data.PageInterval);
            util.postMethods('GetOrderDetail', {
              guid: that.data.OrderGUID
            }, function(newdata) {
              if (newdata.IsOK) {
                that.setData({
                  OrderInfo: JSON.parse(newdata.Data)
                })
                that.OrderInfoShow();
              }
            })
          }
        })
      }
    })
  }
})