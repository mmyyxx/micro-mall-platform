// pages/Setting/Adress.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    PageHeight: app.globalData.systemInfo.windowHeight - 50,
    AdressList: null,
    DefaultGUID: null,
    IsAdressEdit: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //获取用户的收货地址
    this.GetAdress();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    if (this.data.DefaultGUID != null) {
      util.postMethods('UpdateAdressDefault', {
        guid: this.data.DefaultGUID,
        openid: app.globalData.openid
      }, function (data) {
        if (data.IsOK) {

        }
      })
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //获取收货地址
  GetAdress: function () {
    var that = this;
    util.postMethods('GetAdressList', {
      adresstype: 2,
      wxopenId: app.globalData.openid,
      appguId: app.globalData.appId
    }, function (data) {
      if (data) {
        if (data == '') {
          data = null;
        }
        that.setData({
          AdressList: data
        })
      }
    })
  },
  //添加收货地址
  AddAdress: function () {
    this.setData({
      IsAdressEdit: true
    })
  },
  //选择使用地址
  AdressSelect: function (e) {
    var pages = getCurrentPages();
    var lastPage = pages[pages.length - 2];
    var list = this.data.AdressList;
    for (var m = 0; m < list.length; m++) {
      if (list[m].GUID == e.currentTarget.id) {
        lastPage.setData({
          Adress: list[m]
        })
        wx.navigateBack();
        return;
      }
    }
  },
  //选择默认收货地址
  ChooseAdress: function (e) {
    var list = this.data.AdressList;
    for (var m = 0; m < list.length; m++) {
      if (list[m].GUID == e.currentTarget.id) {
        list[m].IsUse = 0;
      }
      else {
        list[m].IsUse = 1;
      }
    }
    this.setData({
      AdressList: list,
      DefaultGUID: e.currentTarget.id
    })
  },
  //编辑地址
  AdressEdit: function (e) {
    this.setData({
      IsAdressEdit: true
    })
    this.selectComponent("#AdressEdit").AdressInit(e.currentTarget.id);
  },
  //删除地址
  AdressDelete: function (e) {
    var that = this;
    wx.showModal({
      content: "是否确认删除？",
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function (res) {
        if (res.confirm) {
          util.postMethods('DeleteAdressByGUID', {
            guid: e.currentTarget.id
          }, function (data) {
            if (data.IsOK) {
              wx.showModal({
                content: "删除成功！",
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function (res) {
                  that.GetAdress();
                }
              })
            }
          })
        }
      }
    })
  },
  //关闭弹窗
  CloseView: function () {
    this.setData({
      IsAdressEdit: false
    });
    this.GetAdress();
  },
})