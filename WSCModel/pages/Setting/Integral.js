// pages/Setting/Integral.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //列表高度
    ScrollHeight: app.globalData.systemInfo.windowHeight - 50,
    //用户积分
    UserIntegral: 0,
    //积分商品
    IntegralProductList: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //获取用户积分
    util.postMethods('GetUserIntegral', {
      openId: app.globalData.openid
    }, function (data) {
      if (data.IsOK) {
        that.setData({
          UserIntegral: parseInt(data.Data)
        })
      }
    })

    //获取积分商品
    util.postMethods('GetIntegralInfo', {
      appguid: app.globalData.appId
    }, function (data) {
      if (data.IsOK) {
        that.setData({
          IntegralProductList: JSON.parse(data.Data)
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //积分兑换
  IntegralBuy: function (e) {
    var that = this
    if (e.currentTarget.dataset.integral > this.data.UserIntegral) {
      wx.showModal({
        content: '积分不足！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
    }
    else {
      wx.showModal({
        content: '是否确认兑换？',
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        success: function (res) {
          if (res.confirm) {
            //积分兑换商品
            util.postMethods('ExChangeProduct', {
              openId: app.globalData.openid,
              cardId: e.currentTarget.id
            }, function (data) {
              if (data.IsOK) {
                //获取用户积分
                util.postMethods('GetUserIntegral', {
                  openId: app.globalData.openid
                }, function (data) {
                  if (data.IsOK) {
                    that.setData({
                      UserIntegral: parseInt(data.Data)
                    })
                  }
                })
              }
              wx.showModal({
                content: data.IsOK ? '兑换成功！' : '兑换失败！',
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            })
          }
        }
      })
    }
  },
  //积分明细
  IntegralDetail: function () {
    wx.navigateTo({ url: '/pages/Setting/IntegralDetail' })
  }
})