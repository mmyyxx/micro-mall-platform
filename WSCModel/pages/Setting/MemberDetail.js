// pages/Setting/MemberDetail.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //列表高度
    ScrollHeight: app.globalData.systemInfo.windowHeight,
    //余额明细信息
    MoneyDetailList: null,
    //页码
    PageIndex: 0,
    //是否是第一次加载
    isFromSearch: true,
    //"上拉加载"的变量，默认false，隐藏
    searchLoading: false,
    //“没有数据”的变量，默认false，隐藏
    searchLoadingComplete: false,
    //提示信息
    MsgShow: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.SearchIntegralDetail();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //滚动到底部触发事件  
  searchScrollLower: function (e) {
    let that = this;
    if (that.data.searchLoading && !that.data.searchLoadingComplete) {
      that.setData({
        PageIndex: that.data.PageIndex + 1,  //每次触发上拉事件，把searchPageNum+1  
        isFromSearch: false,  //触发到上拉事件，把isFromSearch设为为false  
        searchLoading: true  //把"上拉加载"的变量设为true，显示  
      });
      that.SearchIntegralDetail();
    }
  },
  //查询
  SearchIntegralDetail: function () {
    var that = this;
    util.postMethods('GetMemberMoneyDetail', {
      openId: app.globalData.openid,
      pageIndex: that.data.PageIndex
    }, function (data) {
      if (data.IsOK) {
        var resultlist = JSON.parse(data.Data)
        //判断是否有数据，有则取数据  
        if (resultlist.length != 0) {
          var searchList = [];
          //如果isFromSearch是true从data中取出数据，否则先从原来的数据继续添加  
          that.data.isFromSearch ? searchList = resultlist : searchList = that.data.MoneyDetailList.concat(resultlist)
          that.setData({
            MoneyDetailList: searchList, //获取数据数组  
            searchLoading: searchList.length > 9
          });
          //没有数据了，把“没有数据”显示，把“上拉加载”隐藏  
        } else {
          var newMsgShow = '已加载全部';
          if (that.data.isFromSearch) {
            newMsgShow = '未查询到数据';
          }
          that.setData({
            MsgShow: newMsgShow,
            searchLoadingComplete: true, //把“没有数据”设为true，显示  
            searchLoading: false  //把"上拉加载"的变量设为false，隐藏
          });
        }
      }
      else {
        that.setData({
          PageIndex: that.data.PageIndex - 1
        });
      }
    }, function (result) {
      that.setData({
        PageIndex: that.data.PageIndex - 1
      });
    })
  }
})