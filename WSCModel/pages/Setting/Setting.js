// pages/Setting/Setting.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //用户信息
    userInfo: null,
    //屏幕宽度
    ViewWidth: app.globalData.systemInfo.windowWidth,
    //是否登录
    IsSignIn: false,
    //是否是会员
    IsMember: false,
    //用户积分
    UserIntegral: 0,
    //留言板
    IsRemarkShow: false,
    //猜你喜欢
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      userInfo: app.globalData.userInfo,
      IsMember: app.globalData.IsMember
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    util.postMethods('GetMoelListByType',{
      appId: app.globalData.appId,
      type:'modelList3',
      top:1
    },function(result){
      console.log(result);
      that.setData({
        list:result[0].ProductList
      });
    })
    // util.postMethods('IsUserSignIn', {
    //   openid: app.globalData.openid,
    //   appguid: app.globalData.appId
    // }, function(result) {
    //   that.setData({
    //     userInfo: app.globalData.userInfo,
    //     IsMember: app.globalData.IsMember,
    //     IsSignIn: result.IsOK
    //   })
    // })
    //获取用户积分
    // util.postMethods('GetUserIntegral', {
    //   openId: app.globalData.openid
    // }, function(data) {
    //   if (data.IsOK) {
    //     that.setData({
    //       UserIntegral: parseInt(data.Data)
    //     })
    //   }
    // })

    util.postMethods('GetMemberInfo', {
      openid: app.globalData.openid
    }, function (data) {
      console.log(data)
      app.globalData.IsMember = data.IsOK;
      that.setData({
        IsMember: app.globalData.IsMember
      })
      if (data.IsOK) {
        var userdata = JSON.parse(data.Data);
        app.globalData.iseventdraw = userdata.eventdraw;
        app.globalData.score = userdata.integral;
      } else {
      }
    })
  },
  BtnProductNavigateTo: function (e) {
    wx.navigateTo({
      url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //用户登录
  onUserLogin: function(res) {
    if (!res.detail.userInfo) {
      return;
    }
    var that = this
    util.postMethods('GetUserInfo', {
      openid: app.globalData.openid,
      successData: res.detail
    }, function(result) {
      wx.showModal({
        content: "登录成功！",
        showCancel: false,
        confirmColor: "#FD476D",
        confirmText: "确定",
        success: function() {
          that.setData({
            userInfo: res.detail.userInfo,
            IsMember:result.IsOK
          })
          app.globalData.userInfo = res.detail.userInfo;
          app.globalData.IsMember = result.IsOK
        }
      })
    })
  },
  //签到
  UserSignIn: function() {
    if (!this.data.IsMember) {
      wx.showModal({
        content: "非会员无法签到！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return
    }
    if (this.data.IsMember && this.data.userInfo && !this.data.IsSignIn) {
      var that = this
      util.postMethods('UserSignIn', {
        openid: app.globalData.openid,
        appguid: app.globalData.appId
      }, function(result) {
        if (result.IsOK) {
          wx.showModal({
            content: "签到成功！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function() {
              that.setData({
                IsSignIn: true
              })
            }
          })
          //获取用户积分
          util.postMethods('GetUserIntegral', {
            openId: app.globalData.openid
          }, function(data) {
            if (data.IsOK) {
              that.setData({
                UserIntegral: parseInt(data.Data)
              })
            }
          })
        }
      })
    }
  },
  closeOpenMember: function () {
    this.setData({
      IsOpen: false
    });
  },
  //页面跳转
  BtnnavigateTo2: function (e) {
    this.setData({
      IsOpen: true
    });
    this.selectComponent("#openMember").setPrice(app.globalData.oldmemberprice, app.globalData.newmemberprice);
  },
  BtnnavigateTo: function(e) {
    if (!app.globalData.userInfo) {
      wx.showModal({
        content: "请登录后再查看！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }
    console.log(e);
    var id = e.currentTarget.id;
    if (id) {
      wx.navigateTo({
        url: '/pages/Setting/'+id
      })
    }
    else {
      wx.navigateTo({
        url: '/pages/Setting/QRCode'
      })
    }
  },
  //售后
  NavigateAfterSale: function() {
    if (!app.globalData.userInfo) {
      wx.showModal({
        content: "请登录后再查看！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }
    wx.navigateTo({
      url: '/pages/Setting/AfterSale'
    })
  },
  //订单查询
  NavigateToOrder: function(e) {
    if (!app.globalData.userInfo) {
      wx.showModal({
        content: "请登录后再查看！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }
    wx.navigateTo({
      url: '/pages/Setting/OrderList?status=' + e.currentTarget.id
    })
  },
  //打开留言板
  LeavingMessage: function() {
    this.setData({
      IsRemarkShow: true
    })
  },
  //关闭留言板
  CloseView: function() {
    this.setData({
      IsRemarkShow: false
    })
  },
  //扫一扫
  BtnScanCode: function() {
    // 只允许从相机扫码
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        util.postMethods('GetProductGUID', {
          sku: res.result,
          guid: app.globalData.appId,
          openId: app.globalData.openid
        }, function(data) {
          if (data.IsOK) {
            if (data.msg) {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            } else {
              wx.navigateTo({
                url: '../Class/ProductDetail?code=' + data.Data
              })
            }
          } else {
            wx.showModal({
              content: data.msg,
              showCancel: false,
              confirmColor: "#F3ABC6",
              confirmText: "确定"
            })
          }
        })
      }
    })
  },
  //门店定位
  StoreMap: function() {
    util.postMethods('GetAdressList', {
      adresstype: 1,
      wxopenId: app.globalData.openid,
      appguId: app.globalData.appId
    }, function(data) {
      var ItemList = [];
      for (var m = 0; m < data.length; m++) {
        ItemList.push(data[m].AdressName);
      }
      wx.showActionSheet({
        itemList: ItemList,
        success: function(e) {
          wx.openLocation({
            latitude: data[e.tapIndex].Latitude,
            longitude: data[e.tapIndex].Longitude,
            name: data[e.tapIndex].AdressName,
            address: data[e.tapIndex].AdressPlace
          })
        }
      })
    })
  }
})