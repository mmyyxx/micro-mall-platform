// pages/Setting/UserCard.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //列表高度
    ScrollHeight: app.globalData.systemInfo.windowHeight,
    //卡券
    CardList: null,
    //是否可选择
    CanUse: false,
    card_number:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.usecard) {
      var pages = getCurrentPages();
      var lastPage = pages[pages.length - 2];
      this.setData({
        CardList: lastPage.data.CardList,
        CanUse: true,
        isshow:false
      })
    }
    else {
      var that = this
      util.postMethods('GetUserCard', {
        openId: app.globalData.openid,
        type:0
      }, function (data) {
        if (data.IsOK) {
          var json = JSON.parse(data.Data);
          var datainfo = data.msg.split(':');
          that.setData({
            CardList: json,
            datainfo: datainfo,
            isshow: true
          })
        }
      })
    }
  },
  //选择卡券
  ChooseCard: function (e) {
    if (this.data.CanUse) {
      var pages = getCurrentPages();
      var lastPage = pages[pages.length - 2];
      lastPage.setData({
        CardGUID: e.currentTarget.id,
        CardPrice: e.currentTarget.dataset.reducecost
      })
      wx.navigateBack();
      return;
    }
    else{
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },
  /*选择卡种 */
  selectCard:function(e){
    let card_index = e.currentTarget.dataset.index;
    this.setData({
      card_number: card_index
    })
    /*接下来写调用数据刷新CardList的值*/
    var that = this
    util.postMethods('GetUserCard', {
      openId: app.globalData.openid,
      type: card_index
    }, function (data) {
      if (data.IsOK) {
        var json = JSON.parse(data.Data);
        that.setData({
          CardList: json
        })
        if (card_index == 0) {
          var datainfo = data.msg.split(':');
          that.setData({
            datainfo: datainfo
          })
        }
      }
    })
  },
  toHistory:function(){
    this.setData({
      card_number: 1
    })
    var that = this
    util.postMethods('GetUserCard', {
      openId: app.globalData.openid,
      type: 1
    }, function (data) {
      if (data.IsOK) {
        var json = JSON.parse(data.Data);
        var datainfo = data.msg.split(':');
        that.setData({
          CardList: json,
          //datainfo: datainfo
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})