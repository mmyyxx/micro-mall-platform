// pages/Setting/OrderList.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    OrderStatus: 0,
    OrderList: [],
    ListHeight: app.globalData.systemInfo.windowHeight - 45,
    pageIndex: 0,
    searchComplete: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.status) {
      this.setData({
        OrderStatus: options.status
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      searchComplete: false,
      pageIndex: 0,
      OrderList: []
    })
    this.GetOrderList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //订单状态选择
  ChooseOrderStatus: function(e) {
    this.setData({
      OrderStatus: e.currentTarget.id,
      searchComplete: false,
      pageIndex: 0,
      OrderList: []
    })
    this.GetOrderList();
  },
  //获取订单数据
  GetOrderList: function() {
    var that = this;
    util.postMethods('GetOrderList', {
      pageIndex: that.data.pageIndex,
      wxopenId: app.globalData.openid,
      status: that.data.OrderStatus
    }, function(data) {
      if (data.IsOK) {
        var newlist = that.data.OrderList.concat(JSON.parse(data.Data));
        console.log(newlist);
        that.OrderList = newlist;
        that.setData({
          OrderList: newlist
        })
      } else {
        that.setData({
          searchComplete: true
        })
      }
    })
  },
  //滚动到底部触发事件  
  searchScrollLower: function(e) {
    if (!this.data.searchComplete) {
      this.setData({
        pageIndex: this.data.pageIndex + 1
      });
      this.GetOrderList();
    }
  },
  //取消订单
  CancellationOrder: function(e) {
    var that = this
    wx.showModal({
      content: '确认取消订单？',
      confirmColor: "#DB1840",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          util.postMethods('CancellationOrder', {
            wxopenId: app.globalData.openid,
            guid: e.currentTarget.id
          }, function(data) {
            if (data.IsOK) {
              wx.showModal({
                content: '取消成功！',
                showCancel: false,
                confirmColor: "#DB1840",
                confirmText: "确定",
                success: function() {
                  that.setData({
                    searchComplete: false,
                    pageIndex: 0,
                    OrderList: []
                  })
                  that.GetOrderList();
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  },
  //删除订单
  DeleteOrder: function(e) {
    var that = this
    wx.showModal({
      content: '确认删除订单？',
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          util.postMethods('DeleteOrder', {
            wxopenId: app.globalData.openid,
            guid: e.currentTarget.id
          }, function(data) {
            if (data.IsOK) {
              wx.showModal({
                content: '删除成功！',
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function() {
                  that.setData({
                    searchComplete: false,
                    pageIndex: 0,
                    OrderList: []
                  })
                  that.GetOrderList();
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  },
  //立即支付
  PayOrder: function(e) {
    util.WxPayByOrder(app.globalData.appId, app.globalData.openid, e.currentTarget.id, function(res) {
      wx.showModal({
        content: '支付成功！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定",
        success: function() {
          that.setData({
            searchComplete: false,
            pageIndex: 0,
            OrderList: []
          })
          this.GetOrderList();
        }
      })
    });
  },
  BuyAgain: function (e) {
    var that = this;
    var ChooseItems = [];
    console.log(that.OrderList);
    for(var i=0,j=that.OrderList.length;i<j;i++){
      var item = that.OrderList[i];
      if (item.GUID == e.currentTarget.id){
        for (var l = 0, m = item.ProductList.length; l < m; l++) {
          ChooseItems.push(item.ProductList[l].ItemCode);
          wx.setStorage({
            key: item.ProductList[l].ItemCode,
            data: item.ProductList[l].ProductCount,
          })
        }
        break;
      }
    }
    wx.setStorage({
      key: "OrderList",
      data: ChooseItems
    })
    wx.navigateTo({
      url: '/pages/Shop/Order'
    })
  },
  ConfirmationEvalu: function (e) {
    wx.navigateTo({
      url: '/pages/Class/evaluate?id=' + e.currentTarget.id
    })
  },
  //确认收货
  ConfirmationReceipt: function(e) {
    var that = this
    wx.showModal({
      content: '是否确认收货？',
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          util.postMethods('ConfirmOrder', {
            wxopenId: app.globalData.openid,
            guid: e.currentTarget.id
          }, function(data) {
            if (data.IsOK) {
              wx.showModal({
                content: '确认成功！',
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定",
                success: function() {
                  that.setData({
                    searchComplete: false,
                    pageIndex: 0,
                    OrderList: []
                  })
                  that.GetOrderList();
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  },
  //订单详情
  OrderDetail: function(e) {
    wx.navigateTo({
      url: '/pages/Setting/OrderDetail?code=' + e.currentTarget.id
    })
  },
  toFirst: function (e) {
    wx.switchTab({
      url: '/pages/index/index'
    })
  },
  toFenLei: function (e) {
    wx.switchTab({
      url: '/pages/Class/Class'
    })
  }
})