// pages/Setting/MemberRegister.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    MemberInfo: null,
    //生日
    RegisterData: '选择日期',
    //按钮显示
    btnFalse:false
  },
  //输入框修改
  bindKeyInput: function(e) {
    this.setData({
      RegisterData: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.isedit) {
      wx.setNavigationBarTitle({
        title: '会员修改'
      })
      this.setData({
        btnFalse:true
      })
    }
    var that = this
    //获取会员信息
    util.postMethods('GetMemberInfo', {
      openid: app.globalData.openid
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          MemberInfo: JSON.parse(data.Data)
        })
        that.setData({
          RegisterData: that.data.MemberInfo.BirthdayStr
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //用户注册
  BtnRegister: function(res) {
    var formdata = res.detail.value
    //检查必填项
    if (formdata.UserName.length == 0) {
      wx.showModal({
        content: '姓名未填写！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }
    if (this.data.RegisterData == '选择日期') {
      wx.showModal({
        content: '生日未填写！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }
    if (formdata.Mobile.length == 0) {
      wx.showModal({
        content: '手机号未填写！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }
    //提交注册信息
    var that = this
    util.postMethods('RegisterMember', {
      openid: app.globalData.openid,
      memberstr: formdata
    }, function(data) {
      if (data.IsOK) {
        wx.showModal({
          content: '提交成功！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定",
          success: function() {
            app.globalData.IsMember = true;
            wx.navigateBack()
          }
        })
      } else {
        wx.showModal({
          content: '提交失败！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
      }
    })
  }
})