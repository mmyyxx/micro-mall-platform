// pages/Setting/AfterSale.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    AfterSaleOrderList: null,
    //列表高度
    ScrollHeight: app.globalData.systemInfo.windowHeight,
    //售后信息
    ServiceMobile: null
  },
  //获取售后信息
  GetAfterSaleOrder: function (isFirst) {
    var that = this;
    util.postMethods('GetAfterSaleOrder', {
      openid: app.globalData.openid
    }, function (data) {
      if (data.IsOK) {
        that.setData({
          AfterSaleOrderList: JSON.parse(data.Data),
          ServiceMobile: JSON.parse(data.Data)[0].ServiceMobile
        })
      }
      if (!isFirst) {
        //停止下拉刷新
        wx.stopPullDownRefresh()
      }
    })
  },
  toredirect:function(e){
    console.log(e);
    if(e.currentTarget.dataset.after){
      if (e.currentTarget.dataset.type == '1') {
        wx.navigateTo({
          url: '/pages/Class/AfterPriceResult?status=0',
        })
      }
      else{
        wx.navigateTo({
          url: '/pages/Class/AfterPriceResult?status=1',
        })
      }
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.GetAfterSaleOrder(true)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.GetAfterSaleOrder(false)
  },
  //滚动上拉触顶事件
  ScrollUpper: function () {
    wx.startPullDownRefresh()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  toFirst:function(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  getAfter:function(e){
    console.log(e)
    wx.navigateTo({
      url: '/pages/Class/AfterSale?id=' + e.target.id,
    })
  },
  getMoney: function (e) {
    wx.navigateTo({
      url: '/pages/Class/AfterPrice?id='+e.target.id,
    })
  }
})