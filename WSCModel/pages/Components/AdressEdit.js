// pages/Components/AdressEdit.js
const app = getApp()
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    Adress: {
      'AdressName': '',
      'AdressPlace': '',
      'Mobile': '',
      'IsUse': 0
    },
    AdressGUID: '',
    ChooseAdress: '',
    Longitude: 0,
    Latitude: 0,
    isCanUse: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //画面加载
    AdressInit: function(guid) {
      var that = this;
      util.postMethods('GetAdressByGUID', {
        guid: guid
      }, function(data) {
        that.setData({
          Adress: data,
          ChooseAdress: data.MoreAdress,
          AdressGUID: guid
        })
      })
    },
    //收件人输入
    AdressNameInput: function(e) {
      var newAdress = this.data.Adress;
      newAdress.AdressName = e.detail.value;
      this.setData({
        Adress: newAdress
      })
    },
    //收件人手机号输入
    MobileInput: function(e) {
      var newAdress = this.data.Adress;
      newAdress.Mobile = e.detail.value;
      this.setData({
        Adress: newAdress
      })
    },
    //收件人地址输入
    AdressPlaceInput: function(e) {
      var newAdress = this.data.Adress;
      newAdress.AdressPlace = e.detail.value;
      this.setData({
        Adress: newAdress
      })
    },
    //选择地址
    ChooseAdress: function() {
      var that = this
      if (!that.data.isCanUse) {
        wx.openSetting({
          complete(res) {
            that.setData({
              isCanUse: res.authSetting['scope.userLocation']
            })
          }
        })
      } else {
        wx.chooseLocation({
          success: function(res) {
            that.setData({
              ChooseAdress: res.address,
              Longitude: res.longitude,
              Latitude: res.latitude
            })
          }
        })
      }
    },
    //关闭时间
    CloseView: function() {
      this.triggerEvent("CloseView");
    },
    //保存地址信息
    SaveAdress: function() {
      if (this.data.Adress.AdressName.length == 0) {
        wx.showModal({
          content: "请输入收件人姓名！",
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return;
      }
      if (this.data.Adress.Mobile.length == 0) {
        wx.showModal({
          content: "请输入收件人电话！",
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return;
      }
      if (this.data.ChooseAdress.length == 0) {
        wx.showModal({
          content: "请选择收件地址！",
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return;
      }
      if (this.data.Adress.AdressPlace.length == 0) {
        wx.showModal({
          content: "请输入详细地址！",
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return;
      }
      var that = this
      util.postMethods('SaveAdressData', {
        openid: app.globalData.openid,
        guid: this.data.AdressGUID,
        name: this.data.Adress.AdressName,
        adress: this.data.Adress.AdressPlace,
        mobile: this.data.Adress.Mobile,
        moreadress: this.data.ChooseAdress,
        longitude: this.data.Longitude,
        latitude: this.data.Latitude
      }, function(data) {
        if (data.IsOK) {
          wx.showModal({
            content: "保存成功！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function(res) {
              that.triggerEvent("CloseView");
            }
          })
        } else {
          wx.showModal({
            content: "保存失败！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        }
      })
    }
  },
  ready: function() {
    var that= this
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.userLocation']) {
          wx.authorize({
            scope: 'scope.userLocation',
            success() {
              that.setData({
                isCanUse: true
              })
            }
          })
        } else {
          that.setData({
            isCanUse: true
          })
        }
      }
    })
  }
})