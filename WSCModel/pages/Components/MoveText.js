// pages/Components/MoveText.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    MoveText: {
      type: String,
      value: '',
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    //滚动速度
    Pace: 1,
    //初始滚动距离
    Distance: 0,
    size: 14,
    // 时间间隔
    interval: 20,
    //屏幕宽度
    Width: (wx.getSystemInfoSync().windowWidth - 53) * 0.9,
    //View宽度
    ViewWidth: wx.getSystemInfoSync().windowWidth - 53
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //文字滚动
    TextMove: function () {
      var that = this
      var interval = setInterval(function () {
        if (-that.data.Distance < that.data.MoveText.length * 14) {
          that.setData({
            Distance: that.data.Distance - that.data.Pace,
          });
        } else {
          clearInterval(interval);
          that.setData({
            Distance: that.data.Width + 20
          });
          that.TextMove()
        }
      }, that.data.interval);
    }
  },
  //初始化
  ready: function () {
    var that = this
    //延迟时间
    setTimeout(function () {
      that.TextMove()
    }, 500)
  }
})
