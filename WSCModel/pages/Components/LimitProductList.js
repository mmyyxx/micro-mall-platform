// pages/Components/LimitProductList.js
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    CategoryId: {
      type: String,
      value: ''
    },
    ShowMsg: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    List: []
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //获取分类商品
    GetProductList: function() {
      var that = this
      util.postMethods('GetCategoryProduct', {
        guid: this.data.CategoryId
      }, function(data) {
        that.setData({
          List: data
        })
      })
    },
    //秒杀买
    BtnBuyProduct: function(e) {
      wx.navigateTo({
        url: '/pages/Class/ProductDetail?code=' + e.currentTarget.id
      })
    }
  },
  //初始化
  ready: function() {
    this.GetProductList()
  }
})