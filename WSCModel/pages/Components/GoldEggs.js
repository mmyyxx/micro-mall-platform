// pages/Components/GoldEggs.js
const app = getApp()
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    IsShow: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isClick: 0,
    VHeight: 0,
    Number: 0,
    IsShare: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //砸蛋
    ClickEgg: function(e) {
      if (this.data.Number <= 0) {
        wx.showModal({
          content: '您的次数已用完！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      //砸金蛋
      var that = this
      util.postMethods('GoldEgg', {
        openId: app.globalData.openid
      }, function(data) {
        if (data.IsOK) {
          wx.showModal({
            content: data.Data == '无' ? '很遗憾，继续加油哦！' : '恭喜，您抽中了' + data.Data,
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
          that.setData({
            isClick: e.currentTarget.id
          })
        }
        else{
          wx.showModal({
            content: data.msg,
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        }
        that.GetGoldEggCount()
      })
    },
    //关闭
    BtnClose: function() {
      this.GetGoldEggCount()
      this.setData({
        isClick: 0
      })
      this.triggerEvent("BtnClose")
    },
    //获取可砸蛋次数
    GetGoldEggCount: function() {
      var that = this
      // util.postMethods('GetGoldEggsInfo', {
      //   openId: app.globalData.openid
      // }, function(data) {
      //   if (data.IsOK) {
      //     that.setData({
      //       Number: parseInt(data.Data),
      //       IsShare: parseInt(data.msg) == 0
      //     })
      //   }
      // })
    }
  },
  //初始化
  ready: function() {
    this.GetGoldEggCount()
    this.setData({
      VHeight: app.globalData.systemInfo.windowWidth * 0.8
    })
  }
})