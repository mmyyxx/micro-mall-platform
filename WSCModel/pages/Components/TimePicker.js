// pages/Components/TimePicker.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    IsShow: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    //取货时间
    GetGoodsDate: '选择时间',
    //当前时间
    StartDate: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //输入框修改
    bindKeyInput: function(e) {
      var choosedate = new Date(new Date(e.detail.value).toDateString()).getTime()
      var nowDate = new Date(new Date().toDateString()).getTime();
      if (choosedate < nowDate) {
        wx.showModal({
          content: '你选的时间已经过啦！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      this.setData({
        GetGoodsDate: e.detail.value
      })
    },
    //点击确定
    BtnClick:function(){
      this.triggerEvent("BtnClick", this.data.GetGoodsDate)
    },
    //关闭
    BtnClose:function(){
      this.setData({
        IsShow: false,
        GetGoodsDate: '选择时间'
      })
    }
  },
  //加载完成初始化
  ready: function () {
    //获取当前时间
    var nowdate = new Date()
    this.setData({
      StartDate: nowdate.toDateString()
    })
  }
})