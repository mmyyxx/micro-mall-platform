// pages/Components/List.js
const app = getApp()
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    ViewHeight: {
      type: Number,
      value: 0,
    },
    CategoryList: {
      type: Object,
      value: null,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    //是否显示子分类
    IsShowChild: 0,
    //列表数据
    list: [],
    //选中分类
    ChooseItem: '',
    //搜索条件
    Searchstr: '',
    //排序字段
    Orderstr: '',
    //排序方式
    Ordertype: 0,
    //页码
    PageIndex: 0,
    //"上拉加载"的变量，默认false，隐藏
    searchLoading: false,
    //“没有数据”的变量，默认false，隐藏
    searchLoadingComplete: false,
    //提示信息
    MsgShow: '',
    //商品信息
    ProductInfo: null,
    //商品选择是否显示
    IsChooseShow: false,
    //限时
    IsLimit: 1,
    //团购
    IsGroup: 1,
    //滚动条位置
    ScrollTop: 0,
    //众筹
    IsZC: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //滚动到底部触发事件  
    searchScrollLower: function(e) {
      if (this.data.searchLoading && !this.data.searchLoadingComplete) {
        this.setData({
          PageIndex: this.data.PageIndex + 1, //每次触发上拉事件，把searchPageNum+1  
          searchLoading: true //把"上拉加载"的变量设为true，显示  
        });
        this.SearchProducts(null, false);
      }
    },
    //显示子分类数据
    ShowChildCategory: function(childlist) {
      console.log(childlist)
      var isshowchild = 1;
      if (childlist[0].ChildCategoryList.length>0)isshowchild=2;
      
      this.setData({
        IsShowChild: isshowchild,
        list: childlist,
        //滚动条位置
        ScrollTop: 0
      });
    },
    //根据分类查询
    SearchProducts: function(inputdata, isFromSearch) {
      var that = this;
      if (isFromSearch) {
        that.setData({
          IsShowChild: 0,
          //列表
          list: [],
          //选中分类
          ChooseItem: inputdata.ChooseItem,
          //搜索条件
          SearchStr: inputdata.SearchStr,
          //排序方式
          Ordertype: inputdata.Ordertype,
          //排序条件
          Orderstr: inputdata.Orderstr,
          //限时抢购
          IsLimit: inputdata.IsLimit ? 0 : 1,
          //团购
          IsGroup: inputdata.IsGroup ? 0 : 1,
          //"上拉加载"的变量，默认false，隐藏
          searchLoading: false,
          //“没有数据”的变量，默认false，隐藏
          searchLoadingComplete: false,
          //页码
          PageIndex: 0,
          //滚动条位置
          ScrollTop: 0,
          //是否是众筹
          IsZC: inputdata.IsZC ? true : false
        })
      } else {
        if (that.data.IsShowChild==1) {
          return
        }
      }
      util.postMethods('GetProductSearchData', {
        appId: app.globalData.appId,
        categoryId: that.data.ChooseItem,
        searchstr: that.data.SearchStr,
        orderstr: that.data.Orderstr,
        ordertype: that.data.Ordertype,
        pageIndex: that.data.PageIndex,
        IsLimit: that.data.IsLimit,
        IsGroup: that.data.IsGroup
      }, function(data) {
        if (data) {
          //判断是否有数据，有则取数据  
          if (data.length != 0) {
            var searchList = [];
            //如果isFromSearch是true从data中取出数据，否则先从原来的数据继续添加  
            searchList = that.data.list.concat(data)
            console.log(searchList)
            that.setData({
              list: searchList, //获取数据数组  
              searchLoading: searchList.length > 5
            });
            //没有数据了，把“没有数据”显示，把“上拉加载”隐藏  
          } else {
            var newMsgShow = '已加载全部';
            if (isFromSearch) {
              newMsgShow = '无商品信息';
            }
            that.setData({
              MsgShow: newMsgShow,
              searchLoadingComplete: true, //把“没有数据”设为true，显示  
              searchLoading: false //把"上拉加载"的变量设为false，隐藏
            });
          }
        } else {
          that.setData({
            PageIndex: that.data.PageIndex - 1
          });
        }
      }, function(result) {
        that.setData({
          PageIndex: that.data.PageIndex - 1
        });
      })
    },
    //进入分类搜索
    navigateToCategory: function(e) {
      console.log(e)
      wx.navigateTo({
        url: '/pages/Class/CategorySearch?code=' + e.currentTarget.id + '&&title=' + e.currentTarget.dataset.title
      })
    },
    //进入商品详情
    navigateToProduct: function(e) {
      wx.navigateTo({
        url: '../Class/ProductDetail?code=' + e.currentTarget.id + '&&zc=' + this.data.IsZC
      })
    },
    //加入购物车
    BtnAddShop: function(e) {
      var that = this;
      util.postMethods('GetProductInfoByGUID', {
        guid: e.currentTarget.id
      }, function(data) {
        if (data.IsOK) {
          var newProductInfo = JSON.parse(data.Data);
          that.setData({
            ProductInfo: newProductInfo,
            IsChooseShow: true
          })
        }
      })
    },
    //关闭商品选择
    CloseView: function() {
      this.setData({
        IsChooseShow: false
      });
    },
    //确认商品选择
    BtnOK: function() {
      //商品选择控件
      var ProductChoose = this.selectComponent("#ProductChoose")
      //类别选择
      var selectItems = ProductChoose.data.SelectClassItem
      //选择数量
      var productNum = ProductChoose.data.Count;
      for (var m = 0; m < selectItems.length; m++) {
        if (selectItems[m] == '') {
          wx.showModal({
            content: "请选择商品属性！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
          return;
        }
      }
      //加入购物车
      if (this.data.ProductInfo.IsGroupProduct == 0) {
        //参与拼团
        wx.navigateTo({
          url: '/pages/Shop/ParticipateGroup?code=' + selectItems[selectItems.length - 1]
        })
      } else {
        //加入购物车
        //获取已加入购物车的数据
        var userShopList = [];
        var list = wx.getStorageSync('UserShopList')
        if (list) {
          userShopList = list;
        }
        for (var m = 0; m < userShopList.length; m++) {
          //已经存在购物车中
          if (selectItems[selectItems.length - 1] == userShopList[m]) {
            var value = wx.getStorageSync(selectItems[selectItems.length - 1])
            wx.setStorage({
              key: selectItems[selectItems.length - 1],
              data: value + productNum
            })
            wx.showToast({
              title: '加入成功',
              image: '/resources/Icon/Toast_Success.png',
              mask: true
            })
            this.setData({
              IsChooseShow: false
            });
            return;
          }
        }
        //加入购物车
        userShopList.push(selectItems[selectItems.length - 1]);
        wx.setStorage({
          key: 'UserShopList',
          data: userShopList
        })
        wx.setStorage({
          key: selectItems[selectItems.length - 1],
          data: productNum
        })
        wx.showToast({
          title: '加入成功',
          image: '/resources/Icon/Toast_Success.png',
          mask: true
        })
        this.setData({
          IsChooseShow: false
        });
      }
    }
  }
})