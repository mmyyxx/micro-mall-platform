// pages/Components/ProductChoose.js
const app = getApp()
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    ProductInfo: {
      type: Object,
      value: null
    },
    BuyType: {
      type: Number,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    SelectClassItem: null,
    Count: 1,
    MinCount: 1,
    ShowPrice: null,
    ClassStock: 0,
    ClassStockNum: 1,
    IsChooseItem: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //类别点击
    ClassItemClick: function(e) {
      if (e.currentTarget.dataset.stock < e.currentTarget.dataset.stocknum && e.currentTarget.dataset.price > 0) {
        wx.showModal({
          content: '已售罄！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      //初始化数量
      this.setData({
        IsChooseItem: e.currentTarget.dataset.price > 0,
        Count: this.data.MinCount
      })

      var newSelectClassItem = this.data.SelectClassItem
      newSelectClassItem[e.currentTarget.dataset.groupindex] = e.currentTarget.id
      for (var m = e.currentTarget.dataset.groupindex + 1; m < newSelectClassItem.length; m++) {
        newSelectClassItem[m] = ''
      }
      this.setData({
        SelectClassItem: newSelectClassItem,
        ClassStock: e.currentTarget.dataset.stock,
        ClassStockNum: e.currentTarget.dataset.stocknum
      })
      this.ProductClassShow()
    },
    //商品类别操作
    ProductClassShow: function() {
      var newProductInfo = this.data.ProductInfo
      var dic = newProductInfo.ClassDic
      var newSelectClassItem = this.data.SelectClassItem
      var newShowPrice = this.data.ShowPrice
      for (var m = 0; m < newProductInfo.ProductClassList.length; m++) {
        var chooselist = dic[newSelectClassItem[m - 1]];
        if (!chooselist) {
          for (var num = 0; num < newProductInfo.ProductClassList[0].ClassItemList.length; num++) {
            if (newProductInfo.ProductClassList[0].ClassItemList[num].ClassGUID == newSelectClassItem[newSelectClassItem.length - 1]) {
              newShowPrice = app.globalData.IsMember ? newProductInfo.ProductClassList[0].ClassItemList[num].ClassPrice : newProductInfo.ProductClassList[0].ClassItemList[num].ClassOldPrice
            }
          }
          continue;
        }
        for (var n = 0; n < newProductInfo.ProductClassList[m].ClassItemList.length; n++) {
          newProductInfo.ProductClassList[m].ClassItemList[n].ClassShow = true;
          for (var x = 0; x < chooselist.length; x++) {
            if (newProductInfo.ProductClassList[m].ClassItemList[n].ClassGUID == chooselist[x]) {
              newProductInfo.ProductClassList[m].ClassItemList[n].ClassShow = false;
              if (newProductInfo.ProductClassList[m].ClassItemList[n].ClassGUID == newSelectClassItem[newSelectClassItem.length - 1]) {
                newShowPrice = app.globalData.IsMember ? newProductInfo.ProductClassList[m].ClassItemList[n].ClassPrice : newProductInfo.ProductClassList[m].ClassItemList[n].ClassOldPrice
              }
            }
          }
        }
      }
      this.setData({
        SelectClassItem: newSelectClassItem,
        ProductInfo: newProductInfo,
        ShowPrice: newShowPrice
      });
    },
    //减事件
    BtnReduce: function() {
      if (this.data.Count > this.data.MinCount) {
        this.setData({
          Count: this.data.Count - 1
        });
      }
    },
    //加事件
    BtnAdd: function() {
      var limitcount = this.data.ProductInfo.Remark;
      if (this.data.ProductInfo.Remark2){
        limitcount = this.data.ProductInfo.Remark - this.data.ProductInfo.Remark2;
      }
      if (this.data.ProductInfo.IsGroupProduct == 0) {
        wx.showModal({
          content: '团购商品限购数量！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      if (!this.data.IsChooseItem) {
        wx.showModal({
          content: '请选择要购买的商品！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      else if (limitcount < this.data.Count + 1) {
        wx.showModal({
          content: '商品限购' + limitcount+'组！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      else if (this.data.ClassStock < (this.data.Count + 1) * this.data.ClassStockNum) {
        wx.showModal({
          content: '库存不足！',
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      this.setData({
        Count: this.data.Count + 1
      });
    },
    //关闭时间
    CloseView: function() {
      this.triggerEvent("CloseView");
    },
    //确认
    BtnOK: function() {
      this.triggerEvent("BtnOK");
    }
  },

  //加载完成初始化
  ready: function() {
    if (this.data.ProductInfo) {
      var newProductInfo = this.data.ProductInfo
      var newMinCount = this.data.MinCount
      var newCount = this.data.Count
      if (newProductInfo.PackingSpecification) {
        newMinCount = parseInt(newProductInfo.PackingSpecification)
        newCount = parseInt(newProductInfo.PackingSpecification)
      }
      //初始化选中值
      var newSelectClassItem = [];
      for (var m = 0; m < newProductInfo.ProductClassList.length; m++) {
        newSelectClassItem.push('');
      }
      this.setData({
        SelectClassItem: newSelectClassItem,
        MinCount: newMinCount,
        Count: newCount,
        ShowPrice: newProductInfo.DiscountPrice
      })
    }
  }
})