// pages/Components/Welcome.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    ImageUrl: {
      type: String,
      value: ''
    },
    NavigateUrl: {
      type: String,
      value: ''
    },
    ImageUrlBack: {
      type: String,
      value: ''
    },
    ShowTime: {
      type: Number,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    timenum: 0,
    //计时器
    PageInterval: null
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //主页面
    navigateToIndex: function() {
      if (this.data.PageInterval != null) {
        clearInterval(this.data.PageInterval);
      }
      wx.switchTab({
        url: '/pages/index/index'
      })
    },
    //页面跳转
    navigateTo: function () {
      if (this.data.PageInterval != null) {
        clearInterval(this.data.PageInterval);
      }
      wx.setStorageSync('WelcomePage', this.data.NavigateUrl)
      wx.switchTab({
        url: '/pages/index/index'
      })
    },
    //显示
    ShowWelcome: function() {
      var that = this
      that.setData({
        timenum: that.data.ShowTime
      })
      var interval = setInterval(function() {
        that.setData({
          timenum: that.data.timenum - 1
        })
        if (that.data.timenum == 0){
          that.navigateToIndex()
        }
      }, 1000);
      that.setData({
        PageInterval: interval
      })
    }
  }
})