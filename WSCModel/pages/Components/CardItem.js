// pages/Components/CardItem.js
const app = getApp()
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    CardId: {
      type: String,
      value: ''
    },
    IsLine: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    CanGet: false,
    ImgURL: '',
    CardInfo: null
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //优惠券点击事件
    BtnClick: function(e) {
      //已经领取过的无法领取
      if (!this.data.CanGet) {
        return
      }
      var that = this
      util.postMethods('UserGetCard', {
        openId: app.globalData.openid,
        cardId: that.data.CardId
      }, function(data) {
        if (data.IsOK) {
          wx.showModal({
            content: "领取成功！",
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function() {
              that.setData({
                CanGet: false,
                ImgURL: that.data.CardInfo.ImgURL2
              })
            }
          })
        } else {
          wx.showModal({
            content: data.msg == null ? '领取失败！' : data.msg,
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定"
          })
        }
      })
    }
  },
  //初始化
  ready: function() {
    var that = this
    //获取卡券信息
    util.postMethods('GetCardInfo', {
      openId: app.globalData.openid,
      cardId: that.data.CardId
    }, function(data) {
      if (data.IsOK) {
        var cardinfo = JSON.parse(data.Data)
        var Iscanget = data.msg == '0'
        that.setData({
          CanGet: Iscanget,
          ImgURL: Iscanget ? cardinfo.ImgURL : cardinfo.ImgURL2,
          CardInfo: cardinfo
        })
      }
    })
  }
})