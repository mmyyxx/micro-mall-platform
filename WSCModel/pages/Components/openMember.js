// pages/Components/openMember.js
const app = getApp()
Component({
  /**
   * 页面的初始数据
   */
  data: {
  },
  methods: {
    //关闭
    closeOpenMember: function () {
      this.triggerEvent("closeOpenMember");
    },
    sure_go: function () {
      this.triggerEvent("closeOpenMember");
      wx.navigateTo({
        url: '/pages/Member/memberCenter',
      }) 
    },
    setPrice: function (oldprice, newprice) {
      this.setData({
        oldmember: oldprice,
        newmember:newprice
      })
    }
  }
})