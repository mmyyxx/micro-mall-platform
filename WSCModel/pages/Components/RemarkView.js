// pages/Components/RemarkView.js
const app = getApp()
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    RemarkData: ''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //输入框修改
    bindKeyInput: function (e) {
      this.setData({
        RemarkData: e.detail.value
      })
    },
    //关闭留言板
    MemberClose: function () {
      this.triggerEvent("CloseView")
    },
    //留言板
    BtnRemark: function () {
      var that = this
      util.postMethods('UserRemark', {
        openId: app.globalData.openid,
        appguid: app.globalData.appId,
        remark: that.data.RemarkData
      }, function (data) {
        if (data.IsOK) {
          wx.showModal({
            content: '留言成功！',
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function () {
              that.triggerEvent("CloseView")
            }
          })
        }
      })
    }
  }
})
