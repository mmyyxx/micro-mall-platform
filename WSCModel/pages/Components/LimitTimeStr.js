// pages/Components/LimitTimeStr.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    LimitEndTime: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    //是否结束
    IsSaleEnd: false,
    countDownDay: '00',
    countDownHour: '00',
    countDownMinute: '00',
    countDownSecond: '00',
    //计时器
    PageInterval: null
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //倒计时
    TimerShow: function(endtime) {
      var totalSecond = parseInt((parseInt(endtime.replace("/Date(", "").replace(")/", "")) - Date.now()) / 1000);
      if (totalSecond <= 0) {
        this.setData({
          IsSaleEnd: true
        });
        return;
      }
      var interval = setInterval(function() {
        // 秒数  
        var second = totalSecond;
        // 天数位  
        var day = Math.floor(second / 3600 / 24);
        var dayStr = day.toString();
        if (dayStr.length == 1) dayStr = '0' + dayStr;

        // 小时位  
        var hr = Math.floor((second - day * 3600 * 24) / 3600);
        var hrStr = hr.toString();
        if (hrStr.length == 1) hrStr = '0' + hrStr;

        // 分钟位  
        var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
        var minStr = min.toString();
        if (minStr.length == 1) minStr = '0' + minStr;

        // 秒位  
        var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
        var secStr = sec.toString();
        if (secStr.length == 1) secStr = '0' + secStr;

        this.setData({
          countDownDay: dayStr,
          countDownHour: hrStr,
          countDownMinute: minStr,
          countDownSecond: secStr,
        });
        totalSecond--;
        if (totalSecond < 0) {
          clearInterval(interval);
          this.setData({
            countDownDay: '00',
            countDownHour: '00',
            countDownMinute: '00',
            countDownSecond: '00',
            IsSaleEnd: true
          });
        }
      }.bind(this), 1000);

      this.setData({
        PageInterval: interval
      });
    },
    //结束
    TimeEnd: function() {
      if (this.data.PageInterval != null) {
        clearInterval(this.data.PageInterval);
      }
    }
  },
  //初始化
  ready: function () {
    this.TimerShow(this.data.LimitEndTime)
  },
  //结束
  detached: function () {
    this.TimeEnd()
  }
})