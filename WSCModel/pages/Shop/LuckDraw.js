// pages/Shop/LuckDraw.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //高度
    ViewHeight: app.globalData.systemInfo.windowWidth,
    //动画
    animationData: {},
    //旋转角度
    userRotate: 0,
    //抽中奖品
    resultStr: '',
    //能否抽奖
    IsCanUse: false,
    //能否点击
    IsCanClick: true
  },
  //抽奖
  ShowAnimation() {
    var that = this
    if (!that.data.IsCanUse) {
      wx.showModal({
        content: '每日首单付款后即可抽奖！',
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return;
    }

    wx.showModal({
      content: '抽奖后首单将无法取消，是否抽奖？',
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          that.setData({
            IsCanClick: false,
            IsCanUse: false
          })
          util.postMethods('GetPrizeResult', {
            openId: app.globalData.openid
          }, function(result) {
            if (result.IsOK) {
              that.setData({
                userRotate: parseInt(result.Data),
                resultStr: result.msg
              })
              that.animationShow()
            } else {
              that.setData({
                userRotate: 75,
                resultStr: '很遗憾没中奖！'
              })
            }
          })
        }
      }
    })
  },
  //抽奖动画
  animationShow() {
    var that = this
    var n = 0;
    var AnimationInterval = setInterval(function() {
      n++;
      if (n == 1) {
        that.animation.rotate(180).step({
          duration: 700,
          timingFunction: 'ease-in'
        })
      } else if (n == 7) {
        clearInterval(AnimationInterval)
        if (that.data.userRotate > 180) {
          that.animation.rotate(180 * n).step({
            duration: 4000
          })
          that.setData({
            animationData: that.animation.export()
          })
        }
        that.animation.rotate(180 * (n - 1) + that.data.userRotate).step({
          duration: 5000,
          timingFunction: 'ease-out'
        })
        setTimeout(function() {
          var msgtitle = '恭喜您，抽中奖品'
          if (that.data.userRotate > 45 && that.data.userRotate < 90) {
            msgtitle = ''
          }
          wx.showModal({
            title: msgtitle,
            content: that.data.resultStr,
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function() {
              that.setData({
                IsCanClick: true
              })
            }
          })
        }, 5000);
      } else {
        that.animation.rotate(180 * n).step({
          duration: 500 * n
        })
      }

      that.setData({
        animationData: that.animation.export()
      })
    }.bind(that), 500)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const animation = wx.createAnimation({
      duration: 3000,
      timingFunction: 'linear',
    })

    this.animation = animation

    //判断是否能抽奖
    var that = this
    util.postMethods('GetPrizeCount', {
      openId: app.globalData.openid
    }, function(result) {
      that.setData({
        IsCanUse: result.IsOK
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})