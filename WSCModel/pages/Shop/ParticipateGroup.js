// pages/Shop/ParticipateGroup.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //订单列表
    GroupOrderList: null,
    //商品类别Code
    ClassGUID: null
  },
  //获取拼团信息
  GroupOrderShow: function(isFirst) {
    var that = this
    util.postMethods('GetGroupOrder', {
      openid: app.globalData.openid,
      classguid: that.data.ClassGUID
    }, function(data) {
      if (data.IsOK) {
        var newGroupOrderList = JSON.parse(data.Data)
        for (var m = 0; m < newGroupOrderList.length; m++) {
          newGroupOrderList[m].GroupPeopleImg = JSON.parse(newGroupOrderList[m].GroupPeopleImg)
        }
        that.setData({
          GroupOrderList: newGroupOrderList
        })
        if (!isFirst) {
          //停止下拉刷新
          wx.stopPullDownRefresh()
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.code) {
      this.setData({
        ClassGUID: options.code
      })
      this.GroupOrderShow(true)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.GroupOrderShow(false)
  },
  //滚动上拉触顶事件
  ScrollUpper: function() {
    wx.startPullDownRefresh()
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //拼团购买
  BtnGroupBuy: function(e) {
    wx.navigateTo({
      url: '/pages/Shop/Order?code=' + e.currentTarget.dataset.guid + "&&num=1&&group=true&&groupnum=" + e.currentTarget.dataset.groupumber
    })
  }
})