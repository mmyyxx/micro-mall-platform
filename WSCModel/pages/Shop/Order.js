// pages/Shop/Order.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //防止按钮重复点击
    isSale: false,
    PageHeight: app.globalData.systemInfo.windowHeight,
    //送货方式
    DeliveryMode: 1,
    //商品信息
    OrderList: null,
    //订单总价
    TotalPrice: 0,
    //订单实际
    newTotalPrice: 0,
    //是否是团购订单
    IsGroupOrder: false,
    //拼团码
    GroupNumber: '',
    //地址
    Adress: {},
    //备注
    Remark: '',
    //商品列表
    OrderDic: null,
    //运费
    Freight: 0,
    //优惠券信息
    CardList: null,
    //优惠券
    CardGUID: '',
    //减免金额
    CardPrice: 0,
    //订单标题
    OrderTitle: '',
    //是否是会员
    IsMember: false,
    //是否禁用
    buttonClicked: false,
    //满减优惠
    MinPrice: 0,
    // 选择门店ID
    shopId: 0,
    // 门店弹窗状态
    showMen: false,
    sotrelist: [], //门店信息
    username: '',
    usertel: '',
    wait:false
  },
  userNameInput: function(e) {
    this.data.username = e.detail.value;
  },
  userTelInput: function(e) {
    this.data.usertel = e.detail.value;
  },
  // 关闭门店选择弹窗
  close_qu: function() {
    if (!this.data.username) {
      wx.showToast({
        title: '请输入取货人',
        icon: 'none',
        duration: 1000
      })
      return;
    }
    if (!this.data.usertel) {
      wx.showToast({
        title: '请输入取货电话',
        icon: 'none',
        duration: 1000
      })
      return;
    }

    this.setData({
      showMen: false,
      Adress: this.data.sotrelist[this.data.shopId]
    })
  },
  // 门店选择
  selectShop: function(e) {
    console.log(e)
    this.setData({
      shopId: e.currentTarget.dataset.shopid
    })
  },
  //运费计算
  CalculationFreight: function() {
    if (this.data.DeliveryMode == 1) {
      this.setData({
        Freight: 0,
        newTotalPrice: (this.data.TotalPrice - this.data.CardPrice).toFixed(2)
      })
    } else if (this.data.DeliveryMode == 2) {
      var that = this
      util.postMethods('GetFreight', {
        appguId: app.globalData.appId,
        adressguId: that.data.Adress.GUID
      }, function(data) {
        if (data.IsOK) {
          var FreightFree = parseFloat(data.msg)
          if (FreightFree < parseFloat(that.data.TotalPrice)) {
            that.setData({
              Freight: 0,
              newTotalPrice: (that.data.TotalPrice - that.data.CardPrice).toFixed(2)
            })
          } else {
            that.setData({
              Freight: data.Data,
              newTotalPrice: (parseFloat(that.data.TotalPrice) + parseFloat(data.Data) - that.data.CardPrice).toFixed(2)
            })
          }
        }
      })
    }
  },
  //选择优惠券
  UseUserCard: function() {
    wx.navigateTo({
      url: '/pages/Setting/UserCard?usecard=true'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // util.postMethods('GetOrderTitle', {
    //   appguid: app.globalData.appId
    // }, function(data) {
    //   if (data) {
    //     that.setData({
    //       OrderTitle: data
    //     })
    //   }
    // })
    this.ChooseStore();
    //var newAdress = wx.getStorageSync('DeliveryStore')
    var newAdress = null
    var that = this
    //是否是拼团订单
    if (options.group) {
      that.setData({
        IsGroupOrder: options.group
      })
    }
    //拼团码
    if (options.groupnum) {
      that.setData({
        GroupNumber: options.groupnum
      })
    }
    if (options.code) {
      util.postMethods('GetShopItemList', {
        shoplist: [options.code]
      }, function(data) {
        if (data) {
          var newOrderList = data
          var newDic = []
          newOrderList[0].ProductCount = parseInt(options.num)
          var total = (app.globalData.IsMember ? newOrderList[0].ProductPrice : newOrderList[0].OldPrice) * newOrderList[0].ProductCount
          newDic.push([options.code, parseInt(options.num)])
          that.setData({
            OrderList: newOrderList,
            IsMember: app.globalData.IsMember,
            TotalPrice: total,
            newTotalPrice: total.toFixed(2),
            Adress: newAdress,
            isSale: true,
            OrderDic: newDic
          })
          util.postMethods('GetUserCanUseCard', {
            openId: app.globalData.openid,
            totalprice: total,
            productlist: newDic
          }, function(cardData) {
            if (cardData.IsOK) {
              that.setData({
                CardList: JSON.parse(cardData.Data),
                MinPrice: parseInt(cardData.msg),
                TotalPrice: that.data.TotalPrice - parseInt(cardData.msg),
                newTotalPrice: that.data.newTotalPrice - parseInt(cardData.msg)
              })
            } else {
              wx.showModal({
                content: cardData.msg,
                showCancel: false,
                confirmColor: "#F3ABC6",
                confirmText: "确定"
              })
            }
          })
        }
      })
    } else {
      wx.getStorage({
        key: 'OrderList',
        success: function(res) {
          util.postMethods('GetShopItemList', {
            shoplist: res.data
          }, function(data) {
            if (data) {
              var newOrderList = data
              var total = 0
              var newDic = []
              for (var m = 0; m < newOrderList.length; m++) {
                newDic.push([newOrderList[m].ItemCode, wx.getStorageSync(newOrderList[m].ItemCode)])
                newOrderList[m].ProductCount = wx.getStorageSync(newOrderList[m].ItemCode)
                total += (app.globalData.IsMember ? newOrderList[m].ProductPrice : newOrderList[m].OldPrice) * newOrderList[m].ProductCount
              }
              that.setData({
                OrderList: newOrderList,
                IsMember: app.globalData.IsMember,
                TotalPrice: total.toFixed(2),
                newTotalPrice: total.toFixed(2),
                Adress: newAdress,
                OrderDic: newDic
              })
              util.postMethods('GetUserCanUseCard', {
                openId: app.globalData.openid,
                totalprice: total,
                productlist: newDic
              }, function(cardData) {
                if (cardData.IsOK) {
                  that.setData({
                    CardList: JSON.parse(cardData.Data),
                    MinPrice: parseInt(cardData.msg),
                    TotalPrice: that.data.TotalPrice - parseInt(cardData.msg),
                    newTotalPrice: that.data.newTotalPrice - parseInt(cardData.msg)
                  })
                } else {
                  wx.showModal({
                    content: cardData.msg,
                    showCancel: false,
                    confirmColor: "#F3ABC6",
                    confirmText: "确定"
                  })
                }
              })
            }
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.CalculationFreight()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  ShowModeChoose: function(e) {
    this.setData({
      showMen: true,
      usertel:this.data.usertel,
      username:this.data.username
    })
  },
  DeliveryModeChoose2:function(e){
    this.setData({
      wait:true
    })
  },
  closeWait: function () {
    this.setData({
      wait: false
    });
  },
  //收货方式
  DeliveryModeChoose: function(e) {
    wx.navigateTo({
      url: '/pages/Class/addressMain',
    })
    // var newAdress = null
    // var that = this
    // if (e.currentTarget.id == 1) {
    //   that.setData({
    //     DeliveryMode: e.currentTarget.id,
    //     Adress: that.data.sotrelist[that.data.shopId],
    //   })
    //   that.CalculationFreight()
    // } else {
    //   //收货地址
    //   util.postMethods('GetAdressList', {
    //     adresstype: 2,
    //     wxopenId: app.globalData.openid,
    //     appguId: app.globalData.appId
    //   }, function(data) {
    //     if (data) {
    //       for (var m = 0; m < data.length; m++) {
    //         if (data[m].IsUse == 0) {
    //           newAdress = data[m]
    //         }
    //       }
    //       that.setData({
    //         DeliveryMode: e.currentTarget.id,
    //         Adress: newAdress
    //       })
    //       that.CalculationFreight()
    //     }
    //   })
    // }
  },
  //选择门店
  ChooseStore: function() {
    // if (this.data.sotrelist.length == 0) {
    //   var that = this;
    //   util.postMethods('GetAdressList', {
    //     adresstype: 1,
    //     wxopenId: app.globalData.openid,
    //     appguId: app.globalData.appId
    //   }, function(data) {
    //     if (data) {
    //       console.log(data)
    //       that.setData({
    //         sotrelist: data
    //       })
    //     }
    //   })
    // }
  },
  //选择收货地址
  ChooseLogistics: function() {
    wx.navigateTo({
      url: '/pages/Setting/Adress'
    })
  },
  //买家留言输入
  RemarkInput: function(e) {
    this.setData({
      Remark: e.detail.value
    })
  },
  //提交订单
  SaveOrder: function() {
    try {
      if (!app.globalData.userInfo) {
        wx.showModal({
          content: "请登录后再购买！",
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      if (this.data.Adress == null) {
        var msg = '请选择收货地址！'
        if (this.data.DeliveryMode == 1) {
          msg = '请选择取货门店！'
        }
        wx.showModal({
          content: msg,
          showCancel: false,
          confirmColor: "#F3ABC6",
          confirmText: "确定"
        })
        return
      }
      this.setData({
        buttonClicked: true
      })
      //提交信息
      this.SetOrderData()
    } catch (e) {
      console.log(e)
      this.setData({
        buttonClicked: false
      })
    }
  },
  //提交信息
  SetOrderData: function() {
    var that = this
    var newDic = this.data.OrderDic
    if (newDic) {
      util.postMethods('SaveOrderInfo', {
        appId: app.globalData.appId,
        wxopenId: app.globalData.openid,
        productlist: newDic,
        mode: this.data.DeliveryMode,
        classcount: this.data.OrderList.length,
        orderprice: this.data.newTotalPrice,
        modeguid: this.data.Adress.GUID,
        isgrouporder: this.data.IsGroupOrder ? 0 : 1,
        groupnumber: this.data.GroupNumber,
        remark: this.data.Remark,
        freight: this.data.Freight,
        cardguid: this.data.CardGUID,
        getGoodsDate: '',
        username:this.data.username,
        usertel:this.data.usertel
      }, function(data) {
        if (data.IsOK) {
          wx.getStorage({
            key: 'UserShopList',
            success: function(res) {
              if (!that.data.isSale) {
                var newlist = []
                var list = wx.getStorageSync('OrderList')
                for (var x = 0; x < res.data.length; x++) {
                  var isAdd = true
                  for (var y = 0; y < list.length; y++) {
                    if (res.data[x] == list[y]) {
                      isAdd = false
                      continue
                    }
                  }
                  if (isAdd) {
                    newlist.push(res.data[x])
                  }
                }
                wx.setStorage({
                  key: 'UserShopList',
                  data: newlist
                })
              }
            }
          })
          util.MemberCardPay(app.globalData.appId, app.globalData.openid, data.Data)
          setTimeout(function() {
            that.setData({
              buttonClicked: false
            })
          }, 5000)
        } else {
          wx.showModal({
            content: data.msg,
            showCancel: false,
            confirmColor: "#F3ABC6",
            confirmText: "确定",
            success: function() {
              that.setData({
                buttonClicked: false
              })
            }
          })
        }
      })
    }
  }
})