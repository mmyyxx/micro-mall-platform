// pages/Shop/Shop.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAllcheck: false,
    PageHeight: app.globalData.systemInfo.windowHeight - 101,
    IsMember: app.globalData.IsMember,
    ShopList: null,
    ChooseCount: 0,
    ChoosePrice: 0,
    ChooseItems: []
  },
  //购物车显示
  ShopListShow: function() {
    var that = this
    wx.getStorage({
      key: 'UserShopList',
      success: function (res) {
      },
      complete: function (res) {
        if (res.errMsg == 'getStorage:ok') {
          util.postMethods('GetShopItemList', {
            shoplist: res.data
          }, function (data) {
            if (data) {
              that.setData({
                ShopList: data,
                IsMember: app.globalData.IsMember,
                isAllcheck: false
              })
              that.GetStatisticsData(true)
              //停止下拉刷新
              wx.stopPullDownRefresh()
              if(data.length==0){
                wx.setStorage({
                  key: "UserShopList",
                  data: []
                })
              }
            }
          })
        }
        else {
          that.setData({
            ShopList: [],
            IsMember: app.globalData.IsMember,
            isAllcheck: false
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.ShopListShow()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.ShopListShow()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //全选按钮
  AllChoose: function() {
    var newShopList = this.data.ShopList
    if (newShopList == null || newShopList.length == 0) {
      return
    }
    for (var m = 0; m < newShopList.length; m++) {
      newShopList[m].isCheck = !this.data.isAllcheck
    }
    this.setData({
      isAllcheck: !this.data.isAllcheck,
      ShopList: newShopList
    })
    this.GetStatisticsData(false)
  },
  //选择
  ItemClick: function(event) {
    var newShopList = this.data.ShopList
    newShopList[event.currentTarget.id].isCheck = !newShopList[event.currentTarget.id].isCheck
    var isChecked = this.IsAllItemCheck(newShopList)
    this.setData({
      ShopList: newShopList,
      isAllcheck: isChecked,
    })
    this.GetStatisticsData(false)
  },
  //减
  ReduceClick: function(event) {
    var newShopList = this.data.ShopList
    if (newShopList[event.currentTarget.id].ProductCount > newShopList[event.currentTarget.id].MinCount) {
      newShopList[event.currentTarget.id].ProductCount--
      this.setData({
        ShopList: newShopList
      })
      this.GetStatisticsData(false)
    }
  },
  //加
  AddClick: function(event) {
    var newShopList = this.data.ShopList
    if (newShopList[event.currentTarget.id].ProductCount < newShopList[event.currentTarget.id].ProductStock) {
      if (newShopList[event.currentTarget.id].MaxCount < newShopList[event.currentTarget.id].ProductCount+1){
        return;
      }
      newShopList[event.currentTarget.id].ProductCount++
      this.setData({
        ShopList: newShopList
      })
      this.GetStatisticsData(false)
    }
    else{
      wx.showModal({
        content: "库存不足！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
    }
  },
  //删除
  DeleteItemClick: function(event) {
    var that = this
    wx.showModal({
      content: "是否删除选中商品？",
      confirmText: "确定",
      confirmColor: "#EA4C49",
      cancelText: "取消",
      success: function(res) {
        //删除
        if (res.confirm) {
          wx.getStorage({
            key: 'UserShopList',
            success: function(e) {
              //删除缓存code
              var oldCodeList = e.data
              var oldShopList = that.data.ShopList
              var newCodeList = []
              var newShopList = []
              for (var m = 0; m < oldCodeList.length; m++) {
                if (event.currentTarget.dataset.code != oldCodeList[m] && oldCodeList[m]) {
                  newCodeList.push(oldCodeList[m])
                }
                if (oldShopList[m] && event.currentTarget.dataset.code != oldShopList[m].ItemCode) {
                  newShopList.push(oldShopList[m])
                }
              }
              //保存更新
              wx.setStorage({
                key: "UserShopList",
                data: newCodeList
              })
              that.setData({
                ShopList: newShopList
              })
              //删除成功
              wx.showToast({
                title: '删除成功',
                image: '/resources/Icon/Toast_Success.png',
                mask: true
              })
              that.GetStatisticsData(false)
            }
          })
        }
      }
    })
  },
  IsAllItemCheck: function(newShopList) {
    var checked = true
    for (var m = 0; m < newShopList.length; m++) {
      if (!newShopList[m].isCheck) {
        checked = false
        break
      }
    }
    return checked
  },
  //选中商品后处理
  GetStatisticsData: function(isFirst) {
    var count = 0
    var total = 0
    var newChooseItems = []
    for (var m = 0; m < this.data.ShopList.length; m++) {
      if (isFirst) {
        this.data.ShopList[m].ProductCount = wx.getStorageSync(this.data.ShopList[m].ItemCode)
        if (!this.data.ShopList[m].ProductCount) {
          this.data.ShopList[m].ProductCount = 1
        }
      } else {
        wx.setStorageSync(this.data.ShopList[m].ItemCode, this.data.ShopList[m].ProductCount)
      }
      if (this.data.ShopList[m].isCheck) {
        count++
        total += (this.data.IsMember ? this.data.ShopList[m].ProductPrice : this.data.ShopList[m].OldPrice) * this.data.ShopList[m].ProductCount
        newChooseItems.push(this.data.ShopList[m].ItemCode)
      }
    }
    this.setData({
      ChooseCount: count,
      ChoosePrice: total.toFixed(2),
      ShopList: this.data.ShopList,
      ChooseItems: newChooseItems
    })
  },
  //订单结算
  NavigateToOrder: function() {
    if (!app.globalData.userInfo) {
      wx.showModal({
        content: "请登录后再购买！",
        showCancel: false,
        confirmColor: "#F3ABC6",
        confirmText: "确定"
      })
      return
    }

    if (this.data.ChooseItems.length > 0) {
      //设置订单数据
      wx.setStorage({
        key: "OrderList",
        data: this.data.ChooseItems
      })
      wx.navigateTo({
        url: '/pages/Shop/Order'
      })
    } else {
      wx.showModal({
        content: "请至少选择一件商品！",
        confirmText: "确定",
        confirmColor: "#F3ABC6",
        showCancel: false
      })
    }
  }
})