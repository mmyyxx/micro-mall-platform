//app.js
const util = require('/utils/util.js')

App({
  onLaunch: function() {
    //获取设备信息
    this.globalData.systemInfo = wx.getSystemInfoSync();
    var that = this;
    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        util.postMethods('UserLogin', {
          code: res.code,
          appId: that.globalData.appId
        }, function(data) {
          console.log(data)
          that.globalData.openid = data.Data;
          that.globalData.IsMember = data.IsOK;
          var mm = data.msg.split(',');
          that.globalData.iseventdraw = parseInt(mm[0]);
          that.globalData.score = parseInt(mm[1]);
          // 获取用户信息
          wx.getSetting({
            success: res => {
              if (res.authSetting['scope.userInfo']) {
                // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                wx.getUserInfo({
                  success: res => {
                    // 可以将 res 发送给后台解码出 unionId
                    that.globalData.userInfo = res.userInfo
                    // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
                    // 所以此处加入 callback 以防止这种情况
                    if (that.userInfoReadyCallback) {
                      that.userInfoReadyCallback(res)
                    }
                    // util.postMethods('GetUserInfo', {
                    //   openid: that.globalData.openid,
                    //   successData: res
                    // }, function(result) {
                    //   that.globalData.IsMember = result.IsOK
                    // })
                  }
                })
              }
              else {//未授权，先授权
                wx.reLaunch({
                  url: '/pages/Class/prosPectus',
                })
              }
            }
          })
        })
      }
    })
  },
  globalData: {
    userInfo: null,
    openid: null,
    systemInfo: null,
    appId: 'B43E1A5E8A5D4386AE5B6555135BE513',
    IsMember: false,
    currentclass:'',
    oldmemberprice:0,
    newmemberprice:0,
    iseventdraw:0,
    score:0
  }
})