/**
 * 小程序配置文件
 */

// 此处主机域名是腾讯云解决方案分配的域名
// 小程序后台服务解决方案：https://www.qcloud.com/solution/la

var config = {
  // 请求地址
  requestUrl: `http://localhost:4066/wsc/Hmcys/`
};

module.exports = config