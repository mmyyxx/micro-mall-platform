const requestUrl = require('../config.js').requestUrl

const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const cnumber = n=>{
  var array = ['0','一','二','三','四','五','六','七','八','九','十'];
  return array[n];
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function postMethods(menthod, newdata, callback, errorback) {
  wx.showLoading({
    title: '加载中',
  })
  wx.request({
    url: requestUrl + menthod,
    data: newdata,
    success: function(result) {
      if (result.data.length > 0) {
        try {
          callback(JSON.parse(result.data))
        } catch (e) {
          callback(result.data)
        }
      } else {
        callback(result.data)
      }
      wx.hideLoading();
    },
    fail: function(result) {
      if (errorback) {
        errorback(result)
      }
      wx.hideLoading();
    }
  })
}

function getMethods(menthod, newdata, callback, errorback) {
  wx.request({
    url: menthod,
    data: newdata,
    success: function (result) {
      if (result.data.length > 0) {
        try {
          callback(result.data)
        } catch (e) {
          callback(result.data)
        }
      } else {
        callback(result.data)
      }
    },
    fail: function (result) {
      if (errorback) {
        errorback(result)
      }
    }
  })
}

function MemberCardPay(appId, openid, orderguid) {
  postMethods('GetMemberMoney', {
    openId: openid
  }, function(res) {
    if (res.IsOK) {
      var usermoney = parseFloat(res.Data)
      if (usermoney > 0) {
        wx.showModal({
          content: '是否使用会员卡余额付款？',
          confirmColor: "#F3ABC6",
          confirmText: "确定",
          complete(e) {
            if (e.confirm) {
              WxPay(appId, openid, orderguid, 0)
            } else {
              WxPay(appId, openid, orderguid, 1)
            }
          }
        })
      } else {
        WxPay(appId, openid, orderguid, 1)
      }
    } else {
      WxPay(appId, openid, orderguid, 1)
    }
  })
}

function WxPay(appId, openid, orderguid, isUse) {
  try {
    postMethods('GetWXPayOrder', {
      appId: appId,
      openId: openid,
      orderguid: orderguid,
      isUseMemberCard: isUse
    }, function(e) {
      if (e.IsOK) {
        wx.redirectTo({
          url: '/pages/Setting/OrderDetail?code=' + orderguid
        })
      } else {
        var data = JSON.parse(e)
        wx.requestPayment({
          timeStamp: data.timeStamp,
          nonceStr: data.nonceStr,
          package: data.package,
          signType: data.signType,
          paySign: data.paySign,
          success: function(res) {
            wx.redirectTo({
              url: '/pages/Setting/OrderDetail?code=' + orderguid
            })
          },
          fail: function(res) {
            wx.showModal({
              content: "支付未成功，请在半小时内完成支付！",
              showCancel: false,
              confirmColor: "#F3ABC6",
              confirmText: "确定",
              success: function() {
                wx.redirectTo({
                  url: '/pages/Setting/OrderDetail?code=' + orderguid
                })
              }
            })
          }
        })
      }
    })
  } catch (e) {
    wx.showModal({
      content: "订单支付失败！",
      showCancel: false,
      confirmColor: "#F3ABC6",
      confirmText: "确定",
      success: function() {
        wx.redirectTo({
          url: '/pages/Setting/OrderDetail?code=' + orderguid
        })
      }
    })
  }
}

function WxPayByOrder(appId, openid, orderguid, callback) {
  postMethods('PayWXPayOrder', {
    appId: appId,
    openId: openid,
    orderguid: orderguid
  }, function(e) {
    var data = JSON.parse(e)
    wx.requestPayment({
      'timeStamp': data.timeStamp,
      'nonceStr': data.nonceStr,
      'package': data.package,
      'signType': data.signType,
      'paySign': data.paySign,
      'success': function(res) {
        callback(res)
      }
    })
  })
}

module.exports = {
  formatTime: formatTime,
  postMethods: postMethods,
  MemberCardPay: MemberCardPay,
  WxPay: WxPay,
  WxPayByOrder: WxPayByOrder,
  cnumber: cnumber,
  getMethods: getMethods
}