﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using MMM.Application.Entity.BaseManage;

namespace WxPayAPI
{
    public class WxPayApi
    {
        public static WxPayData OrderQuery(WxPayData inputObj, TWXAPPEntity wxapp, int timeOut = 6)
        {
            string url = "https://api.mch.weixin.qq.com/pay/orderquery";
            inputObj.SetValue("mch_id", wxapp.MCHID);
            inputObj.SetValue("nonce_str", WxPayApi.GenerateNonceStr());
            inputObj.SetValue("sign", inputObj.MakeSign());
            string xml = inputObj.ToXml();
            var start = DateTime.Now;
            string response = HttpService.Post(wxapp, xml, url, false, timeOut);
            var end = DateTime.Now;
            int timeCost = (int)((end - start).TotalMilliseconds);
            WxPayData result = new WxPayData();
            result.FromXml(response);
            return result;
        }

        public static WxPayData Refund(WxPayData inputObj, TWXAPPEntity wxapp, int timeOut = 6)
        {
            string url = "https://api.mch.weixin.qq.com/secapi/pay/refund";
            inputObj.SetValue("appid", wxapp.AppId);
            inputObj.SetValue("mch_id", wxapp.MCHID);
            inputObj.SetValue("nonce_str", Guid.NewGuid().ToString().Replace("-", ""));
            inputObj.SetValue("sign", inputObj.MakeSign());
            string xml = inputObj.ToXml();
            var start = DateTime.Now;
            string response = HttpService.Post(wxapp, xml, url, true, timeOut);
            var end = DateTime.Now;
            int timeCost = (int)((end - start).TotalMilliseconds);
            WxPayData result = new WxPayData();
            result.FromXml(response);
            return result;
        }
        public static WxPayData UnifiedOrder(WxPayData inputObj, TWXAPPEntity wxapp, int timeOut = 6)
        {
            string url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
            if (!inputObj.IsSet("notify_url"))
            {
                inputObj.SetValue("notify_url", WxPayConfig.NOTIFY_URL);
            }
            inputObj.SetValue("mch_id", wxapp.MCHID);
            inputObj.SetValue("spbill_create_ip", WxPayConfig.IP);
            inputObj.SetValue("nonce_str", GenerateNonceStr());
            inputObj.SetValue("sign", inputObj.MakeSign());
            string xml = inputObj.ToXml();
            var start = DateTime.Now;
            string response = HttpService.Post(wxapp, xml, url, false, timeOut);
            var end = DateTime.Now;
            int timeCost = (int)((end - start).TotalMilliseconds);
            WxPayData result = new WxPayData();
            result.FromXml(response);
            return result;
        }
        public static string GenerateOutTradeNo(string mchid)
        {
            var ran = new Random();
            return string.Format("{0}{1}{2}", mchid, DateTime.Now.ToString("yyyyMMddHHmmss"), ran.Next(999));
        }

        public static string GenerateTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        public static string GenerateNonceStr()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }
    }
}