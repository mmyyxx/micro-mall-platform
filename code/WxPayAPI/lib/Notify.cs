﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace WxPayAPI
{
    public class Notify
    {
        public Page page { get; set; }
        public Notify(Page page)
        {
            this.page = page;
        }
        public WxPayData GetNotifyData()
        {
            System.IO.Stream s = page.Request.InputStream;
            int count = 0;
            byte[] buffer = new byte[1024];
            StringBuilder builder = new StringBuilder();
            while ((count = s.Read(buffer, 0, 1024)) > 0)
            {
                builder.Append(Encoding.UTF8.GetString(buffer, 0, count));
            }
            s.Flush();
            s.Close();
            s.Dispose();
            WxPayData data = new WxPayData();
            try
            {
                Log.WriteLog(builder.ToString());
                data.FromXml(builder.ToString());
            }
            catch (WxPayException ex)
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", ex.Message);
                page.Response.Write(res.ToXml());
                page.Response.End();
            }
            return data;
        }
        public virtual void ProcessNotify()
        {

        }
    }
}
