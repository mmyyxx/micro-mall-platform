﻿using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.SystemManage;
using MMM.Application.Entity.UserInfo;
using MMM.Data;
using MMM.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace WxPayAPI
{
    public class ResultNotify : Notify
    {
        public ResultNotify(Page page) : base(page)
        {
        }

        public override void ProcessNotify()
        {
            WxPayData notifyData = GetNotifyData();
            if (!notifyData.GetValue("result_code").ToString().Equals("SUCCESS"))
            {
                WxPayData res = new WxPayData();
                res.SetValue("return_code", "FAIL");
                res.SetValue("return_msg", "交易失败");
                page.Response.Write(res.ToXml());
                page.Response.End();
                return;
            }
            string ordernumber = notifyData.GetValue("out_trade_no").ToString();

            IDatabase db = DbFactory.Base();
            if (ordernumber.StartsWith("HY"))//成为会员的时候
            {
                WxPayData res = new WxPayData();
                Log.WriteLog(notifyData.ToPrintStr());

                DataItemDetailEntity dataItemDetail = db.IQueryable<DataItemDetailEntity>().FirstOrDefault(t => t.ItemName == "NewPrice");
                int total_fee = Int32.Parse(notifyData.GetValue("total_fee").ToString());
                //查询价格是否一致
                int money = Int32.Parse((Double.Parse(dataItemDetail.ItemValue) * 100).ToString());
                if (total_fee != money)
                {
                    res.SetValue("return_code", "FAIL");
                    res.SetValue("return_msg", "订单金额不一致");
                    page.Response.Write(res.ToXml());
                    page.Response.End();
                    return;
                }
                string openid = notifyData.GetValue("openid").ToString();
                TMEMBEREntity member = db.IQueryable<TMEMBEREntity>().FirstOrDefault(t => t.FK_UnionId == openid);
                if (member == null)
                {
                    TUSEREntity user = db.IQueryable<TUSEREntity>().FirstOrDefault(t => t.Openid == openid);
                    member = new TMEMBEREntity();
                    member.GUID = Guid.NewGuid().ToString();
                    member.CreateTime = DateTime.Now;
                    member.Creater = openid;
                    member.IsDel = 0;
                    member.Money = 0;
                    member.FK_UnionId = openid;
                    member.UserName = String.IsNullOrEmpty(user.NickName) ? "会员" + (Guid.NewGuid().ToString("N").Substring(0, 6)) : user.NickName;
                    member.usercode = DateTime.Now.Ticks.ToString();
                    db.Insert<TMEMBEREntity>(member);
                }
                res.SetValue("return_code", "SUCCESS");
                res.SetValue("return_msg", "OK");
                page.Response.Write(res.ToXml());
                page.Response.End();
            }
            else
            {
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.OrderNumber == ordernumber && p.IsDel == 0).FirstOrDefault();
                if (order != null)
                {
                    if (order.PayResult.Equals(0))
                    {
                        WxPayData res = new WxPayData();
                        res.SetValue("return_code", "SUCCESS");
                        res.SetValue("return_msg", "OK");
                        page.Response.Write(res.ToXml());
                        page.Response.End();
                        return;
                    }
                    if (!notifyData.IsSet("transaction_id"))
                    {
                        WxPayData res = new WxPayData();
                        res.SetValue("return_code", "FAIL");
                        res.SetValue("return_msg", "支付结果中微信订单号不存在");
                        page.Response.Write(res.ToXml());
                        page.Response.End();
                        return;
                    }
                    string transaction_id = notifyData.GetValue("transaction_id").ToString();
                    int total_fee = Int32.Parse(notifyData.GetValue("total_fee").ToString());
                    if (!QueryOrder(transaction_id, notifyData.GetValue("appid").ToString()))
                    {
                        WxPayData res = new WxPayData();
                        res.SetValue("return_code", "FAIL");
                        res.SetValue("return_msg", "订单查询失败");
                        page.Response.Write(res.ToXml());
                        page.Response.End();
                        return;
                    }
                    else
                    {
                        if (IsOrderPrice(ordernumber, total_fee, order))
                        {
                            UpdateOrderStatus(ordernumber, order);
                            WxPayData res = new WxPayData();
                            res.SetValue("return_code", "SUCCESS");
                            res.SetValue("return_msg", "OK");
                            page.Response.Write(res.ToXml());
                            page.Response.End();
                        }
                        else
                        {
                            WxPayData res = new WxPayData();
                            res.SetValue("return_code", "FAIL");
                            res.SetValue("return_msg", "订单金额不一致");
                            page.Response.Write(res.ToXml());
                            page.Response.End();
                            return;
                        }
                    }
                }
                else
                {
                    WxPayData res = new WxPayData();
                    res.SetValue("return_code", "FAIL");
                    res.SetValue("return_msg", "订单不存在");
                    page.Response.Write(res.ToXml());
                    page.Response.End();
                }
            }
        }

        private bool QueryOrder(string transaction_id, string appid)
        {
            WxPayData req = new WxPayData();
            req.SetValue("transaction_id", transaction_id);
            req.SetValue("appid", appid);

            IDatabase db = DbFactory.Base();

            TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.AppId == appid).SingleOrDefault();

            WxPayData res = WxPayApi.OrderQuery(req, wxapp);
            if (res.GetValue("return_code").ToString() == "SUCCESS" &&
                res.GetValue("result_code").ToString() == "SUCCESS")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsOrderPay(string orderguid)
        {
            bool result = false;
            IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == orderguid && p.IsDel == 0).SingleOrDefault();
                if (order != null)
                {
                    if (order.PayResult.Equals(0))
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteLog(ex.ToString());
            }
            return result;
        }

        private bool IsOrderPrice(string orderguid, int total_fee, TORDEREntity order)
        {
            bool result = false;
            try
            {
                if (order != null)
                {
                    decimal price = order.OrderPrice.Value * 100 - total_fee;
                    if (price == Decimal.Zero)
                    {
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.WriteLog(ex.ToString());
            }
            return result;
        }

        private void UpdateOrderStatus(string orderguid, TORDEREntity order)
        {
            IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();

                if (order != null)
                {
                    order.OrderStatus = 2;
                    order.PayResult = 0;
                    db.Update<TORDEREntity>(order, null);
                    if (order.IsGroupOrder == 0)
                    {
                        List<TORDEREntity> orderlist = db.IQueryable<TORDEREntity>().Where(p => p.IsGroupOrder == 0 && p.OrderStatus == 2 && p.PayResult == 0 && p.GroupNumber == order.GroupNumber).ToList();
                        int? people = db.IQueryable<TORDERDETAILEntity>().Where(p => p.FK_Order == orderguid)
                            .Join(db.IQueryable<T_PRODUCTCLASSEntity>(), m => m.FK_ProductClass, n => n.GUID, (m, n) => n)
                            .Join(db.IQueryable<T_PRODUCTPRICEEntity>(), m => m.FK_Product, n => n.GUID, (m, n) => n).Select(p => p.GroupPeople).SingleOrDefault();
                        if (people != null && people.Value <= orderlist.Count + 1)
                        {
                            order.GroupResult = 0;
                            order.RegimentTime = DateTime.Now;
                            foreach (TORDEREntity torder in orderlist)
                            {
                                torder.GroupResult = 0;
                                torder.RegimentTime = order.RegimentTime;
                                db.Update<TORDEREntity>(torder, null);
                            }
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                Log.WriteLog(ex.ToString());
            }
        }
    }
}