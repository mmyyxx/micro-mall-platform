﻿using MMM.Application.Entity.BaseManage;
using System;
using System.Collections.Generic;
using System.Web;

namespace WxPayAPI
{
    public class Refund
    {
        public static string Run(TWXAPPEntity wxapp, string out_trade_no, string total_fee, string refund_fee,string refund_desc)
        {
            WxPayData data = new WxPayData();
            data.SetValue("out_trade_no", out_trade_no);
            data.SetValue("total_fee", Convert.ToInt32(total_fee));
            data.SetValue("refund_fee", Convert.ToInt32(refund_fee));
            data.SetValue("out_refund_no", WxPayApi.GenerateOutTradeNo(wxapp.MCHID));
            data.SetValue("op_user_id", wxapp.MCHID);
            data.SetValue("refund_desc", refund_desc);
            WxPayData result = WxPayApi.Refund(data, wxapp);
            return result.ToString();
        }
    }
}