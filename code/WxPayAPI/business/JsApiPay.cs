﻿using MMM.Application.Entity.BaseManage;
using MMM.Data;
using MMM.Data.Repository;
using MMM.Util;
using System;
using System.Linq;

namespace WxPayAPI
{
    public class JsApiPay
    {
        public WxPayData unifiedOrderResult { get; set; }

        public JsApiPay()
        {

        }

        public WxPayData GetUnifiedOrderResult(WxPayData data, TWXAPPEntity wxapp)
        {
            WxPayData result = WxPayApi.UnifiedOrder(data, wxapp);
            if (!result.IsSet("appid") || !result.IsSet("prepay_id") || result.GetValue("prepay_id").ToString() == "")
            {
                throw new WxPayException("UnifiedOrder response error!");
            }

            unifiedOrderResult = result;
            return result;
        }

        public string GetJsApiParameters(string orderguid, string openid, TWXAPPEntity wxapp)
        {
            WxPayData jsApiParam = new WxPayData();
            IDatabase db = DbFactory.Base();
            try
            {
                TWXPAYDATAEntity wxpaydata = new TWXPAYDATAEntity();
                wxpaydata.GUID = orderguid;
                wxpaydata.FK_Openid = openid;
                wxpaydata.appId = unifiedOrderResult.GetValue("appid").ToString();
                wxpaydata.package = unifiedOrderResult.GetValue("prepay_id").ToString();
                wxpaydata.CreateTime = DateTime.Now;
                db.Insert<TWXPAYDATAEntity>(wxpaydata);
            }
            catch (Exception ex)
            {
                Log.WriteLog(ex.ToString());
            }

            jsApiParam.SetValue("appId", unifiedOrderResult.GetValue("appid"));
            jsApiParam.SetValue("timeStamp", WxPayApi.GenerateTimeStamp());
            jsApiParam.SetValue("nonceStr", WxPayApi.GenerateNonceStr());
            jsApiParam.SetValue("package", "prepay_id=" + unifiedOrderResult.GetValue("prepay_id"));
            jsApiParam.SetValue("signType", "MD5");
            jsApiParam.SetValue("paySign", jsApiParam.MakeSign());

            string parameters = jsApiParam.ToJson();

            return parameters;
        }

        public string GetJsApiParametersByOrderGUID(string appId, string openId, string orderguid)
        {
            IDatabase db = DbFactory.Base();
            try
            {
                TWXPAYDATAEntity wxpaydata = db.IQueryable<TWXPAYDATAEntity>().Where(p => p.GUID == orderguid && p.FK_Openid == openId && p.appId == appId).SingleOrDefault();
                if (wxpaydata != null)
                {
                    WxPayData jsApiParam = new WxPayData();
                    jsApiParam.SetValue("appId", wxpaydata.appId);
                    jsApiParam.SetValue("timeStamp", WxPayApi.GenerateTimeStamp());
                    jsApiParam.SetValue("nonceStr", WxPayApi.GenerateNonceStr());
                    jsApiParam.SetValue("package", "prepay_id=" + wxpaydata.package);
                    jsApiParam.SetValue("signType", "MD5");
                    jsApiParam.SetValue("paySign", jsApiParam.MakeSign());
                    string parameters = jsApiParam.ToJson();

                    return parameters;
                }
                return null;
            }
            catch (Exception ex)
            {
                Log.WriteLog(ex.ToString());
                return null;
            }
        }

        public string GetJsApiParametersMember(string openid, TWXAPPEntity wxapp)
        {
            WxPayData jsApiParam = new WxPayData();
            IDatabase db = DbFactory.Base();

            jsApiParam.SetValue("appId", unifiedOrderResult.GetValue("appid"));
            jsApiParam.SetValue("timeStamp", WxPayApi.GenerateTimeStamp());
            jsApiParam.SetValue("nonceStr", WxPayApi.GenerateNonceStr());
            jsApiParam.SetValue("package", "prepay_id=" + unifiedOrderResult.GetValue("prepay_id"));
            jsApiParam.SetValue("signType", "MD5");
            jsApiParam.SetValue("paySign", jsApiParam.MakeSign());

            string parameters = jsApiParam.ToJson();

            return parameters;
        }
    }
}