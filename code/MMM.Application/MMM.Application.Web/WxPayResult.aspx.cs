﻿using MMM.Application.Service.WSC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MMM.Application.Web
{
    public partial class WxPayResult : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            WXPayDAL dal = new WXPayDAL();
            dal.ResultNotify(this);
        }
    }
}