﻿using MMM.Application.Busines.BaseManage;
using MMM.Application.Code;
using MMM.Application.Entity.BaseManage;
using MMM.Cache.Factory;
using MMM.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MMM.Application.Cache
{
    /// <summary>
    /// 描 述：组织架构缓存
    /// </summary>
    public class OrganizeCache
    {
        private OrganizeBLL busines = new OrganizeBLL();

        /// <summary>
        /// 组织列表
        /// </summary>
        /// <returns></returns>
        public IEnumerable<OrganizeEntity> GetList()
        {
            var cacheList = CacheFactory.Cache().GetCache<IEnumerable<OrganizeEntity>>(busines.cacheKey);
            if (cacheList == null)
            {
                var data = busines.GetList();
                CacheFactory.Cache().WriteCache(data, busines.cacheKey);
                return data;
            }
            else
            {
                return cacheList;
            }
        }

        /// <summary>
        /// 获取下级组织列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="me">判断是否根据自己,id不是空该参数无效</param>
        /// <returns></returns>
        public IList<OrganizeEntity> GetList(String id)
        {
            IList<OrganizeEntity> cacheList = null;
            String strOrg = WebHelper.GetSession(Cache.Const.OrganizeLevel);
            if (String.IsNullOrEmpty(strOrg))
            {
                if (String.IsNullOrEmpty(id))
                {
                    return null;
                }
                else
                {
                    IList<OrganizeEntity> temList = GetList().ToList();
                    cacheList = FindAllChild(temList, id);
                    cacheList.Add(temList.FirstOrDefault(x => x.OrganizeId == id));
                    return cacheList;
                }
            }
            IList<OrganizeEntity> cacheListTem = DESEncrypt.Decrypt(strOrg).ToObject<IList<OrganizeEntity>>();
            //如果id是空就返回所有
            if (!String.IsNullOrEmpty(id))
            {
                cacheList = FindAllChild(cacheListTem, id);
                cacheList.Add(cacheListTem.FirstOrDefault(x => x.OrganizeId == id));
            }
            else
                cacheList = cacheListTem;
            return cacheList;
        }

        /// <summary>
        /// 获取组织信息，如果是管理员返回空
        /// </summary>
        /// <returns></returns>
        public String GetCurrentOrgId()
        {
            if (OperatorProvider.Provider.Current().IsSystem) { return null; }
            else
                return OperatorProvider.Provider.Current().CompanyId;
        }

        /// <summary>
        /// 获取下级组织列表
        /// </summary>
        /// <param name="id"></param>
        /// <param name="me">判断是否根据自己,id不是空该参数无效</param>
        /// <returns></returns>
        public OrganizeEntity GetNextList()
        {
            IList<OrganizeEntity> cacheListTem = GetList().ToList();
            String strCompanyId = OperatorProvider.Provider.Current().CompanyId;
            return cacheListTem.FirstOrDefault(x => x.OrganizeId == strCompanyId);
        }
        //public IList<OrganizeEntity> GetNextList()
        //{
        //    IList<OrganizeEntity> cacheListTem = GetList().ToList();
        //    IList<OrganizeEntity> cacheList = null;
        //    String strCompanyId = OperatorProvider.Provider.Current().CompanyId;
        //    if (!OperatorProvider.Provider.Current().IsSystem)
        //    {
        //        //cacheList = FindAllChild(cacheListTem, strCompanyId);
        //        cacheList.Add(cacheListTem.FirstOrDefault(x => x.OrganizeId == strCompanyId));
        //    }
        //    else
        //        cacheList = null;
        //    return cacheList;
        //}

        /// <summary>
        /// 查找所有下级公司
        /// </summary>
        /// <param name="list"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        private IList<OrganizeEntity> FindAllChild(IList<OrganizeEntity> list, String pid)
        {
            IList<OrganizeEntity> temList = list.Where(x => x.ParentId == pid).ToList();
            foreach (OrganizeEntity entity in temList)
            {
                temList = temList.Concat(FindAllChild(list, entity.OrganizeId)).ToList();
            }
            return temList;
        }
        /// <summary>
        /// 组织列表
        /// </summary>
        /// <param name="organizeId">公司Id</param>
        /// <returns></returns>
        public OrganizeEntity GetEntity(string organizeId)
        {
            var data = this.GetList();
            if (!string.IsNullOrEmpty(organizeId))
            {
                var d = data.Where(t => t.OrganizeId == organizeId).ToList<OrganizeEntity>();
                if (d.Count > 0)
                {
                    return d[0];
                }
            }
            return new OrganizeEntity();
        }
    }
}
