﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Cache
{
    public class Const
    {
        public static String OrganizeLevel = "OrganizeLevel";//当前用户下级公司的主键

        public static String QrCodeLogoImg = "/Content/images/yllog.jpg";//二维码logo文件

        #region 错误提示
        public static String errWareRepert = "当前编号已经存在!";

        public static String errAreaRepert = "库区编号不能超过两位!";
        public static String errAreaRepert2 = "当前编号已经存在!";

        public static String errLocation = "排、列、层不能为0!";
        #endregion
    }
}
