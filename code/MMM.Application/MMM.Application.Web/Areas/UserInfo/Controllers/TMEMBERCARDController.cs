using MMM.Application.Cache;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Service.UserInfo;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Mvc;
using System.Linq;
using MMM.Application.Entity.SystemManage.ViewModel;
using System.Collections.Generic;
using System.Drawing;

namespace MMM.Application.Web.Areas.UserInfo.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-11 09:07
    /// 描 述：会员卡券
    /// </summary>
    public class TMEMBERCARDController : MvcControllerBase
    {
        private TMEMBERCARDService tmembercardservice = new TMEMBERCARDService();
        private DataItemCache dataItemCache = new DataItemCache();
        private OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        #endregion

        #region 获取数据
        /// <summary>
        /// 导出未使用的卡券
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Export()
        {
            DataItemModel item = dataItemCache.GetDataItemList("IsOr").FirstOrDefault(x => x.ItemName == "不是");
            JObject search = new JObject();
            search["IsUse"] = item.ItemValue;
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            IList<TMEMBERCARDEntity> membercardList = tmembercardservice.GetList(search);

            String logoImgPath = Cache.Const.QrCodeLogoImg;
            String temImgPath = "/TempPath/MemberCard/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "/";
            String temImgPathZip = "/TempPathZip/";

            String logoImgPath2 = Server.MapPath(logoImgPath);
            String temImgPath2 = Server.MapPath(temImgPath);
            String temImgPathZip2 = Server.MapPath(temImgPathZip);

            if (!System.IO.Directory.Exists(temImgPath2))
                System.IO.Directory.CreateDirectory(temImgPath2);

            //生成二维码
            Image logoImage = Image.FromFile(logoImgPath2);
            foreach (TMEMBERCARDEntity entity in membercardList)
            {
                //卡券码
                string guidstr = "CXK-" + entity.GUID;
                //文件名称
                string moneystr = (entity.MemberMoney ?? 0).ToString();
                String path = String.Format("{0}{1}/", temImgPath2, moneystr);
                //判断文件夹是否存在
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                String filePath = String.Format("{0}{1}.png", path, guidstr);
                if (!System.IO.File.Exists(filePath))
                    QRCode.CreateQRCode(guidstr, "Byte", 9, 0, "H", filePath, true, logoImage);
            }

            logoImage.Dispose();

            SharpCode.ZipDir(temImgPath2, String.Format("{0}/membercard.zip", temImgPathZip2));
            //删除数据
            System.IO.Directory.Delete(temImgPath2, true);

            return File(String.Format("{0}/membercard.zip", temImgPathZip), "application/octet-stream", "会员卡券.zip");
        }
        #endregion

        #region 提交数据

        #endregion
    }
}
