using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Service.UserInfo;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.UserInfo.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 16:29
    /// 描 述：会员
    /// </summary>
    public class TMEMBERController : MvcControllerBase
    {
        private TMEMBERService tmemberservice = new TMEMBERService();
        private TMONEYDETAILService tmoneydetailservice = new TMONEYDETAILService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 会员余额
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult MoneyForm(String keyValue)
        {
            TMEMBEREntity entity = tmemberservice.GetEntity(keyValue);
            return View(entity);
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            var data = tmemberservice.GetPageList(pagination, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tmemberservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TMEMBEREntity entity = new TMEMBEREntity();
                tmemberservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TMEMBEREntity entity)
        {
            try
            {
                TMEMBEREntity tem = tmemberservice.GetEntitByUnionId(entity.FK_UnionId);
                if (tem != null)
                {
                    return Error("该客户已经是会员!");
                }
                tmemberservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 保存金额变动
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="newMoney"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveMoney(String keyValue, Decimal? newMoney, String remark)
        {
            try
            {
                if (!newMoney.HasValue || newMoney == 0) return Error("请输入变动价格!");
                String str = tmemberservice.SaveMoney(keyValue, newMoney, remark);
                if (!String.IsNullOrEmpty(str)) return Error(str);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
