﻿using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.SystemManage.ViewModel;
using MMM.Application.Service.Order;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Order.Controllers
{
    /// <summary>
    /// 小程序订单详情
    /// </summary>
    public class VWXORDERDETAILController : MvcControllerBase
    {
        private VWXORDERDETAILService vwxorderdetailservice = new VWXORDERDETAILService();
        private OrganizeCache organize = new OrganizeCache();

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        
        /// <summary>
        /// 明细详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult DetailForm()
        {
            return View();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            IList<VWXORDERDETAILEntity> orderlist = vwxorderdetailservice.GetPageList(search);

            var jsonData = new
            {
                rows = orderlist,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 保存小程序订单明细
        /// </summary>
        /// <param name="FK_Order"></param>
        /// <param name="sku"></param>
        /// <param name="Price"></param>
        /// <param name="Count"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveDetailForm(String FK_Order,String sku,decimal Price,decimal Count)
        {
            if (Price < 0) return Error("价格不能是负数!");
            if (Count < 0) return Error("数量不能是负数!");
            try
            {
                String ret = vwxorderdetailservice.SaveDetailForm(FK_Order,sku, Price,Count);
                if (!String.IsNullOrEmpty(ret)) return Error(ret);
                return Success("保存成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("保存失败。");
            }
        }

        /// <summary>
        /// 删除小程序订单明细
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveDetailForm(string keyValue)
        {
            try
            {
                vwxorderdetailservice.RemoveForm(keyValue);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
    }
}
