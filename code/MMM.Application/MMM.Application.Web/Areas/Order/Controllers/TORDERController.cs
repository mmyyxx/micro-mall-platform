using MMM.Application.Cache;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.SystemManage.ViewModel;
using MMM.Application.Service.Order;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using MMM.Util.Extension;
using MMM.Application.Code;
using MMM.Application.Service.Product;
using MMM.Application.Entity.Product;
using MMM.Application.Service;
using System.Data;
using MMM.Util.Offices;

namespace MMM.Application.Web.Areas.Order.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-14 19:23
    /// 描 述：订单
    /// </summary>
    public class TORDERController : MvcControllerBase
    {
        private TORDERService torderservice = new TORDERService();
        private DataItemCache dataItemCache = new DataItemCache();
        private OrganizeCache organize = new OrganizeCache();
        private VORDERDETAILService vorderdetailService = new VORDERDETAILService();
        private T_PRODUCTCLASSService t_productclassservice = new T_PRODUCTCLASSService();
        private TORDERDETAILService torderdetailservice = new TORDERDETAILService();
        private VORDEREXPORTService vorderexportservice = new VORDEREXPORTService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 门店列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult ShopIndex()
        {
            return View();
        }

        /// <summary>
        /// 订单详情
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderDetailIndex()
        {
            return View();
        }

        /// <summary>
        /// 退款
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RestPay()
        {
            return View();
        }

        /// <summary>
        /// 订单发货
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderExpress()
        {
            return View();
        }

        /// <summary>
        /// 到店取货
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderPic()
        {
            return View();
        }

        /// <summary>
        /// 订单备注
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Remark()
        {
            return View();
        }

        /// <summary>
        /// 手动发货
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderSend()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            search["OrderType"] = "1";
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            var orderlist = torderservice.GetPageList(pagination, search);

            //付款状态
            IList<DataItemModel> paystatusList = dataItemCache.GetDataItemList("paystatus").ToList();
            IList<DataItemModel> DeliveryModeList = dataItemCache.GetDataItemList("DeliveryMode").ToList();
            IList<DataItemModel> payresultList = dataItemCache.GetDataItemList("payresult").ToList();
            IList<DataItemModel> paytypeList = dataItemCache.GetDataItemList("paytype").ToList();
            foreach (TORDEREntity m in orderlist)
            {
                DataItemModel tem = DeliveryModeList.FirstOrDefault(x => x.ItemValue == m.DeliveryMode.ToString());
                if (tem != null) m.DeliveryModeStr = tem.ItemName;

                tem = payresultList.FirstOrDefault(x => x.ItemValue == m.PayResult.ToString());
                if (tem != null) m.PayResultStr = tem.ItemName;

                tem = paystatusList.FirstOrDefault(x => x.ItemValue == m.OrderStatus.ToString());
                if (tem != null) m.OrderStatusStr = tem.ItemName;

                tem = paytypeList.FirstOrDefault(x => x.ItemValue == m.PayWay.ToString());
                if (tem != null) m.PayWayStr = tem.ItemName;

                m.IsDelStr = m.IsDel.Equals(0) ? "未删除" : "已删除";
            }
            var jsonData = new
            {
                rows = orderlist,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 获取门店列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJsonShop(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            search["OrderType"] = "0";
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            var orderlist = torderservice.GetPageList(pagination, search);

            //付款状态
            IList<DataItemModel> paystatusList = dataItemCache.GetDataItemList("paystatus").ToList();
            IList<DataItemModel> DeliveryModeList = dataItemCache.GetDataItemList("DeliveryMode").ToList();
            IList<DataItemModel> payresultList = dataItemCache.GetDataItemList("payresult").ToList();
            IList<DataItemModel> paytypeList = dataItemCache.GetDataItemList("paytype").ToList();
            foreach (TORDEREntity m in orderlist)
            {
                DataItemModel tem = DeliveryModeList.FirstOrDefault(x => x.ItemValue == m.DeliveryMode.ToString());
                if (tem != null) m.DeliveryModeStr = tem.ItemName;

                tem = payresultList.FirstOrDefault(x => x.ItemValue == m.PayResult.ToString());
                if (tem != null) m.PayResultStr = tem.ItemName;

                tem = paystatusList.FirstOrDefault(x => x.ItemValue == m.OrderStatus.ToString());
                if (tem != null) m.OrderStatusStr = tem.ItemName;

                tem = paytypeList.FirstOrDefault(x => x.ItemValue == m.PayWay.ToString());
                if (tem != null) m.PayWayStr = tem.ItemName;

                m.IsDelStr = m.IsDel.Equals(0) ? "未删除" : "已删除";
            }
            var jsonData = new
            {
                rows = orderlist,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            IList<DataItemModel> payresultList = dataItemCache.GetDataItemList("payresult").ToList();

            TORDEREntity data = torderservice.GetEntity(keyValue);

            DataItemModel tem = payresultList.FirstOrDefault(x => x.ItemValue == data.PayResult.ToString());
            if (tem != null) data.PayResultStr = tem.ItemName;

            data.IsGroupOrderStr = data.IsGroupOrder == 0 ? "是" : "否";
            data.GroupRoleStr = data.IsGroupOrder == 0 ? data.GroupRole == 0 ? "发起者" : "参与者" : "无";
            data.GroupResultStr = data.GroupResult == 0 ? "成功" : "未成功";
            return ToJsonResult(data);
        }

        /// <summary>
        /// 获取订单详情
        /// </summary>
        /// <param name="queryJson"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetListJsonDetail(string queryJson)
        {
            JObject search = queryJson.ToJObject();

            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }

            var data = vorderdetailService.GetPageList(search);

            IList<T_PRODUCTCLASSEntity> classList = t_productclassservice.GetList(search["organizeid"].ToString(), "");

            string[] OrderDetailStatusList = new string[] { "正常状态", "待审核", "审核通过", "审核不通过", "已完成" };
            foreach (VORDERDETAILEntity enity in data)
            {
                enity.ProductStatusStr = OrderDetailStatusList[enity.Status];

                string classguid = enity.FK_ProductClass;
                string parameter = String.Empty;
                while (classguid != null)
                {
                    T_PRODUCTCLASSEntity productclass = classList.Where(p => p.GUID == classguid).SingleOrDefault();
                    string classstr = String.Format("{0}：{1}；", productclass.ProductClass, productclass.ClassValue);
                    parameter += classstr;
                    classguid = productclass.FK_ParentClass;
                }
                enity.ProductParameter = parameter;
            }


            return ToJsonResult(data);
        }

        /// <summary>
        /// 导出数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public void export(String storeGUID,String startdate, String enddate)
        {
            JObject j = new JObject();
            j["startdate"] = startdate;
            j["enddate"] = enddate;
            j["organizeid"] = organize.GetCurrentOrgId();
            if (String.IsNullOrEmpty(storeGUID))
                j["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            else j["storeGUID"] = storeGUID;
            IList<VORDEREXPORTEntity> orderexportList = vorderexportservice.GetPageList(j);

            DataTable dt = new DataTable();
            dt.Columns.Add("OrderGUID");
            dt.Columns.Add("OrderNumber");
            dt.Columns.Add("OrderStatus");
            dt.Columns.Add("ProductSKU");
            dt.Columns.Add("Name");
            dt.Columns.Add("Price");
            dt.Columns.Add("OldPrice");
            dt.Columns.Add("Count");
            dt.Columns.Add("ProductDetailCount");
            dt.Columns.Add("YHPrice");
            dt.Columns.Add("OrderPrice");
            dt.Columns.Add("CreateTime");
            dt.Columns.Add("ExpressNumber");
            dt.Columns.Add("DeliverTime");
            dt.Columns.Add("CompleteTime");
            foreach (VORDEREXPORTEntity detail in orderexportList)
            {
                dt.Rows.Add(new String[] { detail.OrderGUID,detail.OrderNumber,detail.OrderStatus == 1 ? "待付款" : detail.OrderStatus == 2 ? "已付款" : detail.OrderStatus == 3 ? "待收货" : "已完成",
                    detail.ProductSKU,detail.Name,(detail.Price).ToString(),(detail.OldPrice).ToString(),
                detail.Count.ToString(),detail.ProductDetailCount.ToString(),(detail.YHPrice).ToString(),detail.OrderPrice.ToString(),
                    detail.CreateTime.ToString("yyyy-MM-dd"),detail.ExpressNumber,detail.DeliverTime.HasValue?detail.DeliverTime.Value.ToString("yyyy-Mm-dd"):"",detail.CompleteTime.HasValue?detail.CompleteTime.Value.ToString("yyyy-Mm-dd"):""
                });
            }
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.TitleFont = "微软雅黑";
            excelconfig.TitlePoint = 25;
            excelconfig.FileName = String.Format("订单详情{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            excelconfig.IsAllSizeColumn = true;

            List<ColumnEntity> listColumnEntity = new List<ColumnEntity>();
            excelconfig.ColumnEntity = listColumnEntity;
            ColumnEntity columnentity = new ColumnEntity();
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderGUID", ExcelColumn = "订单支付号" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderNumber", ExcelColumn = "订单号" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderStatus", ExcelColumn = "订单状态" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductSKU", ExcelColumn = "商品条码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Name", ExcelColumn = "商品名称" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Price", ExcelColumn = "商品会员价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OldPrice", ExcelColumn = "商品原价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Count", ExcelColumn = "商品数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductDetailCount", ExcelColumn = "明细数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "YHPrice", ExcelColumn = "订单优惠" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderPrice", ExcelColumn = "订单总价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CreateTime", ExcelColumn = "订单创建时间" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ExpressNumber", ExcelColumn = "快递单号" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "DeliverTime", ExcelColumn = "订单发货时间" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CompleteTime", ExcelColumn = "订单完成时间" });
            //调用导出方法
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }

        /// <summary>
        /// 导出门店数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public void exportshoporder(String storeGUID, String startdate, String enddate)
        {
            JObject j = new JObject();
            j["startdate"] = startdate;
            j["enddate"] = enddate;
            j["organizeid"] = organize.GetCurrentOrgId();
            j["ordertype"] = "0";
            if (String.IsNullOrEmpty(storeGUID))
                j["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            else j["storeGUID"] = storeGUID;

            IList<ShopOrderExport> orderexportList = torderservice.GetExportList(j);

            DataTable dt = new DataTable();
            dt.Columns.Add("PlanStoreStr");
            dt.Columns.Add("OrderNumber");
            dt.Columns.Add("PayWay");
            dt.Columns.Add("OrderPrice");
            dt.Columns.Add("OrderRemark");
            dt.Columns.Add("Creater");
            dt.Columns.Add("CreateTime");
            dt.Columns.Add("ProductSKU");
            dt.Columns.Add("PdtName");
            dt.Columns.Add("Count");
            dt.Columns.Add("Price");
            foreach (ShopOrderExport detail in orderexportList)
            {
                String payWay = new string[] { "", "微信支付", "支付宝支付", "银行卡支付", "现金支付", "其它支付" }[detail.PayWay ?? 0];

                dt.Rows.Add(new String[] { detail.PlanStoreStr,detail.OrderNumber,
                    payWay,(detail.OrderPrice).ToString(),(detail.OrderRemark),
                detail.Creater,detail.CreateTime.Value.ToString("yyyy-MM-dd"),detail.ProductSKU,detail.PdtName,detail.Count.ToString(),
                (detail.Price??0).ToString()
                });
            }
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.TitleFont = "微软雅黑";
            excelconfig.TitlePoint = 25;
            excelconfig.FileName = String.Format("订单详情{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            excelconfig.IsAllSizeColumn = true;

            List<ColumnEntity> listColumnEntity = new List<ColumnEntity>();
            excelconfig.ColumnEntity = listColumnEntity;
            ColumnEntity columnentity = new ColumnEntity();
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "PlanStoreStr", ExcelColumn = "仓库编码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderNumber", ExcelColumn = "订单号" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "PayWay", ExcelColumn = "付款方式" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderPrice", ExcelColumn = "订单价格" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderRemark", ExcelColumn = "订单备注" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Creater", ExcelColumn = "收银人" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CreateTime", ExcelColumn = "收银时间" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductSKU", ExcelColumn = "商品条码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "PdtName", ExcelColumn = "商品名称" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Count", ExcelColumn = "商品数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Price", ExcelColumn = "商品单价" });
            //调用导出方法
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TORDEREntity entity = new TORDEREntity();
                torderservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TORDEREntity entity)
        {
            try
            {
                TORDEREntity tem = new TORDEREntity();
                tem.Remark = entity.Remark;
                if (MMM.Application.Code.OperatorProvider.Provider.Current().IsSystem)
                {
                    tem.OrderStatus = entity.OrderStatus;
                    tem.DeliveryMode = entity.DeliveryMode;
                    tem.FK_PlanStore = entity.FK_PlanStore;
                    tem.ExpressNumber = entity.ExpressNumber;
                    tem.DeliveryName = entity.DeliveryName;
                    tem.DeliveryMobile = entity.DeliveryMobile;
                    tem.DeliveryAdress = entity.DeliveryAdress;
                }
                torderservice.SaveForm(keyValue, tem);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 退款
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="numPrice"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RestPaySave(string keyValue, Decimal? numPrice)
        {
            try
            {
                String res = torderdetailservice.RestPaySave(keyValue, numPrice);
                if (!String.IsNullOrEmpty(res)) return Error(res);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 订单发货
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="txtExpressNumber"></param>
        /// <param name="storeid"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult ExpressSave(string keyValue, string txtExpressNumber, String storeid)
        {
            try
            {
                TORDEREntity order = torderservice.GetEntity(keyValue, false);
                if (order.IsDel != 0) return Error("订单已经删除!");
                if (order.DeliveryMode == 1)
                {
                    return Error("该订单为到店取货，无需发货！");
                }
                else if (order.OrderStatus != 2)
                {
                    return Error("订单状态不正确，无法发货！");
                }
                else if (order.IsGroupOrder == 0 && order.GroupResult == 1)
                {
                    return Error("团购订单未成团，无法发货！");
                }

                order.FK_ActualStore = storeid;
                order.ExpressNumber = txtExpressNumber;
                order.OrderStatus = 3;
                order.DeliverTime = DateTime.Now;
                torderservice.SaveForm(keyValue, order);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 到店取货
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="storeid"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult PickSave(string keyValue, String storeid)
        {
            try
            {
                TORDEREntity order = torderservice.GetEntity(keyValue, false);
                if (order.IsDel != 0) return Error("订单已经删除!");
                if (order.DeliveryMode != 1)
                {
                    return Error("该订单不是到店取货，无法操作！");
                }
                else if (order.OrderStatus != 2)
                {
                    return Error("订单状态不正确，无法发货！");
                }
                else if (order.IsGroupOrder == 0 && order.GroupResult == 1)
                {
                    return Error("团购订单未成团，无法发货！");
                }

                order.FK_ActualStore = storeid;
                String str = torderservice.OrderPic(order);
                if (!String.IsNullOrEmpty(str)) return Error(str);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 退款
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Refund(string keyValue)
        {
            try
            {
                String str = torderservice.RefundSave(keyValue);
                if (!String.IsNullOrEmpty(str)) return Error(str);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 保存订单备注
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemarkSave(String keyValue, String Remark)
        {
            try
            {
                TORDEREntity tem = new TORDEREntity();
                tem.OrderRemark = Remark;
                torderservice.SaveForm(keyValue, tem);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 手动出库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderSendSave(String storeid, String keyValue)
        {
            try
            {
                String str = torderservice.SaveOrderSend(storeid, keyValue);
                if (!String.IsNullOrEmpty(str))
                {
                    return Error(str);
                }
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
