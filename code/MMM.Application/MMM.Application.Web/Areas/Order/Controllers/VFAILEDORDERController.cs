﻿using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.SystemManage.ViewModel;
using MMM.Application.Service.Cache;
using MMM.Application.Service.Order;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.Offices;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Order.Controllers
{
    /// <summary>
    /// 小程序订单
    /// </summary>
    public class VFAILEDORDERController : MvcControllerBase
    {
        private VFAILEDORDERService vfailedorderservice = new VFAILEDORDERService();
        private OrganizeCache organize = new OrganizeCache();

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 小程序订单手动出库
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderSend()
        {
            return View();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            IList<VFAILEDORDEREntity> orderlist = vfailedorderservice.GetPageList(pagination, search);

            var jsonData = new
            {
                rows = orderlist,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 手动出库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OrderSendSave(String storeid, String keyValue)
        {
            try
            {
                String str = vfailedorderservice.SaveOrderSend(storeid, keyValue);
                if (!String.IsNullOrEmpty(str))
                {
                    return Error(str);
                }
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        [HttpGet]
        public void export(String json, String storeGUID, String startdate, String enddate)
        {
            JObject search = json.ToJObject();

            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;

            IList<VFAILEDORDEREntity> orderexportList = vfailedorderservice.GetList(search);

            DataTable dt = new DataTable();
            dt.Columns.Add("CreateTime");
            dt.Columns.Add("DeliveryName");
            dt.Columns.Add("DeliveryMobile");
            dt.Columns.Add("DeliveryMode");
            dt.Columns.Add("StoreAdress");
            dt.Columns.Add("Name");
            dt.Columns.Add("ProductSKU");
            dt.Columns.Add("Price");
            dt.Columns.Add("Count");
            dt.Columns.Add("ProductUnit");
            dt.Columns.Add("OrderRemark2");
            foreach (VFAILEDORDEREntity detail in orderexportList)
            {
                dt.Rows.Add(new String[] { detail.CreateTime.Value.ToString("yyyy-MM-dd"),detail.DeliveryName,detail.DeliveryMobile,
                    detail.DeliveryMode==1?"到店取货":"送货上门",
                    detail.StoreAdress,detail.Name,detail.ProductSKU,
                (detail.Price??0).ToString(),detail.Count.ToString(),detail.ProductUnit,((detail.Count??0)*(detail.Price??0)).ToString()
                });
            }
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.TitleFont = "微软雅黑";
            excelconfig.TitlePoint = 25;
            excelconfig.FileName = String.Format("订单详情{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            excelconfig.IsAllSizeColumn = true;

            List<ColumnEntity> listColumnEntity = new List<ColumnEntity>();
            excelconfig.ColumnEntity = listColumnEntity;
            ColumnEntity columnentity = new ColumnEntity();
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CreateTime", ExcelColumn = "下单时间" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "DeliveryName", ExcelColumn = "收件人" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "DeliveryMobile", ExcelColumn = "联系电话" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "DeliveryMode", ExcelColumn = "取货方式" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "StoreAdress", ExcelColumn = "取货地址(配送地址)" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Name", ExcelColumn = "商品名称" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductSKU", ExcelColumn = "商品条码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Price", ExcelColumn = "商品会员价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Count", ExcelColumn = "数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductUnit", ExcelColumn = "单位" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "OrderRemark2", ExcelColumn = "商品总价" });
            //调用导出方法
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }

    }
}
