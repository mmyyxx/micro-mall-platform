﻿using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.SystemManage.ViewModel;
using MMM.Application.Service.Cache;
using MMM.Application.Service.Order;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Order.Controllers
{
    /// <summary>
    /// 订单售后
    /// </summary>
    public class VAFTERSALEController : MvcControllerBase
    {
        private VAFTERSALEService vaftersaleservice = new VAFTERSALEService();
        private T_PRODUCTCLASSService t_productclassservice = new T_PRODUCTCLASSService();
        private TORDERDETAILService torderdetailservice = new TORDERDETAILService();
        private OrganizeCache organize = new OrganizeCache();
        private DataItemCache dataItemCache = new DataItemCache();

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        
        /// <summary>
        /// 审核
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult AfterSale()
        {
            return View();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            IList<VAFTERSALEEntity> orderlist = vaftersaleservice.GetPageList(pagination, search);

            IList<T_PRODUCTCLASSEntity> classList = new ProductClassCache().GetCacheList().Where(t => t.organizeid == search["organizeid"].ToString()).ToList();

            //付款状态
            IList<DataItemModel> orderdetailstatusList = dataItemCache.GetDataItemList("OrderDetailStatus").ToList();
            foreach (VAFTERSALEEntity m in orderlist)
            {
                DataItemModel tem = orderdetailstatusList.FirstOrDefault(x => x.ItemValue == m.Status.ToString());
                if (tem != null) m.StatusStr = tem.ItemName;

                string classguid = m.FK_ProductClass;
                string parameter = String.Empty;
                while (!String.IsNullOrEmpty(classguid))
                {
                    T_PRODUCTCLASSEntity productclass = classList.Where(p => p.GUID == classguid).SingleOrDefault();
                    if (productclass != null)
                    {
                        string classstr = String.Format("{0}：{1}；", productclass.ProductClass, productclass.ClassValue);
                        parameter += classstr;
                        classguid = productclass.FK_ParentClass;
                    }
                    else
                        classguid = "";
                }
                m.ProductClass = parameter;
            }
            var jsonData = new
            {
                rows = orderlist,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 审核
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult AfterSaleSave(String keyValue,Int32? OrderStatus,String Remark)
        {
            try
            {
                TORDERDETAILEntity entity = new TORDERDETAILEntity();
                entity.GUID = keyValue;
                entity.Status = OrderStatus;
                entity.Remark = Remark;

                torderdetailservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
    }
}
