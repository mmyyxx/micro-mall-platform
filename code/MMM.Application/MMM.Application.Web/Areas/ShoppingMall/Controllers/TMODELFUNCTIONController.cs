using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity;
using MMM.Application.Entity.ShoppingMall;
using MMM.Application.Service;
using MMM.Application.Service.ShoppingMall;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Mvc;
using System.Xml;

namespace MMM.Application.Web.Areas.ShoppingMall.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-24 10:55
    /// 描 述：商城模块
    /// </summary>
    public class TMODELFUNCTIONController : MvcControllerBase
    {
        private TMODELFUNCTIONService tmodelfunctionservice = new TMODELFUNCTIONService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form(String keyValue)
        {
            String guid = "";
            if (String.IsNullOrEmpty(keyValue))
            {
                guid = Guid.NewGuid().ToString();
            }
            else guid = keyValue;
            ViewBag.guid = guid;
            return View();
        }

        /// <summary>
        /// 轮播信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Swiper()
        {
            SwiperModel swiper = new SwiperModel();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.Load(Server.MapPath(String.Format("\\Swiper\\{0}.xml", CodeConst.WxAppKEY)));

                swiper.appguid = CodeConst.WxAppKEY;
                swiper.dotscolor = (xd.GetElementsByTagName("dotscolor")[0].InnerText);
                swiper.selectdotcolor = xd.GetElementsByTagName("selectdotcolor")[0].InnerText; 
                swiper.duration = Int32.Parse((xd.GetElementsByTagName("duration")[0].InnerText)); 
                swiper.interval = Int32.Parse((xd.GetElementsByTagName("interval")[0].InnerText)); 
                swiper.width = Int32.Parse((xd.GetElementsByTagName("width")[0].InnerText));
                swiper.height = Int32.Parse((xd.GetElementsByTagName("height")[0].InnerText));
            }
            catch (Exception ex)
            {
            }
            return View(swiper);
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (search["organizeid"].IsEmpty())
            {
                organizeid = organize.GetCurrentOrgId();
            }

            var data = tmodelfunctionservice.GetPageList(pagination, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = tmodelfunctionservice.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tmodelfunctionservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TMODELFUNCTIONEntity entity = new TMODELFUNCTIONEntity();
                tmodelfunctionservice.RemoveForm(keyValue,entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TMODELFUNCTIONEntity entity)
        {
            try
            {
                tmodelfunctionservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 修改模块状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveStatus(String keyValue,Int32 IsUse)
        {
            try
            {
                TMODELFUNCTIONEntity entity = new TMODELFUNCTIONEntity();
                entity.IsUse = IsUse;
                tmodelfunctionservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 保存轮播图
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveSwiper(SwiperModel model)
        {
            try
            {
                String xml = Server.MapPath(String.Format("\\Swiper\\{0}.xml", CodeConst.WxAppKEY));
                XmlDocument xd = new XmlDocument();
                xd.Load(xml);

                xd.GetElementsByTagName("dotscolor")[0].InnerText = model.dotscolor;
                xd.GetElementsByTagName("selectdotcolor")[0].InnerText = model.selectdotcolor;
                xd.GetElementsByTagName("duration")[0].InnerText = model.duration.ToString();
                xd.GetElementsByTagName("interval")[0].InnerText = model.interval.ToString();
                xd.GetElementsByTagName("width")[0].InnerText = model.width.ToString();
                xd.GetElementsByTagName("height")[0].InnerText = model.height.ToString();

                xd.Save(xml);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
