using MMM.Application.Entity.Warehouse;
using MMM.Util;
using MMM.Util.WebControl;
using System;
using System.Web.Mvc;
using MMM.Application.Service.Warehouse;
using Newtonsoft.Json.Linq;
using MMM.Application.Cache;
using MMM.Util.Extension;
using MMM.Application.Code;

namespace MMM.Application.Web.Areas.Warehouse.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：仓库
    /// </summary>
    public class T_WAREHOUSEController : MvcControllerBase
    {
        private T_WAREHOUSEService t_warehouseservice = new T_WAREHOUSEService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_WAREHOUSEIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_WAREHOUSEForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();

            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();

            var data = t_warehouseservice.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="all">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(Boolean all)
        {
            String organizeid = "";
            if (!OperatorProvider.Provider.Current().IsSystem)
                organizeid = OperatorProvider.Provider.Current().CompanyId;
            var data = t_warehouseservice.GetList(organizeid);
            if (OperatorProvider.Provider.Current().IsSystem)
                foreach (T_WAREHOUSEEntity entity in data)
                {
                    entity.WarehouseNo = entity.WarehouseNo + "#" + entity.organizeid;
                }
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_warehouseservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 判断仓库编号是否重复
        /// </summary>
        /// <param name="WarehouseNo"></param>
        /// <param name="keyValue">主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExistNo(string WarehouseNo, string keyValue)
        {
            bool IsOk = true;
            T_WAREHOUSEEntity entity = t_warehouseservice.GetEntityByNo(WarehouseNo);
            if (entity != null) IsOk = false;
            return Content(IsOk.ToString());
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_WAREHOUSEEntity entity = new T_WAREHOUSEEntity();
                t_warehouseservice.DelForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_WAREHOUSEEntity entity)
        {
            try
            {
                //新增的时候判断
                if (keyValue == "")
                {
                    T_WAREHOUSEEntity t = t_warehouseservice.GetEntityByNo(entity.WarehouseNo);
                    if (t != null)
                    {
                        return Error(MMM.Application.Cache.Const.errWareRepert);
                    }
                }
                t_warehouseservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
