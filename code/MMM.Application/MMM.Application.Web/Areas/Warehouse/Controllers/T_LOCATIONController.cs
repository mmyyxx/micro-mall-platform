using MMM.Application.Entity.Warehouse;
using MMM.Util;
using MMM.Util.WebControl;
using System;
using System.Web.Mvc;
using MMM.Application.Service.Warehouse;
using System.Collections.Generic;
using System.Linq;
using MMM.Application.Code;
using Newtonsoft.Json.Linq;
using MMM.Application.Cache;
using MMM.Util.Extension;
using MMM.Application.Service.Materiel;

namespace MMM.Application.Web.Areas.Warehouse.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 11:59
    /// 描 述：储位
    /// </summary>
    public class T_LOCATIONController : MvcControllerBase
    {
        private T_LOCATIONService t_locationbll = new T_LOCATIONService();
        private T_AREAService t_areabll = new T_AREAService();
        private VLOCATIONSTOCKService vlocationstockbll = new VLOCATIONSTOCKService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_LOCATIONIndex()
        {
            return View();
        }

        /// <summary>
        /// 查找库存
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult LocationIndex()
        {
            return View();
        }

        /// <summary>
        /// 储位库存
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult LocationStock()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_LOCATIONForm(String keyValue)
        {
            if (String.IsNullOrEmpty(keyValue))
                return View();
            else return View("T_LOCATIONFormEdit");
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();

            var data = t_locationbll.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        
        /// <summary>
        /// 根据location的warehourse获取该仓库下的所有location
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson2(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["location"].IsEmpty())
            {
                T_LOCATIONEntity location = t_locationbll.GetEntity(search["location"].ToString());
                search["FK_WarehouseNo"] = location.FK_WarehouseNo;
            }
            var data = t_locationbll.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 查看储库存
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson3(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            var data = vlocationstockbll.GetPageList(pagination, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }


       /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = t_locationbll.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            T_LOCATIONEntity data = t_locationbll.GetEntity(keyValue);
            if (OperatorProvider.Provider.Current().IsSystem)
                data.FK_WarehouseNo = data.FK_WarehouseNo + "#" + data.organizeid;
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_LOCATIONEntity entity = new T_LOCATIONEntity();
                entity.Remove(keyValue);
                t_locationbll.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_LOCATIONEntity entity, Int32 row = 0, Int32 column = 0, Int32 layer = 0)
        {
            try
            {
                if (!OperatorProvider.Provider.Current().IsSystem)
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                else
                {
                    String[] organizeids = entity.FK_WarehouseNo.Split('#');
                    entity.organizeid = organizeids[1];
                    entity.FK_WarehouseNo = organizeids[0];
                }
                if (String.IsNullOrEmpty(keyValue))//新增
                {
                    if (row == 0 || column == 0 || layer == 0) return Error(MMM.Application.Cache.Const.errLocation);

                    //获取该区域下的所有储位
                    IList<T_LOCATIONEntity> locationList = t_locationbll.GetList(entity.FK_Area);
                    List<T_LOCATIONEntity> saveLocationList = new List<T_LOCATIONEntity>();
                    string areaNo = t_areabll.GetEntity(entity.FK_Area).AreaNo;
                    for (int m = 1; m <= row; m++)
                    {
                        for (int n = 1; n <= column; n++)
                        {
                            for (int l = 1; l <= layer; l++)
                            {
                                string locationNo = String.Format("{0}-{1}-{2}-{3}", areaNo, m.ToString().PadLeft(3, '0'), n.ToString().PadLeft(3, '0'), l.ToString().PadLeft(3, '0'));
                                int cnt = locationList.Count(p => p.LocationNo == locationNo);
                                if (cnt > 0)
                                {
                                    continue;
                                }

                                T_LOCATIONEntity item = new T_LOCATIONEntity();
                                item.Create();
                                item.FK_Area = entity.FK_Area;
                                item.LocationNo = locationNo;
                                item.Length = entity.Length;
                                item.Width = entity.Width;
                                item.Height = entity.Height;
                                item.Volume = item.Length * item.Width * item.Height;
                                item.Remark = entity.Remark;
                                item.FK_WarehouseNo = entity.FK_WarehouseNo;
                                item.organizeid = entity.organizeid;
                                saveLocationList.Add(item);
                            }
                        }
                    }
                    t_locationbll.SaveForm(saveLocationList);
                }
                else
                    t_locationbll.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
