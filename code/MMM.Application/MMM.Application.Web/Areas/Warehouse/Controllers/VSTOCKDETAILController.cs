﻿using MMM.Application.Code;
using MMM.Application.Service.Warehouse;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Warehouse.Controllers
{
    /// <summary>
    /// 储位库存明细
    /// </summary>
    public class VSTOCKDETAILController : MvcControllerBase
    {
        private VSTOCKDETAILService vstockdetailbll = new VSTOCKDETAILService();
        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            if (search["LocationGUID"].IsEmpty() && search["ProductGUID"].IsEmpty())
            {
                return ToJsonResult(new { msg = "出错" });
            }

            var data = vstockdetailbll.GetPageList(pagination, search);

            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
    }
}
