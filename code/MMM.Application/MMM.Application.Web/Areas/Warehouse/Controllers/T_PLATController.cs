using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Service.Warehouse;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Warehouse.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 12:46
    /// 描 述：月台
    /// </summary>
    public class T_PLATController : MvcControllerBase
    {
        private T_PLATService t_platbll = new T_PLATService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_PLATIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_PLATForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();

            var data = t_platbll.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="warehourse">仓库编码</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(String warehourse)
        {
            //管理员判断解析
            if (OperatorProvider.Provider.Current().IsSystem)
            {
                warehourse = warehourse.Split('#')[0];
            }
            var data = t_platbll.GetList(warehourse);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_platbll.GetEntity(keyValue);
            if (OperatorProvider.Provider.Current().IsSystem)
                data.FK_WarehouseNo = data.FK_WarehouseNo + "#" + data.organizeid;
            return ToJsonResult(data);
        }

        /// <summary>
        /// 判断月台编号是否重复
        /// </summary>
        /// <param name="PlatNo"></param>
        /// <param name="keyValue">主键</param>
        /// <param name="FK_WarehouseNo">主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExistNo(string PlatNo, string keyValue, String FK_WarehouseNo)
        {
            bool IsOk = true;
            if (String.IsNullOrEmpty(FK_WarehouseNo)) return Content(false.ToString());

            T_PLATEntity entity = t_platbll.GetEntityByNo(PlatNo, keyValue, FK_WarehouseNo);
            if (entity != null) IsOk = false;
            return Content(IsOk.ToString());
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_PLATEntity entity = new T_PLATEntity();
                entity.Remove(keyValue);
                t_platbll.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_PLATEntity entity)
        {
            try
            {
                T_PLATEntity temEntity = t_platbll.GetEntityByNo(entity.PlatNo, keyValue, entity.FK_WarehouseNo);
                if (temEntity != null)
                {
                    return Error(MMM.Application.Cache.Const.errAreaRepert2);
                }
                if (!OperatorProvider.Provider.Current().IsSystem)
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                else
                {
                    String[] organizeids = entity.FK_WarehouseNo.Split('#');
                    entity.organizeid = organizeids[1];
                    entity.FK_WarehouseNo = organizeids[0];
                }
                t_platbll.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
