using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using MMM.Application.Code;
using MMM.Util.Extension;
using MMM.Application.Cache;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 21:41
    /// 描 述：出库单明细
    /// </summary>
    public class TOUTSTOCKDETAILController : MvcControllerBase
    {
        private TOUTSTOCKDETAILService toutstockdetailservice = new TOUTSTOCKDETAILService();
        private TOUTSTOCKORDERService toutstockorderservice = new TOUTSTOCKORDERService();
        private TSTOCKService tstockservice = new TSTOCKService();
        private VCOSTINFOService vcostinfoservice = new VCOSTINFOService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            JObject search = queryJson.ToJObject();
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            var data = toutstockdetailservice.GetList(search);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = toutstockdetailservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 获取物料的库存和单价
        /// </summary>
        /// <param name="warehouse">仓库</param>
        /// <param name="FK_Product">物料id</param>
        /// <param name="sku"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPriceAndStock(String warehouse, String FK_Product, String sku)
        {
            JObject json = new JObject();
            json["FK_WarehouseNo"] = warehouse;
            json["FK_Product"] = FK_Product;
            IList<TSTOCKEntity> stockList = tstockservice.GetList(json);
            decimal stock = 0;
            if (stockList != null && stockList.Count > 0)
                stock = stockList.Sum(x => x.Stock) ?? 0;

            //获取价格
            decimal price = 0;
            VCOSTINFOEntity vcost = vcostinfoservice.GetEntityBySKU(sku);
            if (vcost != null)
                price = vcost.Price;
            var data = new {
                stock = stock,
                price = price
            };
            return ToJsonResult(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetPrice(String sku)
        {
            JObject json = new JObject();
            //获取价格
            decimal price = 0;
            VCOSTINFOEntity vcost = vcostinfoservice.GetEntityBySKU(sku);
            if (vcost != null)
                price = vcost.Price;
            var data = new
            {
                price = price
            };
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TOUTSTOCKDETAILEntity entity = toutstockdetailservice.GetEntity(keyValue);
                TOUTSTOCKORDEREntity orderEntity = toutstockorderservice.GetEntity(entity.FK_OutStockOrder);
                if (orderEntity.Status != 0)
                {
                    return Error("该出库单已经分配。");
                }
                if (orderEntity != null)
                {
                    decimal newtotal = entity.Count.Value * entity.Price.Value;
                    orderEntity.OrderPrice = orderEntity.OrderPrice - (newtotal);
                    toutstockorderservice.SaveForm(orderEntity.GUID, orderEntity);
                }
                toutstockdetailservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, TOUTSTOCKDETAILEntity entity)
        {
            try
            {
                if ((entity.Count ?? 0) * (entity.Price ?? 0) == 0)
                {
                    return Error("主数量和单价不能是0！");
                }
                if (!String.IsNullOrEmpty(keyValue))
                {
                    TOUTSTOCKDETAILEntity detailEntity = toutstockdetailservice.GetEntity(keyValue);
                    decimal oldPrice = detailEntity.Price.Value;
                    decimal oldCount = detailEntity.Count.Value;
                    decimal oldtotal = oldPrice * oldCount;
                    decimal newtotal = entity.Count.Value * entity.Price.Value;
                    if (oldtotal != newtotal)
                    {
                        TOUTSTOCKORDEREntity orderEntity = toutstockorderservice.GetEntity(detailEntity.FK_OutStockOrder);
                        if (orderEntity != null)
                        {
                            orderEntity.OrderPrice = orderEntity.OrderPrice + (newtotal - oldtotal);
                            toutstockorderservice.SaveForm(orderEntity.GUID, orderEntity);
                        }
                    }
                }
                else
                {
                    TOUTSTOCKORDEREntity orderEntity = toutstockorderservice.GetEntity(entity.FK_OutStockOrder);
                    if (orderEntity != null)
                    {
                        decimal newtotal = entity.Count.Value * entity.Price.Value;
                        orderEntity.OrderPrice = orderEntity.OrderPrice + (newtotal);
                        toutstockorderservice.SaveForm(orderEntity.GUID, orderEntity);
                    }
                }
                if (String.IsNullOrEmpty(entity.organizeid))
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                toutstockdetailservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
