﻿using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Service.Materiel;
using MMM.Application.Service.Warehouse;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 储位库存明细
    /// </summary>
    public class VPRODUCTSTOCKController : MvcControllerBase
    {
        private VPRODUCTSTOCKService vstockdetailbll = new VPRODUCTSTOCKService();
        OrganizeCache organize = new OrganizeCache();
        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 临期商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult ScheIndex()
        {
            return View();
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            search["organizeid"] = organize.GetCurrentOrgId();
            var data = vstockdetailbll.GetPageList(pagination, search);
            
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 获取临期列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson2(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            search["organizeid"] = organize.GetCurrentOrgId();
            var data = vstockdetailbll.GetPageList2(pagination, search);

            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
    }
}
