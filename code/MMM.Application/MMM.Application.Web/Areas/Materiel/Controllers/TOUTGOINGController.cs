using MMM.Application.Cache;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Service.Materiel;
using MMM.Application.Service.Warehouse;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-06 21:48
    /// 描 述：出库分配
    /// </summary>
    public class TOUTGOINGController : MvcControllerBase
    {
        private TOUTGOINGService toutgoingservice = new TOUTGOINGService();
        private T_WAREHOUSEService warehouseService = new T_WAREHOUSEService();
        private TPRODUCTService tproductservice = new TPRODUCTService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            String organizeid = search["organizeid"].ToString();
            IList<String> productIds = new List<String>();
            if (!search["SKU"].IsEmpty() || !search["PdtName"].IsEmpty())
            {
                IList<TPRODUCTEntity> temSupplierList = tproductservice.GetList(search);
                if (temSupplierList.Count > 0) productIds = temSupplierList.Select(x => x.GUID).ToList();
            }

            var data = toutgoingservice.GetPageList(pagination, search, productIds);

            IList<String> supilyids = data.Select(x => x.FK_Product).ToList();
            IList<TPRODUCTEntity> supplierList = tproductservice.GetList(supilyids);
            IList<T_WAREHOUSEEntity> wareHouseList = warehouseService.GetList(organizeid, true);
            foreach (TOUTGOINGEntity entity in data)
            {
                T_WAREHOUSEEntity warehouse = wareHouseList.FirstOrDefault(x => x.WarehouseNo == entity.FK_WarehouseNo);
                if (warehouse != null)
                {
                    entity.FK_WarehouseNoName = warehouse.WarehouseName;
                }
                TPRODUCTEntity supplier = supplierList.FirstOrDefault(x => x.GUID == entity.FK_Product);
                if (supplier != null)
                {
                    entity.PdtName = supplier.PdtName;
                    entity.SKU = supplier.SKU;
                }
            }
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = toutgoingservice.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = toutgoingservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TOUTGOINGEntity entity = new TOUTGOINGEntity();
                entity.Remove(keyValue);
                toutgoingservice.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, TOUTGOINGEntity entity)
        {
            try
            {
                toutgoingservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
