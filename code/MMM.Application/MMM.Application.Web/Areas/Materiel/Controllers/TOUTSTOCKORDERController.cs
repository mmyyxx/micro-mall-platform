using MMM.Application.Code;
using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using MMM.Util.Extension;
using MMM.Application.Cache;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Service.Warehouse;
using System.Collections;
using System.Web;
using MMM.Util.Offices;
using System.Data;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 21:32
    /// 描 述：出库单
    /// </summary>
    public class TOUTSTOCKORDERController : MvcControllerBase
    {
        private TOUTSTOCKORDERService toutstockorderservice = new TOUTSTOCKORDERService();
        private TOUTSTOCKDETAILService toutstockdetailservice = new TOUTSTOCKDETAILService();
        private TSUPPLIERService tsupplierservice = new TSUPPLIERService();
        OrganizeCache organize = new OrganizeCache();
        private T_WAREHOUSEService warehouseService = new T_WAREHOUSEService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form(String keyValue)
        {
            String guid = "";
            if (String.IsNullOrEmpty(keyValue))
            {
                guid = Guid.NewGuid().ToString();
            }
            else guid = keyValue;
            ViewBag.guid = guid;
            return View();
        }

        /// <summary>
        /// 出库导入
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Import()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            String organizeid = search["organizeid"].ToString();
            IList<String> supplierIds = new List<String>();
            if (!search["FK_Supplier"].IsEmpty())
            {
                String FK_Supplier = search["FK_Supplier"].ToString();
                JObject json = new JObject();
                json["SupplierName"] = FK_Supplier;
                IList<TSUPPLIEREntity> temSupplierList = tsupplierservice.GetList(json);
                if (temSupplierList.Count > 0) supplierIds = temSupplierList.Select(x => x.GUID).ToList();
            }

            var data = toutstockorderservice.GetPageList(pagination, search, supplierIds);

            IList<String> supilyids = data.Select(x => x.FK_Supplier).ToList();
            IList<TSUPPLIEREntity> supplierList = tsupplierservice.GetList(supilyids);
            IList<T_WAREHOUSEEntity> wareHouseList = warehouseService.GetList(organizeid, true);
            foreach (TOUTSTOCKORDEREntity entity in data)
            {
                T_WAREHOUSEEntity warehouse = wareHouseList.FirstOrDefault(x => x.WarehouseNo == entity.FK_WarehouseNo);
                if (warehouse != null)
                {
                    entity.FK_WarehouseNoName = warehouse.WarehouseName;
                }
                TSUPPLIEREntity supplier = supplierList.FirstOrDefault(x => x.GUID == entity.FK_Supplier);
                if (supplier != null)
                {
                    entity.FK_SupplierName = supplier.SupplierName;
                }
            }
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = toutstockorderservice.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = toutstockorderservice.GetEntity(keyValue);
            if (OperatorProvider.Provider.Current().IsSystem)
                data.FK_WarehouseNo = data.FK_WarehouseNo + "#" + data.organizeid;

            TSUPPLIEREntity entity = tsupplierservice.GetEntity(data.FK_Supplier);
            if (entity != null)
            {
                data.FK_SupplierName = entity.SupplierName;
            }
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TOUTSTOCKORDEREntity entity = toutstockorderservice.GetEntity(keyValue);
                if (entity.Status != 0)
                {
                    return Error("只有未分配的出库单可以删除！");
                }

                //获取明细
                JObject json = new JObject();
                json["FK_PurchaseOrder"] = keyValue;
                List<TOUTSTOCKDETAILEntity> detailList = toutstockdetailservice.GetList(json).ToList();

                foreach (TOUTSTOCKDETAILEntity detail in detailList)
                {
                    detail.Deleter = OperatorProvider.Provider.Current().Account;
                    detail.DeleteTime = DateTime.Now;
                    detail.IsDel = 1;
                }
                if (detailList.Count > 0)
                    toutstockdetailservice.RemoveForm(detailList);

                toutstockorderservice.RemoveForm(keyValue, entity);

                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TOUTSTOCKORDEREntity entity)
        {
            try
            {
                if (!OperatorProvider.Provider.Current().IsSystem)
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                else
                {
                    String[] organizeids = entity.FK_WarehouseNo.Split('#');
                    entity.organizeid = organizeids[1];
                    entity.FK_WarehouseNo = organizeids[0];
                }
                toutstockorderservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 分配出库单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult OutAll()
        {
            try
            {
                String organizeid = organize.GetCurrentOrgId();
                String str = toutstockorderservice.OutAll(organizeid);
                if (!String.IsNullOrEmpty(str)) return Error(str);

                return Success("上架指示单生成成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }

        }

        /// <summary>
        /// 分配出库单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Out(String keyValue)
        {
            try
            {
                String str = toutstockorderservice.Out(keyValue);
                if (!String.IsNullOrEmpty(str)) return Error(str);

                return Success("上架指示单生成成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }

        }

        /// <summary>
        /// 出库导入
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public String SaveImport(String warehouse)
        {
            try
            {
                if (String.IsNullOrEmpty(warehouse))
                {
                    var error = new { error = "0", message = "请选择仓库" };
                    return (error.ToJson());
                }
                Hashtable extTable = new Hashtable();
                extTable.Add("file", "xls,xlsx");

                HttpPostedFileBase imgFile = Request.Files[0];
                if (imgFile == null)
                {
                    var ret = new { error = "0", message = "请选择文件" };
                    return (ret.ToJson());
                }

                String fileName = imgFile.FileName;
                String fileExt = System.IO.Path.GetExtension(fileName).ToLower();
                if (Array.IndexOf(((String)extTable["file"]).Split(','), fileExt.Substring(1).ToLower()) == -1)
                {
                    var jsonData3 = new
                    {
                        error = "0",
                        message = String.Format("上传文件扩展名是不允许的扩展名。\n只允许{0}格式.", ((String)extTable["file"]))
                    };
                    return (jsonData3.ToJson()); ;
                }

                DataSet ds = ExcelHelper.ExcelImport(imgFile.InputStream, fileExt);

                DataTable ordertable = ds.Tables[0];
                DataTable detailtable = ds.Tables[1];

                String organizeid = "";

                if (!OperatorProvider.Provider.Current().IsSystem)
                    organizeid = OperatorProvider.Provider.Current().CompanyId;
                else
                {
                    String[] organizeids = warehouse.Split('#');
                    organizeid = organizeids[1];
                    warehouse = organizeids[0];
                }
                //入库单信息
                TOUTSTOCKORDEREntity order = new TOUTSTOCKORDEREntity();
                order.Create();
                order.GUID = Guid.NewGuid().ToString();
                //仓库
                order.FK_WarehouseNo = warehouse;
                //供应商编号
                order.FK_Supplier = ordertable.Rows[0][0].ToString();
                //订单类型
                order.OrderType = Convert.ToInt32(ordertable.Rows[0][1]);
                //备注
                order.Remark = ordertable.Rows[0][2].ToString();
                order.organizeid = organizeid;
                //入库单明细
                List<TOUTSTOCKDETAILEntity> detaillist = new List<TOUTSTOCKDETAILEntity>();
                foreach (DataRow row in detailtable.Rows)
                {
                    if (row[0].ToString().Equals(String.Empty))
                    {
                        break;
                    }
                    TOUTSTOCKDETAILEntity detail = new TOUTSTOCKDETAILEntity();
                    detail.Create();
                    detail.GUID = Guid.NewGuid().ToString();
                    detail.FK_OutStockOrder = order.GUID;
                    //SKU
                    detail.SKU = row[0].ToString();
                    //物料主数量
                    detail.Count = row[1].ToDecimal();
                    //物料主单位
                    detail.MainUnit = row[2].ToString();
                    //物料辅单位
                    detail.AuxiliaryUnit = row[3].ToString();
                    //主辅数量换算
                    detail.UnitConversion = row[4].ToDecimal();
                    //物料单价
                    detail.Price = row[5].ToDecimal();
                    //物料折扣
                    detail.Discount = row[6].ToDecimal();
                    //备注
                    detail.Remark = row[7].ToString();
                    if (detail.UnitConversion.Value > Decimal.Zero)
                    {
                        //物料辅数量
                        detail.UnitCount = Decimal.Round((detail.Count ?? 0) / detail.UnitConversion.Value, 2);
                    }
                    detail.organizeid = organizeid;
                    //添加明细
                    detaillist.Add(detail);
                }

                String str = toutstockorderservice.SaveImport(order, detaillist);
                if (!String.IsNullOrEmpty(str))
                {
                    var reterror8 = new
                    {
                        error = "0",
                        message = str
                    };
                    return (reterror8.ToJson());
                }
                else
                {
                    var retsuccess = new
                    {
                        error = "1",
                        message = "导入成功"
                    };
                    return (retsuccess.ToJson());
                }
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                var jsonData1 = new
                {
                    error = "0",
                    message = "导入失败"
                };
                return (jsonData1.ToJson());
            }
        }
        #endregion
    }
}
