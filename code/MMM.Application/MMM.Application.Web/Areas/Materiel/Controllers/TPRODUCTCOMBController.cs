using MMM.Application.Code;
using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 10:38
    /// 描 述：物料组合
    /// </summary>
    public class TPRODUCTCOMBController : MvcControllerBase
    {
        private TPRODUCTCOMBService tproductcombservice = new TPRODUCTCOMBService();
        private TPRODUCTService tproductservice = new TPRODUCTService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            JObject search = queryJson.ToJObject();
            IList<TPRODUCTCOMBEntity> data = tproductcombservice.GetList(search);

            IList<String> productIds = new List<String>();
            foreach (TPRODUCTCOMBEntity entity in data)
            {
                productIds.Add(entity.FK_Child);
                if (!productIds.Contains(entity.FK_Product))
                    productIds.Add(entity.FK_Product);
            }
            IList<TPRODUCTEntity> productLsit = tproductservice.GetList(productIds);
            foreach (TPRODUCTCOMBEntity tem in data)
            {
                foreach (TPRODUCTEntity entity in productLsit)
                {
                    if (entity.GUID == tem.FK_Child) tem.FK_ChildName = entity.PdtName;
                    else if (entity.GUID == tem.FK_Product) tem.FK_ProductName = entity.PdtName;
                }
            }

            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tproductcombservice.GetEntity(keyValue);

            IList<String> productIds = new List<String>();
            productIds.Add(data.FK_Child);
            productIds.Add(data.FK_Product);
            IList<TPRODUCTEntity> productLsit = tproductservice.GetList(productIds);
            foreach (TPRODUCTEntity entity in productLsit)
            {
                if (entity.GUID == data.FK_Child) data.FK_ChildName = entity.PdtName;
                else if (entity.GUID == data.FK_Product) data.FK_ProductName = entity.PdtName;
            }

            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TPRODUCTCOMBEntity entity = new TPRODUCTCOMBEntity();
                entity.Remove(keyValue);
                tproductcombservice.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TPRODUCTCOMBEntity entity)
        {
            try
            {
                if (entity.FK_Child == entity.FK_Product)
                {
                    return Error("子商品不能和主商品相同。");
                }
                JObject search = new JObject();
                search["FK_Product"] = entity.FK_Product;
                search["FK_Child"] = entity.FK_Child;
                search["GUID"] = keyValue;
                IList<TPRODUCTCOMBEntity> list = tproductcombservice.GetList(search);
                if (list.Count > 0)
                {
                    return Error("该产品已经添加。");
                }
                entity.FK_ChildName = null;
                tproductcombservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
