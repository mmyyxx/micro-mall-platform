using MMM.Application.Code;
using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 14:56
    /// 描 述：包装
    /// </summary>
    public class TPACKAGEController : MvcControllerBase
    {
        private TPACKAGEService tpackageservice = new TPACKAGEService();
        private TPRODUCTService tproductservice = new TPRODUCTService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            JObject search = queryJson.ToJObject();
            var data = tpackageservice.GetList(search);

            IList<String> productIds = new List<String>();
            productIds.Add(data[0].FK_Product);
            IList<TPRODUCTEntity> productLsit = tproductservice.GetList(productIds);
            foreach (TPACKAGEEntity tem in data)
            {
                foreach (TPRODUCTEntity entity in productLsit)
                {
                    if (entity.GUID == tem.FK_Product)
                    {
                        tem.FK_ProductName = entity.PdtName;
                        break;
                    }
                }
            }

            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tpackageservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 判断编号是否重复
        /// </summary>
        /// <param name="PackageSKU"></param>
        /// <param name="keyValue">主键</param>
        /// <param name="FK_Product">主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExistNo(string PackageSKU, string keyValue, String FK_Product)
        {
            bool IsOk = true;
            if (String.IsNullOrEmpty(FK_Product)) return Content(false.ToString());

            TPACKAGEEntity entity = tpackageservice.GetEntityByNo(PackageSKU, keyValue, FK_Product);
            if (entity != null) IsOk = false;
            return Content(IsOk.ToString());
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TPACKAGEEntity entity = new TPACKAGEEntity();
                tpackageservice.RemoveForm(keyValue,entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TPACKAGEEntity entity)
        {
            try
            {
                TPACKAGEEntity temEntity = tpackageservice.GetEntityByNo(entity.PackageSKU, keyValue, entity.FK_Product);
                if (temEntity != null)
                {
                    return Error(MMM.Application.Cache.Const.errAreaRepert2);
                }
                if (!OperatorProvider.Provider.Current().IsSystem)
                {
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                }
                else //后去商品的organizeid
                {
                    TPRODUCTEntity product = tproductservice.GetEntity(entity.FK_Product);
                    entity.organizeid = product.organizeid;
                }
                tpackageservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
