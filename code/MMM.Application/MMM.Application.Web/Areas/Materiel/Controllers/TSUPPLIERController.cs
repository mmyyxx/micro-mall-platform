using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 21:50
    /// 描 述：客户
    /// </summary>
    public class TSUPPLIERController : MvcControllerBase
    {
        private TSUPPLIERService tsupplierservice = new TSUPPLIERService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }

        /// <summary>
        /// 选择列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexSearch()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();
            var data = tsupplierservice.GetPageList(pagination,organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            JObject search = queryJson.ToJObject();
            var data = tsupplierservice.GetList(search);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tsupplierservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 判断编号是否重复
        /// </summary>
        /// <param name="SupplierNo"></param>
        /// <param name="keyValue">主键</param>
        /// <param name="organizeid">主键</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ExistNo(string SupplierNo, string keyValue, String organizeid)
        {
            bool IsOk = true;
            if (String.IsNullOrEmpty(organizeid))
                organizeid = OperatorProvider.Provider.Current().CompanyId;

            TSUPPLIEREntity entity = tsupplierservice.GetEntityByNo(SupplierNo, keyValue, organizeid);
            if (entity != null) IsOk = false;
            return Content(IsOk.ToString());
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TSUPPLIEREntity entity = new TSUPPLIEREntity();
                tsupplierservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TSUPPLIEREntity entity)
        {
            try
            {
                String organizeid = "";
                if (OperatorProvider.Provider.Current().IsSystem)
                {
                    if (String.IsNullOrEmpty(keyValue))
                        organizeid = OperatorProvider.Provider.Current().CompanyId;
                    else
                    {
                        var data = tsupplierservice.GetEntity(keyValue);
                        organizeid = data.organizeid;
                    }
                }
                else
                    organizeid = OperatorProvider.Provider.Current().CompanyId;
                TSUPPLIEREntity tem = tsupplierservice.GetEntityByNo(entity.SupplierNo, keyValue, organizeid);
                if (tem != null)
                {
                    return Error(MMM.Application.Cache.Const.errAreaRepert2);
                }

                tsupplierservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
