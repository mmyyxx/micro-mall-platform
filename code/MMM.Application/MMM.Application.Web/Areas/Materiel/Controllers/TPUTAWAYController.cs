using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Application.Service.Warehouse;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using MMM.Application.Entity.Warehouse;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 22:07
    /// 描 述：上架
    /// </summary>
    public class TPUTAWAYController : MvcControllerBase
    {
        private TPUTAWAYService tputawayservice = new TPUTAWAYService();
        private TPURCHASEDETAILService tpurchasedetailservice = new TPURCHASEDETAILService();
        private T_LOCATIONService t_locationbll = new T_LOCATIONService();
        private TPURCHASEORDERService tpurchaseorderservice = new TPURCHASEORDERService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index(String keyValue)
        {
            TPURCHASEORDEREntity order = tpurchaseorderservice.GetEntity(keyValue);
            ViewBag.status = order.Status;
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            //获取单子详情
            IList<TPURCHASEDETAILEntity> detailList = tpurchasedetailservice.GetList(search);
            IList<String> detailids = detailList.Select(x => x.GUID).ToList();

            IList<TPUTAWAYEntity> data = tputawayservice.GetPageList(pagination, detailids, search).ToList();
            IList<String> locationids = data.Select(x => x.FK_Location).ToList();
            IList<T_LOCATIONEntity> locationList = t_locationbll.GetList("", locationids);
            foreach (TPUTAWAYEntity putaway in data)
            {
                TPURCHASEDETAILEntity detail = detailList.FirstOrDefault(x => x.GUID == putaway.FK_PurchaseDetail);
                if (detail != null)
                {
                    putaway.ProductName = detail.ProductName;
                    putaway.SKU = detail.SKU;
                    putaway.Count = detail.Count ?? 0;
                    putaway.MainUnit = detail.MainUnit;
                    putaway.UnitCount = detail.UnitCount;
                    putaway.AuxiliaryUnit = detail.AuxiliaryUnit;
                }
                T_LOCATIONEntity location = locationList.FirstOrDefault(x => x.GUID == putaway.FK_Location);
                if (location != null)
                {
                    putaway.LocationNo = location.LocationNo;
                }
            }
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = tputawayservice.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tputawayservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TPUTAWAYEntity entity = new TPUTAWAYEntity();
                entity.Remove(keyValue);
                tputawayservice.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, TPUTAWAYEntity entity)
        {
            try
            {
                tputawayservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult SaveSataus(String keyValue)
        {
            try
            {
               String str = tputawayservice.SaveSataus(keyValue);
                if(!String.IsNullOrEmpty(str))return Error(str);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult SaveLocation(String keyValue, String locationId)
        {
            try
            {
                TPUTAWAYEntity entity = tputawayservice.GetEntity(keyValue);
                if (entity.Status == 0)
                {
                    return Error("该物料已入库！");
                }
                entity.FK_Location = locationId;
                tputawayservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
