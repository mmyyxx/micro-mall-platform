using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.Offices;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Materiel.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 10:12
    /// 描 述：订单明细
    /// </summary>
    public class TPURCHASEDETAILController : MvcControllerBase
    {
        private TPURCHASEDETAILService tpurchasedetailservice = new TPURCHASEDETAILService();
        private TPURCHASEORDERService tpurchaseorderservice = new TPURCHASEORDERService();
        OrganizeCache organize = new OrganizeCache();
        private TPUTAWAYService tputawayservice = new TPUTAWAYService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();
            var data = tpurchasedetailservice.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            JObject search = queryJson.ToJObject();
            var data = tpurchasedetailservice.GetList(search);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = tpurchasedetailservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 根据orderid导出数据
        /// </summary>
        /// <param name="FK_PurchaseOrder"></param>
        /// <returns></returns>
        [HttpGet]
        public void GetExport(String FK_PurchaseOrder)
        {
            JObject search = new JObject();
            search["FK_PurchaseOrder"] = FK_PurchaseOrder;
            var data = tpurchasedetailservice.GetList(search);

            DataTable dt = new DataTable();
            dt.Columns.Add("SKU");
            dt.Columns.Add("ProductName");
            dt.Columns.Add("Count");
            dt.Columns.Add("MainUnit");
            dt.Columns.Add("Price");
            dt.Columns.Add("Discount");
            dt.Columns.Add("ProductionDate");
            dt.Columns.Add("Creater");
            dt.Columns.Add("CreateTime");
            foreach (TPURCHASEDETAILEntity detail in data)
            {
                dt.Rows.Add(new String[] { detail.SKU,detail.ProductName,(detail.Count??0).ToString(),
                    detail.MainUnit,(detail.Price??0).ToString(),(detail.Discount??0).ToString(),
                detail.ProductionDate.Value.ToString("yyyy-MM-dd"),detail.Creater,detail.CreateTime.Value.ToString("yyyy-MM-dd")});
            }
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.TitleFont = "微软雅黑";
            excelconfig.TitlePoint = 25;
            excelconfig.FileName = String.Format("入库单详情{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            excelconfig.IsAllSizeColumn = true;

            List<ColumnEntity> listColumnEntity = new List<ColumnEntity>();
            excelconfig.ColumnEntity = listColumnEntity;
            ColumnEntity columnentity = new ColumnEntity();
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "SKU", ExcelColumn = "商品条码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductName", ExcelColumn = "商品名称" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Count", ExcelColumn = "物料主数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "MainUnit", ExcelColumn = "主单位" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Price", ExcelColumn = "物料单价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Discount", ExcelColumn = "物料折扣" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductionDate", ExcelColumn = "生产时间" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Creater", ExcelColumn = "操作人" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CreateTime", ExcelColumn = "操作时间" });
            //调用导出方法
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TPURCHASEDETAILEntity entity = tpurchasedetailservice.GetEntity(keyValue);
                TPURCHASEORDEREntity orderEntity = tpurchaseorderservice.GetEntity(entity.FK_PurchaseOrder);
                //获取T_PUTAWAY
                TPUTAWAYEntity putawayInfo = tputawayservice.GetEntityByPurchaseDetail(keyValue);

                if (putawayInfo != null)
                {
                    if (putawayInfo.Status == 0)
                        return Error("该入库明细信息已入库，无法删除！");
                    else
                        tputawayservice.RemoveForm(putawayInfo.GUID, putawayInfo);
                }

                tpurchasedetailservice.RemoveForm(keyValue, entity);

                if (orderEntity != null)
                {
                    decimal newtotal = entity.Count.Value * entity.Price.Value;
                    orderEntity.PurchaseOrderPrice = orderEntity.PurchaseOrderPrice - (newtotal);
                    tpurchaseorderservice.SaveForm(orderEntity.GUID, orderEntity);
                }

                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TPURCHASEDETAILEntity entity)
        {
            try
            {
                if ((entity.Count ?? 0) * (entity.Price ?? 0) == 0)
                {
                    return Error("主数量和单价不能是0！");
                }
                if (!String.IsNullOrEmpty(keyValue))
                {
                    TPURCHASEDETAILEntity detailEntity = tpurchasedetailservice.GetEntity(keyValue);
                    decimal oldPrice = detailEntity.Price.Value;
                    decimal oldCount = detailEntity.Count.Value;
                    decimal oldtotal = oldPrice * oldCount;
                    decimal newtotal = entity.Count.Value * entity.Price.Value;
                    if (oldtotal != newtotal)
                    {
                        TPURCHASEORDEREntity orderEntity = tpurchaseorderservice.GetEntity(detailEntity.FK_PurchaseOrder);
                        if (orderEntity != null)
                        {
                            orderEntity.PurchaseOrderPrice = orderEntity.PurchaseOrderPrice + (newtotal - oldtotal);
                            tpurchaseorderservice.SaveForm(orderEntity.GUID, orderEntity);
                        }
                    }
                }
                else
                {
                    TPURCHASEORDEREntity orderEntity = tpurchaseorderservice.GetEntity(entity.FK_PurchaseOrder);
                    if (orderEntity != null)
                    {
                        decimal newtotal = entity.Count.Value * entity.Price.Value;
                        orderEntity.PurchaseOrderPrice = orderEntity.PurchaseOrderPrice + (newtotal);
                        tpurchaseorderservice.SaveForm(orderEntity.GUID, orderEntity);
                    }
                }

                if (String.IsNullOrEmpty(entity.organizeid))
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                tpurchasedetailservice.SaveForm(keyValue, entity);

                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
