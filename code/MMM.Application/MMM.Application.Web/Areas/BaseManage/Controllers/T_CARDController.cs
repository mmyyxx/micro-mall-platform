using MMM.Application.Entity.BaseManage;
using MMM.Application.Busines.BaseManage;
using MMM.Util;
using MMM.Util.WebControl;
using System.Web.Mvc;
using System;
using MMM.Application.Code;
using MMM.Application.Cache;
using System.Collections.Generic;
using MMM.Application.Entity.SystemManage.ViewModel;
using System.Linq;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using System.Drawing;

namespace MMM.Application.Web.Areas.BaseManage.Controllers
{
    /// <summary>
    /// 版 本 6.1
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：T_CARD
    /// </summary>
    public class T_CARDController : MvcControllerBase
    {
        private T_CARDBLL t_cardbll = new T_CARDBLL();
        private V_CARDBLL v_cardbll = new V_CARDBLL();
        private T_OUTCARDBLL t_outcardbll = new T_OUTCARDBLL();
        private DataItemCache dataItemCache = new DataItemCache();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_CARDIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_CARDForm()
        {
            return View();
        }

        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_CARDCreate()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            String organizeList = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeList = search["organizeid"].ToString();
            }
            else
                organizeList = organize.GetCurrentOrgId();

            var data = v_cardbll.GetPageList(pagination, search, organizeList);

            IList<DataItemModel> cardList = dataItemCache.GetDataItemList("card").ToList();
            //IList<DataItemModel> productBigTypeList = dataItemCache.GetDataItemList("ProductBigType").ToList();
            foreach (V_CARDEntity entity in data)
            {
                String cardType = entity.CardType.HasValue ? entity.CardType.ToString() : "";
                DataItemModel dm = cardList.FirstOrDefault(x => x.ItemValue == cardType);
                if (dm != null)
                {
                    entity.CardTypeName = dm.ItemName;
                }
                //dm = cardList.FirstOrDefault(x => x.ItemValue == entity.FK_AppId);
                //if (dm != null)
                //{
                //    entity.FK_AppIdName = dm.ItemName;
                //}
            }
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            IEnumerable<T_CARDEntity> data = t_cardbll.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_cardbll.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 导出卡券
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public ActionResult ExportCard(String keyValue)
        {
            IList<T_OUTCARDEntity> outCardList = t_outcardbll.ExportCard(keyValue);
            //生成card
            String logoImgPath = Cache.Const.QrCodeLogoImg;
            String temImgPath = String.Format("/TempPath/{0}/", keyValue);
            String temImgPathZip = "/TempPathZip/";

            String logoImgPath2 = Server.MapPath(logoImgPath);
            String temImgPath2 = Server.MapPath(temImgPath);
            String temImgPathZip2 = Server.MapPath(temImgPathZip);

            if (!System.IO.Directory.Exists(temImgPath2))
                System.IO.Directory.CreateDirectory(temImgPath2);

            if (!System.IO.Directory.Exists(temImgPathZip2))
                System.IO.Directory.CreateDirectory(temImgPathZip2);

            Image logoImage = Image.FromFile(logoImgPath2);
            foreach (T_OUTCARDEntity entity in outCardList)
            {
                string guidstr = "YHQ-" + entity.GUID;
                String filePath = String.Format("{0}/{1}.png", temImgPath2, guidstr);
                if (!System.IO.File.Exists(filePath))
                    QRCode.CreateQRCode(guidstr, "Byte", 9, 0, "H", filePath, true, logoImage);
            }
            logoImage.Dispose();

            SharpCode.CreateZipFile(temImgPath2, String.Format("{0}/{1}.zip", temImgPathZip2,keyValue));

            return File(String.Format("{0}/{1}.zip", temImgPathZip,keyValue), "application/octet-stream","卡券.zip");
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_CARDEntity entity = new T_CARDEntity();
                entity.Remove(keyValue);
                t_cardbll.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_CARDEntity entity)
        {
            try
            {
                t_cardbll.SaveForm(keyValue, entity);
                Session["ImgURL2"] = null;
                Session["ImgURL"] = null;
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("保存失败。");
            }
        }

        /// <summary>
        /// 生成卡券
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="CardCount"></param>
        /// <param name="FK_AppId"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveCard(String keyValue, Int32 CardCount, String FK_AppId)
        {
            try
            {
                t_outcardbll.SaveForm(keyValue, CardCount, FK_AppId);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("保存失败。");
            }
        }
        #endregion
    }
}
