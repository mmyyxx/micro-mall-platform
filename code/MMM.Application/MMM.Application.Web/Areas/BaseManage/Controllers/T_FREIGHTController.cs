using MMM.Application.Entity.BaseManage;
using MMM.Application.Busines.BaseManage;
using MMM.Util;
using MMM.Util.WebControl;
using System.Web.Mvc;
using System;
using MMM.Application.Cache;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using MMM.Util.Extension;
using MMM.Application.Code;

namespace MMM.Application.Web.Areas.BaseManage.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-19 11:23
    /// 描 述：T_FREIGHT
    /// </summary>
    public class T_FREIGHTController : MvcControllerBase
    {
        private T_FREIGHTBLL t_freightbll = new T_FREIGHTBLL();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_FREIGHTIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_FREIGHTForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            JObject search = queryJson.ToJObject();
            String organizeList = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeList = search["organizeid"].ToString();
            }
            else
                organizeList = organize.GetCurrentOrgId();
            var data = t_freightbll.GetPageList(pagination, search, organizeList);

            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_freightbll.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_FREIGHTEntity entity = new T_FREIGHTEntity();
                entity.Remove(keyValue);
                t_freightbll.SaveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_FREIGHTEntity entity)
        {
            try
            {
                t_freightbll.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        #endregion
    }
}
