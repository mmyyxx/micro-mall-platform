using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Service.BaseManage;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.BaseManage.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-24 22:44
    /// 描 述：门店
    /// </summary>
    public class T_STOREController : MvcControllerBase
    {
        private T_STOREService t_storeservice = new T_STOREService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult T_STOREIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult T_STOREForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();

            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();
            var data = t_storeservice.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="organizeId">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string organizeId)
        {
            if (String.IsNullOrEmpty(organizeId)) organizeId = organize.GetCurrentOrgId();

            String storeid = "";
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                storeid = OperatorProvider.Provider.Current().storeid;

            var data = t_storeservice.GetList(organizeId, storeid);
            return ToJsonResult(data);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="organizeId">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson2(string organizeId)
        {
            if (String.IsNullOrEmpty(organizeId)) organizeId = organize.GetCurrentOrgId();

            String storeid = "";
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                storeid = OperatorProvider.Provider.Current().storeid;

            var data = t_storeservice.GetList(organizeId, storeid);
            if (OperatorProvider.Provider.Current().IsSystem)
                foreach (T_STOREEntity store in data)
                {
                    store.GUID = store.GUID + "*" + store.organizeid;
                }
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            T_STOREEntity data = t_storeservice.GetEntity(keyValue);
            if (OperatorProvider.Provider.Current().IsSystem)
                data.StoreId = data.StoreId + "#" + data.organizeid;
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_STOREEntity entity = new T_STOREEntity();
                t_storeservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, T_STOREEntity entity)
        {
            try
            {
                if (!OperatorProvider.Provider.Current().IsSystem)
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                else
                {
                    String[] organizeids = entity.StoreId.Split('#');
                    entity.organizeid = organizeids[1];
                    entity.StoreId = organizeids[0];
                }
                if (String.IsNullOrEmpty(keyValue))
                {
                    T_STOREEntity t = t_storeservice.GetEntityByStoreId(entity.StoreId);
                    if (t != null)
                    {
                        return Error("该仓库已经分配了门店!");
                    }
                }
                t_storeservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
