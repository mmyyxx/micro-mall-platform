using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Product;
using MMM.Application.Service.BaseManage;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.WebControl;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.BaseManage.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-28 21:28
    /// 描 述：图片
    /// </summary>
    public class T_PICTUREController : MvcControllerBase
    {
        private T_PICTUREService t_pictureservice = new T_PICTUREService();
        private T_PRODUCTPRICEService t_productpriceservice = new T_PRODUCTPRICEService();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult T_PICTUREIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult T_PICTUREForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            var data = t_pictureservice.GetPageList(pagination, queryJson);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="Type">查询参数</param>
        /// <param name="FK_GUID">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(Int32? Type, String FK_GUID)
        {
            IList<T_PICTUREEntity> data = t_pictureservice.GetList(Type, FK_GUID);
            if (Type == 4 && data.Count == 0)
            {
                T_PRODUCTPRICEEntity productprice = t_productpriceservice.GetEntity(FK_GUID);
                T_PICTUREEntity price = new T_PICTUREEntity();
                if(!String.IsNullOrEmpty(productprice.ProductMainPicture))
                {
                    price.ImageURL = productprice.ProductMainPicture;
                    price.GUID = productprice.GUID;
                    data.Add(price);
                }
            }
            return ToJsonResult(data);
        }

        /// <summary>
        /// 下载图片
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="FK_GUID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownAll(Int32? Type, String FK_GUID)
        {
            IList<T_PICTUREEntity> data = t_pictureservice.GetList(Type, FK_GUID);

            String temImgPath = String.Format("/TempPathImg/");

            String temImgPath2 = Server.MapPath(temImgPath);

            if (!System.IO.Directory.Exists(temImgPath2))
                System.IO.Directory.CreateDirectory(temImgPath2);
            else
            {
                String[] files = System.IO.Directory.GetFiles(temImgPath2);
                foreach (String f in files)
                {
                    System.IO.File.Delete(f);
                }
            }

            foreach (T_PICTUREEntity entity in data)
            {
                if (entity.ImageURL.StartsWith("http:") || entity.ImageURL.StartsWith("https:"))
                {
                    String filename = temImgPath2 + entity.ImageURL.Substring(entity.ImageURL.LastIndexOf('/'));
                    if (!System.IO.File.Exists(filename))
                    {
                        try
                        {

                            HttpWebRequest request = HttpWebRequest.Create(entity.ImageURL) as HttpWebRequest;
                            HttpWebResponse response = null;
                            response = request.GetResponse() as HttpWebResponse; if (response.StatusCode != HttpStatusCode.OK) continue;
                            Stream reader = response.GetResponseStream();
                            FileStream writer = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write);
                            byte[] buff = new byte[512];
                            int c = 0;
                            while ((c = reader.Read(buff, 0, buff.Length)) > 0)
                            {
                                writer.Write(buff, 0, c);
                            }
                            writer.Close();
                            writer.Dispose();
                            reader.Close();
                            reader.Dispose();
                            response.Close();
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
                else
                {
                    String tem = Server.MapPath(entity.ImageURL);
                    if (System.IO.File.Exists(tem))
                    {
                        System.IO.File.Copy(tem, temImgPath2 + tem.Substring(tem.LastIndexOf('\\')));
                    }
                }
            }

            String zipName = String.Format("{0}/{1}.zip", temImgPath2, DateTime.Now.ToString("yyyyMMdd"));
            SharpCode.CreateZipFile(temImgPath2, zipName);

            return File(String.Format("{0}/{1}.zip", temImgPath, DateTime.Now.ToString("yyyyMMdd")), "application/octet-stream", "图片.zip");
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_pictureservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="url">图像路径</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RemoveForm(string keyValue, String url)
        {
            try
            {
                if (!String.IsNullOrEmpty(url)&&!url.StartsWith("http://") && !url.StartsWith("https://"))
                {
                    String dirPath = Server.MapPath(url);
                    if (System.IO.File.Exists(dirPath))
                    {
                        new FileInfo(dirPath).Attributes = FileAttributes.Normal;
                        System.IO.File.Delete(dirPath);
                    }
                }
                T_PICTUREEntity entity = t_pictureservice.GetEntity(keyValue);

                if (entity.Type == 4)//删除主图
                {
                    t_productpriceservice.SaveForm(entity.FK_GUID, new Entity.Product.T_PRODUCTPRICEEntity()
                    {
                        ProductMainPicture = ""
                    });
                }

                t_pictureservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, T_PICTUREEntity entity)
        {
            try
            {
                t_pictureservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
