using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Product;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using MMM.Cache.Factory;
using MMM.Application.Service.Cache;
using MMM.Data.Repository;
using MMM.Application.Entity.ShoppingMall;

namespace MMM.Application.Web.Areas.Product.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 11:03
    /// 描 述：商品
    /// </summary>
    public class T_PRODUCTPRICEController : MvcControllerBase
    {
        private T_CATEGORY_PRODUCTService t_category_productservice = new T_CATEGORY_PRODUCTService();
        private T_PRODUCTCATEGORYService t_productcategoryservice = new T_PRODUCTCATEGORYService();
        private T_PRODUCTPRICEService t_productpriceservice = new T_PRODUCTPRICEService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_PRODUCTPRICEIndex()
        {
            return View();
        }

        /// <summary>
        /// 选择页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexSearch()
        {
            return View();
        }

        /// <summary>
        /// 上架列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IndexOnline()
        {
            return View();
        }

        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_PRODUCTPRICEForm()
        {
            return View();
        }

        /// <summary>
        /// 商品分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult ProductCategory()
        {
            return View();
        }

        /// <summary>
        /// 获取tree，并根据商品选中
        /// </summary>
        /// <param name="productid"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetTreeJsonProduct(string productid)
        {
            JObject search = new JObject();
            search["FK_Product"] = productid;
            List<T_CATEGORY_PRODUCTEntity> categroyProductList = t_category_productservice.GetList(search).ToList();

            string queryJson = "";
            String oid = organize.GetCurrentOrgId();
            List<T_PRODUCTCATEGORYEntity> data = t_productcategoryservice.GetList(oid, queryJson).ToList();

            var treeList = new List<TreeEntity>();
            foreach (T_PRODUCTCATEGORYEntity item in data)
            {
                TreeEntity tree = new TreeEntity();
                bool hasChildren = data.Count(t => t.FK_ParentCategory == item.GUID) == 0 ? false : true;
                tree.id = item.GUID;
                tree.text = item.CategoryName;
                tree.value = item.GUID;
                tree.isexpand = true;
                tree.complete = true;
                tree.hasChildren = hasChildren;
                tree.parentId = item.FK_ParentCategory;
                tree.Level = item.CategoryLevel ?? 1;
                tree.checkstate = categroyProductList.Count(x => x.FK_Category == item.GUID);
                tree.showcheck = true;
                treeList.Add(tree);
            }
            return Content(treeList.TreeToJson(null));
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();

            var data = t_productpriceservice.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        /// <summary>
        /// 获取与分类有关的项目
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJsonProduct(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (search["organizeid"].IsEmpty())
            {
                organizeid = organize.GetCurrentOrgId();
            }

            var data = t_productcategoryservice.GetPageListProduct(pagination, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }


        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = t_productpriceservice.GetList(queryJson);
            return ToJsonResult(data);
        }
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_productpriceservice.GetEntity(keyValue);
            if (!data.PackingSpecification.HasValue)
                data.PackingSpecification = 1;
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                T_PRODUCTPRICEEntity entity = new T_PRODUCTPRICEEntity();
                t_productpriceservice.RemoveForm(keyValue, entity);
                CacheFactory.Cache().RemoveCache(new ProductPriceCache().cacheKey);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveFormAll(string keyValue)
        {
            try
            {
                String[] ids = keyValue.Split(',');

                List<T_PRODUCTPRICEEntity> entitys = new List<T_PRODUCTPRICEEntity>();
                foreach (String s in ids)
                {
                    T_PRODUCTPRICEEntity entity = new T_PRODUCTPRICEEntity();
                    entity.Remove(s);
                    entitys.Add(entity);
                }
                t_productpriceservice.RemoveFormAll(entitys);
                CacheFactory.Cache().RemoveCache(new ProductPriceCache().cacheKey);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_PRODUCTPRICEEntity entity)
        {
            try
            {
                if (!String.IsNullOrEmpty(keyValue))//修改是更新板块中的价格
                {
                    Data.IDatabase db = DbFactory.Base();

                    IList<TMODELPRODUCTEntity> tMODELPRODUCTs = db.IQueryable<TMODELPRODUCTEntity>().Where(t => t.ImageNavigate1 == keyValue && t.IsDel == 0).ToList();
                    IList<TMODELPRODUCTFruitsEntity> tMODELPRODUCTFruits = db.IQueryable<TMODELPRODUCTFruitsEntity>().Where(t => t.ImageNavigate1 == keyValue && t.IsDel == 0).ToList();
                    IList<TMODELPRODUCTPhoneEntity> tMODELPRODUCTPhones = db.IQueryable<TMODELPRODUCTPhoneEntity>().Where(t => t.ImageNavigate1 == keyValue && t.IsDel == 0).ToList();

                    IList<String> modelids = tMODELPRODUCTFruits.Select(t => t.FK_ModelFunction).ToList();
                    //查询是否是秒杀商品
                    IList<TMODELFUNCTIONOverloadEntity> count = db.IQueryable<TMODELFUNCTIONOverloadEntity>().Where(t => modelids.Contains(t.GUID) && t.type == "30" && t.IsDel == 0).ToList();
                    if (count.Count > 0 && entity.IsLimitTimeProduct == 1)
                    {
                        return Error("该商品已设置为秒杀商品。");
                    }
                    foreach (TMODELPRODUCTEntity tem in tMODELPRODUCTs)
                    {
                        tem.Product1 = entity.Name;
                        tem.Product2 = entity.ProductUnit;
                        tem.Price1 = entity.DiscountPrice;
                        tem.Price2 = entity.RetailPrice;
                        tem.ImageAdress1 = entity.ProductMainPicture;

                        if (entity.IsLimitTimeProduct == 0)
                        {
                            TMODELFUNCTIONEntity tMODELFUNCTIONEntity = db.IQueryable<TMODELFUNCTIONEntity>().FirstOrDefault(t => t.GUID == tem.FK_ModelFunction);
                            if (tMODELFUNCTIONEntity != null)
                                return Error("该商品已添加到【商城首页" + tMODELFUNCTIONEntity.ModelTitle + "】模块。");
                            else
                                return Error("该商品已添加到其他模块。");
                        }
                    }
                    foreach (TMODELPRODUCTFruitsEntity tem in tMODELPRODUCTFruits)
                    {
                        //判断是否是秒杀商品
                        if (count.Count(t => t.GUID == tem.FK_ModelFunction) > 0)
                        {
                            tem.Price2 = entity.RetailPrice;
                        }
                        else
                        {
                            tem.Price1 = entity.DiscountPrice;
                            tem.Price2 = entity.RetailPrice;
                        }
                        tem.Product1 = entity.Name;
                        tem.Product2 = entity.ProductUnit;
                        tem.ImageAdress1 = entity.ProductMainPicture;

                        if (entity.IsLimitTimeProduct == 0)
                        {
                            TMODELFUNCTIONOverloadEntity tMODELFUNCTIONEntity = db.IQueryable<TMODELFUNCTIONOverloadEntity>().FirstOrDefault(t => t.GUID == tem.FK_ModelFunction);
                            if (tMODELFUNCTIONEntity != null)
                                return Error("该商品已添加到【" + tMODELFUNCTIONEntity.ModelTitle + "】模块。");
                            else
                            {
                                TMODELFUNCTIONFreshEntity tMODELFUNCTIONFreshEntity = db.IQueryable<TMODELFUNCTIONFreshEntity>().FirstOrDefault(t => t.GUID == tem.FK_ModelFunction);
                                if (tMODELFUNCTIONFreshEntity != null)
                                    return Error("该商品已添加到【" + tMODELFUNCTIONFreshEntity.ModelTitle + "】模块。");
                                else
                                {
                                    TMODELFUNCTIONFruitsEntity tMODELFUNCTIONFruitsEntity = db.IQueryable<TMODELFUNCTIONFruitsEntity>().FirstOrDefault(t => t.GUID == tem.FK_ModelFunction);
                                    if (tMODELFUNCTIONFruitsEntity != null)
                                        return Error("该商品已添加到【" + tMODELFUNCTIONFruitsEntity.ModelTitle + "】模块。");
                                    else
                                        return Error("该商品已添加到其他模块。");
                                }
                            }
                        }
                    }
                    foreach (TMODELPRODUCTPhoneEntity tem in tMODELPRODUCTPhones)
                    {
                        tem.Product1 = entity.Name;
                        tem.Product2 = entity.ProductUnit;
                        tem.Price1 = entity.DiscountPrice;
                        tem.Price2 = entity.RetailPrice;
                        tem.ImageAdress1 = entity.ProductMainPicture;

                        if (entity.IsLimitTimeProduct == 0)
                        {
                            TMODELFUNCTIONPhoneEntity tMODELFUNCTIONEntity = db.IQueryable<TMODELFUNCTIONPhoneEntity>().FirstOrDefault(t => t.GUID == tem.FK_ModelFunction);
                            if (tMODELFUNCTIONEntity != null)
                                return Error("该商品已添加到【" + tMODELFUNCTIONEntity.ModelTitle + "】模块。");
                            else
                            {
                                TMODELFUNCTIONDigitalEntity tMODELFUNCTIONDigitalEntity = db.IQueryable<TMODELFUNCTIONDigitalEntity>().FirstOrDefault(t => t.GUID == tem.FK_ModelFunction);
                                if (tMODELFUNCTIONDigitalEntity != null)
                                    return Error("该商品已添加到【" + tMODELFUNCTIONDigitalEntity.ModelTitle + "】模块。");
                                else
                                    return Error("该商品已添加到其他模块。");
                            }
                        }
                    }
                    try
                    {
                        db.BeginTrans();

                        db.Update<TMODELPRODUCTEntity>(tMODELPRODUCTs, null);
                        db.Update<TMODELPRODUCTFruitsEntity>(tMODELPRODUCTFruits, null);
                        db.Update<TMODELPRODUCTPhoneEntity>(tMODELPRODUCTPhones, null);

                        entity.GUID = keyValue;
                        entity.ModifyTime = DateTime.Now;
                        entity.Modifyer = OperatorProvider.Provider.Current().Account;
                        db.Update<T_PRODUCTPRICEEntity>(entity, null);

                        db.Commit();
                    }
                    catch (Exception ex)
                    {
                        db.Rollback();
                        throw new Exception(ex.ToString());
                    }
                }
                else
                {
                    t_productpriceservice.SaveForm(keyValue, entity);
                }


                CacheFactory.Cache().RemoveCache(new ProductPriceCache().cacheKey);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 保存产品分类
        /// </summary>
        /// <param name="keyValue">产品主键</param>
        /// <param name="cagetroy">分类id</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveProductCategroy(String keyValue, String cagetroy)
        {
            try
            {
                String[] categroys = cagetroy.Split(',');
                List<T_CATEGORY_PRODUCTEntity> cateproductList = new List<T_CATEGORY_PRODUCTEntity>();
                foreach (String s in categroys)
                {
                    T_CATEGORY_PRODUCTEntity tem = new T_CATEGORY_PRODUCTEntity();
                    tem.Create();
                    tem.FK_Category = s;
                    tem.FK_Product = keyValue;
                    cateproductList.Add(tem);
                }
                t_category_productservice.SaveForm(keyValue, cateproductList);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 商品上架
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult UpdateProductIsSale(String[] ids, String issale)
        {
            try
            {
                List<T_PRODUCTPRICEEntity> cateproductList = new List<T_PRODUCTPRICEEntity>();
                foreach (String s in ids)
                {
                    T_PRODUCTPRICEEntity tem = new T_PRODUCTPRICEEntity();
                    tem.GUID = s;
                    tem.IsSale = Convert.ToInt32(issale);
                    cateproductList.Add(tem);
                }
                t_productpriceservice.SaveForm(cateproductList);
                CacheFactory.Cache().RemoveCache(new ProductPriceCache().cacheKey);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
