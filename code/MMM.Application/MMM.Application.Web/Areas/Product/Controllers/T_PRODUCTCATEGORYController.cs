using MMM.Application.Cache;
using MMM.Application.Entity.Product;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.WebControl;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using MMM.Application.Code;

namespace MMM.Application.Web.Areas.Product.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-27 19:51
    /// 描 述：商品分类
    /// </summary>
    public class T_PRODUCTCATEGORYController : MvcControllerBase
    {
        private T_CATEGORY_PRODUCTService t_category_productservice = new T_CATEGORY_PRODUCTService();
        private T_PRODUCTCATEGORYService t_productcategoryservice = new T_PRODUCTCATEGORYService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_PRODUCTCATEGORYIndex()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult T_PRODUCTCATEGORYForm()
        {
            return View();
        }

        /// <summary>
        /// 商品归类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult CategoryProduct()
        {
            return View();
        }

        /// <summary>
        /// 分类图片
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult CategoryPic()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            String organizeid = null;
            if (!search["organizeid"].IsEmpty())
            {
                organizeid = search["organizeid"].ToString();
            }
            else
                organizeid = organize.GetCurrentOrgId();

            var data = t_productcategoryservice.GetPageList(pagination, organizeid, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="keyword">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetTreeJson(string keyword)
        {
            string queryJson = "";
            String oid = organize.GetCurrentOrgId();
            List<T_PRODUCTCATEGORYEntity> data = t_productcategoryservice.GetList(oid, queryJson).ToList();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.TreeWhere(t => t.CategoryName.Contains(keyword),"GUID", "FK_ParentCategory");
            }

            var treeList = new List<TreeEntity>();
            foreach (T_PRODUCTCATEGORYEntity item in data)
            {
                TreeEntity tree = new TreeEntity();
                bool hasChildren = data.Count(t => t.FK_ParentCategory == item.GUID) == 0 ? false : true;
                tree.id = item.GUID;
                tree.text = item.CategoryName;
                tree.value = item.GUID;
                tree.isexpand = true;
                tree.complete = true;
                tree.hasChildren = hasChildren;
                tree.parentId = item.FK_ParentCategory;
                tree.Level = item.CategoryLevel ?? 1;
                treeList.Add(tree);
            }
            return Content(treeList.TreeToJson(null));
        }

        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_productcategoryservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }

        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                IList<T_PRODUCTCATEGORYEntity> childList = t_productcategoryservice.GetChild(keyValue);
                if(childList!=null&& childList.Count > 0)
                {
                    return Error("当前分类存在子分类,无法删除。");
                }

                T_PRODUCTCATEGORYEntity entity = new T_PRODUCTCATEGORYEntity();
                t_productcategoryservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, T_PRODUCTCATEGORYEntity entity)
        {
            try
            {
                t_productcategoryservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
