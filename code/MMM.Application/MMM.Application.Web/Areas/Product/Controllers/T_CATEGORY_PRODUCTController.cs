using MMM.Application.Code;
using MMM.Application.Entity.Product;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.WebControl;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Product.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品分类
    /// </summary>
    public class T_CATEGORY_PRODUCTController : MvcControllerBase
    {
        private T_CATEGORY_PRODUCTService t_category_productservice = new T_CATEGORY_PRODUCTService();

        #region 获取数据
        #endregion

        #region 提交数据

        /// <summary>
        /// 更新商品分类
        /// </summary>
        /// <param name="ids">商品id</param>
        /// <param name="FK_Category"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult UpdateCateGoryProduct(IList<String> ids,String FK_Category)
        {
            try
            {
                t_category_productservice.UpdateCateGoryProduct(ids, FK_Category);
                return Success("保存成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("保存失败。");
            }
        }

        /// <summary>
        /// 移除商品分类
        /// </summary>
        /// <param name="ids">商品id</param>
        /// <param name="FK_Category"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveCateGoryProduct(IList<String> ids, String FK_Category)
        {
            try
            {
                t_category_productservice.RemoveCateGoryProduct(ids, FK_Category);
                return Success("保存成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("保存失败。");
            }
        }
        #endregion
    }
}
