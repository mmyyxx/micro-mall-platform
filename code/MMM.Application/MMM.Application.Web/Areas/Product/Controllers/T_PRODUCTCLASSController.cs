using MMM.Application.Cache;
using MMM.Application.Entity.Product;
using MMM.Application.Service.Product;
using MMM.Util;
using MMM.Util.WebControl;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Newtonsoft.Json.Linq;
using MMM.Application.Code;
using MMM.Cache.Factory;
using MMM.Application.Service.Cache;

namespace MMM.Application.Web.Areas.Product.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品类别
    /// </summary>
    public class T_PRODUCTCLASSController : MvcControllerBase
    {
        private T_PRODUCTCLASSService t_productclassservice = new T_PRODUCTCLASSService();
        private T_PRODUCTPRICEService t_productpriceservice = new T_PRODUCTPRICEService();
        OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            var data = t_productclassservice.GetPageList(pagination, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetListJson(string queryJson)
        {
            var data = t_productclassservice.GetList(queryJson,"");
            return ToJsonResult(data);
        }

        /// <summary>
        /// 获取树信息
        /// </summary>
        /// <param name="productGuid"></param>
        /// <param name="keyword">查询参数</param>
        /// <returns>返回列表Json</returns>
        [HttpGet]
        public ActionResult GetTreeJson(String productGuid,string keyword)
        {
            String oid = organize.GetCurrentOrgId();
            List<T_PRODUCTCLASSEntity> data = t_productclassservice.GetList(oid, productGuid).ToList();
            if (!string.IsNullOrEmpty(keyword))
            {
                data = data.TreeWhere(t => t.ProductClass.Contains(keyword), "GUID", "FK_Product");
            }

            var treeList = new List<TreeEntity>();
            foreach (T_PRODUCTCLASSEntity item in data)
            {
                TreeEntity tree = new TreeEntity();
                bool hasChildren = data.Count(t => t.FK_ParentClass == item.GUID) == 0 ? false : true;
                tree.id = item.GUID;
                tree.text = item.ProductClass+"("+item.ClassValue+")";
                tree.value = item.GUID;
                tree.isexpand = true;
                tree.complete = true;
                tree.hasChildren = hasChildren;
                tree.parentId = item.FK_ParentClass;
                treeList.Add(tree);
            }
            return Content(treeList.TreeToJson(null));
        }

        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = t_productclassservice.GetEntity(keyValue);
            return ToJsonResult(data);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                IList<T_PRODUCTCLASSEntity> childList = t_productclassservice.GetChild(keyValue);
                if (childList != null && childList.Count > 0)
                {
                    return Error("当前分类存在子分类,无法删除。");
                }

                T_PRODUCTCLASSEntity entity = new T_PRODUCTCLASSEntity();
                entity.Remove(keyValue);
                t_productclassservice.SaveForm(keyValue, entity);
                CacheFactory.Cache().RemoveCache(new ProductClassCache().cacheKey);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除成功。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        public ActionResult SaveForm(string keyValue, T_PRODUCTCLASSEntity entity)
        {
            try
            {
                if (!OperatorProvider.Provider.Current().IsSystem||String.IsNullOrEmpty(entity.FK_Product))
                {
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                }
                else //后去商品的organizeid
                {
                    T_PRODUCTPRICEEntity product = t_productpriceservice.GetEntity(entity.FK_Product);
                    entity.organizeid = product.organizeid;
                }

                t_productclassservice.SaveForm(keyValue, entity);
                CacheFactory.Cache().RemoveCache(new ProductClassCache().cacheKey);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
