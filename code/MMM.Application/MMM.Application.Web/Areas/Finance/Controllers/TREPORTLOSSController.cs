using MMM.Application.Cache;
using MMM.Application.Code;
using MMM.Application.Entity.Finance;
using MMM.Application.Entity.Materiel;
using MMM.Application.Service.Finance;
using MMM.Application.Service.Materiel;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.Offices;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace MMM.Application.Web.Areas.Finance.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-20 16:38
    /// 描 述：报损
    /// </summary>
    public class TREPORTLOSSController : MvcControllerBase
    {
        private TREPORTLOSSService treportlossservice = new TREPORTLOSSService();
        private TPRODUCTService tproductservice = new TPRODUCTService();
        private OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 表单页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Form()
        {
            return View();
        }
        
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();

            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;

            var data = treportlossservice.GetPageList(pagination, search);
            var jsonData = new
            {
                rows = data,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }
        
        /// <summary>
        /// 获取实体 
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns>返回对象Json</returns>
        [HttpGet]
        public ActionResult GetFormJson(string keyValue)
        {
            var data = treportlossservice.GetEntity(keyValue);
            ReportLossModel loss = new ReportLossModel();
            loss.CostPrice = data.CostPrice??0;
            loss.CostTotal = data.CostTotal??0;
            loss.PdtCount = data.PdtCount??0;
            loss.FK_Product = data.FK_Product;
            if (OperatorProvider.Provider.Current().IsSystem)
            {
                loss.FK_WarehouseNo = data.FK_WarehouseNo + "*" + data.organizeid;
            }
            TPRODUCTEntity product = tproductservice.GetEntity(data.FK_Product);
            loss.SKU = product.SKU;
            loss.PdtName = product.PdtName;
            return ToJsonResult(loss);
        }

        /// <summary>
        /// 导出数据
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <param name="storeGUID"></param>
        [HttpGet]
        public void export(String sku,String startdate,String enddate,String storeGUID)
        {
            JObject j = new JObject();
            j["startdate"] = startdate;
            j["enddate"] = enddate;
            j["organizeid"] = organize.GetCurrentOrgId();
            if (String.IsNullOrEmpty(storeGUID))
                j["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            else j["storeGUID"] = storeGUID;
            IList<ReportLossModel> orderexportList = treportlossservice.GetList(j);

            DataTable dt = new DataTable();
            dt.Columns.Add("SKU");
            dt.Columns.Add("PdtName");
            dt.Columns.Add("PdtCount");
            dt.Columns.Add("CostPrice");
            dt.Columns.Add("CostTotal");
            dt.Columns.Add("Remark");
            foreach (ReportLossModel detail in orderexportList)
            {
                dt.Rows.Add(new String[] { detail.SKU,detail.PdtName,detail.PdtCount.ToString(),
                    detail.CostPrice.ToString(),detail.CostTotal.ToString(),(detail.Remark)
                });
            }
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.TitleFont = "微软雅黑";
            excelconfig.TitlePoint = 25;
            excelconfig.FileName = String.Format("报损报表{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            excelconfig.IsAllSizeColumn = true;

            List<ColumnEntity> listColumnEntity = new List<ColumnEntity>();
            excelconfig.ColumnEntity = listColumnEntity;
            ColumnEntity columnentity = new ColumnEntity();
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "SKU", ExcelColumn = "商品条码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "PdtName", ExcelColumn = "商品名称" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "PdtCount", ExcelColumn = "报损数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CostPrice", ExcelColumn = "成本单价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CostTotal", ExcelColumn = "成本金额" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Remark", ExcelColumn = "备注" });
            //调用导出方法
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult RemoveForm(string keyValue)
        {
            try
            {
                TREPORTLOSSEntity entity = new TREPORTLOSSEntity();
                treportlossservice.RemoveForm(keyValue, entity);
                return Success("删除成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("删除失败。");
            }
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult SaveForm(string keyValue, TREPORTLOSSEntity entity)
        {
            try
            {
                if (!OperatorProvider.Provider.Current().IsSystem)
                    entity.organizeid = OperatorProvider.Provider.Current().CompanyId;
                else
                {
                    String[] organizeids = entity.FK_WarehouseNo.Split('*');
                    entity.organizeid = organizeids[1];
                    entity.FK_WarehouseNo = organizeids[0];
                }
                treportlossservice.SaveForm(keyValue, entity);
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }

        /// <summary>
        /// 报损出库
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AjaxOnly]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult LossSendSave(Int32 type, String keyValue)
        {
            try
            {
                String str = treportlossservice.SaveOrderSend(type, keyValue);
                if (!String.IsNullOrEmpty(str))
                {
                    return Error(str);
                }
                return Success("操作成功。");
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                return Error("操作失败。");
            }
        }
        #endregion
    }
}
