using MMM.Application.Cache;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.SystemManage.ViewModel;
using MMM.Application.Service.Order;
using MMM.Util;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using MMM.Util.Extension;
using MMM.Application.Code;
using MMM.Application.Service.Product;
using MMM.Application.Entity.Product;
using MMM.Application.Service;
using System.Data;
using MMM.Util.Offices;
using MMM.Application.Service.Finance;
using MMM.Application.Entity.Finance;

namespace MMM.Application.Web.Areas.Finance.Controllers
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-14 19:23
    /// 描 述：财务报表
    /// </summary>
    public class CostManageController : MvcControllerBase
    {
        private CostManageService costManageService = new CostManageService();
        private OrganizeCache organize = new OrganizeCache();

        #region 视图功能
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public ActionResult Index()
        {
            return View();
        }
        
        #endregion

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页参数</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表Json</returns>
        [HttpGet]
        public ActionResult GetPageListJson(Pagination pagination, string queryJson)
        {
            var watch = CommonHelper.TimerStart();
            JObject search = queryJson.ToJObject();
            
            if (search["organizeid"].IsEmpty())
            {
                search["organizeid"] = organize.GetCurrentOrgId();
            }
            if (!String.IsNullOrEmpty(OperatorProvider.Provider.Current().storeid))
                search["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            IList<CostModel> orderlist = costManageService.GetPageList(pagination, search);
            
            var jsonData = new
            {
                rows = orderlist,
                total = pagination.total,
                page = pagination.page,
                records = pagination.records,
                costtime = CommonHelper.TimerEnd(watch)
            };
            return ToJsonResult(jsonData);
        }

        

        /// <summary>
        /// 导出数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HandlerAuthorize(PermissionMode.Enforce)]
        public void export(String storeGUID, String startdate, String enddate)
        {
            JObject j = new JObject();
            j["startdate"] = startdate;
            j["enddate"] = enddate;
            j["organizeid"] = organize.GetCurrentOrgId();
            if (String.IsNullOrEmpty(storeGUID))
                j["storeGUID"] = OperatorProvider.Provider.Current().storeid;
            else
                j["storeGUID"] = storeGUID;

            IList<CostModel> orderexportList = costManageService.GetList(j);

            DataTable dt = new DataTable();
            dt.Columns.Add("SKU");
            dt.Columns.Add("ProductName");
            dt.Columns.Add("SaleCount");
            dt.Columns.Add("SalePrice");
            dt.Columns.Add("SaleTotal");
            dt.Columns.Add("CostPrice");
            dt.Columns.Add("CostTotal");
            dt.Columns.Add("Maori");
            dt.Columns.Add("GrossInterestRate");
            dt.Columns.Add("Creater");
            foreach (CostModel detail in orderexportList)
            {
                dt.Rows.Add(new String[] { detail.SKU,detail.ProductName,
                    Decimal.Round(detail.SaleCount, 2).ToString(),(detail.SalePrice).ToString(),
                detail.SaleTotal.ToString(),detail.CostPrice.ToString(),detail.CostTotal.ToString(),
                (detail.Maori).ToString(),detail.GrossInterestRate.ToString(),detail.Creater
                });
            }
            //设置导出格式
            ExcelConfig excelconfig = new ExcelConfig();
            excelconfig.TitleFont = "微软雅黑";
            excelconfig.TitlePoint = 25;
            excelconfig.FileName = String.Format("财务报表{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            excelconfig.IsAllSizeColumn = true;

            List<ColumnEntity> listColumnEntity = new List<ColumnEntity>();
            excelconfig.ColumnEntity = listColumnEntity;
            ColumnEntity columnentity = new ColumnEntity();
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "SKU", ExcelColumn = "商品条码" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "ProductName", ExcelColumn = "商品名称" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "SaleCount", ExcelColumn = "销售数量" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "SalePrice", ExcelColumn = "销售单价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "SaleTotal", ExcelColumn = "价税合计" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CostPrice", ExcelColumn = "成本单价" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "CostTotal", ExcelColumn = "成本金额" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Maori", ExcelColumn = "毛利" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "GrossInterestRate", ExcelColumn = "毛利率" });
            excelconfig.ColumnEntity.Add(new ColumnEntity() { Column = "Creater", ExcelColumn = "员工" });
            //调用导出方法
            ExcelHelper.ExcelDownload(dt, excelconfig);
        }
        #endregion

        #region 提交数据
        
        #endregion
    }
}
