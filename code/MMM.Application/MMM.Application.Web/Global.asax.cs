﻿using MMM.Application.Service.Cache;
using System;
using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MMM.Application.Web
{
    /// <summary>
    /// 应用程序全局设置
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        /// <summary>
        /// 启动应用程序 
        /// </summary>
        protected void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleTable.EnableOptimizations = false;
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer<Data.EF.SqlServerDbContext>(null);
            //Database.SetInitializer<Data.EF.SqlServerDbContext>(new DropCreateDatabaseIfModelChanges<Data.EF.SqlServerDbContext>());

            new ProductClassCache().GetCacheList();
            new ProductPriceCache().GetCacheList();
        }

        private bool IsNotExcuteAspx()
        {
            string extension = Context.Request.CurrentExecutionFilePathExtension;
            bool bo = extension.Equals(".css", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".js", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".jpg", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".gif", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".png", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".jpeg", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".ico", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".swf", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".html", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".htm", StringComparison.OrdinalIgnoreCase)
                || extension.Equals(".pdf", StringComparison.OrdinalIgnoreCase)
                ;
            return bo;
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            //string requestPath = Context.Request.FilePath;
            //String rawUrl = HttpContext.Current.Request.RawUrl.ToLower();

            //if (IsNotExcuteAspx())
            //    return;

            //Util.Log.Log _logger = Util.Log.LogFactory.GetLogger(this.GetType().ToString());
            //_logger.Info("-begin-" + DateTime.Now.ToString("o") + "\r\n");
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //string requestPath = Context.Request.FilePath;
            //String rawUrl = HttpContext.Current.Request.RawUrl.ToLower();

            //if (IsNotExcuteAspx())
            //    return;

            //Util.Log.Log _logger = Util.Log.LogFactory.GetLogger(this.GetType().ToString());
            //_logger.Info(rawUrl + "-end-" + DateTime.Now.ToString("o") + "\r\n");
        }

        /// <summary>
        /// 应用程序错误处理
        /// </summary>
        protected void Application_Error(object sender, EventArgs e)
        {
            var lastError = Server.GetLastError();
        }
    }
}