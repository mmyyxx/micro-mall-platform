﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MMM.Application.Web
{
    /// <summary>
    /// webservice 头文件
    /// </summary>
    public class CYSSoapHeader : System.Web.Services.Protocols.SoapHeader
    {
        //webservice访问用户名
        private string _uname = string.Empty;

        public string Uname
        {
            get { return _uname; }
            set { _uname = value; }
        }

        //webservice访问密码
        private string _password = string.Empty;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public CYSSoapHeader()
        {
            //  
            //TODO: 在此处添加构造函数逻辑  
            //  
        }
        public CYSSoapHeader(string uname, string upass)
        {
            init(uname, upass);
        }
        private void init(string uname, string upass)
        {
            this._password = upass;
            this._uname = uname;
        }
        //验证用户是否有权访问内部接口  
        private bool isValid(string uname, string upass, out string msg)
        {
            msg = "";
            if (uname == "admin" && upass == "AADFB17FDD412128ED14F47955A57B5D")
            {
                return true;
            }
            else
            {
                msg = "对不起！您无权调用此WebService！";
                return false;
            }
        }
        //验证用户是否有权访问外部接口  
        public bool isValid(out string msg)
        {
            return isValid(_uname, _password, out msg);
        }
    }
}