﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MMM.Application.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{*x}", new { x=@".*\.asmx(/.*)?"});
            routes.IgnoreRoute("{*x}", new { x=@".*\.aspx(/.*)?"});
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}