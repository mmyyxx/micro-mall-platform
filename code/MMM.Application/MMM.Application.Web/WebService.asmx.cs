﻿using MMM.Application.Busines.BaseManage;
using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Entity.WebService;
using MMM.Application.Service;
using MMM.Application.Service.BaseManage;
using MMM.Application.Service.Order;
using MMM.Application.Service.Product;
using MMM.Application.Service.WSC;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace MMM.Application.Web
{
    /// <summary>
    /// WebService 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        public CYSSoapHeader myheader = new CYSSoapHeader();

        Log _logger = LogFactory.GetLogger("webservice");

        #region 接口验证

        private bool ServiceValid()
        {
            string msg = "";

            if (!myheader.isValid(out msg))
            {
                return false;
            }
            return true;
        }

        #endregion

        [SoapHeader("myheader")]
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        #region 登录
        [SoapHeader("myheader")]
        [WebMethod(Description = "获取收银最新版本号")]
        public string GetStoreVersion()
        {
            if (!ServiceValid())
            {
                return string.Empty;
            }
            String storeVersion = System.Configuration.ConfigurationManager.AppSettings["StoreVersion"];
            return storeVersion;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "用户登录")]
        public ResultData StoreUserLogin(string warehouseNo, string userId, string password)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            UserBLL dal = new UserBLL();
            ResultData result = new ResultData();
            try
            {
                UserEntity userEntity = dal.CheckLogin(userId, Md5Helper.MD5(password, 32));
                //if (String.IsNullOrEmpty(userEntity.storeid) || userEntity.storeid != warehouseNo)
                //{
                //    result.msg = "没有登录权限!";
                //    return result;
                //}
                result.Data = userEntity.UserId;
                result.msg = userEntity.RealName;
                result.organizeid = userEntity.OrganizeId;
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }

            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取门店信息")]
        public ResultData GetWarehouseData(string organizeid = "")
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            if (String.IsNullOrEmpty(organizeid)) organizeid = CodeConst.DefaultOrganize;
            T_STOREService dal = new T_STOREService();
            ResultData result = new ResultData();
            try
            {
                IList<T_STOREEntity> storeList = dal.GetList(organizeid);
                var data = storeList.Select(t => new
                {
                    Code = t.GUID,
                    Name = t.StoreName
                });
                result.Data = data.ToJson();
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }
        #endregion

        [SoapHeader("myheader")]
        [WebMethod(Description = "根据SKU获取商品信息")]
        public ResultData GetProductBySKU(string sku, string organizeid = "")
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            if (String.IsNullOrEmpty(sku))
            {
                return new ResultData()
                {
                    msg = "请输入sku!"
                };
            }
            if (String.IsNullOrEmpty(organizeid)) organizeid = CodeConst.DefaultOrganize;

            VPRODUCTStoreService vproductStoreService = new VPRODUCTStoreService();
            ResultData result = new ResultData();
            try
            {
                JObject search = new JObject();
                search["organizeid"] = organizeid;
                search["sku"] = sku;
                VPRODUCTStoreEntity productstore = vproductStoreService.GetEntity(search);
                var data = new
                {
                    ProductSKU = productstore.SKU,
                    ProductName = productstore.PdtName,
                    ProductCount = 1,
                    ProductPrice = productstore.Price == null ? Decimal.Zero : productstore.Price.Value,
                    SalePrice = productstore.SalePrice ?? 0,
                    ProductTotal = productstore.Price == null ? Decimal.Zero : productstore.Price.Value
                };
                result.Data = data.ToJson();
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "查询门店商品信息")]
        public ResultData GetStoreProductInfo(InputData input, ref DividPage dividPage)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            VPRODUCTStoreService vproductStoreService = new VPRODUCTStoreService();
            ResultData result = new ResultData();
            try
            {
                TPRODUCTEntity product = JSONHelper.DecodeObject<TPRODUCTEntity>(input.value);
                JObject search = new JObject();
                search["organizeid"] = String.IsNullOrEmpty(product.organizeid) ? CodeConst.DefaultOrganize : product.organizeid;
                search["sku"] = product.ProductNo;
                search["PdtName"] = product.PdtName;

                Pagination pagination = new Pagination();
                pagination.rows = dividPage.CurrentPageShowCounts;
                pagination.page = dividPage.CurrentPageNumber;
                pagination.sidx = "SKU";
                pagination.sord = "DESC";
                List<VPRODUCTStoreEntity> productstoreList = vproductStoreService.GetList(pagination, search);

                result.Data = productstoreList.ToJson();
                result.IsOK = true;

                dividPage.RecordCounts = pagination.records;

                if (dividPage.RecordCounts > 0)
                {
                    if (dividPage.CurrentPageShowCounts <= 0) dividPage.PagesCount = 0;
                    else dividPage.PagesCount = (dividPage.RecordCounts + dividPage.CurrentPageShowCounts - 1) / dividPage.CurrentPageShowCounts;

                    if (dividPage.CurrentPageNumber < dividPage.PagesCount) dividPage.CurrentPageRecordCounts = dividPage.CurrentPageShowCounts;
                    else dividPage.CurrentPageRecordCounts = dividPage.RecordCounts % dividPage.CurrentPageShowCounts;
                    if (dividPage.CurrentPageRecordCounts == 0) dividPage.CurrentPageRecordCounts = dividPage.CurrentPageShowCounts;
                }
                else
                {
                    dividPage.PagesCount = 0;
                    dividPage.CurrentPageRecordCounts = 0;
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取订单信息")]
        public ResultData GetStoreOrder(InputData input)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }

            ResultData result = new ResultData();
            try
            {
                string[] list = input.valuelist.ToObject<string[]>();

                string warehouseNo = list[0].ToString();

                if (String.IsNullOrEmpty(warehouseNo))
                {
                    result.msg = "必须传入门店号!";
                    return result;
                }

                DateTime starttime = DateTime.Parse(list[1]);
                DateTime endtime = DateTime.Parse(list[2]);

                JObject search = new JObject();
                search["startdate"] = starttime;
                search["enddate"] = endtime;
                search["storeGUID"] = warehouseNo;

                TORDERService orderService = new TORDERService();
                IList<TORDEREntity> orderList = orderService.GetList(search);
                List<T_ORDER> torderList = new List<T_ORDER>();
                foreach (TORDEREntity order in orderList)
                {
                    T_ORDER torder = new T_ORDER()
                    {
                        GUID = order.GUID,
                        FK_WarehouseNo = order.FK_PlanStore,
                        OrderNumber = order.OrderNumber,
                        PayWay = order.PayWay ?? 0,
                        OrderPrice = order.OrderPrice ?? 0,
                        OrderRemark = order.OrderRemark,
                        IsStockOut = order.OrderStatus ?? 0,
                        Creater = order.Creater,
                        CreateTime = order.CreateTime,
                    };
                    torderList.Add(torder);
                }
                result.Data = JSONHelper.ObjectToJson(torderList);
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }

            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "根据订单主键获取明细信息")]
        public ResultData GetProductByOrder(string orderguid)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            ResultData result = new ResultData();
            try
            {
                VORDERDETAILStoreService vORDERDETAILStoreService = new VORDERDETAILStoreService();
                JObject search = new JObject();
                search["FK_Order"] = orderguid;
                var data = vORDERDETAILStoreService.GetList(search).Select(p => new
                {
                    ProductSKU = p.ProductSKU,
                    ProductName = p.ProductName,
                    ProductCount = p.ProductCount,
                    ProductPrice = p.ProductPrice.Value,
                    ProductTotal = p.ProductTotal.Value,
                    OrderTime = p.OrderTime,
                    Creater = p.Creater
                }).ToList();
                result.Data = data.ToJson();
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "保存订单")]
        public ResultData SaveStoreOrder(InputData input)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            ResultData result = new ResultData();
            try
            {
                T_ORDER order = JSONHelper.DecodeObject<T_ORDER>(input.value);
                TORDEREntity orderentity = new TORDEREntity();
                orderentity.GUID = Guid.NewGuid().ToString();
                orderentity.FK_Openid = order.Creater;
                orderentity.CreateTime = DateTime.Now;
                orderentity.Creater = order.Creater;
                orderentity.IsDel = 0;
                orderentity.FK_PlanStore = order.FK_WarehouseNo;
                orderentity.Count = 1;
                orderentity.OrderNumber = order.OrderNumber;
                orderentity.PayWay = order.PayWay;
                orderentity.PayResult = 0;
                orderentity.IsGroupOrder = 1;
                orderentity.OrderPrice = order.OrderPrice;
                orderentity.OrderRemark = order.OrderRemark;
                orderentity.OrderStatus = CodeConst.IsYesNO.NO;
                orderentity.OrderType = "0";

                List<T_ORDERDETAIL> detaillist = JSONHelper.DecodeObject<List<T_ORDERDETAIL>>(input.valuelist);
                IList<TORDERDETAILEntity> detailentityList = new List<TORDERDETAILEntity>();
                foreach (T_ORDERDETAIL detail in detaillist)
                {
                    TORDERDETAILEntity detailentity = new TORDERDETAILEntity();
                    detailentity.GUID = Guid.NewGuid().ToString();
                    detailentity.CreateTime = DateTime.Now;
                    detailentity.Creater = detail.Creater;
                    detailentity.IsDel = 0;
                    detailentity.FK_Order = orderentity.GUID;
                    detailentity.ProductSKU = detail.ProductSKU;
                    detailentity.Count = detail.Count;
                    detailentity.Price = detail.Price;
                    detailentity.Status = CodeConst.IsYesNO.NO;
                    detailentityList.Add(detailentity);
                }
                TORDERService tORDERService = new TORDERService();
                tORDERService.SaveOrderDetail(orderentity, detailentityList);

                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "库存更新")]
        public bool ProductOutStock(string warehouseNo, string operatorId, string orderId, string dicstr)
        {
            if (!ServiceValid())
            {
                return false;
            }
            ResultData result = new ResultData();
            try
            {
                Dictionary<string, decimal> dic = JSONHelper.DecodeObject<Dictionary<string, decimal>>(dicstr);

                TORDERService tORDERService = new TORDERService();
                Boolean str = tORDERService.ProductOutStock(warehouseNo, operatorId, orderId, dic);
                result.IsOK = str;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result.IsOK;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取退货单信息")]
        public ResultData GetStoreOrderReturn(InputData input)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            ResultData result = new ResultData();
            try
            {
                string[] list = input.valuelist.ToObject<string[]>();

                string warehouseNo = list[0].ToString();

                if (String.IsNullOrEmpty(warehouseNo))
                {
                    result.msg = "必须传入门店号!";
                    return result;
                }

                DateTime starttime = DateTime.Parse(list[1]);
                DateTime endtime = DateTime.Parse(list[2]);

                JObject search = new JObject();
                search["startdate"] = starttime;
                search["enddate"] = endtime;
                search["storeGUID"] = warehouseNo;

                TORDERReturnService orderService = new TORDERReturnService();
                IList<TORDERReturnEntity> orderList = orderService.GetList(search);
                List<T_ORDERReturn> torderList = new List<T_ORDERReturn>();
                foreach (TORDERReturnEntity order in orderList)
                {
                    T_ORDERReturn torder = new T_ORDERReturn()
                    {
                        GUID = order.GUID,
                        FK_WarehouseNo = order.FK_WarehouseNo,
                        OrderNumber = order.OrderNumber,
                        ReturnNumber = order.ReturnNumber,
                        OrderPrice = order.OrderPrice ?? 0,
                        OrderRemark = order.OrderRemark,
                        Creater = order.Creater,
                        CreateTime = order.CreateTime,
                    };
                    torderList.Add(torder);
                }
                result.Data = JSONHelper.ObjectToJson(torderList);
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }

            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "根据退货单主键获取明细信息")]
        public ResultData GetProductByOrderReturn(string orderguid)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            ResultData result = new ResultData();
            try
            {
                TORDERReturnDetailService torderreturnservice = new TORDERReturnDetailService();
                IList<ProductItem> orderreturnDetailList = torderreturnservice.GetStoreOrderDetail(orderguid);

                result.Data = JSONHelper.ObjectToJson(orderreturnDetailList);
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "商品退货")]
        public ResultData ProductReturnStore(InputData input)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            ResultData result = new ResultData();
            try
            {
                TORDERReturnService orderreturnService = new TORDERReturnService();
                result = orderreturnService.ProductReturnStore(input);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取小程序订单")]
        public ResultData GetWxOrder(String warehourse, String openid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetOrderListShouyin(openid);
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取小程序订单详情")]
        public ResultData GetOrderShouyinDetail(string orderguid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetOrderShouyinDetail(orderguid);
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "完成小程序订单")]
        public ResultData ComOrderShouyinDetail(String warehourse, string orderguid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.ComOrderShouyinDetail(warehourse, orderguid);
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取导出数据")]
        public ResultData GetExportStoreOrderInfo(InputData input)
        {
            if (!ServiceValid())
            {
                return new ResultData();
            }
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();

                string warehouseNo = input.value;
                DateTime starttime = JSONHelper.DecodeObject<List<DateTime>>(input.valuelist)[0].Date;
                DateTime endtime = JSONHelper.DecodeObject<List<DateTime>>(input.valuelist)[1].Date.AddDays(1);

                T_STOREEntity store = db.IQueryable<T_STOREEntity>().FirstOrDefault(t => t.GUID == warehouseNo);

                var list = db.IQueryable<TORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && (p.FK_PlanStore == warehouseNo || warehouseNo == "") && p.CreateTime >= starttime && p.CreateTime < endtime)
                    .Join(db.IQueryable<TORDERDETAILEntity>(), m => m.GUID, n => n.FK_Order, (m, n) => new { m, n }).Join(db.IQueryable<VPRODUCTSTOCKEntity>(), x => x.n.ProductSKU, y => y.SKU, (x, y) => new
                    {
                        仓库编码 = store.StoreId,
                        订单号 = x.m.OrderNumber,
                        付款方式 = (x.m.PayWay - 1).ToString(),
                        订单价格 = x.m.OrderPrice,
                        订单备注 = x.m.OrderRemark,
                        收银人 = x.m.Creater,
                        收银时间 = x.m.CreateTime,
                        商品条码 = x.n.ProductSKU,
                        商品名称 = y.PdtName,
                        商品数量 = x.n.Count,
                        商品单价 = x.n.Price
                    }).ToList();

                DataTable dt = ListToTable(list);
                foreach (DataRow row in dt.Rows)
                {
                    row["付款方式"] = new string[] { "微信支付", "支付宝支付", "银行卡支付", "现金支付", "其它支付" }[Convert.ToInt32(row["付款方式"])];
                }
                dt.TableName = "OrderExport";
                result.Data = JSONHelper.ObjectToJson(dt);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
            }

            return result;
        }

        [SoapHeader("myheader")]
        [WebMethod(Description = "获取用户信息")]
        public ResultData GetWxUserInfo(String openid)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();
                MMM.Application.Entity.WebServiceSmall.MemberInfo memberInfo = db.IQueryable<TUSEREntity>().Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.usercode == openid), m => m.Openid, n => n.FK_UnionId, (m, n) => new MMM.Application.Entity.WebServiceSmall.MemberInfo
                {
                    UserName = n.UserName,
                    Money = n.Money ?? 0,
                    CreateTime = n.CreateTime,
                    Birthday = n.Birthday,
                    Mobile = n.Mobile,
                    QQNumber = n.QQNumber,
                    Profession = n.Profession,
                    Adress = n.Adress,
                    AvatarUrl = m.AvatarUrl,
                    Remark = n.Remark,
                    eventdraw = m.EventDraw ?? 0,
                    integral = m.Integral ?? 0
                }).SingleOrDefault();

                if (memberInfo != null)
                {
                    memberInfo.CreateTimeStr = memberInfo.CreateTime.Value.ToString("yyyy-MM-dd");
                    memberInfo.BirthdayStr = memberInfo.Birthday.HasValue ? memberInfo.Birthday.Value.ToString("yyyy-MM-dd") : "";
                    result.Data = JSONHelper.WXObjectToJson(memberInfo);
                    result.IsOK = true;
                }
                result.IsOK = true;
            }
            catch (Exception ex)
            {
            }
            return result;
        }

        public DataTable ListToTable<T>(List<T> list, bool isStoreDB = true)
        {
            Type tp = typeof(T);
            PropertyInfo[] proInfos = tp.GetProperties();
            DataTable dt = new DataTable();
            foreach (var item in proInfos)
            {
                if (!item.Name.Contains("Time") && !item.Name.Contains("时间"))
                {
                    if (item.Name == "明细数量" || item.Name == "订单优惠")
                    {
                        dt.Columns.Add(item.Name, Decimal.Zero.GetType());
                    }
                    else
                    {
                        dt.Columns.Add(item.Name, item.PropertyType);
                    }
                }
                else
                {
                    dt.Columns.Add(item.Name, typeof(DateTime));
                }
            }
            foreach (var item in list)
            {
                DataRow dr = dt.NewRow();
                foreach (var proInfo in proInfos)
                {
                    if (!proInfo.Name.Contains("Time"))
                    {
                        object obj = proInfo.GetValue(item);
                        if (obj == null)
                        {
                            continue;
                        }
                        if (isStoreDB && proInfo.PropertyType == typeof(DateTime) && Convert.ToDateTime(obj) < Convert.ToDateTime("1753-01-01"))
                        {
                            continue;
                        }
                        dr[proInfo.Name] = obj;
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }

    }
}
