﻿using MMM.Application.Cache;
using MMM.Application.Entity;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.SystemManage.ViewModel;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Service;
using MMM.Application.Service.Order;
using MMM.Application.Service.ShoppingMall;
using MMM.Application.Service.UserInfo;
using MMM.Application.Service.WSC;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace MMM.Application.Web.Controllers
{
    /// <summary>
    /// 日 期：2019.06.25
    /// </summary>
    public class HmcysController : ApiController
    {
        Log _logger = LogFactory.GetLogger("hmcys");

        #region 登录
        [ActionName("UserLogin")]
        [HttpGet]
        public string UserLogin(string code, string appId)
        {
            try
            {
                TUSERService dal = new TUSERService();
                return dal.GetUserSession(code, appId);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetUserInfo")]
        [HttpGet]
        public string GetUserInfo(string openid, string successData)
        {
            ResultData result = new ResultData();
            try
            {
                TUSERService dal = new TUSERService();
                result.IsOK = dal.SaveUserInfo(openid, successData);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return JSONHelper.WXObjectToJson(result);
        }

        [ActionName("GetUser")]
        [HttpGet]
        public string GetUser(string openid)
        {
            ResultData result = new ResultData();
            try
            {
                TUSERService dal = new TUSERService();
                result.Data = dal.GetUserInfo(openid);
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                result.msg = e.Message;
            }
            return JSONHelper.WXObjectToJson(result);
        }

        [ActionName("RegisterMember")]
        [HttpGet]
        public string RegisterMember(string openid, string memberstr)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.RegisterMember(openid, memberstr, CodeConst.DefaultOrganize);
        }

        [ActionName("GetWXAppInfo")]
        [HttpGet]
        public string GetWXAppInfo(string appguid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetWXAppInfo(appguid);
        }

        [ActionName("UserRemark")]
        [HttpGet]
        public string UserRemark(string openId, string appguid, string remark)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.UserRemark(openId, appguid, remark, CodeConst.DefaultOrganize);
        }

        [ActionName("GetUserQRCode")]
        [HttpGet]
        public string GetUserQRCode(string openId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetUserQRCode(openId);
        }
        #endregion

        #region 主页

        [ActionName("GetSwiperConfig")]
        [HttpGet]
        public string GetSwiperConfig(string appId)
        {
            Thread t = new Thread(Order);
            t.Start();

            SwiperModel swiper = new SwiperModel();

            XmlDocument xd = new XmlDocument();
            xd.Load(System.Web.Hosting.HostingEnvironment.MapPath(String.Format("\\Swiper\\{0}.xml", appId)));

            swiper.dotscolor = (xd.GetElementsByTagName("dotscolor")[0].InnerText);
            swiper.selectdotcolor = xd.GetElementsByTagName("selectdotcolor")[0].InnerText;
            swiper.duration = Int32.Parse((xd.GetElementsByTagName("duration")[0].InnerText));
            swiper.interval = Int32.Parse((xd.GetElementsByTagName("interval")[0].InnerText));
            swiper.width = Int32.Parse((xd.GetElementsByTagName("width")[0].InnerText));
            swiper.height = Int32.Parse((xd.GetElementsByTagName("height")[0].InnerText));

            return JSONHelper.WXObjectToJson(swiper);
        }

        [ActionName("GetSwiperConfigType")]
        [HttpGet]
        public string GetSwiperConfigType(string appId, String type)
        {
            Thread t = new Thread(Order);
            t.Start();

            SwiperModel swiper = new SwiperModel();

            String fileName = "";
            fileName = String.Format("\\Swiper\\{0}{1}.xml", appId, type);

            XmlDocument xd = new XmlDocument();
            xd.Load(System.Web.Hosting.HostingEnvironment.MapPath(fileName));

            swiper.dotscolor = (xd.GetElementsByTagName("dotscolor")[0].InnerText);
            swiper.selectdotcolor = xd.GetElementsByTagName("selectdotcolor")[0].InnerText;
            swiper.duration = Int32.Parse((xd.GetElementsByTagName("duration")[0].InnerText));
            swiper.interval = Int32.Parse((xd.GetElementsByTagName("interval")[0].InnerText));
            swiper.width = Int32.Parse((xd.GetElementsByTagName("width")[0].InnerText));
            swiper.height = Int32.Parse((xd.GetElementsByTagName("height")[0].InnerText));

            return JSONHelper.WXObjectToJson(swiper);
        }

        [ActionName("GetSwiperPictures")]
        [HttpGet]
        public string GetSwiperPictures(string appId)
        {
            WXIndexDAL dal = new WXIndexDAL();
            return dal.GetSwiperPictures(appId);
        }

        [ActionName("GetModelList")]
        [HttpGet]
        public string GetModelList(string windowWidth, string appId, String orderinfo = "")
        {
            try
            {
                TMODELFUNCTIONService dal = new TMODELFUNCTIONService();
                return dal.GetModelList(windowWidth, appId, orderinfo);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetHome")]
        [HttpGet]
        public string GetHome(string windowWidth, string appId)
        {
            try
            {
                TMODELFUNCTIONService dal = new TMODELFUNCTIONService();
                return dal.GetHome(windowWidth, appId);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetMoelListOther")]
        [HttpGet]
        public String GetMoelListOther(string windowWidth, string appId, String type, String orderinfo = "")
        {
            try
            {
                if (type == "fruits")
                {
                    TMODELFUNCTIONFruitsService dal = new TMODELFUNCTIONFruitsService();
                    return dal.GetModelList(windowWidth, appId, orderinfo);
                }
                else if (type == "phone")
                {
                    TMODELFUNCTIONPhoneService dal = new TMODELFUNCTIONPhoneService();
                    return dal.GetModelList(windowWidth, appId, orderinfo);
                }
                else if (type == "fresh")
                {
                    TMODELFUNCTIONFreshService dal = new TMODELFUNCTIONFreshService();
                    return dal.GetModelList(windowWidth, appId, orderinfo);
                }
                else if (type == "digital")
                {
                    TMODELFUNCTIONDigitalService dal = new TMODELFUNCTIONDigitalService();
                    return dal.GetModelList(windowWidth, appId, orderinfo);
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetMoelListOther2")]
        [HttpGet]
        public String GetMoelListOther2(string windowWidth, string appId, String type, String orderinfo = "")
        {
            try
            {
                TMODELFUNCTIONOverloadService dal = new TMODELFUNCTIONOverloadService();
                return dal.GetModelList(windowWidth, appId, type, orderinfo);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        /// <summary>
        /// 获取抢购的商品
        /// </summary>
        /// <param name="windowWidth"></param>
        /// <param name="appId"></param>
        /// <param name="type"></param>
        /// <param name="orderinfo"></param>
        /// <returns></returns>
        [ActionName("GetMoelListTimer")]
        [HttpGet]
        public String GetMoelListTimer(string windowWidth, string appId, String type, String orderinfo = "")
        {
            try
            {
                TMODELFUNCTIONOverloadService dal = new TMODELFUNCTIONOverloadService();
                return dal.GetMoelListTimer(windowWidth, appId, type, orderinfo);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        /// <summary>
        /// 品牌
        /// </summary>
        /// <param name="windowWidth"></param>
        /// <param name="appId"></param>
        /// <param name="type"></param>
        /// <param name="orderinfo"></param>
        /// <returns></returns>
        [ActionName("GetMoelListOther3")]
        [HttpGet]
        public String GetMoelListOther3(string windowWidth, string appId, String type,String modelurl, String orderinfo = "")
        {
            try
            {
                TMODELFUNCTIONOverloadService dal = new TMODELFUNCTIONOverloadService();
                return dal.GetModelList3(windowWidth, appId, type,modelurl, orderinfo);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetModelFreshGood")]
        [HttpGet]
        public String GetModelFreshGood(String appId, String type, Int32 top, String orderinfo = "")
        {
            try
            {
                if (type == "fruits")
                {
                    TMODELFUNCTIONFruitsService dal = new TMODELFUNCTIONFruitsService();
                    return dal.GetMoelListByType(appId, orderinfo, top);
                }
                else if (type == "phone")
                {
                    TMODELFUNCTIONPhoneService dal = new TMODELFUNCTIONPhoneService();
                    return dal.GetMoelListByType(appId, orderinfo, top);
                }
                else if (type == "fresh")
                {
                    TMODELFUNCTIONFreshService dal = new TMODELFUNCTIONFreshService();
                    return dal.GetMoelListByType(appId, orderinfo, top);
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetMoelListByType")]
        [HttpGet]
        public String GetMoelListByType(string appId, String type, Int32 top)
        {
            try
            {
                TMODELFUNCTIONService dal = new TMODELFUNCTIONService();
                return dal.GetMoelListByType(appId, type, top);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetLimitedProducts")]
        [HttpGet]
        public string GetLimitedProducts(int pageIndex, string appId)
        {
            WXIndexDAL dal = new WXIndexDAL();
            return dal.GetLimitedProducts(pageIndex, appId, CodeConst.DefaultOrganize);
        }

        #endregion

        #region 商品分类
        [ActionName("GetClassData")]
        [HttpGet]
        public string GetClassData(string appId)
        {
            WXClassDAL dal = new WXClassDAL();
            return dal.GetClassData(appId, CodeConst.DefaultOrganize);
        }

        [ActionName("GetCategoryDataByGUID")]
        [HttpGet]
        public string GetCategoryDataByGUID(string guid)
        {
            WXClassDAL dal = new WXClassDAL();
            return dal.GetCategoryDataByGUID(guid);
        }

        [ActionName("GetProductSearchData")]
        [HttpGet]
        public string GetProductSearchData(string appId, string categoryId, string searchstr, string orderstr, string ordertype, int pageIndex, int IsLimit, int IsGroup)
        {
            try
            {
                WXClassDAL dal = new WXClassDAL();
                return dal.GetProductSearchData(appId, categoryId, searchstr, orderstr, ordertype, pageIndex, IsLimit, IsGroup);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetCategoryProduct")]
        [HttpGet]
        public string GetCategoryProduct(string guid)
        {
            WXClassDAL dal = new WXClassDAL();
            return dal.GetCategoryProduct(guid, DateTime.Now);
        }

        [ActionName("GetIndexProduct")]
        [HttpGet]
        public string GetIndexProduct()
        {
            WXClassDAL dal = new WXClassDAL();
            return dal.GetIndexProduct();
        }
        #endregion

        #region 购物车

        [ActionName("GetShopItemList")]
        [HttpGet]
        public string GetShopItemList(string shoplist)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetShopItemList(shoplist, CodeConst.DefaultOrganize);
        }

        [ActionName("GetProductInfoByGUID")]
        [HttpGet]
        public string GetProductInfoByGUID(string guid, String islimit)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetProductInfoByGUID(guid, islimit);
        }

        [ActionName("GetZanCountByGUID")]
        [HttpGet]
        public string GetZanCountByGUID(string guid, string openId)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetZanCountByGUID(guid, openId);
        }

        [ActionName("UserZan")]
        [HttpGet]
        public string UserZan(string guid, string openId)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.UserZan(guid, openId);
        }

        #endregion

        #region 商品订单

        [ActionName("GetAdressList")]
        [HttpGet]
        public string GetAdressList(int adresstype, string wxopenId, string appguId)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetAdressList(adresstype, wxopenId, CodeConst.DefaultOrganize);
        }

        [ActionName("GetFreight")]
        [HttpGet]
        public string GetFreight(string appguId, string adressguId)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetFreight(appguId, adressguId, CodeConst.DefaultOrganize);
        }
        [ActionName("GetUserCanUseCard")]
        [HttpGet]
        public string GetUserCanUseCard(string openId, decimal totalprice, string productlist)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetUserCanUseCard(openId, totalprice, productlist);
        }

        [ActionName("GetUserCanUseCard")]
        [HttpGet]
        public string GetUserCanUseCard(string openId, decimal totalprice)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetUserCanUseCard(openId, totalprice, "");
        }

        [ActionName("SaveAdressData")]
        [HttpGet]
        public string SaveAdressData(string openid, string guid, string name, string adress, string mobile, string moreadress, decimal longitude, decimal latitude)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.SaveAdressData(openid, guid, name, adress, mobile, moreadress, longitude, latitude, CodeConst.DefaultOrganize);
        }

        [ActionName("GetAdressByGUID")]
        [HttpGet]
        public string GetAdressByGUID(string guid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetAdressByGUID(guid);
        }

        [ActionName("DeleteAdressByGUID")]
        [HttpGet]
        public string DeleteAdressByGUID(string guid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.DeleteAdressByGUID(guid);
        }

        [ActionName("UpdateAdressDefault")]
        [HttpGet]
        public string UpdateAdressDefault(string guid, string openid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.UpdateAdressDefault(guid, openid);
        }

        [ActionName("SaveOrderInfo")]
        [HttpGet]
        public string SaveOrderInfo(string appId, string wxopenId, string productlist, int mode, int classcount, decimal orderprice, string modeguid, int isgrouporder, string groupnumber, string remark, decimal freight, string cardguid, string getGoodsDate,String username,String usertel)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.SaveOrderInfo(appId, wxopenId, productlist, mode, classcount, orderprice, modeguid, isgrouporder, groupnumber, remark, freight, cardguid, getGoodsDate, username,usertel, CodeConst.DefaultOrganize);
        }

        [ActionName("GetOrderList")]
        [HttpGet]
        public string GetOrderList(int pageIndex, string wxopenId, int status)
        {
            Thread t = new Thread(Order);
            t.Start();

            WXShopDAL dal = new WXShopDAL();
            return dal.GetOrderList(pageIndex, wxopenId, status);
        }

        [ActionName("GetOrderTitle")]
        [HttpGet]
        public string GetOrderTitle(string appguid)
        {
            WXIndexDAL dal = new WXIndexDAL();
            return dal.GetOrderTitle(appguid);
        }

        [ActionName("GetGoldEggStart")]
        [HttpGet]
        public string GetGoldEggStart()
        {
            ResultData result = new ResultData();
            try
            {
                WXIndexDAL dal = new WXIndexDAL();
                result.IsOK = dal.GetGoldEggStart();
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        [ActionName("GetGoldEggsInfo")]
        [HttpGet]
        public string GetGoldEggsInfo()
        {
            try
            {
                ResultData result = new ResultData();
                return JSONHelper.WXObjectToJson(result);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return "";
            }
        }

        [ActionName("GetProductStockByGUID")]
        [HttpGet]
        public string GetProductStockByGUID(string guidlist)
        {
            WXIndexDAL dal = new WXIndexDAL();
            return dal.GetProductStockByGUID(guidlist);
        }

        [ActionName("UserShareGoldEggs")]
        [HttpGet]
        public string UserShareGoldEggs(string openId)
        {
            WXIndexDAL dal = new WXIndexDAL();
            return dal.UserShareGoldEggs(openId);
        }

        [ActionName("GetOrderDetail")]
        [HttpGet]
        public string GetOrderDetail(string guid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetOrderDetail(guid);
        }

        [ActionName("GetOrderDetail2")]
        [HttpGet]
        public string GetOrderDetail2(string guid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetOrderDetail2(guid);
        }

        [ActionName("GetMyGroupOrder")]
        [HttpGet]
        public string GetMyGroupOrder(string openid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetMyGroupOrder(openid, CodeConst.DefaultOrganize);
        }

        [ActionName("GetGroupOrder")]
        [HttpGet]
        public string GetGroupOrder(string openid, string classguid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetGroupOrder(openid, classguid);
        }

        [ActionName("GetGroupMember")]
        [HttpGet]
        public string GetGroupMember(string groupnumber)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetGroupMember(groupnumber);
        }
        [ActionName("GetZCCount")]
        [HttpGet]
        public string GetZCCount(string productguid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetZCCount(productguid);
        }

        [ActionName("SetAfterSale")]
        [HttpGet]
        public string SetAfterSale(string detailguid, string remark)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.SetAfterSale(detailguid, remark);
        }

        [ActionName("GetAfterSaleOrder")]
        [HttpGet]
        public string GetAfterSaleOrder(string openid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.GetAfterSaleOrder(openid, CodeConst.DefaultOrganize);
        }

        [ActionName("DeleteOrder")]
        [HttpGet]
        public string DeleteOrder(string wxopenId, string guid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.DeleteOrder(wxopenId, guid);
        }

        [ActionName("ConfirmOrder")]
        [HttpGet]
        public string ConfirmOrder(string wxopenId, string guid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.ConfirmOrder(wxopenId, guid);
        }

        [ActionName("CancellationOrder")]
        [HttpGet]
        public string CancellationOrder(string wxopenId, string guid)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.CancellationOrder(wxopenId, guid);
        }

        [ActionName("SavePingJia")]
        [HttpGet]
        public String SavePingJia(String OrderGUID, String OrderNumber, String openid, Int32 m_des, Int32 m_du, Int32 m_wuliu, Boolean n_name, String codeid, String eval, String typeNumber, String productParam, String productCount, String productname)
        {
            WXShopDAL dal = new WXShopDAL();
            return dal.SavePingJia(OrderGUID, OrderNumber, openid, m_des, m_du, m_wuliu, n_name, codeid, eval, typeNumber, productParam, productCount, productname);
        }
        #endregion

        #region 微信支付

        [ActionName("GetWXPayOrder")]
        [HttpGet]
        public string GetWXPayOrder(string appId, string openId, string orderguid, int isUseMemberCard)
        {
            WXPayDAL dal = new WXPayDAL();
            return dal.GetWXPayOrder(appId, openId, orderguid, isUseMemberCard);
        }

        [ActionName("PayWXPayOrder")]
        [HttpGet]
        public string PayWXPayOrder(string appId, string openId, string orderguid)
        {
            WXPayDAL dal = new WXPayDAL();
            return dal.PayWXPayOrder(appId, openId, orderguid);
        }

        /// <summary>
        /// 会员支付
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="openId"></param>
        /// <returns></returns>
        [ActionName("MemberPay")]
        [HttpGet]
        public string MemberPay(string appId, string openId)
        {
            WXPayDAL dal = new WXPayDAL();
            return dal.MemberPay(appId, openId);
        }

        #endregion

        #region 积分管理

        [ActionName("UserSignIn")]
        [HttpGet]
        public string UserSignIn(string openid, string appguid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.UserSignIn(openid, appguid);
        }

        [ActionName("IsUserSignIn")]
        [HttpGet]
        public string IsUserSignIn(string openid, string appguid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.IsUserSignIn(openid, appguid);
        }

        [ActionName("GetUserIntegral")]
        [HttpGet]
        public string GetUserIntegral(string openId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetUserIntegral(openId);
        }

        [ActionName("GetIntegralInfo")]
        [HttpGet]
        public string GetIntegralInfo(string appguid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetIntegralInfo(appguid, CodeConst.DefaultOrganize);
        }

        [ActionName("ExChangeProduct")]
        [HttpGet]
        public string ExChangeProduct(string openId, string cardId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.ExChangeProduct(openId, cardId);
        }

        [ActionName("GetUserCard")]
        [HttpGet]
        public string GetUserCard(string openId, Int32 type)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetUserCard(openId, type);
        }

        [ActionName("AddUserCard")]
        [HttpGet]
        public string AddUserCard(string openId, string guid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.AddUserCard(openId, guid);
        }

        [ActionName("GetProductGUID")]
        [HttpGet]
        public string GetProductGUID(string sku, string guid, string openId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetProductGUID(sku, guid, openId);
        }


        [ActionName("GetCardInfo")]
        [HttpGet]
        public string GetCardInfo(string openId, string cardId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetCardInfo(openId, cardId);
        }

        [ActionName("UserGetCard")]
        [HttpGet]
        public string UserGetCard(string openId, string cardId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.UserGetCard(openId, cardId);
        }

        [ActionName("GetMemberMoney")]
        [HttpGet]
        public string GetMemberMoney(string openId)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetMemberMoney(openId);
        }

        [ActionName("GetUserIntegralDetail")]
        [HttpGet]
        public string GetUserIntegralDetail(string openId, int pageIndex)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetUserIntegralDetail(openId, pageIndex);
        }

        [ActionName("ShareApp")]
        [HttpGet]
        public string ShareApp(string openid, string appguid, int sharetype)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.ShareApp(openid, appguid, sharetype);
        }

        #endregion

        #region 客服管理

        [ActionName("KFUserUpdate")]
        [HttpGet]
        public string KFUserUpdate(string appguid, string userId, string password)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.KFUserUpdate(appguid, userId, password);
        }

        [ActionName("IsCardUse")]
        [HttpGet]
        public string IsCardUse(string card)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.IsCardUse(card);
        }

        [ActionName("UseCard")]
        [HttpGet]
        public string UseCard(string card, string userId)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.UseCard(card, userId);
        }

        [ActionName("GetStoreOrderList")]
        [HttpGet]
        public string GetStoreOrderList(string wxopenId)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.GetStoreOrderList(wxopenId, CodeConst.DefaultOrganize);
        }

        [ActionName("GetStoreOrderPHList")]
        [HttpGet]
        public string GetStoreOrderPHList(string userId)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.GetStoreOrderPHList(userId, CodeConst.DefaultOrganize);
        }

        [ActionName("KFConfirmOrder")]
        [HttpGet]
        public string KFConfirmOrder(string userId, string guid, string warehouseNo)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.KFConfirmOrder(userId, guid, warehouseNo);
        }

        [ActionName("KFPHConfirmOrder")]
        [HttpGet]
        public string KFPHConfirmOrder(string userId, string guid)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.KFPHConfirmOrder(userId, guid);
        }

        [ActionName("GetProductInfoByCode")]
        [HttpGet]
        public string GetProductInfoByCode(string sku)
        {
            WXManageDAL dal = new WXManageDAL();
            return dal.GetProductInfoByCode(sku, CodeConst.DefaultOrganize);
        }

        [ActionName("SystemDeleteTimeOutOrder")]
        [HttpGet]
        public void SystemDeleteTimeOutOrder()
        {
            try
            {
                TORDERService tORDERService = new TORDERService();
                tORDERService.SystemDeleteTimeOutOrder("");
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
            }
        }

        #endregion

        #region 会员信息

        [ActionName("GetMemberInfo")]
        [HttpGet]
        public string GetMemberInfo(string openid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetMemberInfo(openid);
        }

        [ActionName("GetMemberMoneyDetail")]
        [HttpGet]
        public string GetMemberMoneyDetail(string openId, int pageIndex)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.GetMemberMoneyDetail(openId, pageIndex);
        }

        [ActionName("GetPrizeCount")]
        [HttpGet]
        public String GetPrizeCount(String openId)
        {
            ResultData result = new ResultData();
            return JSONHelper.WXObjectToJson(result);
        }

        [ActionName("SaveUserQuHuo")]
        [HttpGet]
        public String SaveUserQuHuo(string wxopenId,String username,String usertel,String address,String selectid)
        {
            WXUserDAL dal = new WXUserDAL();
            return dal.SaveUserQuHuo(wxopenId, username, usertel, address, selectid);
        }
        #endregion

        #region
        [ActionName("SaveAfterSale")]
        [HttpGet]
        public String SaveAfterSale(String orderid, String type, String remark, String adduser, String storeid)
        {
            WXShopDAL dal = new WXShopDAL();
            T_OrderAfterSaleEntity entity = new T_OrderAfterSaleEntity();
            entity.Create();
            entity.orderid = orderid;
            entity.type = type;
            entity.remark = remark;
            entity.adduser = adduser;
            entity.storeid = storeid;
            return dal.SaveAfterSale(entity);
        }
        #endregion

        [ActionName("getDataType")]
        [HttpGet]
        public String getDataType(String type)
        {
            DataItemCache dataItemCache = new DataItemCache();
            try
            {
                IList<DataItemModel> list = dataItemCache.GetDataItemList(type).OrderByDescending(t => t.SortCode).ToList();
                var item = list.Select(t => new
                {
                    ItemName = t.ItemName,
                    ItemValue = t.ItemValue
                });
                //判断验证码
                return JSONHelper.WXObjectToJson(new { IsOK = true, data = list });
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                return JSONHelper.WXObjectToJson(new { IsOK = false, Data = "系统错误,请联系客服" });
            }
        }

        private void Order()
        {
            try
            {
                TORDERService shopdal = new TORDERService();
                shopdal.SystemDeleteTimeOutOrder(null);
                shopdal.ConfirmOrders();
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
            }
        }
    }
}
