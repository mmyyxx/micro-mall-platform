﻿using MMM.Application.Busines.BaseManage;
using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Service;
using MMM.Application.Service.Order;
using MMM.Application.Service.ShoppingMall;
using MMM.Application.Service.UserInfo;
using MMM.Application.Service.WMS;
using MMM.Application.Service.WSC;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace MMM.Application.Web.Controllers
{
    /// <summary>
    /// 日 期：2019.06.25
    /// </summary>
    public class WMSController : ApiController
    {
        Log _logger = LogFactory.GetLogger("wms");

        #region 仓库管理登录

        [ActionName("GetWarehouseList")]
        [HttpGet]
        public string GetWarehouseList()
        {
            return WMSLoginDAL.GetWarehouseList(CodeConst.DefaultOrganize);
        }


        [ActionName("WMSUserLogin")]
        [HttpGet]
        public string WMSUserLogin(string userId, string password)
        {
            ResultData result = new ResultData();
            try
            {
                UserBLL dal = new UserBLL();

                string jmpassword = Md5Helper.MD5(password, 32);
                UserEntity userEntity = dal.CheckLogin(userId, Md5Helper.MD5(password, 32));

                if (userEntity != null)
                {
                    result.Data = userEntity.Account;
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        #endregion

        #region 公共部分

        [ActionName("GetSupplierList")]
        [HttpGet]
        public string GetSupplierList()
        {
            return WMSLoginDAL.GetSupplierList(CodeConst.DefaultOrganize);
        }

        #endregion

        #region 入库上架

        [ActionName("GetPutAwayList")]
        [HttpGet]
        public string GetPutAwayList(string warehouseNo)
        {
            return WMSInComing.GetPutAwayList(warehouseNo);
        }

        [ActionName("GetPutAwayDetailList")]
        [HttpGet]
        public string GetPutAwayDetailList(string productguid)
        {
            return WMSInComing.GetPutAwayDetailList(productguid);
        }

        [ActionName("CompletePutAwayByGUID")]
        [HttpGet]
        public string CompletePutAwayByGUID(string guid, string operatorId, string warehouseNo, string actualLocationNo)
        {
            return WMSInComing.CompletePutAwayByGUID(guid, operatorId, warehouseNo, actualLocationNo);
        }

        [ActionName("GetNoReadyPurchaseOrders")]
        [HttpGet]
        public string GetNoReadyPurchaseOrders(string warehouseNo)
        {
            return WMSInComing.GetNoReadyPurchaseOrders(warehouseNo);
        }

        [ActionName("GetNoReadyPurchaseOrderDetail")]
        [HttpGet]
        public string GetNoReadyPurchaseOrderDetail(string guid)
        {
            return WMSInComing.GetNoReadyPurchaseOrderDetail(guid);
        }

        [ActionName("SavePurchaseOrderByXCX")]
        [HttpGet]
        public string SavePurchaseOrderByXCX(string warehouseNo, string userId, string ordertype, string supplierguid, string remark)
        {
            return WMSInComing.SavePurchaseOrderByXCX(warehouseNo, userId, ordertype, supplierguid, remark);
        }

        [ActionName("GetProductInfo")]
        [HttpGet]
        public string GetProductInfo(string sku, string guid)
        {
            return WMSInComing.GetProductInfo(sku, guid, CodeConst.DefaultOrganize);
        }

        [ActionName("SavePurchaseDetail")]
        [HttpGet]
        public string SavePurchaseDetail(string qualityDate, string pdtCount, string detailInfo)
        {
            return WMSInComing.SavePurchaseDetail(qualityDate, pdtCount, detailInfo, CodeConst.DefaultOrganize);
        }

        [ActionName("UpdatePurchaseOrder")]
        [HttpGet]
        public string UpdatePurchaseOrder(string orderguid)
        {
            return WMSInComing.UpdatePurchaseOrder(orderguid);
        }

        #endregion

        #region 出库分配

        [ActionName("GetOutGoingList")]
        [HttpGet]
        public string GetOutGoingList(string warehouseNo, string operatorId)
        {
            return WMSOutGoing.GetOutGoingList(warehouseNo, operatorId);
        }

        [ActionName("GetOutGoingStockList")]
        [HttpGet]
        public string GetOutGoingStockList(string warehouseNo, string operatorId)
        {
            return WMSOutGoing.GetOutGoingStockList(warehouseNo, operatorId);
        }

        [ActionName("GetOutGoingDetail")]
        [HttpGet]
        public string GetOutGoingDetail(string warehouseNo, string outgoingguid)
        {
            return WMSOutGoing.GetOutGoingDetail(warehouseNo, outgoingguid);
        }

        [ActionName("GetOutGoingStockInfo")]
        [HttpGet]
        public string GetOutGoingStockInfo(string outgoingguid, string stockguid)
        {
            return WMSOutGoing.GetOutGoingStockInfo(outgoingguid, stockguid);
        }

        [ActionName("OutStockInfo")]
        [HttpGet]
        public string OutStockInfo(string outgoingguid, string stockguid, string operatorId, decimal stock)
        {
            return WMSOutGoing.OutStockInfo(outgoingguid, stockguid, operatorId, stock);
        }

        [ActionName("GetOutStockOrderInfo")]
        [HttpGet]
        public string GetOutStockOrderInfo(string warehouseNo, string outgoingguid, string operatorId)
        {
            return WMSOutGoing.GetOutStockOrderInfo(warehouseNo, outgoingguid, operatorId);
        }

        [ActionName("OutStockDetailComplete")]
        [HttpGet]
        public string OutStockDetailComplete(string detailguid, string outgoingguid)
        {
            return WMSOutGoing.OutStockDetailComplete(detailguid, outgoingguid);
        }

        #endregion

        #region 库内操作

        #region 订单配货

        [ActionName("GetPHOrderInfo")]
        [HttpGet]
        public string GetPHOrderInfo(string storeId, string operatorId)
        {
            return WMSStockManage.GetPHOrderInfo(storeId, operatorId);
        }

        [ActionName("GetOrderDetail")]
        [HttpGet]
        public string GetOrderDetail(string orderguid, string operatorId)
        {
            return WMSStockManage.GetOrderDetail(orderguid, operatorId);
        }

        [ActionName("GetProductStock")]
        [HttpGet]
        public string GetProductStock(string sku, string warehouseNo)
        {
            return WMSStockManage.GetProductStock(sku, warehouseNo);
        }

        [ActionName("OrderProductOutStock")]
        [HttpGet]
        public string OrderProductOutStock(string orderguid, string warehouseNo, string operatorId)
        {
            return WMSStockManage.OrderProductOutStock(orderguid, warehouseNo, operatorId);
        }

        [ActionName("GetPDProductStock")]
        [HttpGet]
        public string GetPDProductStock(string sku, string warehouseNo)
        {
            return WMSStockManage.GetPDProductStock(sku, warehouseNo, CodeConst.DefaultOrganize);
        }

        [ActionName("SaveStockPD")]
        [HttpGet]
        public string SaveStockPD(string sku, string warehouseNo, string pdtDate, decimal pdtStock, int qualityDate, string operatorId)
        {
            return WMSStockManage.SaveStockPD(sku, warehouseNo, pdtDate, pdtStock, qualityDate, operatorId,CodeConst.DefaultOrganize);
        }

        #endregion

        #region 库存调拨

        [ActionName("GetProductInfoForDB")]
        [HttpGet]
        public string GetProductInfoForDB(string sku, string warehouseNo)
        {
            return WMSStockManage.GetProductInfoForDB(sku, warehouseNo,CodeConst.DefaultOrganize);
        }

        [ActionName("CompleteStockAllocation")]
        [HttpGet]
        public string CompleteStockAllocation(string data)
        {
            return WMSStockManage.CompleteStockAllocation(data, CodeConst.DefaultOrganize);
        }

        #endregion

        #endregion
    }
}
