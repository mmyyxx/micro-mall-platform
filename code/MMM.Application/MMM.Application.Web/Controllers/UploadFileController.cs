﻿using MMM.Application.Entity.BaseManage;
using MMM.Application.Service.BaseManage;
using MMM.Application.Service.Product;
using MMM.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MMM.Application.Web.Controllers
{
    /// <summary>
    /// 附件上传
    /// </summary>
    public class UploadFileController : MvcControllerBase
    {
        private T_PICTUREService t_pictureservice = new T_PICTUREService();
        private T_PRODUCTPRICEService t_productpriceservice = new T_PRODUCTPRICEService();
        /// <summary>
        /// base64方式上传图片
        /// </summary>
        /// <returns></returns>
        public String IndexBase()
        {
            String savePath = "/Upload/";
            String saveUrl = "/Upload/";

            //上传文件类型
            String dirName = Request["dir"];
            if (String.IsNullOrEmpty(dirName))
                dirName = "image";

            //目录名
            String dirtype = Request["dirtype"];
            if (String.IsNullOrEmpty(dirtype))
                dirtype = "image";

            String dirPath = Server.MapPath(savePath);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            //创建文件夹
            String time = DateTime.Now.ToString("yyyyMMdd");
            dirPath += dirtype + "/" + time + "/";
            saveUrl += dirtype + "/" + time + "/";
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            String newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + ".jpg";
            String filePath = dirPath + newFileName;

            String imgFile = Request["imgData"];

            String sessionKey = Request["sessionKey"];
            if (!String.IsNullOrEmpty(sessionKey))
            {
                String oldFiles = (String)Session[sessionKey];
                if (oldFiles != null && System.IO.File.Exists(oldFiles))
                    System.IO.File.Delete(oldFiles);
                Session[sessionKey] = filePath;
            }
            try
            {
                byte[] arr2 = Convert.FromBase64String(imgFile.Substring(23));
                using (MemoryStream ms2 = new MemoryStream(arr2))
                {
                    System.Drawing.Bitmap bmp2 = new System.Drawing.Bitmap(ms2);
                    bmp2.Save(filePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                String fileUrl = saveUrl + newFileName;

                var jsonData = new
                {
                    error = "0",
                    url = fileUrl
                };
                return (jsonData.ToJson());
            }
            catch (Exception e)
            {
                Logger.Error(e.ToString());
                var jsonData1 = new
                {
                    error = "1",
                    message = "请选择图片!"
                };
                return (jsonData1.ToJson());
            }
        }

        /// <summary>
        /// 文件保存
        /// 图片类型:0-商品图片;1-分类图片;2-商品详情图片;3-轮播图片;4-商品主图;10-小程序商城的模块图片
        /// </summary>
        /// <returns></returns>
        public String IndexFile()
        {
            //判断类型,分类图片只需要传一条
            Int32 type;
            Int32.TryParse(Request["Type"], out type);
            String fk_Guid = Request["FK_GUID"];
            if (type == 1 || type == 4)//分类图片
            {
                IList<T_PICTUREEntity> pictureList = t_pictureservice.GetList(1, fk_Guid);
                if (pictureList != null && pictureList.Count > 0)
                {
                    var jsonData1 = new
                    {
                        error = "1",
                        message = "该图片已经上传!"
                    };
                    return (jsonData1.ToJson());
                }
            }

            String savePath = "/Upload/";
            String saveUrl = "/Upload/";

            //定义允许上传的文件扩展名
            Hashtable extTable = new Hashtable();
            extTable.Add("image", "gif,jpg,jpeg,png,bmp");
            extTable.Add("flash", "swf,flv");
            extTable.Add("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,rm,rmvb");
            extTable.Add("file", "gif,jpg,jpeg,png,bmp,doc,docx,xls,xlsx,ppt,txt,zip,rar,gz,bz2,pdf");

            //最大文件大小
            int maxSize = 1024000000;

            HttpPostedFileBase imgFile = Request.Files[0];
            if (imgFile == null)
            {
                var jsonData1 = new
                {
                    error = "1",
                    message = "请选择文件"
                };
                return (jsonData1.ToJson());
            }

            String dirPath = Server.MapPath(savePath);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            //上传文件类型
            String dirName = Request.QueryString["dir"];
            if (String.IsNullOrEmpty(dirName))
                dirName = "image";

            //目录名
            String dirtype = Request.QueryString["dirtype"];
            if (String.IsNullOrEmpty(dirtype))
                dirtype = "image";

            String fileName = imgFile.FileName;
            String fileExt = Path.GetExtension(fileName).ToLower();

            if (imgFile.InputStream == null || imgFile.InputStream.Length > maxSize)
            {
                var jsonData2 = new
                {
                    error = "1",
                    message = "上传文件大小超过限制"
                };
                return (jsonData2.ToJson());
            }

            long size = imgFile.InputStream.Length;

            if (String.IsNullOrEmpty(fileExt) || Array.IndexOf(((String)extTable[dirName]).Split(','), fileExt.Substring(1).ToLower()) == -1)
            {
                var jsonData3 = new
                {
                    error = "1",
                    message = String.Format("上传文件扩展名是不允许的扩展名。\n只允许{0}格式.", ((String)extTable[dirName]))
                };
                return (jsonData3.ToJson()); ;
            }

            //创建文件夹
            String time = DateTime.Now.ToString("yyyyMMdd");
            dirPath += dirtype + "/" + time + "/";
            saveUrl += dirtype + "/" + time + "/";
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            String newFileName = DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + fileExt;
            String filePath = dirPath + newFileName;

            imgFile.SaveAs(filePath);

            if (dirtype == "Order")
            {
                //订单照片，需要生成缩略图
                String thumbPath = dirPath + "thumb/";
                if (!Directory.Exists(thumbPath))
                    Directory.CreateDirectory(thumbPath);
                MakeThumbnail(filePath, thumbPath + newFileName, Int32.Parse(ConfigurationManager.AppSettings["thumb_w"]), Int32.Parse(ConfigurationManager.AppSettings["thumb_h"]), "W");
            }

            if (Request["is"] != null && Request["is"] == "1")
            {
                String sessionKey = Request.QueryString["sessionKey"];
                if (!String.IsNullOrEmpty(sessionKey))
                {
                    String oldFiles = (String)Session[sessionKey];
                    if (oldFiles != null && System.IO.File.Exists(oldFiles))
                        System.IO.File.Delete(oldFiles);
                }
                Session[sessionKey] = filePath;
            }

            String fileUrl = saveUrl + newFileName;
            String[] names = fileName.Split('\\');

            //保存到数据库
            if (type != 10)//部分类型不需要存储到数据库
            {
                T_PICTUREEntity entity = new T_PICTUREEntity();
                entity.Create();
                entity.FK_GUID = fk_Guid;
                entity.Type = type;
                entity.ImageURL = fileUrl;
                entity.ImageName = names[names.Length - 1];
                t_pictureservice.SaveForm("", entity);
            }

            if (type == 4)//商品主图保存到商品里面
            {
                t_productpriceservice.SaveForm(fk_Guid, new Entity.Product.T_PRODUCTPRICEEntity()
                {
                    ProductMainPicture = fileUrl
                });
            }

            var jsonData = new
            {
                error = "0",
                url = fileUrl,
                size = size,
                ext = fileExt,
                fileName = names[names.Length - 1],
            };
            return (jsonData.ToJson());
        }

        /// <summary>
        /// 删除文件
        /// </summary>
        /// <returns></returns>
        public String deleteFile()
        {
            String path = Request["path"].Split('|')[0];
            String dirPath = Server.MapPath(path);
            if (System.IO.File.Exists(dirPath))
                System.IO.File.Delete(dirPath);

            Int32 index = dirPath.LastIndexOf('\\');

            String temPath = dirPath.Substring(0, index) + "/thumb" + dirPath.Substring(index);

            if (System.IO.File.Exists(temPath))
                System.IO.File.Delete(temPath);

            var jsonData = new
            {
                success = "1"
            };
            return (jsonData.ToJson());
        }

        /// <summary> 
        /// 生成缩略图 
        /// </summary> 
        /// <param name="originalImagePath">源图路径（物理路径）</param> 
        /// <param name="thumbnailPath">缩略图路径（物理路径）</param> 
        /// <param name="width">缩略图宽度</param> 
        /// <param name="height">缩略图高度</param> 
        /// <param name="mode">生成缩略图的方式</param>  
        public void MakeThumbnail(string originalImagePath, string thumbnailPath, int width, int height, string mode)
        {
            Image originalImage = Image.FromFile(originalImagePath);

            int towidth = width;
            int toheight = height;

            int x = 0;
            int y = 0;
            int ow = originalImage.Width;
            int oh = originalImage.Height;

            switch (mode)
            {
                case "HW"://指定高宽缩放（可能变形）                 
                    break;
                case "W"://指定宽，高按比例                     
                    toheight = originalImage.Height * width / originalImage.Width;
                    break;
                case "H"://指定高，宽按比例 
                    towidth = originalImage.Width * height / originalImage.Height;
                    break;
                case "Cut"://指定高宽裁减（不变形）                 
                    if ((double)originalImage.Width / (double)originalImage.Height > (double)towidth / (double)toheight)
                    {
                        oh = originalImage.Height;
                        ow = originalImage.Height * towidth / toheight;
                        y = 0;
                        x = (originalImage.Width - ow) / 2;
                    }
                    else
                    {
                        ow = originalImage.Width;
                        oh = originalImage.Width * height / towidth;
                        x = 0;
                        y = (originalImage.Height - oh) / 2;
                    }
                    break;
                default:
                    break;
            }

            //新建一个bmp图片 
            Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

            //新建一个画板 
            Graphics g = System.Drawing.Graphics.FromImage(bitmap);

            //设置高质量插值法 
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

            //设置高质量,低速度呈现平滑程度 
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //清空画布并以透明背景色填充 
            g.Clear(Color.Transparent);

            //在指定位置并且按指定大小绘制原图片的指定部分 
            g.DrawImage(originalImage, new Rectangle(0, 0, towidth, toheight),
                new Rectangle(x, y, ow, oh),
                GraphicsUnit.Pixel);

            try
            {
                //以jpg格式保存缩略图 
                bitmap.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                originalImage.Dispose();
                bitmap.Dispose();
                g.Dispose();
            }
        }
    }
}
