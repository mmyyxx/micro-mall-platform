using MMM.Application.Entity.ShoppingMall;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.ShoppingMall
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-24 11:01
    /// 描 述：模块商品
    /// </summary>
    public class TMODELPRODUCTPhoneMap : EntityTypeConfiguration<TMODELPRODUCTPhoneEntity>
    {
        public TMODELPRODUCTPhoneMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_MODELPRODUCT_Phone");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
