using MMM.Application.Entity.ShoppingMall;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.ShoppingMall
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-24 10:55
    /// 描 述：数码
    /// </summary>
    public class TMODELFUNCTIONDigitalMap : EntityTypeConfiguration<TMODELFUNCTIONDigitalEntity>
    {
        public TMODELFUNCTIONDigitalMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_MODELFUNCTION_Digital");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
