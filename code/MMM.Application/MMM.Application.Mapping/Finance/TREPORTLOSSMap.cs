using MMM.Application.Entity.Finance;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Finance
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-20 16:38
    /// 描 述：报损
    /// </summary>
    public class TREPORTLOSSMap : EntityTypeConfiguration<TREPORTLOSSEntity>
    {
        public TREPORTLOSSMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_REPORTLOSS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
