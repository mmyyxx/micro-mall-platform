using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 12:46
    /// 描 述：月台
    /// </summary>
    public class T_PLATMap : EntityTypeConfiguration<T_PLATEntity>
    {
        public T_PLATMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PLAT");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
