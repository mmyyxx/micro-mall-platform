using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Warehouse
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-22 11:55
    /// 描 述：库区
    /// </summary>
    public class T_AREAMap : EntityTypeConfiguration<T_AREAEntity>
    {
        public T_AREAMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_AREA");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(a => a.AreaTypeName);
            this.Ignore(a => a.FK_WarehouseNoName);
            #endregion
        }
    }
}
