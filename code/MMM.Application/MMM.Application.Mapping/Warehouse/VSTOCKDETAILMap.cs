using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Warehouse
{
    /// <summary>
    /// 版 本
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// </summary>
    public class VSTOCKDETAILMap : EntityTypeConfiguration<VSTOCKDETAILEntity>
    {
        public VSTOCKDETAILMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_STOCKDETAIL");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
