using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 11:59
    /// 描 述：储位
    /// </summary>
    public class T_LOCATIONMap : EntityTypeConfiguration<T_LOCATIONEntity>
    {
        public T_LOCATIONMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_LOCATION");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
