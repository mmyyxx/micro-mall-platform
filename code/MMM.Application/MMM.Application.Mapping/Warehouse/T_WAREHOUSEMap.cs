using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：仓库
    /// </summary>
    public class T_WAREHOUSEMap : EntityTypeConfiguration<T_WAREHOUSEEntity>
    {
        public T_WAREHOUSEMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_WAREHOUSE");
            //主键
            this.HasKey(t => t.WarehouseNo);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
