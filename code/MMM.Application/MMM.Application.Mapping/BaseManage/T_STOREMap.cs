using MMM.Application.Entity.BaseManage;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-24 22:44
    /// 描 述：门店
    /// </summary>
    public class T_STOREMap : EntityTypeConfiguration<T_STOREEntity>
    {
        public T_STOREMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_STORE");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Property(x => x.Longitude).HasPrecision(18, 8);
            this.Property(x => x.Latitude).HasPrecision(18, 8);
            #endregion
        }
    }
}
