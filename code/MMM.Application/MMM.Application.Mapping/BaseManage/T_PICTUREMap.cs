using MMM.Application.Entity.BaseManage;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-28 21:28
    /// 描 述：图片
    /// </summary>
    public class T_PICTUREMap : EntityTypeConfiguration<T_PICTUREEntity>
    {
        public T_PICTUREMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PICTURE");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
