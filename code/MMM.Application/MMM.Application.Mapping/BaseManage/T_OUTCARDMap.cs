using MMM.Application.Entity.BaseManage;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-18 22:59
    /// 描 述：T_OUTCARD
    /// </summary>
    public class T_OUTCARDMap : EntityTypeConfiguration<T_OUTCARDEntity>
    {
        public T_OUTCARDMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_OUTCARD");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
