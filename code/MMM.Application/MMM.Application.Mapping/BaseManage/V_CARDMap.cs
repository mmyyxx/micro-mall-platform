using MMM.Application.Entity.BaseManage;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.BaseManage
{
    /// <summary>
    /// 版 本
    
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：T_CARD
    /// </summary>
    public class V_CARDMap : EntityTypeConfiguration<V_CARDEntity>
    {
        public V_CARDMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_CARD");
            //主键
            this.HasKey(t => t.CardId);
            #endregion

            #region 配置关系
            this.Ignore(a => a.CardTypeName);
            this.Ignore(a => a.FK_AppIdName);
            #endregion
        }
    }
}
