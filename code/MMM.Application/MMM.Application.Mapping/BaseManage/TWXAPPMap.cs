using MMM.Application.Entity.BaseManage;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.BaseManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-06-16 16:33
    /// 描 述：TWXAPP
    /// </summary>
    public class TWXAPPMap : EntityTypeConfiguration<TWXAPPEntity>
    {
        public TWXAPPMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_WXAPP");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
