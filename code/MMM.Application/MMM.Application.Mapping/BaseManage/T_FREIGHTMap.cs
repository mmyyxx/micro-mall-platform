using MMM.Application.Entity.BaseManage;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-19 11:23
    /// 描 述：T_FREIGHT
    /// </summary>
    public class T_FREIGHTMap : EntityTypeConfiguration<T_FREIGHTEntity>
    {
        public T_FREIGHTMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_FREIGHT");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
