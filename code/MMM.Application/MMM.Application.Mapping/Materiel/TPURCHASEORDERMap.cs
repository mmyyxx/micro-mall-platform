using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 10:02
    /// 描 述：入库单
    /// </summary>
    public class TPURCHASEORDERMap : EntityTypeConfiguration<TPURCHASEORDEREntity>
    {
        public TPURCHASEORDERMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PURCHASEORDER");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.FK_SupplierName);
            this.Ignore(x => x.FK_WarehouseNoName);
            #endregion
        }
    }
}
