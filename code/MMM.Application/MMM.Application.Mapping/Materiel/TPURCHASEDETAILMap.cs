using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 10:12
    /// 描 述：订单明细
    /// </summary>
    public class TPURCHASEDETAILMap : EntityTypeConfiguration<TPURCHASEDETAILEntity>
    {
        public TPURCHASEDETAILMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PURCHASEDETAIL");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
