using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-02 12:43
    /// 描 述：T_OUTGOINGSTOCK
    /// </summary>
    public class TOUTGOINGSTOCKMap : EntityTypeConfiguration<TOUTGOINGSTOCKEntity>
    {
        public TOUTGOINGSTOCKMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_OUTGOINGSTOCK");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
