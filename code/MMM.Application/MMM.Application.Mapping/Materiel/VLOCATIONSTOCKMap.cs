using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 版 本
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// </summary>
    public class VLOCATIONSTOCKMap : EntityTypeConfiguration<VLOCATIONSTOCKEntity>
    {
        public VLOCATIONSTOCKMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_LOCATIONSTOCK");
            //主键
            this.HasKey(t => t.FK_Location);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
