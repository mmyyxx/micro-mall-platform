using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 21:32
    /// 描 述：出库单
    /// </summary>
    public class TOUTSTOCKORDERMap : EntityTypeConfiguration<TOUTSTOCKORDEREntity>
    {
        public TOUTSTOCKORDERMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_OUTSTOCKORDER");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.FK_SupplierName);
            this.Ignore(x => x.FK_WarehouseNoName);
            #endregion
        }
    }
}
