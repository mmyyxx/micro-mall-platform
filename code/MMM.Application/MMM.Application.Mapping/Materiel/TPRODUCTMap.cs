using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-31 16:55
    /// 描 述：物料
    /// </summary>
    public class TPRODUCTMap : EntityTypeConfiguration<TPRODUCTEntity>
    {
        public TPRODUCTMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PRODUCT");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
