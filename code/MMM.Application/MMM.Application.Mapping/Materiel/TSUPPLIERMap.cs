using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 21:50
    /// 描 述：客户
    /// </summary>
    public class TSUPPLIERMap : EntityTypeConfiguration<TSUPPLIEREntity>
    {
        public TSUPPLIERMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_SUPPLIER");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
