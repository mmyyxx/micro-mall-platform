using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 14:56
    /// 描 述：包装
    /// </summary>
    public class TPACKAGEMap : EntityTypeConfiguration<TPACKAGEEntity>
    {
        public TPACKAGEMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PACKAGE");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.FK_ProductName);
            #endregion
        }
    }
}
