using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// 描 述：库存
    /// </summary>
    public class TSTOCKMap : EntityTypeConfiguration<TSTOCKEntity>
    {
        public TSTOCKMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_STOCK");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
