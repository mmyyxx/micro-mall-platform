using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-06 21:48
    /// 描 述：出库分配
    /// </summary>
    public class TOUTGOINGMap : EntityTypeConfiguration<TOUTGOINGEntity>
    {
        public TOUTGOINGMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_OUTGOING");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.FK_WarehouseNoName);
            this.Ignore(x => x.PdtName);
            this.Ignore(x => x.SKU);
            #endregion
        }
    }
}
