using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 00:18
    /// 描 述：库存明细
    /// </summary>
    public class TSTOCKDEAILMap : EntityTypeConfiguration<TSTOCKDEAILEntity>
    {
        public TSTOCKDEAILMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_STOCKDEAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
