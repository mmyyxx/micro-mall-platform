using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 22:07
    /// 描 述：上架
    /// </summary>
    public class TPUTAWAYMap : EntityTypeConfiguration<TPUTAWAYEntity>
    {
        public TPUTAWAYMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PUTAWAY");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x=>x.LocationNo);
            this.Ignore(x=>x.SKU);
            this.Ignore(x=>x.ProductName);
            this.Ignore(x=>x.Count);
            this.Ignore(x=>x.MainUnit);
            this.Ignore(x=>x.UnitCount);
            this.Ignore(x=>x.AuxiliaryUnit);
        #endregion
    }
    }
}
