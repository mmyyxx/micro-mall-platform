using MMM.Application.Entity.Materiel;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 10:38
    /// 描 述：物料组合
    /// </summary>
    public class TPRODUCTCOMBMap : EntityTypeConfiguration<TPRODUCTCOMBEntity>
    {
        public TPRODUCTCOMBMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PRODUCTCOMB");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.FK_ChildName);
            this.Ignore(x => x.FK_ProductName);
            #endregion
        }
    }
}
