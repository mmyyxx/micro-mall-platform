using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-23 21:39
    /// 描 述：T_ORDERReturnDetail
    /// </summary>
    public class TORDERReturnDetailMap : EntityTypeConfiguration<TORDERReturnDetailEntity>
    {
        public TORDERReturnDetailMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ORDERReturnDetail");
            //主键
            this.HasKey(t => t.guid);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
