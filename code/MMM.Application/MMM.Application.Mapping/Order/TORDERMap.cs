using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-14 19:23
    /// 描 述：订单
    /// </summary>
    public class TORDERMap : EntityTypeConfiguration<TORDEREntity>
    {
        public TORDERMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ORDER");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.OrderStatusStr);
            this.Ignore(x => x.PayWayStr);
            this.Ignore(x => x.PayResultStr);
            this.Ignore(x => x.OpenIdName);
            this.Ignore(x => x.IsGroupOrderStr);
            this.Ignore(x => x.CouponMoney);
            this.Ignore(x => x.GroupRoleStr);
            this.Ignore(x => x.GroupResultStr);
            this.Ignore(x => x.GroupEndTimeStr);
            this.Ignore(x => x.CreateTimeStr);
            this.Ignore(x => x.RegimentTimeStr);
            this.Ignore(x => x.DeliverTimeStr);
            this.Ignore(x => x.CompleteTimeStr);
            this.Ignore(x => x.DeliveryModeStr);
            this.Ignore(x => x.PlanStoreId);
            this.Ignore(x => x.PlanStoreStr);
            this.Ignore(x => x.ActualStoreStr);
            this.Ignore(x => x.DeliverAdress);
            this.Ignore(x => x.IsDelStr);
            #endregion
        }
    }
}
