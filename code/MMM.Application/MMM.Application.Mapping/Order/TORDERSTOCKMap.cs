using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-19 17:20
    /// 描 述：订单库存
    /// </summary>
    public class TORDERSTOCKMap : EntityTypeConfiguration<TORDERSTOCKEntity>
    {
        public TORDERSTOCKMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ORDERSTOCK");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
