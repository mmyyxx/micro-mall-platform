using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-09-12 15:21
    /// 描 述：T_CommentDetail
    /// </summary>
    public class T_CommentDetailMap : EntityTypeConfiguration<T_CommentDetailEntity>
    {
        public T_CommentDetailMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_CommentDetail");
            //主键
            this.HasKey(t => t.id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
