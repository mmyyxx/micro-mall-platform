using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-23 21:32
    /// 描 述：退货单
    /// </summary>
    public class TORDERReturnMap : EntityTypeConfiguration<TORDERReturnEntity>
    {
        public TORDERReturnMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ORDERReturn");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
