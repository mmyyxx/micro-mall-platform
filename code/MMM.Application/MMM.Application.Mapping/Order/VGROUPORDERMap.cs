using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// </summary>
    public class VGROUPORDERMap : EntityTypeConfiguration<VGROUPORDEREntity>
    {
        public VGROUPORDERMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_GROUPORDER");
            //主键
            this.HasKey(t => t.OrderGUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
