using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VAFTERSALEMap : EntityTypeConfiguration<VAFTERSALEEntity>
    {
        public VAFTERSALEMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_AFTERSALE");
            //主键
            this.HasKey(t => t.GUID);
            #endregion
            this.Ignore(t => t.StatusStr);
            this.Ignore(t => t.ProductClass);
            #region 配置关系
            #endregion
        }
    }
}
