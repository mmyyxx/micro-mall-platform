using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-09-16 10:21
    /// 描 述：售后状态
    /// </summary>
    public class T_OrderAfterSaleMap : EntityTypeConfiguration<T_OrderAfterSaleEntity>
    {
        public T_OrderAfterSaleMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_OrderAfterSale");
            //主键
            this.HasKey(t => t.id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
