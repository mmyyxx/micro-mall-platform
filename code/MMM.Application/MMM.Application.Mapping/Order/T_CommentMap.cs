using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-09-11 14:12
    /// 描 述：商品评论
    /// </summary>
    public class T_CommentMap : EntityTypeConfiguration<T_CommentEntity>
    {
        public T_CommentMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_Comment");
            //主键
            this.HasKey(t => t.id);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
