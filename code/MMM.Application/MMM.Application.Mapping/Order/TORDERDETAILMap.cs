using MMM.Application.Entity.Order;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-16 16:31
    /// 描 述：订单明细
    /// </summary>
    public class TORDERDETAILMap : EntityTypeConfiguration<TORDERDETAILEntity>
    {
        public TORDERDETAILMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ORDERDETAIL");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.ProductName);
            #endregion
        }
    }
}
