using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：订单详情
    /// </summary>
    public class VFAILEDORDERMap : EntityTypeConfiguration<VFAILEDORDEREntity>
    {
        public VFAILEDORDERMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_FAILEDORDER");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
