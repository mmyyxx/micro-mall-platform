using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品分类
    /// </summary>
    public class T_CATEGORY_PRODUCTMap : EntityTypeConfiguration<T_CATEGORY_PRODUCTEntity>
    {
        public T_CATEGORY_PRODUCTMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_CATEGORY_PRODUCT");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
