using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 11:03
    /// 描 述：商品
    /// </summary>
    public class T_PRODUCTPRICEMap : EntityTypeConfiguration<T_PRODUCTPRICEEntity>
    {
        public T_PRODUCTPRICEMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PRODUCTPRICE");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
