using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-02 14:27
    /// 描 述：TSTOCKPD
    /// </summary>
    public class TSTOCKPDMap : EntityTypeConfiguration<TSTOCKPDEntity>
    {
        public TSTOCKPDMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_STOCKPD");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
