using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品类别
    /// </summary>
    public class T_PRODUCTCLASSMap : EntityTypeConfiguration<T_PRODUCTCLASSEntity>
    {
        public T_PRODUCTCLASSMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PRODUCTCLASS");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(x => x.FK_ParentClassName);
            #endregion
        }
    }
}
