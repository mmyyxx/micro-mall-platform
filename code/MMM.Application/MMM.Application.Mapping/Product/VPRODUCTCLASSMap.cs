using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 版 本
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// </summary>
    public class VPRODUCTCLASSMap : EntityTypeConfiguration<VPRODUCTCLASSEntity>
    {
        public VPRODUCTCLASSMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_PRODUCTCLASS");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
