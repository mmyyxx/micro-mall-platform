using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-27 19:51
    /// 描 述：商品分类
    /// </summary>
    public class T_PRODUCTCATEGORYMap : EntityTypeConfiguration<T_PRODUCTCATEGORYEntity>
    {
        public T_PRODUCTCATEGORYMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_PRODUCTCATEGORY");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(a => a.FK_ParentCategoryName);
            #endregion
        }
    }
}
