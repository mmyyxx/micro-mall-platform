using MMM.Application.Entity.Product;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.Product
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-07-02 15:02
    /// 描 述：T_ALLOCATION
    /// </summary>
    public class TALLOCATIONMap : EntityTypeConfiguration<TALLOCATIONEntity>
    {
        public TALLOCATIONMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ALLOCATION");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
