using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-16 21:49
    /// 描 述：积分明细
    /// </summary>
    public class TINTEGRALMap : EntityTypeConfiguration<TINTEGRALEntity>
    {
        public TINTEGRALMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_INTEGRAL");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(t => t.CreateDateStr);
            #endregion
        }
    }
}
