using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-29 10:12
    /// 描 述：地址信息
    /// </summary>
    public class TADRESSMap : EntityTypeConfiguration<TADRESSEntity>
    {
        public TADRESSMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ADRESS");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
