using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 14:33
    /// 描 述：客户
    /// </summary>
    public class VWXUSERMap : EntityTypeConfiguration<VWXUSEREntity>
    {
        public VWXUSERMap()
        {
            #region 表、主键
            //表
            this.ToTable("V_WXUSER");
            //主键
            this.HasKey(t => t.Openid);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
