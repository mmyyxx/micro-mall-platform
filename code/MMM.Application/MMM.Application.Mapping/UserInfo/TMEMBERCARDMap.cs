using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-11 09:07
    /// 描 述：会员卡券
    /// </summary>
    public class TMEMBERCARDMap : EntityTypeConfiguration<TMEMBERCARDEntity>
    {
        public TMEMBERCARDMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_MEMBERCARD");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
