using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 23:28
    /// 描 述：余额明细
    /// </summary>
    public class TMONEYDETAILMap : EntityTypeConfiguration<TMONEYDETAILEntity>
    {
        public TMONEYDETAILMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_MONEYDETAIL");
            //主键
            this.HasKey(t => t.ID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
