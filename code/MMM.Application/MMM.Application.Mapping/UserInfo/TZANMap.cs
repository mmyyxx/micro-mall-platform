using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-29 09:49
    /// 描 述：点赞
    /// </summary>
    public class TZANMap : EntityTypeConfiguration<TZANEntity>
    {
        public TZANMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_ZAN");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
