using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 16:29
    /// 描 述：会员
    /// </summary>
    public class TMEMBERMap : EntityTypeConfiguration<TMEMBEREntity>
    {
        public TMEMBERMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_MEMBER");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
