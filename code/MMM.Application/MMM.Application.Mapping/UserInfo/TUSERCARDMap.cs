using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-15 10:38
    /// 描 述：客户卡
    /// </summary>
    public class TUSERCARDMap : EntityTypeConfiguration<TUSERCARDEntity>
    {
        public TUSERCARDMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_USERCARD");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            this.Ignore(t => t.ValidDateStr);
            this.Ignore(t => t.CreatetimeStr);
            this.Ignore(t => t.day);
            #endregion
        }
    }
}
