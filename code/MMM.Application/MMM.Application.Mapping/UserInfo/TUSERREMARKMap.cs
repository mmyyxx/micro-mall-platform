using MMM.Application.Entity.UserInfo;
using System.Data.Entity.ModelConfiguration;

namespace MMM.Application.Mapping.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-28 15:57
    /// 描 述：用户反馈
    /// </summary>
    public class TUSERREMARKMap : EntityTypeConfiguration<TUSERREMARKEntity>
    {
        public TUSERREMARKMap()
        {
            #region 表、主键
            //表
            this.ToTable("T_USERREMARK");
            //主键
            this.HasKey(t => t.GUID);
            #endregion

            #region 配置关系
            #endregion
        }
    }
}
