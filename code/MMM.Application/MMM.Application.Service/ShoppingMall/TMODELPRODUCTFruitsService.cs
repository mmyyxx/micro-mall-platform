using MMM.Application.Entity.ShoppingMall;
using MMM.Data.Repository;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.ShoppingMall
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-24 11:01
    /// 描 述：模块商品
    /// </summary>
    public class TMODELPRODUCTFruitsService : RepositoryFactory<TMODELPRODUCTFruitsEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IEnumerable<TMODELPRODUCTFruitsEntity> GetList(Pagination pagination, JObject queryJson)
        {
            var query = LinqExtensions.True<TMODELPRODUCTFruitsEntity>();
            if (!queryJson["FK_ModelFunction"].IsEmpty())
            {
                String FK_ModelFunction = queryJson["FK_ModelFunction"].ToString();
                query = query.And(t => FK_ModelFunction== t.FK_ModelFunction);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                query = query.And(t => organizeid==t.organizeid);
            }
            query = query.And(t => t.IsDel == 0);

            return this.BaseRepository().FindList(query, pagination);
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TMODELPRODUCTFruitsEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TMODELPRODUCTFruitsEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TMODELPRODUCTFruitsEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
