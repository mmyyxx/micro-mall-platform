using MMM.Application.Entity.Product;
using MMM.Application.Entity.ShoppingMall;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Service.Cache;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.ShoppingMall
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-24 10:55
    /// 描 述：商城模块
    /// </summary>
    public class TMODELFUNCTIONDigitalService : RepositoryFactory<TMODELFUNCTIONDigitalEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TMODELFUNCTIONDigitalEntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<TMODELFUNCTIONDigitalEntity>();

            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            if (!queryJson["ModelTitle"].IsEmpty())
            {
                String ModelTitle = queryJson["ModelTitle"].ToString();
                expression = expression.And(t => t.ModelTitle.Contains(ModelTitle));
            }

            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TMODELFUNCTIONDigitalEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TMODELFUNCTIONDigitalEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TMODELFUNCTIONDigitalEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                entity.FK_AppId = CodeConst.WxAppKEY;
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TMODELFUNCTIONDigitalEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 获取模块信息
        /// </summary>
        /// <param name="windowWidth"></param>
        /// <param name="appId"></param>
        /// <returns></returns>
        public string GetModelList(string windowWidth, string appId, String orderinfo)
        {
            Data.IDatabase db = DbFactory.Base();

            IQueryable<TMODELFUNCTIONDigitalEntity> functionList = db.IQueryable<TMODELFUNCTIONDigitalEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.IsUse.YES && p.FK_AppId == appId);
            if (!String.IsNullOrEmpty(orderinfo))
            {
                Int32 i;
                Int32.TryParse(orderinfo, out i);
                functionList = functionList.Where(t => t.ModelOrder == i);
            }
            List<ModelList> list = functionList.Select(p => new ModelList
            {
                GUID = p.GUID,
                ModelName = p.ModelName,
                ModelTitle = p.ModelTitle,
                ModelURL = p.ModelURL,
                ModelOrder = p.ModelOrder
            }).OrderBy(p => p.ModelOrder).ToList();

            List<String> ids = list.Select(x => x.GUID).ToList();

            List<TMODELPRODUCTPhoneEntity> productList = db.IQueryable<TMODELPRODUCTPhoneEntity>().Where(t => ids.Contains(t.FK_ModelFunction) && t.IsDel == CodeConst.IsDel.NO).ToList();
            List<String> productids = productList.Where(t => !String.IsNullOrEmpty(t.ImageNavigate1)).Select(x => x.ImageNavigate1).ToList();
            List<T_PRODUCTPRICEEntity> productpriceList = new ProductPriceCache().GetCacheList().Where(t => productids.Contains(t.GUID) && t.IsDel == CodeConst.IsDel.NO).ToList();

            String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];

            ModelList modelFisrt = null;
            foreach (ModelList model in list)
            {
                model.WindowWidth = Int32.Parse(windowWidth);
                model.DateTime = DateTime.Now.ToString("yyyy/MM/dd");
                model.Week = DateTime.Now.ToString("dddd");

                model.ProductList = productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_ModelFunction == model.GUID).Select(p => new ProductModel
                {
                    ProductOrder = p.ProductOrder ?? 0,
                    productName = p.Product1,
                    image = p.ImageAdress1,
                    imageurl = p.ImageNavigate1,
                    price1 = p.Price1 == null ? "0.00" : p.Price1.ToString(),
                    productName2 = p.Product2,
                    image2 = p.ImageAdress2,
                    imageurl2 = p.ImageNavigate2,
                    price2 = p.Price2 == null ? "0.00" : p.Price2.ToString(),
                    ProductDescribes = p.ProductDescribe
                }).OrderBy(p => p.ProductOrder).ToList();

                for (int i = model.ProductList.Count - 1, j = 0; i >= j; i--)
                {
                    ProductModel pm = model.ProductList[i];
                    T_PRODUCTPRICEEntity t_PRODUCTPRICE = productpriceList.FirstOrDefault(t => t.GUID == pm.imageurl);
                    if (t_PRODUCTPRICE != null)
                    {
                        //产品未上架过滤掉
                        if (t_PRODUCTPRICE.IsSale != CodeConst.IsSale.IsShelves)
                        {
                            model.ProductList.Remove(pm);
                            continue;
                        }
                    }
                    if (!String.IsNullOrEmpty(pm.image) && !pm.image.StartsWith("http:") && !pm.image.StartsWith("https:"))
                    {
                        pm.image = ImgUrl + pm.image;
                    }
                    if (!String.IsNullOrEmpty(pm.image2) && !pm.image2.StartsWith("http:") && !pm.image2.StartsWith("https:"))
                    {
                        pm.image2 = ImgUrl + pm.image2;
                    }
                    if (model.ModelName == "modelList7")
                    {
                        //T_PRODUCTPRICEEntity productInfo = productpriceList.Where(p => p.GUID == pm.imageurl).SingleOrDefault();
                        //if (productInfo != null)
                        //{
                        //    //pm.productName = productInfo.Name;
                        //    pm.price1 = productInfo.DiscountPrice.ToString();
                        //    //pm.productName2 = productInfo.ProductUnit;
                        //    //pm.image = productInfo.ProductMainPicture;
                        //}
                        if (!String.IsNullOrEmpty(pm.ProductDescribes))
                            pm.ProductDescribe = pm.ProductDescribes.Split(' ');
                    }
                }
                if (model.ModelName == "modelList5")
                {
                    if (modelFisrt == null)
                    {
                        modelFisrt = model;
                        model.ProductList3 = new List<List<ProductModel>>();
                        model.ProductList3.Add(model.ProductList);
                        model.ProductList = new List<ProductModel>();
                    }
                    else
                    {
                        modelFisrt.ModelTitle += "#" + model.ModelTitle;
                        modelFisrt.ProductList3.Add(model.ProductList);
                        model.ProductList = null;
                    }
                }
            }
            IList<ModelList> tem = new List<ModelList>();
            foreach (ModelList model in list)
            {
                if (model.ProductList != null)
                    tem.Add(model);
            }

            return JSONHelper.WXObjectToJson(tem);
        }

        public string GetMoelListByType(string appId, string type, int top)
        {
            Data.IDatabase db = DbFactory.Base();

            IQueryable<TMODELFUNCTIONDigitalEntity> functionList = db.IQueryable<TMODELFUNCTIONDigitalEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.IsUse.YES && p.ModelName == type && p.FK_AppId == appId);
            List<ModelList> list = functionList.Select(p => new ModelList
            {
                GUID = p.GUID,
                ModelName = p.ModelName,
                ModelTitle = p.ModelTitle,
                ModelURL = p.ModelURL,
                ModelOrder = p.ModelOrder
            }).OrderBy(p => p.ModelOrder).Take(top).ToList();

            List<String> ids = list.Select(x => x.GUID).ToList();

            List<TMODELPRODUCTPhoneEntity> productList = db.IQueryable<TMODELPRODUCTPhoneEntity>().Where(t => ids.Contains(t.FK_ModelFunction) && t.IsDel == CodeConst.IsDel.NO).ToList();
            List<String> productids = productList.Where(t => !String.IsNullOrEmpty(t.ImageNavigate1)).Select(x => x.ImageNavigate1).ToList();
            List<T_PRODUCTPRICEEntity> productpriceList = new ProductPriceCache().GetCacheList().Where(t => productids.Contains(t.GUID) && t.IsDel == CodeConst.IsDel.NO).ToList();

            String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];

            ModelList modelFisrt = null;
            foreach (ModelList model in list)
            {
                model.DateTime = DateTime.Now.ToString("yyyy/MM/dd");
                model.Week = DateTime.Now.ToString("dddd");

                model.ProductList = productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_ModelFunction == model.GUID).Select(p => new ProductModel
                {
                    ProductOrder = p.ProductOrder ?? 0,
                    productName = p.Product1,
                    image = p.ImageAdress1,
                    imageurl = p.ImageNavigate1,
                    price1 = p.Price1 == null ? "0.00" : p.Price1.ToString(),
                    productName2 = p.Product2,
                    image2 = p.ImageAdress2,
                    imageurl2 = p.ImageNavigate2,
                    price2 = p.Price2 == null ? "0.00" : p.Price2.ToString(),
                    ProductDescribes = p.ProductDescribe
                }).OrderBy(p => p.ProductOrder).ToList();

                for (int i = model.ProductList.Count - 1, j = 0; i >= j; i--)
                {
                    ProductModel pm = model.ProductList[i];
                    T_PRODUCTPRICEEntity t_PRODUCTPRICE = productpriceList.FirstOrDefault(t => t.GUID == pm.imageurl);
                    if (t_PRODUCTPRICE != null)
                    {
                        //产品未上架过滤掉
                        if (t_PRODUCTPRICE.IsSale != CodeConst.IsSale.IsShelves)
                        {
                            model.ProductList.Remove(pm);
                            continue;
                        }
                    }
                    if (!String.IsNullOrEmpty(pm.image) && !pm.image.StartsWith("http:") && !pm.image.StartsWith("https:"))
                    {
                        pm.image = ImgUrl + pm.image;
                    }
                    if (!String.IsNullOrEmpty(pm.image2) && !pm.image2.StartsWith("http:") && !pm.image2.StartsWith("https:"))
                    {
                        pm.image2 = ImgUrl + pm.image2;
                    }
                    if (model.ModelName == "modelList7")
                    {
                        pm.ProductDescribe = pm.ProductDescribes.Split(' ');
                    }
                }
            }

            return JSONHelper.WXObjectToJson(list);
        }
        #endregion
    }
}
