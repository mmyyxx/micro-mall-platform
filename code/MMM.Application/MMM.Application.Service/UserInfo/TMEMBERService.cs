using MMM.Application.Entity.UserInfo;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using MMM.Application.Code;

namespace MMM.Application.Service.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 16:29
    /// 描 述：会员
    /// </summary>
    public class TMEMBERService : RepositoryFactory<TMEMBEREntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TMEMBEREntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<TMEMBEREntity>();
            if (!queryJson["UserName"].IsEmpty())
            {
                String UserName = Convert.ToString(queryJson["UserName"]);
                expression = expression.And(t => t.UserName.Contains(UserName));
            }
            if (!queryJson["Mobile"].IsEmpty())
            {
                String Mobile = Convert.ToString(queryJson["Mobile"]);
                expression = expression.And(t => t.Mobile.Contains(Mobile));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TMEMBEREntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TMEMBEREntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="unionid">wx外键</param>
        /// <returns></returns>
        public TMEMBEREntity GetEntitByUnionId(string unionid)
        {
            return this.BaseRepository().IQueryable().FirstOrDefault(x => x.FK_UnionId == unionid);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TMEMBEREntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TMEMBEREntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 保存价格变动
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="newMoney"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public string SaveMoney(string keyValue, decimal? newMoney, string remark)
        {
            Data.IDatabase db = DbFactory.Base();

            TMEMBEREntity tem = db.FindEntity<TMEMBEREntity>(keyValue);
            if (tem == null) return "会员信息不存在!";

            TMONEYDETAILEntity moneydetail = new TMONEYDETAILEntity();
            moneydetail.ID = Guid.NewGuid().ToString();
            moneydetail.FK_Member = tem.GUID;
            moneydetail.MoneyChange = newMoney;
            moneydetail.Remark = remark;
            moneydetail.Creater = OperatorProvider.Provider.Current().CompanyId;
            moneydetail.CreateTime = DateTime.Now;
            moneydetail.organizeid = tem.organizeid;
            moneydetail.BeforeMoney = tem.Money;
            tem.Money += (newMoney ?? 0);
            moneydetail.AfterMoney = tem.Money;
            try
            {
                db.BeginTrans();
                db.Insert<TMONEYDETAILEntity>(moneydetail);
                db.Update<TMEMBEREntity>(tem, null);
                db.Commit();
                return "";
            }
            catch (Exception e)
            {
                db.Rollback();
                return e.ToString();
            }

        }
        #endregion
    }
}
