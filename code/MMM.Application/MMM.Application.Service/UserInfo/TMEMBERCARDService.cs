using MMM.Application.Entity.UserInfo;
using MMM.Data.Repository;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-11 09:07
    /// 描 述：会员卡券
    /// </summary>
    public class TMEMBERCARDService : RepositoryFactory<TMEMBERCARDEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TMEMBERCARDEntity> GetList(JObject queryJson)
        {
            IQueryable<TMEMBERCARDEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["IsUse"].IsEmpty())
            {
                Int32 IsUse = Convert.ToInt32(queryJson["IsUse"]);
                query = query.Where(t => t.IsUse == IsUse);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                query = query.Where(t => t.organizeid == organizeid);
            }
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TMEMBERCARDEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TMEMBERCARDEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }
        #endregion
    }
}
