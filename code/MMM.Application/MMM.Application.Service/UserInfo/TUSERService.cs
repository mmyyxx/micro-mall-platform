using MMM.Application.Entity.UserInfo;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using MMM.Application.Entity;
using MMM.Util;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Util.Security;

namespace MMM.Application.Service.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 14:33
    /// 描 述：客户
    /// </summary>
    public class TUSERService : RepositoryFactory<TUSEREntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TUSEREntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }

        /// <summary>
        /// 微信小程序登录
        /// </summary>
        /// <param name="code"></param>
        /// <param name="appId"></param>
        /// <returns></returns>
        public string GetUserSession(string code, string appId)
        {
            ResultData result = new ResultData();

            Data.IDatabase db = DbFactory.Base();

            String[] key = CodeConst.wxinfo[appId].Split('-');

            try
            {
                db.BeginTrans();
                string sessionURL = String.Format(CodeConst.LoginCheck, key[0], key[1], code);
                string session = HttpMethods.HttpGet(sessionURL);
                UserSession usersession = JSONHelper.WXDecodeObject<UserSession>(session);
                if (usersession.openid != null)
                {
                    TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == usersession.openid).SingleOrDefault();
                    if (user == null)
                    {
                        user = new TUSEREntity();
                        user.Openid = usersession.openid;
                        user.FK_AppId = appId;
                        user.SessionKey = usersession.session_key;
                        if (!String.IsNullOrEmpty(usersession.unionid))
                        {
                            user.Unionid = usersession.unionid;
                        }
                        user.GoldEgg = 1;
                        user.IsShare = CodeConst.IsUse.NO;
                        user.Integral = 0;
                        user.SignInDate = null;
                        user.IsDel = CodeConst.IsDel.NO;
                        user.Creater = usersession.openid;
                        user.CreateTime = DateTime.Now;

                        db.Insert<TUSEREntity>(user);
                    }
                    else
                    {
                        user.SessionKey = usersession.session_key;
                        user.IsDel = CodeConst.IsDel.NO;
                        user.Modifyer = "wx";
                        user.ModifyTime = DateTime.Now;

                        if (!String.IsNullOrEmpty(usersession.unionid))
                            user.Unionid = usersession.unionid;
                        else
                            usersession.unionid = usersession.openid;

                        db.Update<TUSEREntity>(user, null);

                        TMEMBEREntity newmember = db.IQueryable<TMEMBEREntity>().Where(p => (p.FK_UnionId == usersession.openid || p.FK_UnionId == usersession.unionid) && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                        if (newmember != null)
                        {
                            if (newmember.FK_UnionId != usersession.unionid)
                            {
                                //newmember.FK_UnionId = usersession.unionid;
                                //db.Update<TMEMBEREntity>(newmember, null);
                            }
                            result.IsOK = true;
                        }
                    }

                    result.msg = (user.EventDraw ?? 0).ToString() + "," + (user.Integral ?? 0).ToString();
                    result.Data = usersession.openid;
                }
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TUSEREntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TUSEREntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }


        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TUSEREntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TUSEREntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        public String GetUserInfo(string openid)
        {
            Data.IDatabase db = DbFactory.Base();

            TUSEREntity user = db.IQueryable<TUSEREntity>().SingleOrDefault(p => p.Openid == openid && p.IsDel == CodeConst.IsDel.NO);

            return JSONHelper.WXObjectToJson(user);
        }
        /// <summary>
        /// 返回是否是会员
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="successData"></param>
        /// <returns></returns>
        public bool SaveUserInfo(string openid, string successData)
        {
            Data.IDatabase db = DbFactory.Base();

            GetUserInfo getuserinfo = JSONHelper.WXDecodeObject<GetUserInfo>(successData);

            TUSEREntity user = db.IQueryable<TUSEREntity>().SingleOrDefault(p => p.Openid == openid && p.IsDel == CodeConst.IsDel.NO);
            if (user != null)
            {
                try
                {
                    db.BeginTrans();

                    string decryptData = AESInfo.AESDecrypt(getuserinfo.encryptedData, user.SessionKey, getuserinfo.iv);
                    EncryptedData newdecryptData = JSONHelper.WXDecodeObject<EncryptedData>(decryptData);

                    user.NickName = newdecryptData.nickName;
                    user.AvatarUrl = newdecryptData.avatarUrl;
                    user.Gender = newdecryptData.gender;
                    user.City = newdecryptData.city;
                    user.Province = newdecryptData.province;
                    user.Country = newdecryptData.country;
                    user.Language = newdecryptData.language;
                    user.Unionid = newdecryptData.unionId;
                    user.Modifyer = openid;
                    user.ModifyTime = DateTime.Now;

                    db.Update<TUSEREntity>(user, null);

                    TMEMBEREntity newmember = db.IQueryable<TMEMBEREntity>().SingleOrDefault(p => (p.FK_UnionId == openid || (p.FK_UnionId == newdecryptData.unionId && !String.IsNullOrEmpty(newdecryptData.unionId))) && p.IsDel == CodeConst.IsDel.NO);
                    //if (newmember != null && newmember.FK_UnionId != newdecryptData.unionId)
                    //{
                    //    newmember.FK_UnionId = newdecryptData.unionId;
                    //    db.Update<TMEMBEREntity>(newmember, null);
                    //}
                    db.Commit();
                    return newmember != null;
                }
                catch (Exception e)
                {
                    db.Rollback();
                    throw new Exception(e.ToString());
                }
            }

            return false;
        }
        #endregion
    }
}
