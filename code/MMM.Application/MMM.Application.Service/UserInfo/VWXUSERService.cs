using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.UserInfo;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.UserInfo
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VWXUSERService : RepositoryFactory<VWXUSEREntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<VWXUSEREntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<VWXUSEREntity>();
            if (!queryJson["NickName"].IsEmpty())
            {
                String NickName = Convert.ToString(queryJson["NickName"]);
                expression = expression.And(t => t.NickName.Contains(NickName) || t.UserName.Contains(NickName));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid || String.IsNullOrEmpty(t.organizeid));
            }
            expression = expression.And(t => t.IsDel == 0 && !String.IsNullOrEmpty(t.Unionid));
            return this.BaseRepository().FindList(expression, pagination);
        }

        #endregion
    }
}
