using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：T_CARD
    /// </summary>
    public class VCOSTINFOService : RepositoryFactory<VCOSTINFOEntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<VCOSTINFOEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public VCOSTINFOEntity GetEntityBySKU(string sku)
        {
            return this.BaseRepository().IQueryable().FirstOrDefault(x=>x.SKU==sku);
        }
        #endregion
    }
}
