using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VOUTSTOCKDETAILService : RepositoryFactory<VOUTSTOCKDETAILEntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<VOUTSTOCKDETAILEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <returns></returns>
        public VOUTSTOCKDETAILEntity GetEntityBySKU(string warehouseNo)
        {
            return this.BaseRepository().IQueryable().FirstOrDefault(x=>x.WarehouseNo== warehouseNo);
        }
        #endregion
    }
}
