using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 14:56
    /// 描 述：包装
    /// </summary>
    public class TPACKAGEService : RepositoryFactory<TPACKAGEEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TPACKAGEEntity> GetList(JObject queryJson)
        {
            IQueryable<TPACKAGEEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["FK_Product"].IsEmpty())
            {
                String FK_Product = queryJson["FK_Product"].ToString();
                query = query.Where(x => x.FK_Product == FK_Product);
            }
            query = query.Where(x => x.IsDel == 0);
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TPACKAGEEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 判断编号是否重复
        /// </summary>
        /// <param name="PackageSKU"></param>
        /// <param name="keyValue"></param>
        /// <param name="FK_Product"></param>
        /// <returns></returns>
        public TPACKAGEEntity GetEntityByNo(String PackageSKU, String keyValue, String FK_Product)
        {
            if (String.IsNullOrEmpty(keyValue))
                return BaseRepository().FindEntity(x => x.PackageSKU == PackageSKU && FK_Product == x.FK_Product && 0 == x.IsDel);
            else
                return BaseRepository().FindEntity(x => x.PackageSKU == PackageSKU && FK_Product == x.FK_Product && x.GUID != keyValue && 0 == x.IsDel);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TPACKAGEEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TPACKAGEEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
