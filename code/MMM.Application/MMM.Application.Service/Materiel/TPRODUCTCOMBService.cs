using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 10:38
    /// 描 述：物料组合
    /// </summary>
    public class TPRODUCTCOMBService : RepositoryFactory<TPRODUCTCOMBEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TPRODUCTCOMBEntity> GetList(JObject queryJson)
        {
            IQueryable<TPRODUCTCOMBEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["FK_Product"].IsEmpty())
            {
                String FK_Product = queryJson["FK_Product"].ToString();
                query = query.Where(x => x.FK_Product == FK_Product);
            }
            if (!queryJson["FK_Child"].IsEmpty())
            {
                String FK_Child = queryJson["FK_Child"].ToString();
                query = query.Where(x => x.FK_Child == FK_Child);
            }
            if (!queryJson["GUID"].IsEmpty())
            {
                String GUID = queryJson["GUID"].ToString();
                query = query.Where(x => x.GUID != GUID);
            }
            query = query.Where(x => x.IsDel == 0);
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TPRODUCTCOMBEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TPRODUCTCOMBEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }
        #endregion
    }
}
