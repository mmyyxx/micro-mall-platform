using MMM.Application.Code;
using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// 描 述：库存
    /// </summary>
    public class TSTOCKService : RepositoryFactory<TSTOCKEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TSTOCKEntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TSTOCKEntity> GetList(JObject queryJson)
        {
            IQueryable<TSTOCKEntity> query = this.BaseRepository().IQueryable().Where(x => x.IsDel == 0);
            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = queryJson["FK_WarehouseNo"].ToString();
                query = query.Where(t => FK_WarehouseNo.Contains(t.FK_WarehouseNo));
            }
            if (!queryJson["FK_Product"].IsEmpty())
            {
                String FK_Product = queryJson["FK_Product"].ToString();
                query = query.Where(t => t.FK_Product == FK_Product);
            }
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TSTOCKEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <param name="Remark"></param>
        /// <returns></returns>
        public String SaveForm(string keyValue, TSTOCKEntity entity, String Remark)
        {
            Data.IDatabase db = DbFactory.Base();

            TSTOCKEntity stockInfo = db.FindEntity<TSTOCKEntity>(keyValue);
            stockInfo.Stock += entity.Stock;
            stockInfo.ActualStock += entity.Stock;
            stockInfo.Modifyer = OperatorProvider.Provider.Current().Account;
            stockInfo.ModifyTime = DateTime.Now;
            if (stockInfo.Stock < Decimal.Zero || stockInfo.ActualStock < Decimal.Zero)
            {
                return("库存不能为负数！");
            }

            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
            stockdetail.ID = Guid.NewGuid().ToString();
            stockdetail.CreateTime = DateTime.Now;
            stockdetail.Creater = OperatorProvider.Provider.Current().Account;
            stockdetail.FK_Stock = stockInfo.GUID;
            stockdetail.Stock = entity.Stock;
            stockdetail.ActualStock = entity.Stock;
            stockdetail.Type = 3;
            stockdetail.Remark = Remark;
            stockdetail.organizeid = stockInfo.organizeid;

            try
            {
                db.BeginTrans();
                db.Update<TSTOCKEntity>(stockInfo,null);
                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                db.Commit();
            }
            catch(Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }
        #endregion
    }
}
