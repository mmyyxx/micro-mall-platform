using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using MMM.Application.Code;
using MMM.Application.Entity.Warehouse;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 10:02
    /// 描 述：入库单
    /// </summary>
    public class TPURCHASEORDERService : RepositoryFactory<TPURCHASEORDEREntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TPURCHASEORDEREntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson, IList<String> supplierIds, IList<String> detailIds)
        {
            var expression = LinqExtensions.True<TPURCHASEORDEREntity>();
            if (!queryJson["PurchaseOrderNo"].IsEmpty())
            {
                String PurchaseOrderNo = queryJson["PurchaseOrderNo"].ToString();
                expression = expression.And(t => t.PurchaseOrderNo.Contains(PurchaseOrderNo));
            }
            if (!queryJson["Status"].IsEmpty())
            {
                Int32 Status = Convert.ToInt32(queryJson["Status"]);
                expression = expression.And(t => t.Status == Status);
            }
            if (!queryJson["OrderType"].IsEmpty())
            {
                Int32 OrderType = Convert.ToInt32(queryJson["OrderType"]);
                expression = expression.And(t => t.OrderType == OrderType);
            }
            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = Convert.ToString(queryJson["FK_WarehouseNo"]);
                expression = expression.And(t => FK_WarehouseNo.Contains(t.FK_WarehouseNo));
            }
            if (!queryJson["StartTime"].IsEmpty())
            {
                DateTime StartTime = Convert.ToDateTime(queryJson["StartTime"]);
                expression = expression.And(t => t.CreateTime >= StartTime);
            }
            if (!queryJson["EndTime"].IsEmpty())
            {
                DateTime EndTime = Convert.ToDateTime(queryJson["EndTime"]).AddDays(1);
                expression = expression.And(t => t.CreateTime < EndTime);
            }
            if (detailIds != null && detailIds.Count > 0)
            {
                expression = expression.And(t => detailIds.Contains(t.GUID));
            }
            if (supplierIds != null && supplierIds.Count > 0)
            {
                expression = expression.And(t => supplierIds.Contains(t.FK_Supplier));
            }
            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);

            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TPURCHASEORDEREntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TPURCHASEORDEREntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TPURCHASEORDEREntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Status = null;
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                Random rd = new Random();
                entity.PurchaseOrderNo = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0');
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TPURCHASEORDEREntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 物料单上架
        /// </summary>
        /// <param name="keyValue"></param>
        public String Upper(String keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            TPURCHASEORDEREntity entity = db.FindEntity<TPURCHASEORDEREntity>(keyValue);
            if (entity.Status != 0)
                return "只有未上架的入库单可以上架。";

            Random rd = new Random();
            string batchNumber = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(99).ToString().PadLeft(2, '0');

            IList<TPURCHASEDETAILEntity> detaillist = db.IQueryable<TPURCHASEDETAILEntity>().Where(x => x.IsDel == 0 && x.FK_PurchaseOrder == keyValue).ToList();

            //获取仓库对应的区域
            IList<T_AREAEntity> areaList = db.IQueryable<T_AREAEntity>().Where(x => x.FK_WarehouseNo == entity.FK_WarehouseNo && x.IsDel == 0).ToList();
            IList<String> areaids = areaList.Select(x => x.GUID).ToList();

            //取出所有的location
            IList<T_LOCATIONEntity> locationList = db.IQueryable<T_LOCATIONEntity>().Where(x => x.IsDel == 0 && areaids.Contains(x.FK_Area)).ToList();
            IList<String> locationids = locationList.Select(x => x.GUID).ToList();

            //获取库存
            IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(x => locationids.Contains(x.FK_Location)).ToList();

            //物料信息
            IList<String> detailskus = detaillist.Select(x => x.SKU).ToList();
            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == 0 && detailskus.Contains(p.SKU)).ToList();

            try
            {

                db.BeginTrans();
                IList<TPUTAWAYEntity> putWayList = new List<TPUTAWAYEntity>();
                foreach (TPURCHASEDETAILEntity detail in detaillist)
                {
                    TPRODUCTEntity product = productList.FirstOrDefault(p => p.SKU == detail.SKU);
                    String FK_Location = "";
                    if (product == null)
                        FK_Location = "";
                    else
                    {
                        string location = stockList.Where(p => p.FK_Product == product.GUID).GroupBy(p => p.FK_Location).OrderBy(p => p.Sum(m => m.Stock)).Select(p => p.Key).FirstOrDefault();
                        if (location != null)
                        {
                            FK_Location = location;
                        }
                        else
                        {
                            List<string> locationlist = stockList.Select(p => p.FK_Location).ToList();
                            IList<String> areaTempIds = areaList.Where(x => x.AreaType == product.StorageMode).Select(x => x.GUID).ToList();
                            T_LOCATIONEntity locationInfo = locationList.Where(p => !locationlist.Contains(p.GUID) && areaTempIds.Contains(p.FK_Area)).OrderBy(p => p.LocationNo).FirstOrDefault();
                            if (locationInfo != null)
                            {
                                FK_Location = locationInfo.GUID;
                            }
                            else
                            {
                                locationInfo = locationList.Where(p => areaTempIds.Contains(p.FK_Area)).OrderBy(p => p.LocationNo).FirstOrDefault();
                                if (locationInfo != null)
                                {
                                    FK_Location = locationInfo.GUID;
                                }
                            }
                        }
                    }

                    TPUTAWAYEntity putaway = new TPUTAWAYEntity();
                    putaway.GUID = Guid.NewGuid().ToString();
                    putaway.FK_PurchaseDetail = detail.GUID;
                    putaway.FK_Location = FK_Location;
                    putaway.BatchNumber = batchNumber;
                    putaway.Status = 1;
                    putaway.IsDel = 0;
                    putaway.Creater = OperatorProvider.Provider.Current().Account;
                    putaway.CreateTime = DateTime.Now;
                    putaway.organizeid = detail.organizeid;
                    putWayList.Add(putaway);
                }
                entity.Status = 1;
                entity.Modifyer = OperatorProvider.Provider.Current().Account;
                entity.ModifyTime = DateTime.Now;

                db.Insert<TPUTAWAYEntity>(putWayList);
                db.Update<TPURCHASEORDEREntity>(entity, null);
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw (new Exception(e.ToString()));
            }
            return "";
        }

        /// <summary>
        /// 物料单上架
        /// </summary>
        /// <param name="organizeid"></param>
        public string UpperAll(String organizeid)
        {
            Data.IDatabase db = DbFactory.Base();

            IQueryable<TPURCHASEORDEREntity> query = db.IQueryable<TPURCHASEORDEREntity>();
            if (!String.IsNullOrEmpty(organizeid))
                query = query.Where(x => x.organizeid == organizeid);
            IList<TPURCHASEORDEREntity> purchaseOrderlist = query.Where(p => p.IsDel == 0 && p.Status == 0).ToList();
            if (purchaseOrderlist.Count == 0)
            {
                return "不存在未上架的入库单！";
            }
            IList<String> orderIds = purchaseOrderlist.Select(t => t.GUID).ToList();
            IList<TPURCHASEDETAILEntity> detaillist = db.IQueryable<TPURCHASEDETAILEntity>().Where(x => orderIds.Contains(x.FK_PurchaseOrder)).ToList();

            Random rd = new Random();
            string batchNumber = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(99).ToString().PadLeft(2, '0');

            //获取单子中仓库的所有区域
            IList<String> wareHourseids = purchaseOrderlist.Select(t => t.FK_WarehouseNo).ToList();
            IList<T_AREAEntity> areaList = db.IQueryable<T_AREAEntity>().Where(x => x.IsDel == 0 && wareHourseids.Contains(x.FK_WarehouseNo)).ToList();
            IList<String> areaids = areaList.Select(x => x.GUID).ToList();

            //取出所有的location
            IList<T_LOCATIONEntity> locationList = db.IQueryable<T_LOCATIONEntity>().Where(x => x.IsDel == 0 && areaids.Contains(x.FK_Area)).ToList();
            IList<String> locationids = locationList.Select(x => x.GUID).ToList();

            //获取库存
            IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(x => locationids.Contains(x.FK_Location)).ToList();

            //物料信息
            IList<String> detailskus = detaillist.Select(x => x.SKU).ToList();
            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == 0 && detailskus.Contains(p.SKU)).ToList();
            try
            {
                db.BeginTrans();
                IList<TPUTAWAYEntity> putWayList = new List<TPUTAWAYEntity>();
                foreach (TPURCHASEDETAILEntity detail in detaillist)
                {
                    TPRODUCTEntity product = productList.FirstOrDefault(p => p.SKU == detail.SKU);
                    organizeid = product.organizeid;
                    String FK_Location = "";
                    if (product == null)
                        FK_Location = "";
                    else
                    {
                        string location = stockList.Where(p => p.FK_Product == product.GUID).GroupBy(p => p.FK_Location).OrderBy(p => p.Sum(m => m.Stock)).Select(p => p.Key).FirstOrDefault();
                        if (location != null)
                        {
                            FK_Location = location;
                        }
                        else
                        {
                            List<string> locationlist = stockList.Where(p => p.organizeid == organizeid).Select(p => p.FK_Location).ToList();
                            IList<String> areaTempIds = areaList.Where(x => x.organizeid == organizeid && x.AreaType == product.StorageMode).Select(x => x.GUID).ToList();
                            T_LOCATIONEntity locationInfo = locationList.Where(p => !locationlist.Contains(p.GUID) && areaTempIds.Contains(p.FK_Area)).OrderBy(p => p.LocationNo).FirstOrDefault();
                            if (locationInfo != null)
                            {
                                FK_Location = locationInfo.GUID;
                            }
                            else
                            {
                                locationInfo = locationList.Where(p => areaTempIds.Contains(p.FK_Area)).OrderBy(p => p.LocationNo).FirstOrDefault();
                                if (locationInfo != null)
                                {
                                    FK_Location = locationInfo.GUID;
                                }
                            }
                        }
                    }

                    TPUTAWAYEntity putaway = new TPUTAWAYEntity();
                    putaway.GUID = Guid.NewGuid().ToString();
                    putaway.FK_PurchaseDetail = detail.GUID;
                    putaway.FK_Location = FK_Location;
                    putaway.BatchNumber = batchNumber;
                    putaway.Status = 1;
                    putaway.IsDel = 0;
                    putaway.Creater = OperatorProvider.Provider.Current().Account;
                    putaway.CreateTime = DateTime.Now;
                    putaway.organizeid = detail.organizeid;
                    putWayList.Add(putaway);
                }
                db.Insert<TPUTAWAYEntity>(putWayList);

                foreach (TPURCHASEORDEREntity entity in purchaseOrderlist)
                {
                    entity.Status = 1;
                    entity.Modifyer = OperatorProvider.Provider.Current().Account;
                    entity.ModifyTime = DateTime.Now;
                    db.Update<TPURCHASEORDEREntity>(entity, null);
                }

                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw (new Exception(e.ToString()));
            }

            return "";
        }

        /// <summary>
        /// 上架完成
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public string SaveStatus(string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            TPURCHASEORDEREntity purchaseorder = db.FindEntity<TPURCHASEORDEREntity>(keyValue);
            if (purchaseorder.Status == 0)
            {
                return "物料尚未上架!";
            }

            IList<TPURCHASEDETAILEntity> detailList = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == 0 && p.FK_PurchaseOrder == purchaseorder.GUID).ToList();
            IList<String> detailIds = new List<String>();
            IList<String> productIds = new List<String>();
            foreach (TPURCHASEDETAILEntity detail in detailList)
            {
                detailIds.Add(detail.GUID);
                productIds.Add(detail.FK_Product);
            }
            //物料数据
            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == 0 && productIds.Contains(p.GUID)).ToList();


            List<TPUTAWAYEntity> list = db.IQueryable<TPUTAWAYEntity>().Where(p => p.IsDel == 0 && p.Status == 1 && detailIds.Contains(p.FK_PurchaseDetail)).ToList();

            List<TSTOCKEntity> stockList = null;
            if (list.Count != 0)
            {
                //获取当前所有的
                IList<String> putawayIds = list.Select(x => x.GUID).ToList();
                stockList = db.IQueryable<TSTOCKEntity>().Where(x => x.IsDel == 0 && productIds.Contains(x.FK_Product)).ToList();
            }
            try
            {
                db.BeginTrans();
                foreach (TPUTAWAYEntity putaway in list)
                {
                    putaway.ActualLocation = putaway.FK_Location;
                    putaway.Status = 0;
                    putaway.Operator = OperatorProvider.Provider.Current().Account;
                    putaway.CompleteTime = DateTime.Now;

                    db.Update<TPUTAWAYEntity>(putaway, null);

                    TPURCHASEDETAILEntity purchasedetail = detailList.FirstOrDefault(p => p.IsDel == 0 && p.GUID == putaway.FK_PurchaseDetail);

                    if (purchasedetail == null)
                    {
                        return "入库单明细不存在！";
                    }

                    TSTOCKEntity stock = stockList.SingleOrDefault(p => p.FK_Location == putaway.FK_Location && p.FK_Product == purchasedetail.FK_Product && p.ProductionDate == purchasedetail.ProductionDate);

                    if (stock != null)
                    {
                        stock.Stock += purchasedetail.Count;
                        stock.ActualStock += purchasedetail.Count;
                        stock.Modifyer = OperatorProvider.Provider.Current().Account;
                        stock.ModifyTime = DateTime.Now;
                        db.Update<TSTOCKEntity>(stock, null);
                    }
                    else
                    {
                        stock = new TSTOCKEntity();
                        stock.GUID = Guid.NewGuid().ToString();
                        stock.FK_Location = putaway.FK_Location;
                        stock.FK_Product = purchasedetail.FK_Product;
                        stock.BatchNumber = putaway.BatchNumber;
                        stock.Stock = purchasedetail.Count;
                        stock.ActualStock = purchasedetail.Count;
                        stock.ProductionDate = purchasedetail.ProductionDate;
                        stock.QualityDay = productList.SingleOrDefault(p => p.IsDel == 0 && p.GUID == purchasedetail.FK_Product).QualityDate;
                        stock.IsDel = 0;
                        stock.Creater = OperatorProvider.Provider.Current().Account;
                        stock.CreateTime = DateTime.Now;
                        stock.organizeid = putaway.organizeid;
                        stock.FK_WarehouseNo = purchaseorder.FK_WarehouseNo;

                        db.Insert<TSTOCKEntity>(stock);
                    }

                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                    stockdetail.ID = Guid.NewGuid().ToString();
                    stockdetail.FK_Stock = stock.GUID;
                    stockdetail.Stock = purchasedetail.Count;
                    stockdetail.ActualStock = purchasedetail.Count;
                    stockdetail.Type = 1;
                    stockdetail.Remark = String.Format("入库单号：{0}", purchaseorder.PurchaseOrderNo);
                    stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                    stockdetail.CreateTime = DateTime.Now;
                    stockdetail.organizeid = putaway.organizeid;

                    db.Insert<TSTOCKDEAILEntity>(stockdetail);
                }
                purchaseorder.Status = 2;
                db.Update<TPURCHASEORDEREntity>(purchaseorder, null);
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }

            return "";
        }

        /// <summary>
        /// 上架导入
        /// </summary>
        /// <param name="purchaseorder"></param>
        /// <param name="detaillist"></param>
        /// <returns></returns>
        public string SaveImport(TPURCHASEORDEREntity purchaseorder, List<TPURCHASEDETAILEntity> detaillist)
        {
            Data.IDatabase db = DbFactory.Base();
            TSUPPLIEREntity supplier = db.IQueryable<TSUPPLIEREntity>().Where(p => p.IsDel == 0 && p.SupplierNo == purchaseorder.FK_Supplier).FirstOrDefault();
            if (supplier == null)
            {
                return "供应商不存在！";
            }
            
            Random rd = new Random();
            purchaseorder.PurchaseOrderNo = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0');
            purchaseorder.FK_Supplier = supplier.GUID;
            purchaseorder.Status = 0;
            purchaseorder.PurchaseOrderPrice = Decimal.Zero;

            IList<String> skuList = detaillist.Select(x => x.SKU).ToList();
            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == 0 && skuList.Contains(p.SKU)).ToList();

            foreach (TPURCHASEDETAILEntity detail in detaillist)
            {
                TPRODUCTEntity product = productList.FirstOrDefault(p => p.IsDel == 0 && p.SKU == detail.SKU);
                if (product == null)
                {
                    return String.Format("物料SKU{0}不存在！", detail.SKU);
                }
                detail.FK_PurchaseOrder = purchaseorder.GUID;
                detail.FK_Product = product.GUID;
                detail.ProductName = product.PdtName;
                purchaseorder.PurchaseOrderPrice += (detail.Price * detail.Count * detail.Discount);
            }
            try
            {
                db.BeginTrans();
                db.Insert<TPURCHASEORDEREntity>(purchaseorder);
                db.Insert<TPURCHASEDETAILEntity>(detaillist);

                db.Commit();
            }
            catch(Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }

            return "";
        }
        #endregion
    }
}
