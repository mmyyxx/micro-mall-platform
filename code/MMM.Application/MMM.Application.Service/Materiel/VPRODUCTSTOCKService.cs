using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VPRODUCTSTOCKService : RepositoryFactory<VPRODUCTSTOCKEntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<VPRODUCTSTOCKEntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<VPRODUCTSTOCKEntity>();
            if (!queryJson["ProductNo"].IsEmpty())
            {
                String ProductNo = queryJson["ProductNo"].ToString();
                expression = expression.And(t => t.ProductNo.Contains(ProductNo) || t.SKU.Contains(ProductNo));
            }
            if (!queryJson["PdtName"].IsEmpty())
            {
                String PdtName = queryJson["PdtName"].ToString();
                expression = expression.And(t => t.PdtName.Contains(PdtName));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }

            return this.BaseRepository().FindList(expression, pagination);
        }

        /// <summary>
        /// 获取过期商品
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson"></param>
        /// <returns></returns>
        public IList<VPRODUCTSTOCK> GetPageList2(Pagination pagination, JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();
            // && p.Stock > Decimal.Zero
            IQueryable<ProductStock> query = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == 0).Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == 0), m => m.FK_Product, n => n.GUID, (m, n) => new ProductStock
            {
                FK_Product = m.FK_Product,
                ProductNo = n.ProductNo,
                SKU = n.SKU,
                PdtName = n.PdtName,
                SaleCount = n.SaleCount,
                UseStock = m.Stock,
                Stock = m.Stock,
                ActualStock = m.ActualStock,
                ProductionDate = m.ProductionDate.Value,
                QualityDate = n.QualityDate,
                organizeid = n.organizeid
            });
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                query = query.Where(t => t.organizeid == organizeid);
            }
            if (!queryJson["days"].IsEmpty())
            {
                DateTime dat = DateTime.Now.AddDays(Int32.Parse(queryJson["days"].ToString()));
                query = query.Where(x => DbFunctions.DiffDays(x.ProductionDate, dat) >= x.QualityDate);
            }

            IQueryable<VPRODUCTSTOCK> list = query.GroupBy(p => p.FK_Product).Select(p => new VPRODUCTSTOCK
            {
                FK_Product = p.Key,
                ProductNo = p.Max(a => a.ProductNo),
                SKU = p.Max(a => a.SKU),
                PdtName = p.Max(a => a.PdtName),
                UseStock = p.Sum(a => a.Stock) - p.Max(a => a.SaleCount),
                Stock = p.Sum(a => a.Stock),
                ActualStock = p.Sum(a => a.ActualStock)
            }).OrderByDescending(p => p.Stock).ThenBy(p => p.ProductNo);

            IList<VPRODUCTSTOCK> vproductstockList = list.Skip(pagination.rows * (pagination.page - 1)).Take(pagination.rows).ToList();
            pagination.records = list.Count();
            return vproductstockList;// .AsEnumerable();
        }

        #endregion
    }

    public class VPRODUCTSTOCK
    {
        public decimal? ActualStock { get; set; }
        public string FK_Product { get; set; }
        public string PdtName { get; set; }
        public string ProductNo { get; set; }
        public string SKU { get; set; }
        public decimal? Stock { get; set; }
        public decimal? UseStock { get; set; }
    }

    internal class ProductStock
    {
        public decimal? ActualStock { get; set; }
        public string FK_Product { get; set; }
        public string PdtName { get; set; }
        public DateTime ProductionDate { get; set; }
        public string ProductNo { get; set; }
        public int? QualityDate { get; set; }
        public decimal? SaleCount { get; set; }
        public string SKU { get; set; }
        public decimal? Stock { get; set; }
        public decimal? UseStock { get; set; }
        public String organizeid { get; set; }
    }
}
