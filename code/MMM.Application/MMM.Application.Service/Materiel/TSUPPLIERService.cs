using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-01 21:50
    /// 描 述：客户
    /// </summary>
    public class TSUPPLIERService : RepositoryFactory<TSUPPLIEREntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TSUPPLIEREntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            var expression = LinqExtensions.True<TSUPPLIEREntity>();
            if (!queryJson["SupplierNo"].IsEmpty())
            {
                String SupplierNo = queryJson["SupplierNo"].ToString();
                expression = expression.And(t => t.SupplierNo.Contains(SupplierNo));
            }
            if (!queryJson["SupplierName"].IsEmpty())
            {
                String SupplierName = queryJson["SupplierName"].ToString();
                expression = expression.And(t => t.SupplierName.Contains(SupplierName));
            }
            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);

            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TSUPPLIEREntity> GetList(JObject queryJson)
        {
            IQueryable<TSUPPLIEREntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["SupplierName"].IsEmpty())
            {
                String SupplierName = queryJson["SupplierName"].ToString();
                query = query.Where(t => t.SupplierName.Contains(SupplierName));
            }
            return query.Where(x => x.IsDel == 0).ToList();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="supilyids">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TSUPPLIEREntity> GetList(IList<String> supilyids)
        {
            return this.BaseRepository().IQueryable().Where(x => supilyids.Contains(x.GUID) && x.IsDel == 0).ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TSUPPLIEREntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 判断编号是否重复
        /// </summary>
        /// <param name="supplierNo"></param>
        /// <param name="keyValue"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public TSUPPLIEREntity GetEntityByNo(string supplierNo, string keyValue, string organizeid)
        {
            if (String.IsNullOrEmpty(keyValue))
                return BaseRepository().FindEntity(x => x.SupplierNo == supplierNo && organizeid == x.organizeid && 0 == x.IsDel);
            else
                return BaseRepository().FindEntity(x => x.SupplierNo == supplierNo && organizeid == x.organizeid && x.GUID != keyValue && 0 == x.IsDel);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TSUPPLIEREntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TSUPPLIEREntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
