using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using MMM.Application.Code;
using MMM.Application.Entity.Warehouse;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 21:32
    /// 描 述：出库单
    /// </summary>
    public class TOUTSTOCKORDERService : RepositoryFactory<TOUTSTOCKORDEREntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <param name="supplierIds">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TOUTSTOCKORDEREntity> GetPageList(Pagination pagination, JObject queryJson, IList<String> supplierIds)
        {
            var expression = LinqExtensions.True<TOUTSTOCKORDEREntity>();
            if (!queryJson["Status"].IsEmpty())
            {
                Int32 Status = Convert.ToInt32(queryJson["Status"]);
                expression = expression.And(t => t.Status == Status);
            }
            if (!queryJson["OutStockOrderNo"].IsEmpty())
            {
                String OutStockOrderNo = queryJson["OutStockOrderNo"].ToString();
                expression = expression.And(t => t.OutStockOrderNo == OutStockOrderNo);
            }
            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = Convert.ToString(queryJson["FK_WarehouseNo"]);
                expression = expression.And(t => FK_WarehouseNo.Contains(t.FK_WarehouseNo));
            }
            if (supplierIds != null && supplierIds.Count > 0)
            {
                expression = expression.And(t => supplierIds.Contains(t.FK_Supplier));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression,pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TOUTSTOCKORDEREntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TOUTSTOCKORDEREntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TOUTSTOCKORDEREntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                Random rd = new Random();
                entity.OutStockOrderNo = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0');
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TOUTSTOCKORDEREntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 分配入库单
        /// </summary>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string OutAll(string organizeid)
        {
            Data.IDatabase db = DbFactory.Base();

            IQueryable<VOUTSTOCKDETAILEntity> query = db.IQueryable<VOUTSTOCKDETAILEntity>();
            if (!String.IsNullOrEmpty(organizeid))
                query = query.Where(p => p.organizeid == organizeid);
            IList<VOUTSTOCKDETAILEntity> list = query.ToList();
            if (list.Count > 0)
            {
                String msg = "";
                foreach (VOUTSTOCKDETAILEntity str in list)
                {
                    msg += String.Format("{0}，", str);
                }
                msg = msg.Substring(0, msg.Length - 1) + String.Format("共{0}个物料库存不足！", list.Count);
                return msg;
            }

            Random rd = new Random();
            string batchNumber = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(99).ToString().PadLeft(2, '0');

            IQueryable<TOUTSTOCKORDEREntity> queryStock = db.IQueryable<TOUTSTOCKORDEREntity>().Where(p => p.IsDel == 0 && p.Status == 0);
            if (!String.IsNullOrEmpty(organizeid))
                query = query.Where(p => p.organizeid == organizeid);
            List<TOUTSTOCKORDEREntity> outstocklist = queryStock.ToList();
            if (outstocklist.Count == 0)
            {
                return "不存在未分配的出库单！";
            }
            Dictionary<String, List<String>> outstockdict = new Dictionary<string, List<string>>();
            List<String> outstockids = new List<string>();
            foreach (TOUTSTOCKORDEREntity outstock in outstocklist)
            {
                if (!outstockdict.Keys.Contains(outstock.FK_WarehouseNo + "#" + outstock.organizeid))
                {
                    outstockdict.Add(outstock.FK_WarehouseNo + "#" + outstock.organizeid, new List<string>());
                    outstockdict[outstock.FK_WarehouseNo + "#" + outstock.organizeid].Add(outstock.GUID);
                }
                else
                {
                    outstockdict[outstock.FK_WarehouseNo + "#" + outstock.organizeid].Add(outstock.GUID);
                }
                outstockids.Add(outstock.GUID);
            }

            List<TOUTSTOCKDETAILEntity> detailList = db.IQueryable<TOUTSTOCKDETAILEntity>().Where(p => p.IsDel == 0 && outstockids.Contains(p.FK_OutStockOrder)).ToList();

            try
            {
                db.BeginTrans();
                foreach (TOUTSTOCKORDEREntity order in outstocklist)
                {
                    order.Status = 1;
                    order.Modifyer = OperatorProvider.Provider.Current().Account;
                    order.ModifyTime = DateTime.Now;
                    db.Update<TOUTSTOCKORDEREntity>(order, null);
                }
                foreach (String key in outstockdict.Keys)
                {
                    String[] keys = key.Split('#');
                    List<string> productlist = detailList.Where(x => outstockdict[key].Contains(x.FK_OutStockOrder)).Select(p => p.FK_Product).Distinct().ToList();
                    foreach (string productguid in productlist)
                    {
                        TOUTGOINGEntity outgoing = new TOUTGOINGEntity();
                        outgoing.GUID = Guid.NewGuid().ToString();
                        outgoing.FK_WarehouseNo = keys[0];
                        outgoing.organizeid = keys[1];
                        outgoing.FK_Product = productguid;
                        outgoing.BatchNumber = batchNumber;
                        outgoing.Count = detailList.Where(p => p.FK_Product == productguid).Sum(p => p.Count);
                        outgoing.OutCount = Decimal.Zero;
                        outgoing.StockCount = Decimal.Zero;
                        outgoing.Status = 1;
                        outgoing.IsDel = 0;
                        outgoing.Creater = OperatorProvider.Provider.Current().Account;
                        outgoing.CreateTime = DateTime.Now;
                        db.Insert<TOUTGOINGEntity>(outgoing);
                    }
                }
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }

        /// <summary>
        /// 单条出库
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public string Out(string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            TOUTSTOCKORDEREntity outstockorder = db.FindEntity<TOUTSTOCKORDEREntity>(keyValue);
            if (outstockorder.Status != 0)
            {
                return "只有未分配的出库单可以分配!";
            }

            List<TOUTSTOCKDETAILEntity> detailList = db.IQueryable<TOUTSTOCKDETAILEntity>().Where(p => p.IsDel == 0 && p.FK_OutStockOrder == keyValue).ToList();
            List<string> productlist = detailList.Select(p => p.FK_Product).Distinct().ToList();

            List<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(x => x.FK_WarehouseNo == outstockorder.FK_WarehouseNo && x.IsDel == 0 && productlist.Contains(x.FK_Product)).ToList();

            Random rd = new Random();
            string batchNumber = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(99).ToString().PadLeft(2, '0');
            IList<TOUTGOINGEntity> outgoingList = new List<TOUTGOINGEntity>();
            foreach (String productguid in productlist)
            {
                TOUTGOINGEntity outgoing = new TOUTGOINGEntity();
                outgoing.GUID = Guid.NewGuid().ToString();
                outgoing.FK_WarehouseNo = outstockorder.FK_WarehouseNo;
                outgoing.FK_Product = productguid;
                outgoing.BatchNumber = batchNumber;
                outgoing.Count = detailList.Where(p => p.FK_Product == productguid).Sum(p => p.Count);
                decimal productstock = stockList.Sum(p => p.Stock) ?? 0;

                if (productstock < outgoing.Count)
                {
                    return String.Format("商品{0}库存不足！", detailList.Where(p => p.FK_Product == productguid).Select(p => p.ProductName).FirstOrDefault());
                }

                outgoing.OutCount = Decimal.Zero;
                outgoing.StockCount = Decimal.Zero;
                outgoing.Status = 1;
                outgoing.IsDel = 0;
                outgoing.Creater = OperatorProvider.Provider.Current().Account;
                outgoing.CreateTime = DateTime.Now;
                outgoing.organizeid = outstockorder.organizeid;

                outgoingList.Add(outgoing);
            }
            try
            {
                db.BeginTrans();
                outstockorder.Status = 1;
                outstockorder.Modifyer = OperatorProvider.Provider.Current().Account;
                outstockorder.ModifyTime = DateTime.Now;
                db.Update<TOUTSTOCKORDEREntity>(outstockorder, null);
                db.Insert<TOUTGOINGEntity>(outgoingList);
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }

        /// <summary>
        /// 出库导入
        /// </summary>
        /// <param name="order"></param>
        /// <param name="detaillist"></param>
        /// <returns></returns>
        public string SaveImport(TOUTSTOCKORDEREntity order, List<TOUTSTOCKDETAILEntity> detaillist)
        {
            Data.IDatabase db = DbFactory.Base();
            TSUPPLIEREntity supplier = db.IQueryable<TSUPPLIEREntity>().Where(p => p.IsDel == 0 && p.SupplierNo == order.FK_Supplier).FirstOrDefault();
            if (supplier == null)
            {
                return "供应商不存在！";
            }

            Random rd = new Random();
            order.OutStockOrderNo = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0');
            order.FK_Supplier = supplier.GUID;
            order.FK_Plat = db.IQueryable<T_PLATEntity>().FirstOrDefault(p => p.FK_WarehouseNo == order.FK_WarehouseNo && p.IsDel == 0).GUID;
            order.Status = 0;
            order.OrderPrice = Decimal.Zero;

            IList<String> productskus = detaillist.Select(x => x.SKU).ToList();

            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(x => productskus.Contains(x.SKU) && x.IsDel == 0).ToList();

            foreach (TOUTSTOCKDETAILEntity detail in detaillist)
            {
                TPRODUCTEntity product = productList.Where(p => p.SKU == detail.SKU).FirstOrDefault();
                if (product == null)
                {
                    return String.Format("物料SKU{0}不存在！", detail.SKU);
                }
                detail.FK_OutStockOrder = order.GUID;
                detail.FK_Product = product.GUID;
                detail.ProductName = product.PdtName;
                order.OrderPrice += (detail.Price * detail.Count * detail.Discount);
            }
            try
            {
                db.BeginTrans();
                db.Insert<TOUTSTOCKORDEREntity>(order);
                db.Insert<TOUTSTOCKDETAILEntity>(detaillist);

                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }
        #endregion
    }
}
