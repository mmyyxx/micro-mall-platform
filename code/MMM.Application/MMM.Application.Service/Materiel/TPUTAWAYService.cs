using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using MMM.Util.Extension;
using Newtonsoft.Json.Linq;
using MMM.Application.Code;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 22:07
    /// 描 述：上架
    /// </summary>
    public class TPUTAWAYService : RepositoryFactory<TPUTAWAYEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="purchaseIds"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TPUTAWAYEntity> GetPageList(Pagination pagination, IList<String> purchaseIds, JObject queryJson)
        {
            var expression = LinqExtensions.True<TPUTAWAYEntity>();
            if (purchaseIds != null && purchaseIds.Count > 0)
            {
                expression = expression.And(t => purchaseIds.Contains(t.FK_PurchaseDetail));
            }
            expression = expression.And(x => x.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TPUTAWAYEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 根据detailids列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TPUTAWAYEntity> GetList(IList<String> detailsids)
        {
            IQueryable<TPUTAWAYEntity> query = this.BaseRepository().IQueryable();

            query = query.Where(t => t.IsDel == 0 && detailsids.Contains(t.FK_PurchaseDetail));
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TPUTAWAYEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 根据物料单的主键获取记录
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public TPUTAWAYEntity GetEntityByPurchaseDetail(string keyValue)
        {
            return this.BaseRepository().IQueryable().FirstOrDefault(x => x.FK_PurchaseDetail == keyValue && x.IsDel == 0);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="list"></param>
        public void RemoveForm(List<TPUTAWAYEntity> list)
        {
            this.BaseRepository().Update(list);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="gUID"></param>
        /// <param name="putawayInfo"></param>
        public void RemoveForm(string gUID, TPUTAWAYEntity putawayInfo)
        {
            putawayInfo.Remove(gUID);
            this.BaseRepository().Update(putawayInfo);
        }

        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TPUTAWAYEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 上架完成
        /// </summary>
        /// <param name="keyValue"></param>
        public String SaveSataus(string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();

            TPUTAWAYEntity putaway = db.FindEntity<TPUTAWAYEntity>(keyValue);
            if (putaway.Status == 0)
            {
                return "该物料已入库！";
            }

            putaway.ActualLocation = putaway.FK_Location;
            putaway.Status = 0;
            putaway.Operator = OperatorProvider.Provider.Current().CompanyId;
            putaway.CompleteTime = DateTime.Now;

            TPURCHASEDETAILEntity purchasedetail = db.IQueryable<TPURCHASEDETAILEntity>().SingleOrDefault(p => p.IsDel == 0 && p.GUID == putaway.FK_PurchaseDetail);
            if (purchasedetail == null)
            {
                return "入库单明细不存在！";
            }
            //查询该批次中是否还有未入库的物料
            TPURCHASEORDEREntity purchaseorder = db.IQueryable<TPURCHASEORDEREntity>().SingleOrDefault(p => p.GUID == purchasedetail.FK_PurchaseOrder);
            int cnt = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == 0 && p.GUID != purchasedetail.GUID && p.FK_PurchaseOrder == purchasedetail.FK_PurchaseOrder)
    .Join(db.IQueryable<TPUTAWAYEntity>().Where(p => p.IsDel == 0 && p.Status == 1), m => m.GUID, n => n.FK_PurchaseDetail, (m, n) => m).Count();
            if (cnt == 0)
            {
                purchaseorder.Status = 2;
            }

            TSTOCKEntity stock = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == 0 && p.FK_Location == putaway.FK_Location && p.FK_Product == purchasedetail.FK_Product && p.ProductionDate == purchasedetail.ProductionDate).SingleOrDefault();

            try
            {
                db.BeginTrans();
                if (stock != null)
                {
                    stock.Stock += purchasedetail.Count;
                    stock.ActualStock += purchasedetail.Count;
                    stock.Modifyer = OperatorProvider.Provider.Current().CompanyId;
                    stock.ModifyTime = DateTime.Now;
                    db.Update<TSTOCKEntity>(stock, null);
                }
                else
                {
                    stock = new TSTOCKEntity();
                    stock.GUID = Guid.NewGuid().ToString();
                    stock.FK_Location = putaway.FK_Location;
                    stock.FK_Product = purchasedetail.FK_Product;
                    stock.BatchNumber = putaway.BatchNumber;
                    stock.Stock = purchasedetail.Count;
                    stock.ActualStock = purchasedetail.Count;
                    stock.ProductionDate = purchasedetail.ProductionDate;
                    stock.QualityDay = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == 0 && p.GUID == purchasedetail.FK_Product).Select(p => p.QualityDate).SingleOrDefault();
                    stock.IsDel = 0;
                    stock.Creater = OperatorProvider.Provider.Current().CompanyId;
                    stock.CreateTime = DateTime.Now;
                    stock.organizeid = purchasedetail.organizeid;
                    stock.FK_WarehouseNo = purchaseorder.FK_WarehouseNo;
                    db.Insert<TSTOCKEntity>(stock);
                }

                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                stockdetail.FK_Stock = stock.GUID;
                stockdetail.ID = Guid.NewGuid().ToString();
                stockdetail.Stock = purchasedetail.Count;
                stockdetail.ActualStock = purchasedetail.Count;
                stockdetail.Type = 1;
                stockdetail.Remark = String.Format("入库单号：{0}", purchaseorder.PurchaseOrderNo);
                stockdetail.Creater = OperatorProvider.Provider.Current().CompanyId;
                stockdetail.CreateTime = DateTime.Now;
                stockdetail.organizeid = purchasedetail.organizeid;

                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                db.Update<TPUTAWAYEntity>(putaway, null);
                if (cnt == 0)
                    db.Update<TPURCHASEORDEREntity>(purchaseorder, null);
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }
        #endregion
    }
}
