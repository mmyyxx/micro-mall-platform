using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 10:12
    /// 描 述：订单明细
    /// </summary>
    public class TPURCHASEDETAILService : RepositoryFactory<TPURCHASEDETAILEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<TPURCHASEDETAILEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            IQueryable<TPURCHASEDETAILEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["FK_PurchaseOrder"].IsEmpty())
            {
                String FK_PurchaseOrder = queryJson["FK_PurchaseOrder"].ToString();
                query = query.Where(t => FK_PurchaseOrder.Contains(t.FK_PurchaseOrder));
            }
            if (!String.IsNullOrEmpty(organizeid))
                query = query.Where(t => t.organizeid == organizeid);
            query = query.Where(t => t.IsDel == 0);

            return query.OrderBy(t=>t.CreateTime).ToList();
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TPURCHASEDETAILEntity> GetList(JObject queryJson)
        {
            IQueryable<TPURCHASEDETAILEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["FK_PurchaseOrder"].IsEmpty())
            {
                String FK_PurchaseOrder = queryJson["FK_PurchaseOrder"].ToString();
                query = query.Where(t => t.FK_PurchaseOrder == FK_PurchaseOrder);
            }
            if (!queryJson["SKU"].IsEmpty())
            {
                String SKU = queryJson["SKU"].ToString();
                query = query.Where(t => t.SKU.Contains(SKU));
            }
            query = query.Where(t => t.IsDel == 0);
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TPURCHASEDETAILEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="list"></param>
        public void RemoveForm(List<TPURCHASEDETAILEntity> list)
        {
            this.BaseRepository().Update(list);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TPURCHASEDETAILEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TPURCHASEDETAILEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
