using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 21:41
    /// 描 述：出库单明细
    /// </summary>
    public class TOUTSTOCKDETAILService : RepositoryFactory<TOUTSTOCKDETAILEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TOUTSTOCKDETAILEntity> GetList(JObject queryJson)
        {
            IQueryable<TOUTSTOCKDETAILEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["FK_OutStockOrder"].IsEmpty())
            {
                String FK_OutStockOrder = queryJson["FK_OutStockOrder"].ToString();
                query = query.Where(t => FK_OutStockOrder == t.FK_OutStockOrder);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                query = query.Where(t => organizeid == t.organizeid);
            }
            query = query.Where(t => t.IsDel == 0);

            return query.OrderBy(t=>t.CreateTime).ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TOUTSTOCKDETAILEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="list"></param>
        public void RemoveForm(List<TOUTSTOCKDETAILEntity> list)
        {
            this.BaseRepository().Update(list);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TOUTSTOCKDETAILEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TOUTSTOCKDETAILEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
