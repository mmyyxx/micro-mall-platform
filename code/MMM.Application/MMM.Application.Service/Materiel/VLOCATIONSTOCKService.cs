using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VLOCATIONSTOCKService : RepositoryFactory<VLOCATIONSTOCKEntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<VLOCATIONSTOCKEntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<VLOCATIONSTOCKEntity>();
            if (!queryJson["LocationNo"].IsEmpty())
            {
                String LocationNo = queryJson["LocationNo"].ToString();
                expression = expression.And(t => t.LocationNo.Contains(LocationNo));
            }
            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = queryJson["FK_WarehouseNo"].ToString();
                expression = expression.And(t => FK_WarehouseNo.Contains(t.FK_WarehouseNo));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }

            return this.BaseRepository().FindList(expression, pagination);
        }

        #endregion
    }
}
