using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-31 16:55
    /// 描 述：物料
    /// </summary>
    public class TPRODUCTService : RepositoryFactory<TPRODUCTEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TPRODUCTEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            var expression = LinqExtensions.True<TPRODUCTEntity>();
            if (!queryJson["ProductNo"].IsEmpty())
            {
                String ProductNo = queryJson["ProductNo"].ToString();
                expression = expression.And(t => t.ProductNo.Contains(ProductNo));
            }
            if (!queryJson["PdtName"].IsEmpty())
            {
                String PdtName = queryJson["PdtName"].ToString();
                expression = expression.And(t => t.PdtName.Contains(PdtName));
            }
            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);

            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="guids">主键列表</param>
        /// <returns>返回列表</returns>
        public IList<TPRODUCTEntity> GetList(IList<String> guids)
        {
            return this.BaseRepository().IQueryable().Where(x => guids.Contains(x.GUID) && x.IsDel == 0).ToList();
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson"></param>
        /// <returns></returns>
        public IList<TPRODUCTEntity> GetList(JObject queryJson)
        {
            IQueryable<TPRODUCTEntity> query = this.BaseRepository().IQueryable().Where(x => x.IsDel == 0);

            if (!queryJson["ProductNo"].IsEmpty())
            {
                String ProductNo = queryJson["ProductNo"].ToString();
                query = query.Where(t => t.ProductNo.Contains(ProductNo));
            }
            if (!queryJson["PdtName"].IsEmpty())
            {
                String PdtName = queryJson["PdtName"].ToString();
                query = query.Where(t => t.PdtName.Contains(PdtName));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                query = query.Where(t => t.organizeid == organizeid);
            }
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TPRODUCTEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TPRODUCTEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TPRODUCTEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 同步功能
        /// </summary>
        public void Sync()
        {
            this.BaseRepository().ExecuteBySql("PRO_SYNCHRONIZEPRODUCT");
        }
        #endregion
    }
}
