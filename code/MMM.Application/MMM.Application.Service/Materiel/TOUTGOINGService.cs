using MMM.Application.Entity.Materiel;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using System;
using MMM.Util.Extension;

namespace MMM.Application.Service.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-06 21:48
    /// 描 述：出库分配
    /// </summary>
    public class TOUTGOINGService : RepositoryFactory<TOUTGOINGEntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取数据
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="search"></param>
        /// <param name="productIds"></param>
        /// <returns></returns>
        public IEnumerable<TOUTGOINGEntity> GetPageList(Pagination pagination, JObject queryJson, IList<string> productIds)
        {
            var expression = LinqExtensions.True<TOUTGOINGEntity>();
            if (!queryJson["Status"].IsEmpty())
            {
                Int32 Status = Convert.ToInt32(queryJson["Status"]);
                expression = expression.And(t => t.Status == Status);
            }
            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = Convert.ToString(queryJson["FK_WarehouseNo"]);
                expression = expression.And(t => FK_WarehouseNo.Contains(t.FK_WarehouseNo));
            }
            if (productIds != null && productIds.Count > 0)
            {
                expression = expression.And(t => productIds.Contains(t.FK_Product));
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TOUTGOINGEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TOUTGOINGEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TOUTGOINGEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }
        #endregion
    }
}
