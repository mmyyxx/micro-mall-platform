﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service
{
    public class CodeConst
    {
        public static IDictionary<String, String> wxinfo
        {
            get
            {
                IDictionary<String, String> tem = new Dictionary<String, String>();
                tem.Add("B43E1A5E8A5D4386AE5B6555135BE513", "wxeffd660ce222be32-0daf525c119eeb086ad5dce66febe471");
                tem.Add("C23D8879C79646EDA67EECABB4FD3C76", "wx98f7dc1beff0b31d-de1b85676e9276474f57ecb8d5159e0d");

                return tem;
            }
        }

        public const string WxAppKEY = "B43E1A5E8A5D4386AE5B6555135BE513";

        /// <summary>
        /// 登录校验
        /// </summary>
        public const string LoginCheck = "https://api.weixin.qq.com/sns/jscode2session?appid={0}&secret={1}&js_code={2}&grant_type=authorization_code";

        public const String DefaultOrganize = "207fa1a9-160c-4943-a89b-8fa4db0547ce";

        /// <summary>
        /// 默认密码
        /// </summary>
        public const string DefaultPassWord = "000000";

        /// <summary>
        /// 免运费
        /// </summary>
        public const string FreightFree = "128";

        /// <summary>
        /// 起送费
        /// </summary>
        public const decimal FreightSend = 38;

        /// <summary>
        /// 运费
        /// </summary>
        public const string Freight = "6.00";

        /// <summary>
        /// 是否启用库存
        /// </summary>
        public const bool IsUseStock = false;

        /// <summary>
        /// 版本号
        /// </summary>
        public const string WSCVersion = "3.8";

        /// <summary>
        /// 订单有效期
        /// </summary>
        public const int PayMinutes = 30;

        /// <summary>
        /// 自动确认收货时间(天)
        /// </summary>
        public const int DeliveryDays = 1;

        /// <summary>
        /// 删除标志
        /// </summary>
        public class IsDel
        {
            /// <summary>
            /// 未删除
            /// </summary>
            public const int NO = 0;

            /// <summary>
            /// 已删除
            /// </summary>
            public const int YES = 1;
        }

        /// <summary>
        /// 付款情况
        /// </summary>
        public class PayResult
        {
            /// <summary>
            /// 已付款
            /// </summary>
            public const int PayYes = 0;

            /// <summary>
            /// 未付款
            /// </summary>
            public const int PayNo = 1;

            /// <summary>
            /// 已退款
            /// </summary>
            public const int PayReturn = 2;
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        public class IsUse
        {
            /// <summary>
            /// 是
            /// </summary>
            public const int YES = 0;

            /// <summary>
            /// 否
            /// </summary>
            public const int NO = 1;
        }

        /// <summary>
        /// 编辑模式
        /// </summary>
        public class SubType
        {
            /// <summary>
            /// 新增
            /// </summary>
            public const string Insert = "Insert";

            /// <summary>
            /// 编辑
            /// </summary>
            public const string Edit = "Edit";
        }

        /// <summary>
        /// 图片类型
        /// </summary>
        public class PictureType
        {
            /// <summary>
            /// 商品图片
            /// </summary>
            public const int ProductPicture = 0;

            /// <summary>
            /// 分类图片
            /// </summary>
            public const int Category = 1;

            /// <summary>
            /// 商品详情图片
            /// </summary>
            public const int ProductContent = 2;

            /// <summary>
            /// 轮播图片
            /// </summary>
            public const int Swiper = 3;
        }

        /// <summary>
        /// 是否
        /// </summary>
        public class Whether
        {
            /// <summary>
            /// 是
            /// </summary>
            public const string YES = "0";

            /// <summary>
            /// 不是
            /// </summary>
            public const string NO = "1";
        }

        /// <summary>
        /// 是否上架
        /// </summary>
        public class IsSale
        {
            /// <summary>
            /// 未上架
            /// </summary>
            public const int NotShelves = 0;

            /// <summary>
            /// 已上架
            /// </summary>
            public const int IsShelves = 1;

            /// <summary>
            /// 已下架
            /// </summary>
            public const int OutShelves = 2;
        }

        /// <summary>
        /// 是否在分类中
        /// </summary>
        public class IsInCategory
        {
            /// <summary>
            /// 是
            /// </summary>
            public const int YES = 0;

            /// <summary>
            /// 不是
            /// </summary>
            public const int NO = 1;
        }

        /// <summary>
        /// 排序方式
        /// </summary>
        public class OrderType
        {
            /// <summary>
            /// 正序
            /// </summary>
            public const string ASC = "0";

            /// <summary>
            /// 倒序
            /// </summary>
            public const string DESC = "1";
        }

        /// <summary>
        /// 取货方式
        /// </summary>
        public class DeliveryMode
        {
            /// <summary>
            /// 到店取货
            /// </summary>
            public const int STORE = 1;

            /// <summary>
            /// 送货上门
            /// </summary>
            public const int EXPRESS = 2;
        }

        /// <summary>
        /// 订单状态
        /// </summary>
        public class OrderStatus
        {
            /// <summary>
            /// 待付款
            /// </summary>
            public const int PAYMENT = 1;

            /// <summary>
            /// 已付款
            /// </summary>
            public const int DELIVER = 2;

            /// <summary>
            /// 待收货
            /// </summary>
            public const int COLLECT = 3;

            /// <summary>
            /// 已完成
            /// </summary>
            public const int COMPLETE = 4;

            /// <summary>
            /// 订单失败
            /// </summary>
            public const int FAILED = 5;

            /// <summary>
            /// 订单取消
            /// </summary>
            public const int CANCEL = 6;
        }

        /// <summary>
        /// 订单明细状态
        /// </summary>
        public class OrderDetailStatus
        {
            /// <summary>
            /// 正常状态
            /// </summary>
            public const int NORMAL = 0;

            /// <summary>
            /// 申请售后(待审核)
            /// </summary>
            public const int AFTERSALE = 1;

            /// <summary>
            /// 审核通过
            /// </summary>
            public const int AUDITSUCCESS = 2;

            /// <summary>
            /// 审核不通过
            /// </summary>
            public const int AUDITFAIL = 3;

            /// <summary>
            /// 已完成
            /// </summary>
            public const int COMPLETED = 4;
        }

        /// <summary>
        /// 拼团参与方式
        /// </summary>
        public class GroupRole
        {
            /// <summary>
            /// 发起者
            /// </summary>
            public const int INITIATOR = 0;

            /// <summary>
            /// 参与者
            /// </summary>
            public const int PARTICIPANT = 1;
        }

        /// <summary>
        /// 付款方式
        /// </summary>
        public class PayWay
        {
            /// <summary>
            /// 微信支付
            /// </summary>
            public const int WXPAY = 1;

            /// <summary>
            /// 会员卡支付
            /// </summary>
            public const int CARDPAY = 2;

            /// <summary>
            /// 混合支付
            /// </summary>
            public const int HHPAY = 3;
        }

        /// <summary>
        /// 积分
        /// </summary>
        public class Integral
        {
            /// <summary>
            /// 签到积分
            /// </summary>
            public const int SIGNIN = 2;

            /// <summary>
            /// 分享商品
            /// </summary>
            public const int SHAREGOODS = 5;

            /// <summary>
            /// 分享订单
            /// </summary>
            public const int SHAREORDER = 8;

            /// <summary>
            /// 消费获取
            /// </summary>
            public const int PAYORDER = 10;
        }

        /// <summary>
        /// 积分获取方式
        /// </summary>
        public class IntegralType
        {
            /// <summary>
            /// 签到积分
            /// </summary>
            public const int SIGNIN = 0;

            /// <summary>
            /// 分享商品
            /// </summary>
            public const int SHAREGOODS = 1;

            /// <summary>
            /// 分享订单
            /// </summary>
            public const int SHAREORDER = 2;

            /// <summary>
            /// 消费获取
            /// </summary>
            public const int PAYORDER = 3;

            /// <summary>
            /// 兑换消费
            /// </summary>
            public const int PAYSHOP = 4;

            /// <summary>
            /// 客服修改
            /// </summary>
            public const int SERVICE = 5;
        }

        /// <summary>
        /// 积分获取说明
        /// </summary>
        public class IntegralRemark
        {
            /// <summary>
            /// 签到积分
            /// </summary>
            public const string SIGNIN = "每日签到奖励";

            /// <summary>
            /// 分享商品
            /// </summary>
            public const string SHAREGOODS = "分享商品奖励";

            /// <summary>
            /// 分享订单
            /// </summary>
            public const string SHAREORDER = "分享订单奖励";

            /// <summary>
            /// 消费获取
            /// </summary>
            public const string PAYORDER = "消费奖励";

            /// <summary>
            /// 兑换消费
            /// </summary>
            public const string PAYSHOP = "积分商城兑换消费";
        }

        /// <summary>
        /// 卡券类型
        /// </summary>
        public class CardType
        {
            /// <summary>
            /// 代金券
            /// </summary>
            public const int DJQ = 0;

            /// <summary>
            /// 兑换券
            /// </summary>
            public const int DHQ = 1;

            public const int XRQ = 2;
        }

        /// <summary>
        /// 卡券使用情况
        /// </summary>
        public class CardUse
        {
            /// <summary>
            /// 未使用
            /// </summary>
            public const int NOUSE = 0;

            /// <summary>
            /// 已使用
            /// </summary>
            public const int ISUSE = 1;

            /// <summary>
            /// 已过期
            /// </summary>
            public const int ISOUTTIME = 2;
        }

        /// <summary>
        /// 性别
        /// </summary>
        public class Gender
        {
            /// <summary>
            /// 未知
            /// </summary>
            public const string NOSET = "0";

            /// <summary>
            /// 男
            /// </summary>
            public const string MAN = "1";

            /// <summary>
            /// 女
            /// </summary>
            public const string FEMALE = "2";
        }

        /// <summary>
        /// 订单出库状态
        /// </summary>
        public class OrderStockStatus
        {
            /// <summary>
            /// 未出库
            /// </summary>
            public const int WCK = 0;

            /// <summary>
            /// 已提出
            /// </summary>
            public const int YTC = 1;

            /// <summary>
            /// 已完成
            /// </summary>
            public const int YWC = 2;

            /// <summary>
            /// 已取消
            /// </summary>
            public const int YQX = 3;

            /// <summary>
            /// 已退单
            /// </summary>
            public const int YTD = 4;
        }

        /// <summary>
        /// 库存明细类型
        /// </summary>
        public class StockDetailType
        {
            /// <summary>
            /// 入库
            /// </summary>
            public const int RK = 1;

            /// <summary>
            /// 出库
            /// </summary>
            public const int CK = 2;

            /// <summary>
            /// 库内
            /// </summary>
            public const int KN = 3;
        }

        /// <summary>
        /// 是否
        /// </summary>
        public class IsYesNO
        {
            /// <summary>
            /// 是
            /// </summary>
            public const int YES = 0;

            /// <summary>
            /// 否
            /// </summary>
            public const int NO = 1;
        }

        /// <summary>
        /// 小程序订单状态
        /// </summary>
        public class WXOrderStatus
        {
            /// <summary>
            /// 待付款
            /// </summary>
            public const int PAYMENT = 1;

            /// <summary>
            /// 已付款
            /// </summary>
            public const int DELIVER = 2;

            /// <summary>
            /// 待收货
            /// </summary>
            public const int COLLECT = 3;

            /// <summary>
            /// 已完成
            /// </summary>
            public const int COMPLETE = 4;

            /// <summary>
            /// 订单失败
            /// </summary>
            public const int FAILED = 5;

            /// <summary>
            /// 订单取消
            /// </summary>
            public const int CANCEL = 6;
        }

        /// <summary>
        /// 订单标题
        /// </summary>
        public class OrderTitle
        {
            /// <summary>
            /// 有恋水果零食
            /// </summary>
            public const string YLSG = @"·  所有商品同城海门市区配送
                                         ·  同城配送范围，市区3公里以内
                                         ·  全场满38元起送";

            /// <summary>
            /// 生活馆
            /// </summary>
            public const string SMQX = @"·  数码产品";

        }

        /// <summary>
        /// 订单状态
        /// </summary>
        public class WMSOrderStatus
        {
            /// <summary>
            /// 未生成
            /// </summary>
            public const int NOREADY = -1;

            /// <summary>
            /// 未上架(未分配)
            /// </summary>
            public const int NOSHELF = 0;

            /// <summary>
            /// 已上架(已分配)
            /// </summary>
            public const int ONSHELF = 1;

            /// <summary>
            /// 已入库(已出库)
            /// </summary>
            public const int INSTOCK = 2;
        }

        /// <summary>
        /// 出库分配状态
        /// </summary>
        public class OutStockStatus
        {
            /// <summary>
            /// 未提出
            /// </summary>
            public const int WTC = 1;

            /// <summary>
            /// 已提出
            /// </summary>
            public const int YTC = 2;

            /// <summary>
            /// 已完成
            /// </summary>
            public const int YWC = 3;
        }
    }
}
