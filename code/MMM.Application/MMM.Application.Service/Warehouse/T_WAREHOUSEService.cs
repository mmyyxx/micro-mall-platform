using MMM.Application.Code;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：仓库
    /// </summary>
    public class T_WAREHOUSEService : RepositoryFactory<T_WAREHOUSEEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_WAREHOUSEEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            IRepository<T_WAREHOUSEEntity> db = this.BaseRepository();
            var expression = LinqExtensions.True<T_WAREHOUSEEntity>();

            if (!queryJson["WarehouseName"].IsEmpty())
            {
                String WarehouseName = queryJson["WarehouseName"].ToString();
                expression = expression.And(t => t.WarehouseName.Contains(WarehouseName));
            }

            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);

            return db.FindList(expression, pagination);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <param name="organizeid">公司id</param>
        /// <returns>返回列表</returns>
        public IList<T_WAREHOUSEEntity> GetList(String organizeid, Boolean isAll = false)
        {
            var expression = this.BaseRepository().IQueryable();
            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.Where(t => t.organizeid == organizeid);
            if (!isAll)
                expression = expression.Where(t => t.IsDel == 0);
            IList<T_WAREHOUSEEntity> list = expression.OrderByDescending(x => x.CreateTime).ToList();

            return list;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_WAREHOUSEEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 根据仓库编号获取实体
        /// </summary>
        /// <param name="wareno"></param>
        /// <returns></returns>
        public T_WAREHOUSEEntity GetEntityByNo(string wareno)
        {
            return BaseRepository().FindEntity(x => x.WarehouseNo == wareno);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public string SaveForm(string keyValue, T_WAREHOUSEEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
            return "";
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string DelForm(string keyValue, T_WAREHOUSEEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
            return "";
        }
        #endregion
    }
}
