using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MMM.Application.Service.Warehouse
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VSTOCKDETAILService : RepositoryFactory<VSTOCKDETAILEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<VSTOCKDETAILEntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<VSTOCKDETAILEntity>();
            if (!queryJson["LocationGUID"].IsEmpty())
            {
                String LocationGUID = queryJson["LocationGUID"].ToString();
                expression = expression.And(t => t.LocationGUID== LocationGUID);
            }
            if (!queryJson["ProductGUID"].IsEmpty())
            {
                String ProductGUID = queryJson["ProductGUID"].ToString();
                expression = expression.And(t => t.ProductGUID == ProductGUID);
            }
            if (!queryJson["BatchNumber"].IsEmpty())
            {
                String BatchNumber = queryJson["BatchNumber"].ToString();
                expression = expression.And(t => t.BatchNumber.Contains(BatchNumber));
            }
            
            if (!queryJson["WarehouseNo"].IsEmpty())
            {
                String WarehouseNo = queryJson["WarehouseNo"].ToString();
                expression = expression.And(t => WarehouseNo.Contains(t.WarehouseNo));
            }
            if (!queryJson["days"].IsEmpty())
            {
                DateTime dat = DateTime.Now.AddDays(Int32.Parse(queryJson["days"].ToString()));
                expression = expression.And(x => DbFunctions.DiffDays(x.ProductionDate, dat) >= x.QualityDay);
                
            }
            //if (!queryJson["organizeid"].IsEmpty())
            //{
            //    String organizeid = queryJson["organizeid"].ToString();
            //    expression = expression.And(t => t.organizeid == organizeid);
            //}

            return this.BaseRepository().FindList(expression, pagination);
        }
        #endregion
    }
}
