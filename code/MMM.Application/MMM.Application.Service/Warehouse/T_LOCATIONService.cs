using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using MMM.Util.Extension;
using Newtonsoft.Json.Linq;

namespace MMM.Application.Service.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 11:59
    /// 描 述：储位
    /// </summary>
    public class T_LOCATIONService : RepositoryFactory<T_LOCATIONEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_LOCATIONEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            var expression = LinqExtensions.True<T_LOCATIONEntity>();

            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = queryJson["FK_WarehouseNo"].ToString();
                expression = expression.And(t => t.FK_WarehouseNo.Contains(FK_WarehouseNo));
            }
            if (!queryJson["LocationNo"].IsEmpty())
            {
                String LocationNo = queryJson["LocationNo"].ToString();
                expression = expression.And(t => t.LocationNo.Contains(LocationNo));
            }

            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="fk_area">查询参数</param>
        /// <param name="locationids">主键列表</param>
        /// <returns>返回列表</returns>
        public IList<T_LOCATIONEntity> GetList(string fk_area, IList<String> locationids = null)
        {
            IQueryable<T_LOCATIONEntity> query = this.BaseRepository().IQueryable().Where(x => x.IsDel == 0);
            if (!String.IsNullOrEmpty(fk_area))
                query = query.Where(x => x.FK_Area == fk_area);
            if (locationids != null && locationids.Count > 0)
                query = query.Where(x => locationids.Contains(x.GUID));
            return query.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_LOCATIONEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_LOCATIONEntity entity)
        {
            entity.Modify(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 批量保存
        /// </summary>
        /// <param name="saveLocationList"></param>
        public void SaveForm(List<T_LOCATIONEntity> saveLocationList)
        {
            this.BaseRepository().Insert(saveLocationList);
        }
        #endregion
    }
}
