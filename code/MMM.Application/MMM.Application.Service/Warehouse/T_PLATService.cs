using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 12:46
    /// 描 述：月台
    /// </summary>
    public class T_PLATService : RepositoryFactory<T_PLATEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_PLATEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            var expression = LinqExtensions.True<T_PLATEntity>();

            if (!queryJson["FK_WarehouseNo"].IsEmpty())
            {
                String FK_WarehouseNo = queryJson["FK_WarehouseNo"].ToString();
                expression = expression.And(t => t.FK_WarehouseNo.Contains(FK_WarehouseNo));
            }
            if (!queryJson["PlatName"].IsEmpty())
            {
                String PlatName = queryJson["PlatName"].ToString();
                expression = expression.And(t => t.PlatName.Contains(PlatName));
            }

            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression,pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="warehourse">查询参数</param>
        /// <param name="isAll">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_PLATEntity> GetList(String warehourse, Boolean isAll = false)
        {
            var expression = this.BaseRepository().IQueryable();
            expression = expression.Where(t => t.FK_WarehouseNo == warehourse);
            if (!isAll)
                expression = expression.Where(t => t.IsDel == 0);
            return expression.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_PLATEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_PLATEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 判断月台编号是否重复
        /// </summary>
        /// <param name="platNo"></param>
        /// <param name="keyValue"></param>
        /// <param name="FK_WarehouseNo"></param>
        /// <returns></returns>
        public T_PLATEntity GetEntityByNo(String platNo, String keyValue, String FK_WarehouseNo)
        {
            if (String.IsNullOrEmpty(keyValue))
                return BaseRepository().FindEntity(x => x.PlatNo == platNo && FK_WarehouseNo == x.FK_WarehouseNo && 0 == x.IsDel);
            else
                return BaseRepository().FindEntity(x => x.PlatNo == platNo && FK_WarehouseNo == x.FK_WarehouseNo && x.GUID != keyValue && 0 == x.IsDel);
        }
        #endregion
    }
}
