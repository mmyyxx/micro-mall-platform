﻿using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Entity.WMS;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WMS
{
    public class WMSLoginDAL
    {
        static MMM.Util.Log.Log _logger = LogFactory.GetLogger("wms");

        /// <summary>
        /// 获取仓库信息
        /// </summary>
        /// <returns></returns>
        public static string GetWarehouseList(String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<DropData> list = db.IQueryable<T_WAREHOUSEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO&&p.organizeid == organizeid).Select(p => new DropData()
                {
                    Code = p.WarehouseNo,
                    Name = p.WarehouseName
                }).ToList();

                result.Data = JSONHelper.WXObjectToJson(list);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取供应商
        /// </summary>
        /// <returns></returns>
        public static string GetSupplierList(String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<DropData> list = db.IQueryable<TSUPPLIEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.organizeid == organizeid).Select(p => new DropData()
                {
                    Code = p.GUID,
                    Name = p.SupplierName
                }).ToList();

                result.Data = JSONHelper.WXObjectToJson(list);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }
    }
}
