﻿using MMM.Application.Entity;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Mapping.WMS;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WMS
{
    public class WMSStockManage
    {
        static MMM.Util.Log.Log _logger = LogFactory.GetLogger("wms");

        /// <summary>
        /// 订单配货
        /// </summary>
        /// <param name="storeId"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static string GetPHOrderInfo(string storeId, string operatorId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                IList<VWXORDEREntity> list = db.IQueryable<VWXORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.OrderStatus == CodeConst.OrderStatus.DELIVER && p.DistributionTime == null && (p.Distribution == null || p.Distribution.ToUpper() == operatorId.ToUpper()) && (p.StoreId == storeId || p.StoreId == null)).ToList();
                IList<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (VWXORDEREntity wxorder in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = wxorder.GUID;
                        item.TitleName = "订单号";
                        item.Title = wxorder.OrderNumber;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "订单状态",
                            Value1 = new string[] { "待付款", "已付款", "待收货(待取货)", "已完成", "订单失败", "订单取消" }[(wxorder.OrderStatus ?? 0) - 1],
                            Name2 = "收货方式",
                            Value2 = new string[] { "到店取货", "送货上门" }[(wxorder.DeliveryMode ?? 0) - 1]
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "数量",
                            Value1 = wxorder.Count.ToString(),
                            Name2 = "订单价格",
                            Value2 = wxorder.OrderPrice.ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取详情
        /// </summary>
        /// <param name="orderguid"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static string GetOrderDetail(string orderguid, string operatorId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity orderInfo = db.IQueryable<TORDEREntity>().Where(p => p.GUID == orderguid).SingleOrDefault();

                if (orderInfo != null && orderInfo.Distribution == null)
                {
                    orderInfo.Distribution = operatorId;
                    db.Update<TORDEREntity>(orderInfo, null);
                }
                else if (orderInfo.Distribution != operatorId)
                {
                    result.msg = "该订单已有人配货或已出库！";
                    return JSONHelper.WXObjectToJson(result);
                }
                IList<TORDERDETAILEntity> list = db.IQueryable<TORDERDETAILEntity>().Where(p => p.FK_Order == orderguid).ToList();
                //IList<VORDERDETAILEntity> list = db.IQueryable<VORDERDETAILEntity>().Where(p => p.FK_Order == orderguid).ToList();
                IList<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    IList<String> orderdetailids = list.Select(t => t.FK_ProductClass).ToList();
                    IList<T_PRODUCTCLASSEntity> productClassList = db.IQueryable<T_PRODUCTCLASSEntity>().Where(t => orderdetailids.Contains(t.GUID)).ToList();

                    IList<String> productclassids = productClassList.Select(t => t.FK_Product).ToList();
                    IList<String> productskus = productClassList.Select(t => t.ProductSKU).ToList();

                    IList<T_PRODUCTPRICEEntity> productPriceList = db.IQueryable<T_PRODUCTPRICEEntity>(t => productclassids.Contains(t.GUID)).ToList();
                    IList<VPRODUCTCLASSEntity> vproductclassList = db.IQueryable<VPRODUCTCLASSEntity>().Where(t => productskus.Contains(t.ProductSKU)).ToList();
                    //获取会员信息
                    TMEMBEREntity member = db.IQueryable<TUSEREntity>().Where(t => t.Openid == orderInfo.FK_Openid)
                        .Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => n).FirstOrDefault();
                    foreach (TORDERDETAILEntity detailInfo in list)
                    {
                        ItemInfo item = new ItemInfo();
                        T_PRODUCTCLASSEntity productclass = productClassList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == detailInfo.FK_ProductClass).FirstOrDefault();
                        if (productclass == null) continue;
                        item.GUID = productclass.ProductSKU;
                        T_PRODUCTPRICEEntity productprice = productPriceList.FirstOrDefault(p => p.GUID == productclass.FK_Product);
                        item.ImgURL = productprice.ProductMainPicture;
                        item.Title = productprice.Name;
                        item.TitleName = "商品名称";
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "商品条码",
                            Value1 = item.GUID,
                            Name2 = "商品分类",
                            Value2 = vproductclassList.Where(p => p.ProductSKU == item.GUID).Select(p => p.ClassName).FirstOrDefault()
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "商品数量",
                            Value1 = detailInfo.Count.ToString(),
                            Name2 = "商品价格",
                            Value2 = ((member == null ? productclass.ClassOldPrice : productclass.ClassPrice) ?? 0).ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取库存
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="warehouseNo"></param>
        /// <returns></returns>
        public static string GetProductStock(string sku, string warehouseNo)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<TSTOCKEntity> list = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo)
                    .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == sku), a => a.FK_Product, b => b.GUID, (a, b) => a).ToList();

                if (list.Count == 0)
                {
                    result.msg = "商品库存不存在！";
                    return JSONHelper.WXObjectToJson(result);
                }
                IList<String> locationids = list.Select(t => t.FK_Location).ToList();
                IList<String> productids = list.Select(t => t.FK_Product).ToList();
                IList<T_LOCATIONEntity> locationList = db.IQueryable<T_LOCATIONEntity>().Where(t => locationids.Contains(t.GUID)).ToList();
                IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(t => productids.Contains(t.GUID)).ToList();

                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (TSTOCKEntity stockInfo in list)
                    {
                        if (stockInfo.Stock == Decimal.Zero)
                        {
                            continue;
                        }
                        ItemInfo item = new ItemInfo();
                        item.GUID = stockInfo.GUID;
                        item.TitleName = "储位编号";
                        item.Title = locationList.Where(p => p.GUID == stockInfo.FK_Location).Select(p => p.LocationNo).SingleOrDefault();
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "商品库存",
                            Value1 = stockInfo.Stock.ToString(),
                            Name2 = "实际库存",
                            Value2 = stockInfo.ActualStock.ToString()
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "生产日期",
                            Value1 = stockInfo.ProductionDate.Value.ToString("yyyy-MM-dd"),
                            Name2 = "保质期",
                            Value2 = productList.Where(p => p.GUID == stockInfo.FK_Product).Select(p => p.QualityDate).SingleOrDefault().ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                if (ItemList.Count == 0)
                {
                    result.msg = "商品无库存！";
                }
                else
                {
                    result.Data = JSONHelper.WXObjectToJson(ItemList);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="warehouseNo"></param>
        /// <returns></returns>
        public static string GetPDProductStock(string sku, string warehouseNo, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                VSTOCKPDEntity stockpd = db.IQueryable<VSTOCKPDEntity>().Where(p => p.SKU == sku && p.organizeid == organizeid).FirstOrDefault();

                result.Data = JSONHelper.WXObjectToJson(stockpd);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="warehouseNo"></param>
        /// <param name="pdtDate"></param>
        /// <param name="pdtStock"></param>
        /// <param name="qualityDate"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static string SaveStockPD(string sku, string warehouseNo, string pdtDate, decimal pdtStock, int qualityDate, string operatorId, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TSTOCKPDEntity stockpd = new TSTOCKPDEntity();
                stockpd.ID = Guid.NewGuid().ToString();
                stockpd.FK_WarehouseNo = warehouseNo;
                stockpd.SKU = sku;
                stockpd.ProductDate = DateTime.Parse(pdtDate);
                stockpd.Stock = pdtStock;
                stockpd.Creater = operatorId;
                stockpd.CreateTime = DateTime.Now;
                db.Insert<TSTOCKPDEntity>(stockpd);

                TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == sku && p.organizeid == organizeid).FirstOrDefault();
                if (productInfo != null && productInfo.QualityDate != qualityDate)
                {
                    productInfo.QualityDate = qualityDate;
                    db.Update<TPRODUCTEntity>(productInfo, null);
                }

                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="warehouseNo"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public static string GetProductInfoForDB(string sku, string warehouseNo, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                ProductDB resultInfo = new ProductDB();
                TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == sku && p.organizeid == organizeid).FirstOrDefault();
                if (productInfo == null)
                {
                    TPACKAGEEntity packageInfo = db.IQueryable<TPACKAGEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PackageSKU == sku && p.organizeid == organizeid).FirstOrDefault();
                    if (packageInfo == null)
                    {
                        return JSONHelper.WXObjectToJson(result);
                    }
                    resultInfo.PackageSKU = packageInfo.PackageSKU;
                    resultInfo.PackageName = packageInfo.PackageName;
                    resultInfo.PackageChildCount = packageInfo.PdtCount ?? 0;
                    productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == packageInfo.FK_Product).SingleOrDefault();
                }

                resultInfo.SKU = productInfo.SKU;
                resultInfo.PdtName = productInfo.PdtName;
                resultInfo.CostPrice = db.IQueryable<VCOSTINFOEntity>().Where(p => p.SKU == productInfo.SKU).Select(p => p.Price).FirstOrDefault();
                List<TSTOCKEntity> list = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Product == productInfo.GUID && p.FK_WarehouseNo == warehouseNo).ToList();

                if (list.Count > 0)
                {
                    resultInfo.StockCount = list.Sum(p => p.Stock) ?? 0;
                }
                if (!String.IsNullOrEmpty(resultInfo.PackageSKU))
                {
                    resultInfo.PackageStockCount = resultInfo.StockCount / resultInfo.PackageChildCount;
                    resultInfo.CostPrice = resultInfo.CostPrice * resultInfo.PackageChildCount;
                }

                result.Data = JSONHelper.WXObjectToJson(resultInfo);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 完成调拨
        /// </summary>
        /// <param name="data"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public static string CompleteStockAllocation(string data, string organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TALLOCATIONEntity allocationInfo = JSONHelper.WXDecodeObject<TALLOCATIONEntity>(data);
                TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == allocationInfo.SKU && p.organizeid == organizeid).FirstOrDefault();
                if (productInfo == null)
                {
                    TPACKAGEEntity packageInfo = db.IQueryable<TPACKAGEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PackageSKU == allocationInfo.SKU).FirstOrDefault();
                    if (packageInfo == null)
                    {
                        result.msg = "商品信息有问题";
                        return JSONHelper.WXObjectToJson(result);
                    }
                    productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == packageInfo.FK_Product).SingleOrDefault();
                    allocationInfo.AllocationCount = allocationInfo.AllocationCount * packageInfo.PdtCount;
                    allocationInfo.Price = allocationInfo.Price / packageInfo.PdtCount;
                }
                allocationInfo.ID = Guid.NewGuid().ToString();
                allocationInfo.FK_Product = productInfo.GUID;
                allocationInfo.SKU = productInfo.SKU;
                allocationInfo.Name = productInfo.PdtName;
                allocationInfo.CreateTime = DateTime.Now;
                db.Insert<TALLOCATIONEntity>(allocationInfo);

                List<TSTOCKEntity> list = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == allocationInfo.FK_WarehouseFrom && p.FK_Product == allocationInfo.FK_Product && p.Stock > Decimal.Zero).OrderBy(p => p.ProductionDate).ToList();

                if (list.Count == 0 || list.Sum(p => p.Stock) < allocationInfo.AllocationCount)
                {
                    result.msg = "库存数量不足";
                    return JSONHelper.WXObjectToJson(result);
                }
                else
                {
                    decimal outStock = allocationInfo.AllocationCount ?? 0;
                    string dbInLocation = db.IQueryable<T_AREAEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == allocationInfo.FK_WarehouseTo)
                        .Join(db.IQueryable<T_LOCATIONEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.GUID, n => n.FK_Area, (m, n) => new
                        {
                            AreaNo = m.AreaNo,
                            LocationGUID = n.GUID
                        }).OrderByDescending(p => p.AreaNo).Select(p => p.LocationGUID).FirstOrDefault();

                    if (dbInLocation == null)
                    {
                        result.msg = "目标仓库储位不存在";
                        return JSONHelper.WXObjectToJson(result);
                    }

                    foreach (TSTOCKEntity stockInfo in list)
                    {
                        if (stockInfo.Stock < outStock)
                        {
                            outStock -= stockInfo.Stock ?? 0;

                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                            stockdetail.ID = Guid.NewGuid().ToString();
                            stockdetail.FK_Stock = stockInfo.GUID;
                            stockdetail.Stock = -stockInfo.Stock;
                            stockdetail.ActualStock = -stockInfo.Stock;
                            stockdetail.Type = CodeConst.StockDetailType.CK;
                            stockdetail.Remark = "调拨出库：" + allocationInfo.ID;
                            stockdetail.Creater = allocationInfo.Creater;
                            stockdetail.CreateTime = DateTime.Now;
                            stockdetail.organizeid = stockInfo.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdetail);

                            stockInfo.Modifyer = allocationInfo.Creater;
                            stockInfo.ModifyTime = DateTime.Now;

                            TSTOCKEntity stockdbIn = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == allocationInfo.FK_WarehouseTo && p.FK_Product == allocationInfo.FK_Product && p.ProductionDate == stockInfo.ProductionDate).OrderBy(p => p.ProductionDate).SingleOrDefault();
                            if (stockdbIn == null)
                            {
                                stockdbIn = new TSTOCKEntity();
                                stockdbIn.GUID = Guid.NewGuid().ToString();
                                stockdbIn.FK_Location = dbInLocation;
                                stockdbIn.FK_Product = allocationInfo.FK_Product;
                                stockdbIn.BatchNumber = allocationInfo.ID.ToString();
                                stockdbIn.Stock = stockInfo.Stock;
                                stockdbIn.ActualStock = stockInfo.Stock;
                                stockdbIn.ProductionDate = stockInfo.ProductionDate;
                                stockdbIn.QualityDay = stockInfo.QualityDay;
                                stockdbIn.IsDel = CodeConst.IsDel.NO;
                                stockdbIn.Creater = allocationInfo.Creater;
                                stockdbIn.CreateTime = DateTime.Now;
                                stockdbIn.Modifyer = allocationInfo.Creater;
                                stockdbIn.ModifyTime = DateTime.Now;
                                stockdbIn.organizeid = stockInfo.organizeid;
                                stockdbIn.FK_WarehouseNo = allocationInfo.FK_WarehouseTo;
                                db.Insert<TSTOCKEntity>(stockdbIn);
                            }
                            else
                            {
                                stockdbIn.Stock += stockInfo.Stock;
                                stockdbIn.ActualStock += stockInfo.Stock;
                                stockdbIn.Modifyer = allocationInfo.Creater;
                                stockdbIn.ModifyTime = DateTime.Now;
                                db.Update<TSTOCKEntity>(stockdbIn, null);
                            }
                            TSTOCKDEAILEntity stockdbInDetail = new TSTOCKDEAILEntity();
                            stockdbInDetail.ID = Guid.NewGuid().ToString();
                            stockdbInDetail.FK_Stock = stockdbIn.GUID;
                            stockdbInDetail.Stock = stockInfo.Stock;
                            stockdbInDetail.ActualStock = stockInfo.Stock;
                            stockdbInDetail.Type = CodeConst.StockDetailType.RK;
                            stockdbInDetail.Remark = "调拨入库：" + allocationInfo.ID;
                            stockdbInDetail.Creater = allocationInfo.Creater;
                            stockdbInDetail.CreateTime = DateTime.Now;
                            stockdbInDetail.organizeid = stockInfo.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdbInDetail);

                            stockInfo.ActualStock -= stockInfo.Stock;
                            stockInfo.Stock = Decimal.Zero;
                            db.Update<TSTOCKEntity>(stockInfo, null);
                        }
                        else
                        {
                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                            stockdetail.ID = Guid.NewGuid().ToString();
                            stockdetail.FK_Stock = stockInfo.GUID;
                            stockdetail.Stock = -outStock;
                            stockdetail.ActualStock = -outStock;
                            stockdetail.Type = CodeConst.StockDetailType.CK;
                            stockdetail.Remark = "调拨出库：" + allocationInfo.ID;
                            stockdetail.Creater = allocationInfo.Creater;
                            stockdetail.CreateTime = DateTime.Now;
                            stockdetail.organizeid = stockInfo.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdetail);

                            stockInfo.ActualStock -= outStock;
                            stockInfo.Stock -= outStock;
                            stockInfo.Modifyer = allocationInfo.Creater;
                            stockInfo.ModifyTime = DateTime.Now;
                            db.Update<TSTOCKEntity>(stockInfo, null);

                            TSTOCKEntity stockdbIn = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == allocationInfo.FK_WarehouseTo && p.FK_Product == allocationInfo.FK_Product && p.ProductionDate == stockInfo.ProductionDate).OrderBy(p => p.ProductionDate).SingleOrDefault();
                            if (stockdbIn == null)
                            {
                                stockdbIn = new TSTOCKEntity();
                                stockdbIn.GUID = Guid.NewGuid().ToString();
                                stockdbIn.FK_Location = dbInLocation;
                                stockdbIn.FK_Product = allocationInfo.FK_Product;
                                stockdbIn.BatchNumber = String.Format("调拨单{0}的库存信息", allocationInfo.ID.ToString());
                                stockdbIn.Stock = outStock;
                                stockdbIn.ActualStock = outStock;
                                stockdbIn.ProductionDate = stockInfo.ProductionDate;
                                stockdbIn.QualityDay = stockInfo.QualityDay;
                                stockdbIn.IsDel = CodeConst.IsDel.NO;
                                stockdbIn.Creater = allocationInfo.Creater;
                                stockdbIn.CreateTime = DateTime.Now;
                                stockdbIn.Modifyer = allocationInfo.Creater;
                                stockdbIn.ModifyTime = DateTime.Now;
                                stockdbIn.organizeid = stockInfo.organizeid;
                                stockdbIn.FK_WarehouseNo = allocationInfo.FK_WarehouseTo;
                                db.Insert<TSTOCKEntity>(stockdbIn);
                            }
                            else
                            {
                                stockdbIn.Stock += outStock;
                                stockdbIn.ActualStock += outStock;
                                stockdbIn.Modifyer = allocationInfo.Creater;
                                stockdbIn.ModifyTime = DateTime.Now;
                                db.Update<TSTOCKEntity>(stockdbIn, null);
                            }
                            TSTOCKDEAILEntity stockdbInDetail = new TSTOCKDEAILEntity();
                            stockdbInDetail.ID = Guid.NewGuid().ToString();
                            stockdbInDetail.FK_Stock = stockdbIn.GUID;
                            stockdbInDetail.Stock = outStock;
                            stockdbInDetail.ActualStock = outStock;
                            stockdbInDetail.Type = CodeConst.StockDetailType.RK;
                            stockdbInDetail.Remark = "调拨入库：" + allocationInfo.ID;
                            stockdbInDetail.Creater = allocationInfo.Creater;
                            stockdbInDetail.CreateTime = DateTime.Now;
                            stockdbInDetail.organizeid = stockInfo.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdbInDetail);

                            outStock = Decimal.Zero;

                            break;
                        }
                    }
                }

                db.Commit();
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public static string OrderProductOutStock(string orderguid, string warehouseNo, string operatorId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                IList<TORDERDETAILEntity> orderDetailList = db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Order == orderguid).ToList();
                IList<String> productclassids = orderDetailList.Select(t => t.FK_ProductClass).ToList();
                IList<T_PRODUCTCLASSEntity> productclassList = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && productclassids.Contains(p.GUID)).ToList();

                List<ProductInfo> orderdetaillist = orderDetailList.Join(productclassList, x => x.FK_ProductClass, y => y.GUID, (x, y) => new ProductInfo
                {
                    ProductName = y.ProductSKU,
                    ProductCount = x.Count ?? 0
                }).ToList();

                bool isResult = true;

                db.BeginTrans();

                IList<String> skus = orderdetaillist.Select(t => t.ProductName).ToList();

                var productLists = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && skus.Contains(p.SKU))
                    .Join(db.IQueryable<TPRODUCTCOMBEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.GUID, n => n.FK_Product, (m, n) => n)
                    .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), x => x.FK_Child, y => y.GUID, (x, y) => new
                    {
                        SKU = y.SKU,
                        pdtCount = x.ChildCount
                    }).ToList();

                foreach (ProductInfo pdtInfo in orderdetaillist)
                {
                    var productList = productLists.Where(p => p.SKU == pdtInfo.ProductName).ToList();

                    if (productList.Count > 0)
                    {
                        foreach (var itemInfo in productList)
                        {
                            TPACKAGEEntity package = db.IQueryable<TPACKAGEEntity>().Where(p => p.PackageSKU == itemInfo.SKU && p.IsDel == CodeConst.IsDel.NO).FirstOrDefault();

                            string SKUStr = itemInfo.SKU;

                            if (package != null)
                            {
                                SKUStr = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == package.FK_Product).Select(p => p.SKU).FirstOrDefault();
                            }

                            List<TSTOCKEntity> stocklist = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo)
                       .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == SKUStr), a => a.FK_Product, b => b.GUID, (a, b) => a).OrderBy(p => p.FK_Location).Select(p => p).ToList();

                            if (stocklist.Count > 0)
                            {
                                decimal totalstock = stocklist.Sum(p => p.Stock) ?? 0;
                                decimal pdtCount = pdtInfo.ProductCount * itemInfo.pdtCount.Value;
                                if (package != null)
                                {
                                    pdtCount = pdtCount * (package.PdtCount ?? 0);
                                }
                                if (totalstock >= pdtCount)
                                {
                                    TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= pdtCount).FirstOrDefault();
                                    if (stockinfo != null)
                                    {
                                        stockinfo.Stock -= pdtCount;
                                        db.Update<TSTOCKEntity>(stockinfo, null);

                                        TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                        stockdetail.ID = Guid.NewGuid().ToString();
                                        stockdetail.FK_Stock = stockinfo.GUID;
                                        stockdetail.Stock = -pdtCount;
                                        stockdetail.Type = CodeConst.StockDetailType.CK;
                                        stockdetail.Remark = "订单配货出库";
                                        stockdetail.Creater = operatorId;
                                        stockdetail.CreateTime = DateTime.Now;
                                        stockdetail.organizeid = stockinfo.organizeid;
                                        db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                        TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                        orderstock.ID = Guid.NewGuid().ToString();
                                        orderstock.FK_Order = orderguid;
                                        orderstock.FK_Stock = stockinfo.GUID;
                                        orderstock.StockCount = pdtCount;
                                        orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                        orderstock.Creater = operatorId;
                                        orderstock.CreateTime = DateTime.Now;
                                        orderstock.organizeid = stockinfo.organizeid;
                                        db.Insert<TORDERSTOCKEntity>(orderstock);
                                    }
                                    else
                                    {
                                        decimal outstock = pdtCount;
                                        while (outstock > 0)
                                        {
                                            foreach (TSTOCKEntity stockitem in stocklist)
                                            {
                                                if (stockitem.Stock >= outstock)
                                                {
                                                    stockitem.Stock -= outstock;
                                                    db.Update<TSTOCKEntity>(stockitem, null);

                                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                    stockdetail.ID = Guid.NewGuid().ToString();
                                                    stockdetail.FK_Stock = stockitem.GUID;
                                                    stockdetail.Stock = -outstock;
                                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                                    stockdetail.Remark = "订单配货出库";
                                                    stockdetail.Creater = operatorId;
                                                    stockdetail.CreateTime = DateTime.Now;
                                                    stockdetail.organizeid = stockinfo.organizeid;
                                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                                    TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                                    orderstock.ID = Guid.NewGuid().ToString();
                                                    orderstock.FK_Order = orderguid;
                                                    orderstock.FK_Stock = stockinfo.GUID;
                                                    orderstock.StockCount = outstock;
                                                    orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                                    orderstock.Creater = operatorId;
                                                    orderstock.CreateTime = DateTime.Now;
                                                    orderstock.organizeid = stockinfo.organizeid;
                                                    db.Insert<TORDERSTOCKEntity>(orderstock);
                                                    outstock = Decimal.Zero;
                                                }
                                                else
                                                {
                                                    outstock -= stockitem.Stock ?? 0;

                                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                    stockdetail.ID = Guid.NewGuid().ToString();
                                                    stockdetail.FK_Stock = stockitem.GUID;
                                                    stockdetail.Stock = -stockitem.Stock;
                                                    stockdetail.ActualStock = -stockitem.Stock;
                                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                                    stockdetail.Remark = "订单配货出库";
                                                    stockdetail.Creater = operatorId;
                                                    stockdetail.CreateTime = DateTime.Now;
                                                    stockdetail.organizeid = stockinfo.organizeid;
                                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                                    TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                                    orderstock.ID = Guid.NewGuid().ToString();
                                                    orderstock.FK_Order = orderguid;
                                                    orderstock.FK_Stock = stockinfo.GUID;
                                                    orderstock.StockCount = stockitem.Stock;
                                                    orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                                    orderstock.Creater = operatorId;
                                                    orderstock.CreateTime = DateTime.Now;
                                                    orderstock.organizeid = stockinfo.organizeid;
                                                    db.Insert<TORDERSTOCKEntity>(orderstock);

                                                    stockitem.Stock = Decimal.Zero;
                                                    db.Update<TSTOCKEntity>(stockitem, null);
                                                }
                                            }
                                        }
                                    }

                                    TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == SKUStr).FirstOrDefault();

                                    if (productInfo != null)
                                    {
                                        productInfo.SaleCount -= pdtInfo.ProductCount;
                                        db.Update<TPRODUCTEntity>(productInfo, null);
                                    }
                                }
                                else
                                {
                                    isResult = false;
                                    break;
                                }
                            }
                            else
                            {
                                isResult = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        TPACKAGEEntity package = db.IQueryable<TPACKAGEEntity>().Where(p => p.PackageSKU == pdtInfo.ProductName && p.IsDel == CodeConst.IsDel.NO).FirstOrDefault();

                        string SKUStr = pdtInfo.ProductName;

                        if (package != null)
                        {
                            SKUStr = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == package.FK_Product).Select(p => p.SKU).FirstOrDefault();
                        }

                        List<TSTOCKEntity> stocklist = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo)
                   .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == SKUStr), a => a.FK_Product, b => b.GUID, (a, b) => a).OrderBy(p => p.FK_Location).ToList();

                        if (stocklist.Count > 0)
                        {
                            decimal totalstock = stocklist.Sum(p => p.Stock) ?? 0;
                            decimal pdtCount = pdtInfo.ProductCount;
                            if (package != null)
                            {
                                pdtCount = pdtInfo.ProductCount * (package.PdtCount ?? 0);
                            }
                            if (totalstock >= pdtCount)
                            {
                                TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= pdtCount).FirstOrDefault();
                                if (stockinfo != null)
                                {
                                    stockinfo.Stock -= pdtCount;
                                    db.Update<TSTOCKEntity>(stockinfo, null);

                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                    stockdetail.ID = Guid.NewGuid().ToString();
                                    stockdetail.FK_Stock = stockinfo.GUID;
                                    stockdetail.Stock = -pdtCount;
                                    stockdetail.ActualStock = -pdtCount;
                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                    stockdetail.Remark = "订单配货出库";
                                    stockdetail.Creater = operatorId;
                                    stockdetail.CreateTime = DateTime.Now;
                                    stockdetail.organizeid = stockinfo.organizeid;
                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                    TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                    orderstock.ID = Guid.NewGuid().ToString();
                                    orderstock.FK_Order = orderguid;
                                    orderstock.FK_Stock = stockinfo.GUID;
                                    orderstock.StockCount = pdtCount;
                                    orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                    orderstock.Creater = operatorId;
                                    orderstock.CreateTime = DateTime.Now;
                                    orderstock.organizeid = stockinfo.organizeid;
                                    db.Insert<TORDERSTOCKEntity>(orderstock);
                                }
                                else
                                {
                                    decimal outstock = pdtCount;
                                    while (outstock > 0)
                                    {
                                        foreach (TSTOCKEntity stockitem in stocklist)
                                        {
                                            if (stockitem.Stock >= outstock)
                                            {
                                                stockitem.Stock -= outstock;
                                                db.Update<TSTOCKEntity>(stockitem, null);

                                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                stockdetail.ID = Guid.NewGuid().ToString();
                                                stockdetail.FK_Stock = stockitem.GUID;
                                                stockdetail.Stock = -outstock;
                                                stockdetail.ActualStock = -outstock;
                                                stockdetail.Type = CodeConst.StockDetailType.CK;
                                                stockdetail.Remark = "订单配货出库";
                                                stockdetail.Creater = operatorId;
                                                stockdetail.CreateTime = DateTime.Now;
                                                stockdetail.organizeid = stockinfo.organizeid;
                                                db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                                TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                                orderstock.ID = Guid.NewGuid().ToString();
                                                orderstock.FK_Order = orderguid;
                                                orderstock.FK_Stock = stockinfo.GUID;
                                                orderstock.StockCount = outstock;
                                                orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                                orderstock.Creater = operatorId;
                                                orderstock.CreateTime = DateTime.Now;
                                                orderstock.organizeid = stockinfo.organizeid;
                                                db.Insert<TORDERSTOCKEntity>(orderstock);
                                                outstock = Decimal.Zero;
                                            }
                                            else
                                            {
                                                outstock -= stockitem.Stock ?? 0;

                                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                stockdetail.ID = Guid.NewGuid().ToString();
                                                stockdetail.FK_Stock = stockitem.GUID;
                                                stockdetail.Stock = -stockitem.Stock;
                                                stockdetail.ActualStock = -stockitem.Stock;
                                                stockdetail.Type = CodeConst.StockDetailType.CK;
                                                stockdetail.Remark = "订单配货出库";
                                                stockdetail.Creater = operatorId;
                                                stockdetail.CreateTime = DateTime.Now;
                                                stockdetail.organizeid = stockinfo.organizeid;
                                                db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                                TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                                orderstock.ID = Guid.NewGuid().ToString();
                                                orderstock.FK_Order = orderguid;
                                                orderstock.FK_Stock = stockinfo.GUID;
                                                orderstock.StockCount = stockitem.Stock;
                                                orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                                orderstock.Creater = operatorId;
                                                orderstock.CreateTime = DateTime.Now;
                                                orderstock.organizeid = stockinfo.organizeid;
                                                db.Insert<TORDERSTOCKEntity>(orderstock);

                                                stockitem.Stock = Decimal.Zero;
                                                db.Update<TSTOCKEntity>(stockitem, null);
                                            }
                                        }
                                    }
                                }

                                TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == SKUStr).FirstOrDefault();

                                if (productInfo != null)
                                {
                                    productInfo.SaleCount -= pdtInfo.ProductCount;
                                    db.Update<TPRODUCTEntity>(productInfo, null);
                                }
                            }
                            else
                            {
                                isResult = false;
                                break;
                            }
                        }
                        else
                        {
                            isResult = false;
                            break;
                        }
                    }
                }
                if (isResult)
                {
                    TORDEREntity orderInfo = db.IQueryable<TORDEREntity>().Where(p => p.GUID == orderguid).SingleOrDefault();

                    orderInfo.Distribution = operatorId;
                    orderInfo.DistributionTime = DateTime.Now;
                    db.Update<TORDEREntity>(orderInfo, null);

                    db.Commit();
                }
                else
                {
                    db.Rollback();
                    result.msg = "库存不足，无法出库！";
                }

                result.IsOK = isResult;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

    }
}
