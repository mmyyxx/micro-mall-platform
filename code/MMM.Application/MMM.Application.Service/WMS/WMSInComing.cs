﻿using MMM.Application.Entity;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Entity.WMS;
using MMM.Application.Mapping.WMS;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WMS
{
    /// <summary>
    /// 仓库入库
    /// </summary>
    public class WMSInComing
    {
        static MMM.Util.Log.Log _logger = LogFactory.GetLogger("wms");

        /// <summary>
        /// 获取上架商品
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <returns></returns>
        public static string GetPutAwayList(string warehouseNo)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                IList<VPUTAWAYEntity> list = db.IQueryable<VPUTAWAYEntity>().Where(p => p.FK_WarehouseNo == warehouseNo).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (VPUTAWAYEntity putaway in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = putaway.FK_Product;
                        item.TitleName = "商品名称";
                        item.Title = putaway.ProductName;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "条码",
                            Value1 = putaway.SKU
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "单位",
                            Value1 = putaway.Unit,
                            Name2 = "数量",
                            Value2 = putaway.PdtCount.ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取上架明细
        /// </summary>
        /// <param name="productguid"></param>
        /// <returns></returns>
        public static string GetPutAwayDetailList(string productguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                IList<PutAwayDetail> list = db.IQueryable<TPUTAWAYEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Status == CodeConst.IsUse.NO)
                    .Join(db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Product == productguid), m => m.FK_PurchaseDetail, n => n.GUID, (m, n) => new { m, n })
                    .Join(db.IQueryable<T_LOCATIONEntity>(), x => x.m.FK_Location, y => y.GUID, (x, y) => new PutAwayDetail
                    {
                        GUID = x.m.GUID,
                        PurchaseOrderGUID = x.n.FK_PurchaseOrder,
                        PdtName = x.n.ProductName,
                        LocationNo = y.LocationNo,
                        MainUnit = x.n.MainUnit,
                        PdtCount = x.n.Count ?? 0,
                        AuxiliaryUnit = x.n.AuxiliaryUnit,
                        UnitCount = x.n.UnitCount
                    }).ToList();
                //获取所有的明细
                IList<String> purchaseorderids = list.Select(t => t.PurchaseOrderGUID).ToList();
                IList<TPURCHASEORDEREntity> purchaseorderList = db.IQueryable<TPURCHASEORDEREntity>().Where(t => t.IsDel == CodeConst.IsDel.NO && purchaseorderids.Contains(t.GUID)).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (PutAwayDetail putawaydetail in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = putawaydetail.GUID;
                        item.TitleName = "入库单号";
                        item.Title = purchaseorderList.Where(p => p.GUID == putawaydetail.PurchaseOrderGUID).Select(p => p.PurchaseOrderNo).SingleOrDefault();
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "物料",
                            Value1 = putawaydetail.PdtName,
                            Name2 = "储位",
                            Value2 = putawaydetail.LocationNo
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "单位",
                            Value1 = putawaydetail.MainUnit,
                            Name2 = "数量",
                            Value2 = putawaydetail.PdtCount.ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 完成入库
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="operatorId"></param>
        /// <param name="warehouseNo"></param>
        /// <param name="actualLocationNo"></param>
        /// <returns></returns>
        public static string CompletePutAwayByGUID(string guid, string operatorId, string warehouseNo, string actualLocationNo)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();

                string actualLocationGUID = "";
                T_LOCATIONEntity location = db.IQueryable<T_LOCATIONEntity>().SingleOrDefault(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo && p.LocationNo == actualLocationNo);
                if (location != null) actualLocationGUID = location.GUID;
                if (String.IsNullOrEmpty(actualLocationGUID) && !String.IsNullOrEmpty(actualLocationNo))
                {
                    result.msg = "储位码不正确！";
                    return JSONHelper.WXObjectToJson(result);
                }
                TPUTAWAYEntity putaway = db.IQueryable<TPUTAWAYEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Status == CodeConst.IsUse.NO && p.GUID == guid).SingleOrDefault();
                if (putaway != null)
                {
                    putaway.ActualLocation = String.IsNullOrEmpty(actualLocationGUID) ? putaway.FK_Location : actualLocationGUID;
                    putaway.Status = CodeConst.IsUse.YES;
                    putaway.Operator = operatorId;
                    putaway.CompleteTime = DateTime.Now;
                    db.Update<TPUTAWAYEntity>(putaway, null);

                    TPURCHASEDETAILEntity purchasedetail = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == putaway.FK_PurchaseDetail).SingleOrDefault();

                    if (purchasedetail == null)
                    {
                        result.msg = "入库单明细不存在！";
                        return JSONHelper.WXObjectToJson(result);
                    }

                    TPURCHASEORDEREntity purchaseorder = db.IQueryable<TPURCHASEORDEREntity>().Where(p => p.GUID == purchasedetail.FK_PurchaseOrder).SingleOrDefault();

                    //更新入库单状态
                    int cnt = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID != purchasedetail.GUID && p.FK_PurchaseOrder == purchasedetail.FK_PurchaseOrder)
.Join(db.IQueryable<TPUTAWAYEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Status == CodeConst.IsUse.NO), m => m.GUID, n => n.FK_PurchaseDetail, (m, n) => m).Count();
                    if (cnt == 0)
                    {
                        purchaseorder.Status = CodeConst.WMSOrderStatus.INSTOCK;
                        db.Update<TPURCHASEORDEREntity>(purchaseorder, null);
                    }

                    TSTOCKEntity stock = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Location == putaway.FK_Location && p.FK_Product == purchasedetail.FK_Product && p.ProductionDate == purchasedetail.ProductionDate).SingleOrDefault();

                    if (stock != null)
                    {
                        stock.Stock += purchasedetail.Count;
                        stock.ActualStock += purchasedetail.Count;
                        stock.Modifyer = operatorId;
                        stock.ModifyTime = DateTime.Now;
                        db.Update<TSTOCKEntity>(stock, null);
                    }
                    else
                    {
                        stock = new TSTOCKEntity();
                        stock.GUID = Guid.NewGuid().ToString("N");
                        stock.FK_Location = putaway.FK_Location;
                        stock.FK_Product = purchasedetail.FK_Product;
                        stock.BatchNumber = putaway.BatchNumber;
                        stock.Stock = purchasedetail.Count;
                        stock.ActualStock = purchasedetail.Count;
                        stock.ProductionDate = purchasedetail.ProductionDate;
                        stock.QualityDay = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == purchasedetail.FK_Product).Select(p => p.QualityDate).SingleOrDefault();
                        stock.IsDel = CodeConst.IsDel.NO;
                        stock.Creater = operatorId;
                        stock.CreateTime = DateTime.Now;
                        stock.Modifyer = operatorId;
                        stock.ModifyTime = DateTime.Now;
                        stock.organizeid = purchaseorder.organizeid;
                        stock.FK_WarehouseNo = warehouseNo;
                        db.Insert<TSTOCKEntity>(stock);
                    }

                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                    stockdetail.ID = Guid.NewGuid().ToString();
                    stockdetail.FK_Stock = stock.GUID;
                    stockdetail.Stock = purchasedetail.Count;
                    stockdetail.ActualStock = purchasedetail.Count;
                    stockdetail.Type = CodeConst.StockDetailType.RK;
                    stockdetail.Remark = String.Format("入库单号：{0}", purchaseorder.PurchaseOrderNo);
                    stockdetail.Creater = operatorId;
                    stockdetail.CreateTime = DateTime.Now;
                    stockdetail.organizeid = purchaseorder.organizeid;
                    db.Insert<TSTOCKDEAILEntity>(stockdetail);

                    result.IsOK = true;
                }
                else
                {
                    result.msg = "上架指示信息不是最新！";
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取未完成的入库单
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <returns></returns>
        public static string GetNoReadyPurchaseOrders(string warehouseNo)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<TPURCHASEORDEREntity> list = db.IQueryable<TPURCHASEORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo && p.Status == CodeConst.WMSOrderStatus.NOREADY).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    IList<String> supplierIds = list.Select(t => t.FK_Supplier).ToList();
                    IList<TSUPPLIEREntity> supplierList = db.IQueryable<TSUPPLIEREntity>().Where(t => supplierIds.Contains(t.GUID)).ToList();
                    foreach (TPURCHASEORDEREntity purchaseorder in list)
                    {
                        string supplierName = supplierList.Where(p => p.GUID == purchaseorder.FK_Supplier).Select(p => p.SupplierName).SingleOrDefault();
                        ItemInfo item = new ItemInfo();
                        item.GUID = purchaseorder.GUID;
                        item.TitleName = "入库单号";
                        item.Tag = purchaseorder.Creater;
                        item.Title = purchaseorder.PurchaseOrderNo;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "订单类型",
                            Value1 = new string[] { "进货入库", "退货入库", "调拨入库" }[(purchaseorder.OrderType ?? 0) - 1],
                            Name2 = "供应商",
                            Value2 = supplierName
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "创建人",
                            Value1 = purchaseorder.Creater,
                            Name2 = "创建时间",
                            Value2 = purchaseorder.CreateTime.Value.ToString("yyyy-MM-dd HH:mm:ss")
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取未完成的入库单详情
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string GetNoReadyPurchaseOrderDetail(string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<TPURCHASEDETAILEntity> list = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.FK_PurchaseOrder == guid).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (TPURCHASEDETAILEntity detailInfo in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = detailInfo.GUID;
                        item.TitleName = "物料条码";
                        item.Title = detailInfo.SKU;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "物料名称",
                            Value1 = detailInfo.ProductName,
                            Name2 = "物料数量",
                            Value2 = detailInfo.Count.ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 保存入库单
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <param name="userId"></param>
        /// <param name="ordertype"></param>
        /// <param name="supplierguid"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public static string SavePurchaseOrderByXCX(string warehouseNo, string userId, string ordertype, string supplierguid, string remark)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                T_WAREHOUSEEntity warehouse = db.IQueryable<T_WAREHOUSEEntity>().FirstOrDefault(t => t.WarehouseNo == warehouseNo);

                TPURCHASEORDEREntity purchaseorder = new TPURCHASEORDEREntity();
                Random rd = new Random();
                purchaseorder.GUID = Guid.NewGuid().ToString();
                purchaseorder.PurchaseOrderNo = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0');
                purchaseorder.FK_WarehouseNo = warehouseNo;
                purchaseorder.FK_Supplier = supplierguid;
                purchaseorder.Status = CodeConst.WMSOrderStatus.NOREADY;
                purchaseorder.PurchaseOrderPrice = Decimal.Zero;
                purchaseorder.OrderType = Int32.Parse(ordertype) + 1;
                purchaseorder.Remark = remark;
                purchaseorder.IsDel = CodeConst.IsDel.NO;
                purchaseorder.Creater = userId;
                purchaseorder.CreateTime = DateTime.Now;
                purchaseorder.Modifyer = userId;
                purchaseorder.ModifyTime = DateTime.Now;
                purchaseorder.organizeid = warehouse.organizeid;
                db.Insert<TPURCHASEORDEREntity>(purchaseorder);

                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取物料信息
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="guid"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public static string GetProductInfo(string sku, string guid, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TPURCHASEDETAILEntity purchasedetail = new TPURCHASEDETAILEntity();

                TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == sku && p.organizeid == organizeid).FirstOrDefault();
                if (productInfo == null)
                {
                    TPACKAGEEntity packageInfo = db.IQueryable<TPACKAGEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PackageSKU == sku && p.organizeid == organizeid).FirstOrDefault();
                    productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == packageInfo.FK_Product).SingleOrDefault();
                    purchasedetail.AuxiliaryUnit = sku;

                    List<TPURCHASEDETAILEntity> numberlist = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_PurchaseOrder == guid && p.SKU == productInfo.SKU).ToList();
                    purchasedetail.Count = numberlist.Count > 0 ? numberlist.Sum(p => p.Count) : Decimal.Zero;
                    purchasedetail.UnitConversion = packageInfo.PdtCount;
                    if (purchasedetail.UnitConversion > Decimal.Zero)
                    {
                        purchasedetail.UnitCount = purchasedetail.Count / purchasedetail.UnitConversion;
                    }
                }
                else
                {
                    List<TPURCHASEDETAILEntity> numberlist = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_PurchaseOrder == guid && p.SKU == sku).ToList();
                    purchasedetail.FK_PurchaseOrder = guid;
                    purchasedetail.Count = numberlist.Count > 0 ? numberlist.Sum(p => p.Count) : Decimal.Zero;

                }

                purchasedetail.FK_Product = productInfo.GUID;
                purchasedetail.SKU = productInfo.SKU;
                purchasedetail.ProductName = productInfo.PdtName;
                purchasedetail.MainUnit = productInfo.PdtUnit;
                purchasedetail.Remark = productInfo.QualityDate.ToString();

                result.Data = JSONHelper.WXObjectToJson(purchasedetail);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 保存入库单明细
        /// </summary>
        /// <param name="qualityDate"></param>
        /// <param name="pdtCount"></param>
        /// <param name="detailInfo"></param>
        /// <returns></returns>
        public static string SavePurchaseDetail(string qualityDate, string pdtCount, string detailInfo, string organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TPURCHASEDETAILEntity detailItem = JSONHelper.WXDecodeObject<TPURCHASEDETAILEntity>(detailInfo);

                TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == detailItem.SKU && p.organizeid == organizeid).FirstOrDefault();
                if (productInfo != null && productInfo.QualityDate != Int32.Parse(qualityDate))
                {
                    productInfo.QualityDate = Int32.Parse(qualityDate);
                    db.Update<TPRODUCTEntity>(productInfo, null);
                }

                TPURCHASEDETAILEntity detail = db.IQueryable<TPURCHASEDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_PurchaseOrder == detailItem.FK_PurchaseOrder
&& p.SKU == detailItem.SKU && p.ProductionDate == detailItem.ProductionDate && p.organizeid == organizeid).FirstOrDefault();
                if (detail == null)
                {
                    detail = new TPURCHASEDETAILEntity();
                    detail.GUID = Guid.NewGuid().ToString();
                    detail.FK_PurchaseOrder = detailItem.FK_PurchaseOrder;
                    detail.FK_Product = detailItem.FK_Product;
                    detail.SKU = detailItem.SKU;
                    detail.ProductName = detailItem.ProductName;
                    if (String.IsNullOrEmpty(detailItem.AuxiliaryUnit))
                    {
                        detail.Count = Decimal.Parse(pdtCount);
                        detail.UnitCount = Decimal.Zero;
                    }
                    else
                    {
                        detail.Count = Decimal.Parse(pdtCount) * detailItem.UnitConversion.Value;
                        detail.UnitCount = Decimal.Parse(pdtCount);
                    }
                    detail.MainUnit = detailItem.MainUnit;
                    detail.UnitConversion = detailItem.UnitConversion;
                    detail.Price = Decimal.Zero;
                    detail.Discount = 1;
                    detail.ProductionDate = detailItem.ProductionDate;
                    detail.IsDel = CodeConst.IsDel.NO;
                    detail.Creater = detailItem.Creater;
                    detail.CreateTime = DateTime.Now;
                    detail.Modifyer = detailItem.Creater;
                    detail.ModifyTime = DateTime.Now;
                    detail.organizeid = organizeid;
                    db.Insert<TPURCHASEDETAILEntity>(detail);
                }
                else
                {
                    if (String.IsNullOrEmpty(detailItem.AuxiliaryUnit))
                    {
                        detail.Count += Decimal.Parse(pdtCount);
                    }
                    else
                    {
                        detail.Count += Decimal.Parse(pdtCount) * detailItem.UnitConversion.Value;
                        detail.UnitCount = detail.Count / detailItem.UnitConversion.Value;
                    }
                    detail.Modifyer = detailItem.Creater;
                    detail.ModifyTime = DateTime.Now;
                    db.Update<TPURCHASEDETAILEntity>(detail, null);
                }

                db.Commit();
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Commit();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 更新入库单
        /// </summary>
        /// <param name="orderguid"></param>
        /// <returns></returns>
        public static string UpdatePurchaseOrder(string orderguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TPURCHASEORDEREntity purchaseorder = db.IQueryable<TPURCHASEORDEREntity>().Where(p => p.GUID == orderguid).SingleOrDefault();
                if (purchaseorder != null)
                {
                    purchaseorder.Status = CodeConst.WMSOrderStatus.NOSHELF;
                    purchaseorder.ModifyTime = DateTime.Now;
                    db.Update<TPURCHASEORDEREntity>(purchaseorder, null);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }
    }
}
