﻿using MMM.Application.Entity;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Entity.WMS;
using MMM.Application.Mapping.WMS;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WMS
{
    /// <summary>
    /// 出库
    /// </summary>
    public class WMSOutGoing
    {
        static MMM.Util.Log.Log _logger = LogFactory.GetLogger("wms");

        /// <summary>
        /// 获取未提出出库信息
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static string GetOutGoingList(string warehouseNo, string operatorId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<OutGoingInfo> list = db.IQueryable<TOUTGOINGEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo && p.Status == CodeConst.OutStockStatus.WTC && (p.Operator == null || p.Operator == operatorId))
                    .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.FK_Product, n => n.GUID, (m, n) => new OutGoingInfo
                    {
                        GUID = m.GUID,
                        SKU = n.SKU,
                        PdtName = n.PdtName,
                        Unit = n.PdtUnit,
                        Count = m.Count ?? 0,
                        OutCount = m.OutCount ?? 0
                    }).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (OutGoingInfo outgoing in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = outgoing.GUID;
                        item.TitleName = "商品名称";
                        item.Title = outgoing.PdtName;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "条码",
                            Value1 = outgoing.SKU,
                            Name2 = "单位",
                            Value2 = outgoing.Unit
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "需求数量",
                            Value1 = outgoing.Count.ToString(),
                            Name2 = "提出数量",
                            Value2 = outgoing.OutCount.ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取已提出得出库信息
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static string GetOutGoingStockList(string warehouseNo, string operatorId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<OutGoingInfo> list = db.IQueryable<TOUTGOINGEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo && p.Status == CodeConst.OutStockStatus.YTC && p.Operator == operatorId)
                    .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.FK_Product, n => n.GUID, (m, n) => new OutGoingInfo
                    {
                        GUID = m.GUID,
                        SKU = n.SKU,
                        PdtName = n.PdtName,
                        Unit = n.PdtUnit,
                        Count = m.Count ?? 0,
                        StockCount = m.StockCount ?? 0
                    }).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (OutGoingInfo outgoing in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = outgoing.GUID;
                        item.TitleName = "商品名称";
                        item.Title = outgoing.PdtName;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "条码",
                            Value1 = outgoing.SKU,
                            Name2 = "单位",
                            Value2 = outgoing.Unit
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "需求数量",
                            Value1 = outgoing.Count.ToString(),
                            Name2 = "出库数量",
                            Value2 = outgoing.StockCount.ToString()
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 出库详情
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <param name="outgoingguid"></param>
        /// <returns></returns>
        public static string GetOutGoingDetail(string warehouseNo, string outgoingguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<VSTOCKINFOEntity> list = db.IQueryable<VSTOCKINFOEntity>().Where(p => p.WarehouseNo == warehouseNo && p.OutGoingGUID == outgoingguid).ToList();
                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    foreach (VSTOCKINFOEntity stockInfo in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = stockInfo.StockGUID;
                        item.TitleName = "储位编码";
                        item.Title = stockInfo.LocationNo;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "条码",
                            Value1 = stockInfo.SKU,
                            Name2 = "名称",
                            Value2 = stockInfo.PdtName
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "生产日期",
                            Value1 = stockInfo.ProductionDate.Value.ToString("yyyyMMdd"),
                            Name2 = "保质期",
                            Value2 = stockInfo.QualityDate.ToString()
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "库存数量",
                            Value1 = stockInfo.Stock.ToString(),
                            Name2 = "",
                            Value2 = ""
                        });
                        ItemList.Add(item);
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 出库库存
        /// </summary>
        /// <param name="outgoingguid"></param>
        /// <param name="stockguid"></param>
        /// <returns></returns>
        public static string GetOutGoingStockInfo(string outgoingguid, string stockguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                OutGoingStockInfo itemInfo = db.IQueryable<TOUTGOINGEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == outgoingguid)
                    .Join(db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == stockguid), m => m.FK_Product, n => n.FK_Product, (m, n) => new OutGoingStockInfo
                    {
                        Count = (m.Count ?? 0) - (m.OutCount ?? 0),
                        LocationCount = n.Stock ?? 0
                    }).SingleOrDefault();

                result.Data = JSONHelper.WXObjectToJson(itemInfo);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 出库
        /// </summary>
        /// <param name="outgoingguid"></param>
        /// <param name="stockguid"></param>
        /// <param name="operatorId"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        public static string OutStockInfo(string outgoingguid, string stockguid, string operatorId, decimal stock)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TOUTGOINGEntity outgoingInfo = db.IQueryable<TOUTGOINGEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Status == CodeConst.OutStockStatus.WTC && p.GUID == outgoingguid).SingleOrDefault();
                if (outgoingInfo == null)
                {
                    result.msg = "出库分配不存在，请更新数据！";
                    return JSONHelper.WXObjectToJson(result);
                }
                if (outgoingInfo.Count < outgoingInfo.OutCount + stock)
                {
                    result.msg = "超出需要提出的物料数量！";
                    return JSONHelper.WXObjectToJson(result);
                }
                outgoingInfo.OutCount += stock;
                outgoingInfo.Operator = operatorId;
                if (outgoingInfo.Count == outgoingInfo.OutCount)
                {
                    outgoingInfo.Status = CodeConst.OutStockStatus.YTC;
                }
                db.Update<TOUTGOINGEntity>(outgoingInfo, null);

                TSTOCKEntity stockInfo = db.IQueryable<TSTOCKEntity>().Where(p => p.GUID == stockguid).SingleOrDefault();
                stockInfo.Stock -= stock;
                db.Update<TSTOCKEntity>(stockInfo, null);

                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                stockdetail.ID = Guid.NewGuid().ToString();
                stockdetail.FK_Stock = stockInfo.GUID;
                stockdetail.Stock = -stock;
                stockdetail.ActualStock = -stock;
                stockdetail.Type = CodeConst.StockDetailType.CK;
                stockdetail.Remark = String.Format("{0}批次出库", outgoingInfo.BatchNumber);
                stockdetail.Creater = outgoingInfo.Operator;
                stockdetail.CreateTime = DateTime.Now;
                stockdetail.organizeid = outgoingInfo.organizeid;
                db.Insert<TSTOCKDEAILEntity>(stockdetail);

                TOUTGOINGSTOCKEntity outgoingstock = new TOUTGOINGSTOCKEntity();
                outgoingstock.ID = Guid.NewGuid().ToString();
                outgoingstock.FK_OutGoing = outgoingguid;
                outgoingstock.FK_Stock = stockguid;
                outgoingstock.StockCount = stock;
                outgoingstock.Creater = operatorId;
                outgoingstock.CreateTime = DateTime.Now;
                db.Insert<TOUTGOINGSTOCKEntity>(outgoingstock);

                db.Commit();

                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取出库单信息
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <param name="outgoingguid"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static string GetOutStockOrderInfo(string warehouseNo, string outgoingguid, string operatorId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<OutStockDetailInfo> list = db.IQueryable<TOUTGOINGEntity>().Where(p => p.GUID == outgoingguid && p.Status == CodeConst.OutStockStatus.YTC)
                    .Join(db.IQueryable<TOUTSTOCKDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.DetailStatus == CodeConst.IsUse.NO), m => m.FK_Product, n => n.FK_Product, (m, n) => new { m, n })
                    .Join(db.IQueryable<TOUTSTOCKORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == warehouseNo && p.Status == CodeConst.WMSOrderStatus.ONSHELF), x => x.n.FK_OutStockOrder, y => y.GUID, (x, y) => new OutStockDetailInfo
                    {
                        DetailGUID = x.n.GUID,
                        OutStockOrderNo = y.OutStockOrderNo,
                        SKU = x.n.SKU,
                        ProductName = x.n.ProductName,
                        Count = x.n.Count ?? 0,
                        PlatName = y.FK_Plat
                    }).ToList();

                List<ItemInfo> ItemList = new List<ItemInfo>();
                if (list.Count > 0)
                {
                    IList<String> platids = list.Select(t => t.PlatName).ToList();
                    IList<T_PLATEntity> platList = db.IQueryable<T_PLATEntity>().Where(t => platids.Contains(t.GUID)).ToList();
                    foreach (OutStockDetailInfo detailInfo in list)
                    {
                        ItemInfo item = new ItemInfo();
                        item.GUID = detailInfo.DetailGUID;
                        item.TitleName = "出库单号";
                        item.Title = detailInfo.OutStockOrderNo;
                        item.DetailList = new List<ItemDetailInfo>();
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "条码",
                            Value1 = detailInfo.SKU,
                            Name2 = "名称",
                            Value2 = detailInfo.ProductName
                        });
                        item.DetailList.Add(new ItemDetailInfo()
                        {
                            Name1 = "出库数量",
                            Value1 = detailInfo.Count.ToString(),
                            Name2 = "月台",
                            Value2 = platList.Where(p => p.GUID == detailInfo.PlatName).Select(p => p.PlatName).SingleOrDefault()
                        });
                        ItemList.Add(item);
                    }
                }

                result.Data = JSONHelper.WXObjectToJson(ItemList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 出库完成
        /// </summary>
        /// <param name="detailguid"></param>
        /// <param name="outgoingguid"></param>
        /// <returns></returns>
        public static string OutStockDetailComplete(string detailguid, string outgoingguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TOUTSTOCKDETAILEntity detail = db.IQueryable<TOUTSTOCKDETAILEntity>().Where(p => p.GUID == detailguid).SingleOrDefault();
                if (detail == null)
                {
                    result.msg = "出库单明细不存在！";
                    return JSONHelper.WXObjectToJson(result);
                }
                detail.DetailStatus = CodeConst.IsUse.YES;
                db.Update<TOUTSTOCKDETAILEntity>(detail, null);

                int cnt = db.IQueryable<TOUTSTOCKDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.DetailStatus == CodeConst.IsUse.NO && p.GUID != detail.GUID && p.FK_OutStockOrder == detail.FK_OutStockOrder).Count();
                if (cnt == 0)
                {
                    TOUTSTOCKORDEREntity outstockorder = db.IQueryable<TOUTSTOCKORDEREntity>().Where(p => p.GUID == detail.FK_OutStockOrder).SingleOrDefault();
                    outstockorder.Status = CodeConst.WMSOrderStatus.INSTOCK;
                    db.Update<TOUTSTOCKORDEREntity>(outstockorder, null);
                }

                TOUTGOINGEntity outgoing = db.IQueryable<TOUTGOINGEntity>().Where(p => p.GUID == outgoingguid).SingleOrDefault();
                outgoing.StockCount += detail.Count;
                if (outgoing.Count == outgoing.StockCount)
                {
                    outgoing.Status = CodeConst.OutStockStatus.YWC;
                    outgoing.CompleteTime = DateTime.Now;
                    db.Update<TOUTGOINGEntity>(outgoing, null);

                    List<TOUTGOINGSTOCKEntity> outgoingstocklist = db.IQueryable<TOUTGOINGSTOCKEntity>().Where(p => p.FK_OutGoing == outgoing.GUID).ToList();
                    List<TSTOCKEntity> stocklist = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TOUTGOINGSTOCKEntity>().Where(p => p.FK_OutGoing == outgoing.GUID), m => m.GUID, n => n.FK_Stock, (m, n) => m).Distinct().ToList();

                    foreach (TSTOCKEntity stockInfo in stocklist)
                    {
                        decimal outstock = outgoingstocklist.Where(p => p.FK_Stock == stockInfo.GUID).Sum(p => p.StockCount) ?? 0;
                        stockInfo.ActualStock -= outstock;
                        db.Update<TSTOCKEntity>(stockInfo, null);

                        TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                        stockdetail.ID = Guid.NewGuid().ToString();
                        stockdetail.FK_Stock = stockInfo.GUID;
                        stockdetail.ActualStock = -outstock;
                        stockdetail.Stock = -outstock;
                        stockdetail.Type = CodeConst.StockDetailType.CK;
                        stockdetail.Remark = String.Format("{0}批次出库", outgoing.BatchNumber);
                        stockdetail.Creater = outgoing.Operator;
                        stockdetail.CreateTime = DateTime.Now;
                        stockdetail.organizeid = detail.organizeid;
                        db.Insert<TSTOCKDEAILEntity>(stockdetail);
                    }
                }

                db.Commit();

                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }
    }
}
