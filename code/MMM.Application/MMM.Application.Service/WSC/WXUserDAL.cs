﻿using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Service.Order;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WSC
{
    public class WXUserDAL
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        /// <summary>
        /// 注册会员
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="memberstr"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string RegisterMember(string openid, string memberstr, String organizeid)
        {
            ResultData result = new ResultData();

            TMEMBEREntity memberInfo = JSONHelper.WXDecodeObject<TMEMBEREntity>(memberstr);

            Data.IDatabase db = DbFactory.Base();

            TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openid && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
            TMEMBEREntity member = db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_UnionId == user.Openid).FirstOrDefault();
            bool isAdd = false;

            try
            {
                db.BeginTrans();
                if (member == null)
                {
                    isAdd = true;
                    member = new TMEMBEREntity();
                    member.GUID = Guid.NewGuid().ToString();
                    member.Birthday = memberInfo.Birthday;
                }
                member.UserName = memberInfo.UserName;
                member.Gender = user.Gender;
                member.Mobile = memberInfo.Mobile;
                member.Adress = memberInfo.Adress;
                member.QQNumber = memberInfo.QQNumber;
                member.FK_UnionId = user.Unionid;
                member.Profession = memberInfo.Profession;
                member.Money = Decimal.Zero;
                member.Remark = memberInfo.Remark;
                member.organizeid = organizeid;
                if (isAdd)
                {
                    member.IsDel = CodeConst.IsDel.NO;
                    member.Creater = openid;
                    member.CreateTime = DateTime.Now;
                    member.usercode = DateTime.Now.Ticks.ToString();
                    db.Insert<TMEMBEREntity>(member);

                    IList<TUSERCARDEntity> usercardList = db.IQueryable<TUSERCARDEntity>().Where(p => p.CardType == CodeConst.CardType.XRQ && p.IsDel == CodeConst.IsDel.NO && p.FK_OpenId == openid && p.IsUse == CodeConst.CardUse.NOUSE).ToList();
                    IList<T_CARDEntity> t_CARDEntities = db.IQueryable<T_CARDEntity>().Where(p => p.CardType == CodeConst.CardType.XRQ && p.IsDel == CodeConst.IsDel.NO).ToList();
                    foreach (T_CARDEntity card in t_CARDEntities)
                    {
                        TUSERCARDEntity temcard = usercardList.FirstOrDefault(t => t.FK_CardID == card.CardId);
                        //没有卡就新增
                        if (temcard == null)
                        {
                            T_CARDEntity cardinfo = t_CARDEntities.Where(p => p.CardId == card.CardId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                            if (cardinfo != null)
                            {
                                TUSERCARDEntity usercard = new TUSERCARDEntity();
                                usercard.GUID = Guid.NewGuid().ToString();
                                usercard.FK_AppId = cardinfo.FK_AppId;
                                usercard.FK_OpenId = openid;
                                usercard.FK_CardID = cardinfo.CardId;
                                usercard.CardType = cardinfo.CardType;
                                usercard.LeastCost = cardinfo.LeastCost ?? 0;
                                usercard.ReduceCost = cardinfo.ReduceCost ?? 0;
                                usercard.Explain = cardinfo.Explain;
                                usercard.ValidDate = DateTime.Now.AddDays(cardinfo.ValidDate ?? 0);
                                usercard.IsUse = CodeConst.CardUse.NOUSE;
                                usercard.IsDel = CodeConst.IsDel.NO;
                                usercard.Creater = openid;
                                usercard.CreateTime = DateTime.Now;
                                usercard.organizeid = organizeid;

                                db.Insert<TUSERCARDEntity>(usercard);
                            }
                        }
                    }
                }
                else
                {
                    member.Modifyer = openid;
                    member.ModifyTime = DateTime.Now;

                    db.Update<TMEMBEREntity>(member, null);
                }
                db.Commit();
                result.IsOK = true;
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                db.Rollback();
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 生成二维码
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string GetUserQRCode(string openId)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();
                TMEMBEREntity member = db.IQueryable<TMEMBEREntity>().FirstOrDefault(t => t.FK_UnionId == openId);
                result.Data = QRCode.CreateQRCode(member.usercode, "Byte", 10, 0, "H");
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 用户备注
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="appguid"></param>
        /// <param name="remark"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string UserRemark(string openId, string appguid, string remark, String organizeid)
        {
            ResultData result = new ResultData();
            try
            {
                TUSERREMARKEntity userremark = new TUSERREMARKEntity();
                userremark.GUID = Guid.NewGuid().ToString();
                userremark.FK_AppId = appguid;
                userremark.FK_OpenId = openId;
                userremark.Remark = remark;
                userremark.CreateTime = DateTime.Now;
                userremark.organizeid = organizeid;

                Data.IDatabase db = DbFactory.Base();
                db.Insert<TUSERREMARKEntity>(userremark);

                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 保存用户自提信息
        /// </summary>
        /// <param name="wxopenId"></param>
        /// <param name="username"></param>
        /// <param name="usertel"></param>
        /// <param name="address"></param>
        /// <returns></returns>
        public String SaveUserQuHuo(string wxopenId, String username, String usertel, String address,String selectid)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();

                TUSEREntity userremark = db.IQueryable<TUSEREntity>().FirstOrDefault(t=>t.Openid==wxopenId);
                if (userremark != null)
                {
                    userremark.username = username;
                    userremark.usertel = usertel;
                    userremark.useraddress = address;
                    userremark.selectid = selectid;

                    db.Update<TUSEREntity>(userremark,null);
                }

                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取小程序的配置信息
        /// </summary>
        /// <param name="appguid"></param>
        /// <returns></returns>
        public string GetWXAppInfo(string appguid)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();
                TWXAPPEntity wxapp = db.FindEntity<TWXAPPEntity>(appguid);
                if (wxapp != null)
                {
                    result.Data = JSONHelper.WXObjectToJson(wxapp);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 保存地址
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="guid"></param>
        /// <param name="name"></param>
        /// <param name="adress"></param>
        /// <param name="mobile"></param>
        /// <param name="moreadress"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="defaultOrganize"></param>
        /// <returns></returns>
        public string SaveAdressData(string openid, string guid, string name, string adress, string mobile, string moreadress, decimal longitude, decimal latitude, string defaultOrganize)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();

                bool IsAdd = true;
                int isuse = db.IQueryable<TADRESSEntity>().Where(p => p.FK_OpenId == openid && p.IsDel == CodeConst.IsDel.NO).Count();

                TADRESSEntity tadress = new TADRESSEntity();
                tadress.GUID = Guid.NewGuid().ToString();
                tadress.IsDel = CodeConst.IsDel.NO;
                tadress.Creater = openid;
                tadress.CreateTime = DateTime.Now;
                tadress.IsUse = isuse > 0 ? CodeConst.IsUse.NO : CodeConst.IsUse.YES;
                tadress.organizeid = defaultOrganize;

                if (!String.IsNullOrEmpty(guid))
                {
                    IsAdd = false;
                    tadress = db.IQueryable<TADRESSEntity>().SingleOrDefault(p => p.GUID == guid);
                }

                tadress.FK_OpenId = openid;
                tadress.UserName = name;
                tadress.Mobile = mobile;
                tadress.Adress = adress;
                tadress.MoreAdress = moreadress;
                if (longitude > 0)
                {
                    tadress.Longitude = longitude;
                }
                if (latitude > 0)
                {
                    tadress.Latitude = latitude;
                }
                tadress.Modifyer = openid;
                tadress.ModifyTime = DateTime.Now;

                if (IsAdd)
                {
                    db.Insert<TADRESSEntity>(tadress);
                }
                else
                {
                    db.Update<TADRESSEntity>(tadress, null);
                }
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                result.msg = ex.Message;
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 更新地址
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public string UpdateAdressDefault(string guid, string openid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<TADRESSEntity> list = db.IQueryable<TADRESSEntity>().Where(p => p.FK_OpenId == openid && p.IsDel == CodeConst.IsDel.NO).ToList();
                db.BeginTrans();
                foreach (TADRESSEntity adress in list)
                {
                    if (adress.GUID == guid)
                    {
                        adress.IsUse = CodeConst.IsUse.YES;
                    }
                    else
                    {
                        adress.IsUse = CodeConst.IsUse.NO;
                    }
                    db.Update<TADRESSEntity>(adress, null);
                }
                db.Commit();
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 删除地址
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string DeleteAdressByGUID(string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TADRESSEntity adress = db.IQueryable<TADRESSEntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (adress != null)
                {
                    adress.IsDel = CodeConst.IsDel.YES;
                    adress.DeleteTime = DateTime.Now;
                    db.Update<TADRESSEntity>(adress, null);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取地址
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetAdressByGUID(string guid)
        {
            AdressModel adressmodel = null;
            Data.IDatabase db = DbFactory.Base();
            try
            {
                adressmodel = db.IQueryable<TADRESSEntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO).Select(p => new AdressModel
                {
                    GUID = p.GUID,
                    AdressName = p.UserName,
                    AdressPlace = p.Adress,
                    MoreAdress = p.MoreAdress,
                    Mobile = p.Mobile,
                    IsUse = p.IsUse ?? 0
                }).SingleOrDefault();
                if (adressmodel == null)
                {
                    adressmodel = new AdressModel();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(adressmodel);
        }

        /// <summary>
        /// 用户是否签到
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="appguid"></param>
        /// <returns></returns>
        public string IsUserSignIn(string openid, string appguid)
        {
            ResultData result = new ResultData();
            try
            {
                result.IsOK = !UserCanSignIn(openid, appguid);
            }
            catch (Exception ex)
            {
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 用户签到
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="appguid"></param>
        /// <returns></returns>
        public string UserSignIn(string openid, string appguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openid && p.IsDel == CodeConst.IsDel.NO)
                    .Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();

                if (user != null)
                {
                    if (UserCanSignIn(openid, appguid))
                    {
                        user.SignInDate = DateTime.Now;
                        user.Integral += CodeConst.Integral.SIGNIN;

                        TORDERService tORDERService = new TORDERService();
                        TINTEGRALEntity tintegral = tORDERService.IntegralRecord(openid, appguid, CodeConst.Integral.SIGNIN, user.Integral.Value, CodeConst.IntegralType.SIGNIN, CodeConst.IntegralRemark.SIGNIN);
                        db.Insert<TINTEGRALEntity>(tintegral);
                        db.Update<TUSEREntity>(user, null);
                    }

                }
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取用户积分
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string GetUserIntegral(string openId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (user != null)
                {
                    result.Data = user.Integral.ToString();
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取优惠券
        /// </summary>
        /// <param name="appguid"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetIntegralInfo(string appguid, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<T_CARDEntity> list = db.IQueryable<T_CARDEntity>().Where(p => p.organizeid == organizeid && p.IsDel == CodeConst.IsDel.NO && p.Integral > 0).OrderByDescending(p => p.ModifyTime).ToList();
                if (list.Count > 0)
                {
                    result.Data = JSONHelper.WXObjectToJson(list);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 给用户添加优惠券
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public string ExChangeProduct(string openId, string cardId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();

                T_CARDEntity cardinfo = db.IQueryable<T_CARDEntity>().Where(p => p.CardId == cardId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                TUSEREntity userinfo = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (cardinfo != null && userinfo != null)
                {
                    TUSERCARDEntity usercard = new TUSERCARDEntity();
                    usercard.GUID = Guid.NewGuid().ToString();
                    usercard.FK_AppId = cardinfo.FK_AppId;
                    usercard.FK_OpenId = openId;
                    usercard.FK_CardID = cardId;
                    usercard.CardType = cardinfo.CardType;
                    usercard.LeastCost = cardinfo.LeastCost ?? 0;
                    usercard.ReduceCost = cardinfo.ReduceCost ?? 0;
                    usercard.Explain = cardinfo.Explain;
                    usercard.ValidDate = DateTime.Now.AddDays(cardinfo.ValidDate ?? 0);
                    usercard.IsUse = CodeConst.CardUse.NOUSE;
                    usercard.IsDel = CodeConst.IsDel.NO;
                    usercard.Creater = openId;
                    usercard.CreateTime = DateTime.Now;
                    usercard.Modifyer = openId;
                    usercard.ModifyTime = DateTime.Now;
                    usercard.organizeid = cardinfo.organizeid;

                    db.Insert<TUSERCARDEntity>(usercard);

                    userinfo.Integral -= cardinfo.Integral;
                    db.Update<TUSEREntity>(userinfo, null);

                    if (cardinfo.Integral > 0)
                    {
                        TORDERService tORDERService = new TORDERService();
                        TINTEGRALEntity tintegral = tORDERService.IntegralRecord(openId, cardinfo.FK_AppId, (cardinfo.Integral ?? 0) * -1, userinfo.Integral.Value, CodeConst.IntegralType.PAYSHOP, CodeConst.IntegralRemark.PAYSHOP);
                        db.Insert<TINTEGRALEntity>(tintegral);
                    }
                    db.Commit();
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取用户优惠券
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string GetUserCard(string openId, Int32 type)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                //取出所有券
                var query = db.IQueryable<TUSERCARDEntity>().Where(p => p.FK_OpenId == openId && p.IsDel == CodeConst.IsDel.NO);
                if (type != CodeConst.CardUse.NOUSE)
                {
                    query = query.Where(t => t.IsUse == type);
                }
                List<TUSERCARDEntity> list = query.OrderBy(p => p.ValidDate).ToList();
                List<TUSERCARDEntity> resultlist = new List<TUSERCARDEntity>();

                Int32 nouse = 0;
                Int32 use = 0;
                Int32 guoqi = 0;
                foreach (TUSERCARDEntity usercard in list)
                {
                    if (usercard.ValidDate < DateTime.Now.Date && usercard.IsUse == CodeConst.CardUse.NOUSE)
                    {
                        usercard.IsUse = CodeConst.CardUse.ISOUTTIME;
                    }
                    if (usercard.ValidDate.Value.AddDays(30) < DateTime.Now.Date)
                    {
                        usercard.IsDel = CodeConst.IsDel.YES;
                    }
                    db.Update<TUSERCARDEntity>(usercard, null);

                    TUSERCARDEntity newusercard = new TUSERCARDEntity();
                    Str.UpdateObject(usercard, newusercard);
                    newusercard.ValidDateStr = usercard.ValidDate.Value.ToString("yyyy.MM.dd");
                    newusercard.CreatetimeStr = usercard.CreateTime.Value.ToString("yyyy.MM.dd");
                    newusercard.day = (usercard.ValidDate.Value - usercard.CreateTime.Value).TotalDays.ToString();

                    if (usercard.IsUse == CodeConst.CardUse.ISOUTTIME)
                    {
                        guoqi++;
                    }
                    else if (usercard.IsUse == CodeConst.CardUse.ISUSE)
                    {
                        use++;
                    }
                    else
                        nouse++;
                    if (usercard.IsUse == type)
                    {
                        resultlist.Add(newusercard);
                    }
                }
                db.Commit();
                result.Data = JSONHelper.WXObjectToJson(resultlist);
                result.msg = nouse.ToString() + ":" + use.ToString() + ":" + guoqi.ToString();
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 增加优惠券
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string AddUserCard(string openId, string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                T_OUTCARDEntity outcard = db.IQueryable<T_OUTCARDEntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.IsUse.NO).FirstOrDefault();
                if (outcard == null)
                {
                    result.IsOK = false;
                    result.msg = "该卡券码无效！";
                    return JSONHelper.WXObjectToJson(result);
                }
                T_CARDEntity cardinfo = db.IQueryable<T_CARDEntity>().Where(p => p.CardId == outcard.CardId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (cardinfo != null)
                {
                    TUSERCARDEntity usercard = new TUSERCARDEntity();
                    usercard.GUID = Guid.NewGuid().ToString();
                    usercard.FK_AppId = cardinfo.FK_AppId;
                    usercard.FK_OpenId = openId;
                    usercard.FK_CardID = cardinfo.CardId;
                    usercard.CardType = cardinfo.CardType;
                    usercard.LeastCost = cardinfo.LeastCost ?? 0;
                    usercard.ReduceCost = cardinfo.ReduceCost ?? 0;
                    usercard.Explain = cardinfo.Explain;
                    usercard.ValidDate = DateTime.Now.AddDays(cardinfo.ValidDate ?? 0);
                    usercard.IsUse = CodeConst.CardUse.NOUSE;
                    usercard.IsDel = CodeConst.IsDel.NO;
                    usercard.Creater = openId;
                    usercard.CreateTime = DateTime.Now;
                    usercard.Modifyer = openId;
                    usercard.ModifyTime = DateTime.Now;
                    usercard.organizeid = cardinfo.organizeid;
                    db.Insert<TUSERCARDEntity>(usercard);

                    outcard.IsUse = CodeConst.IsUse.YES;
                    outcard.FK_OpenId = openId;
                    db.Update<T_OUTCARDEntity>(outcard, null);

                    result.IsOK = true;
                    result.msg = "卡券领取成功！";
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取商品信息
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="guid"></param>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string GetProductGUID(string sku, string guid, string openId)
        {
            ResultData result = new ResultData();
            try
            {
                if (sku.Contains("YHQ-"))
                {
                    return AddUserCard(openId, sku.Replace("YHQ-", ""));
                }
                else if (sku.Contains("CXK-"))
                {
                    Data.IDatabase db = DbFactory.Base();
                    string memberCardId = sku.Replace("CXK-", "");
                    TMEMBERCARDEntity membercard = db.IQueryable<TMEMBERCARDEntity>().Where(p => p.GUID == memberCardId).SingleOrDefault();
                    if (membercard != null)
                    {
                        if (membercard.IsUse == CodeConst.IsUse.NO)
                        {
                            TMEMBEREntity member = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId).Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).FirstOrDefault();
                            if (member != null)
                            {
                                try
                                {
                                    db.BeginTrans();
                                    member.Money += membercard.MemberMoney;
                                    db.Update<TMEMBEREntity>(member, null);

                                    TMONEYDETAILEntity moneydetail = new TMONEYDETAILEntity();
                                    moneydetail.ID = Guid.NewGuid().ToString();
                                    moneydetail.FK_Member = member.GUID;
                                    moneydetail.MoneyChange = membercard.MemberMoney;
                                    moneydetail.Remark = "会员卡金额充值";
                                    moneydetail.Creater = openId;
                                    moneydetail.CreateTime = DateTime.Now;
                                    moneydetail.organizeid = member.organizeid;
                                    db.Insert<TMONEYDETAILEntity>(moneydetail);

                                    membercard.IsUse = CodeConst.IsUse.YES;
                                    membercard.FK_AppId = guid;
                                    membercard.FK_OpenId = openId;
                                    membercard.Modifyer = openId;
                                    membercard.ModifyTime = DateTime.Now;
                                    db.Update<TMEMBERCARDEntity>(membercard, null);

                                    result.IsOK = true;
                                    result.msg = "充值成功！";

                                    db.Commit();
                                }
                                catch (Exception ex)
                                {
                                    db.Rollback();
                                    throw new Exception(ex.ToString());
                                }
                            }
                            else
                            {
                                result.msg = "请注册会员后再使用！";
                            }
                        }
                        else
                        {
                            result.msg = "该储蓄卡已使用！";
                        }
                    }
                    else
                    {
                        result.msg = "该储蓄卡无效！";
                    }
                }
                else
                {
                    Data.IDatabase db = DbFactory.Base();
                    string productGUID = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.ProductSKU == sku && p.IsDel == CodeConst.IsDel.NO)
.Join(db.IQueryable<T_PRODUCTPRICEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.IsSale == CodeConst.IsSale.IsShelves), m => m.FK_Product, n => n.GUID, (m, n) => n.GUID).FirstOrDefault();
                    if (productGUID != null)
                    {
                        result.Data = productGUID;
                        result.IsOK = true;
                    }
                    else
                    {
                        result.msg = "该商品不存在！";
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取优惠券信息
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public string GetCardInfo(string openId, string cardId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                T_CARDEntity card = db.IQueryable<T_CARDEntity>().Where(p => p.CardId == cardId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                TUSERCARDEntity usercard = db.IQueryable<TUSERCARDEntity>().Where(p => p.FK_OpenId == openId && p.IsDel == CodeConst.IsDel.NO && p.FK_CardID == cardId && p.IsUse == CodeConst.CardUse.NOUSE).SingleOrDefault();
                if (card != null)
                {
                    result.Data = JSONHelper.WXObjectToJson(card);
                    result.msg = usercard == null ? "0" : "1";
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 用户领取优惠券
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public string UserGetCard(string openId, string cardId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TUSERCARDEntity card = db.IQueryable<TUSERCARDEntity>().Where(p => p.FK_CardID == cardId && p.IsDel == CodeConst.IsDel.NO && p.FK_OpenId == openId && p.IsUse == CodeConst.CardUse.NOUSE).FirstOrDefault();
                if (card != null)
                {
                    result.msg = "您已领取过该卡券！";
                    return JSONHelper.WXObjectToJson(result);
                }
                T_CARDEntity cardinfo = db.IQueryable<T_CARDEntity>().Where(p => p.CardId == cardId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (cardinfo != null)
                {
                    TUSERCARDEntity usercard = new TUSERCARDEntity();
                    usercard.GUID = Guid.NewGuid().ToString();
                    usercard.FK_AppId = cardinfo.FK_AppId;
                    usercard.FK_OpenId = openId;
                    usercard.FK_CardID = cardinfo.CardId;
                    usercard.CardType = cardinfo.CardType;
                    usercard.LeastCost = cardinfo.LeastCost ?? 0;
                    usercard.ReduceCost = cardinfo.ReduceCost ?? 0;
                    usercard.Explain = cardinfo.Explain;
                    usercard.ValidDate = DateTime.Now.AddDays(cardinfo.ValidDate ?? 0);
                    usercard.IsUse = CodeConst.CardUse.NOUSE;
                    usercard.IsDel = CodeConst.IsDel.NO;
                    usercard.Creater = openId;
                    usercard.CreateTime = DateTime.Now;
                    usercard.Modifyer = openId;
                    usercard.ModifyTime = DateTime.Now;
                    usercard.organizeid = cardinfo.organizeid;
                    db.Insert<TUSERCARDEntity>(usercard);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 会员金额
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string GetMemberMoney(string openId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TMEMBEREntity member = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId).Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).FirstOrDefault();
                if (member != null)
                {
                    result.Data = member.Money.ToString();
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        private int PageSize = 10;

        /// <summary>
        /// 用户积分详情
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public string GetUserIntegralDetail(string openId, int pageIndex)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<TINTEGRALEntity> list = db.IQueryable<TINTEGRALEntity>().Where(p => p.FK_Openid == openId).Select(p => new TINTEGRALEntity
                {
                    IntegralRemark = p.IntegralRemark,
                    Integral = p.Integral,
                    CreateTime = p.CreateTime,
                    SurplusIntegral = p.SurplusIntegral
                }).OrderByDescending(p => p.CreateTime).Skip(pageIndex * PageSize).Take(PageSize).ToList();

                foreach (TINTEGRALEntity detail in list)
                {
                    detail.CreateDateStr = detail.CreateTime.Value.ToString("yyyy-MM-dd HH:mm:ss");
                }

                result.Data = JSONHelper.WXObjectToJson(list);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 分享积分
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="appguid"></param>
        /// <param name="sharetype"></param>
        /// <returns></returns>
        public string ShareApp(string openid, string appguid, int sharetype)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openid && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();

                if (user != null)
                {
                    if (CanGetIntegral(openid, sharetype))
                    {
                        user.Integral += sharetype.Equals(CodeConst.IntegralType.SHAREGOODS) ? CodeConst.Integral.SHAREGOODS : CodeConst.Integral.SHAREORDER;

                        TORDERService tORDERService = new TORDERService();
                        TINTEGRALEntity tintegral = tORDERService.IntegralRecord(openid, appguid, sharetype.Equals(CodeConst.IntegralType.SHAREGOODS) ? CodeConst.Integral.SHAREGOODS : CodeConst.Integral.SHAREORDER,
                            user.Integral.Value, sharetype, sharetype.Equals(CodeConst.IntegralType.SHAREGOODS) ? CodeConst.IntegralRemark.SHAREGOODS : CodeConst.IntegralRemark.SHAREORDER);

                        db.Insert<TINTEGRALEntity>(tintegral);
                        db.Update<TUSEREntity>(user, null);
                    }
                }
                db.Commit();
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public string GetMemberInfo(string openid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                MemberInfo memberInfo = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openid).Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => new MemberInfo
                {
                    UserName = n.UserName,
                    Money = n.Money ?? 0,
                    CreateTime = n.CreateTime,
                    Birthday = n.Birthday,
                    Mobile = n.Mobile,
                    QQNumber = n.QQNumber,
                    Profession = n.Profession,
                    Adress = n.Adress,
                    AvatarUrl = m.AvatarUrl,
                    Remark = n.Remark,
                    eventdraw = m.EventDraw ?? 0,
                    integral = m.Integral ?? 0
                }).SingleOrDefault();

                if (memberInfo != null)
                {
                    memberInfo.CreateTimeStr = memberInfo.CreateTime.Value.ToString("yyyy-MM-dd");
                    memberInfo.BirthdayStr = memberInfo.Birthday.HasValue ? memberInfo.Birthday.Value.ToString("yyyy-MM-dd") : "";
                    result.Data = JSONHelper.WXObjectToJson(memberInfo);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                result.msg = ex.Message;
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public string GetMemberMoneyDetail(string openId, int pageIndex)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<MoneyDetail> list = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId).Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n)
                    .Join(db.IQueryable<TMONEYDETAILEntity>(), x => x.GUID, y => y.FK_Member, (x, y) => new MoneyDetail
                    {
                        MoneyChange = y.MoneyChange ?? 0,
                        CreateTime = y.CreateTime.Value
                    }).OrderByDescending(p => p.CreateTime).Skip(pageIndex * PageSize).Take(PageSize).ToList();

                foreach (MoneyDetail detail in list)
                {
                    detail.CreateDateStr = detail.CreateTime.ToString("yyyy-MM-dd HH:mm:ss");
                }

                result.Data = JSONHelper.WXObjectToJson(list);
                result.IsOK = true;

            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }


        private bool CanGetIntegral(string openid, int type)
        {
            bool isresult = false;
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TINTEGRALEntity integral = db.IQueryable<TINTEGRALEntity>().Where(p => p.FK_Openid == openid && p.IntegralType == type).OrderByDescending(p => p.CreateTime).SingleOrDefault();

                if (integral != null)
                {
                    if (integral.CreateTime.Value.Date < DateTime.Now.Date)
                    {
                        isresult = true;
                    }
                }
                else
                {
                    isresult = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return isresult;
        }

        private bool UserCanSignIn(string openid, string appguid)
        {
            bool isresult = false;
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openid && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();

                if (user != null)
                {
                    if (user.SignInDate == null || user.SignInDate.Value.Date < DateTime.Now.Date)
                    {
                        isresult = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return isresult;
        }
    }
}
