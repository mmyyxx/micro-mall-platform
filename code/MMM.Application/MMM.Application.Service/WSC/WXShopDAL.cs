﻿using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.ShoppingMall;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Service.Cache;
using MMM.Application.Service.Order;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace MMM.Application.Service.WSC
{
    public class WXShopDAL
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        private string[] statuslist = new string[] { "全部", "待付款", "待发货", "待收货", "已完成", "订单失败", "订单取消" };

        private const int DeliveryTime = 15;

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="shoplist"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetShopItemList(string shoplist, String organizeid)
        {
            List<ShopItem> list = new List<ShopItem>();
            try
            {
                Data.IDatabase db = DbFactory.Base();

                List<string> codelist = JSONHelper.WXDecodeObject<List<string>>(shoplist);
                IList<T_PRODUCTCLASSEntity> productClassList = new ProductClassCache().GetCacheList().Where(x => codelist.Contains(x.GUID)).ToList();

                IList<String> skus = productClassList.Select(t => t.ProductSKU).ToList();
                IList<String> productids = productClassList.Select(t => t.FK_Product).ToList();

                IList<VSTOCKEntity> vstocklist = db.IQueryable<VSTOCKEntity>().Where(t => skus.Contains(t.SKU)).ToList();
                IList<VPRODUCTEntity> vproductList = db.IQueryable<VPRODUCTEntity>().Where(p => p.IsSale == CodeConst.IsSale.IsShelves && productids.Contains(p.ProductGUID)).ToList();

                String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];

                IList<TMODELPRODUCTFruitsEntity> fruitsEntities = db.IQueryable<TMODELPRODUCTFruitsEntity>().Where(t => productids.Contains(t.ImageNavigate1) && t.time1.HasValue && t.IsDel == 0).ToList();

                IList<TMODELPRODUCTEntity> tMODELPRODUCTEntities = db.IQueryable<TMODELFUNCTIONEntity>().Where(t => t.GUID == "3928a1785517460183a4d531530fdd3b")
                    .Join(db.IQueryable<TMODELPRODUCTEntity>().Where(t => productids.Contains(t.ImageNavigate1)), m => m.GUID, n => n.FK_ModelFunction, (m, n) => n).ToList();

                for (int m = 0; m < codelist.Count; m++)
                {
                    string guid = codelist[m];
                    ShopItem item = productClassList.Where(p => p.GUID == guid).Join(vproductList, x => x.FK_Product, y => y.ProductGUID, (x, y) => new ShopItem
                    {
                        isCheck = false,
                        ItemCode = x.GUID,
                        ImageURL = y.ImageURL,
                        ProductName = y.ProductName,
                        ProductDescribe = y.ProductDescribe,
                        ProductUnit = y.ProductUnit,
                        ProductPrice = x.ClassPrice ?? 0,
                        OldPrice = x.ClassOldPrice ?? 0,
                        MinCount = y.PackingSpecification,
                        FK_Product = x.FK_Product
                    }).SingleOrDefault();
                    if (item == null)
                    {
                        continue;
                    }
                    if (!String.IsNullOrEmpty(item.ImageURL) && !item.ImageURL.StartsWith("http:") && !item.ImageURL.StartsWith("https:"))
                    {
                        item.ImageURL = ImgUrl + item.ImageURL;
                    }
                    item.ProductClassList = new List<string>();
                    string classguid = item.ItemCode;
                    decimal? productStock = productClassList.Where(p => p.GUID == classguid).Join(vstocklist, x => x.ProductSKU, y => y.SKU, (x, y) => y.Stock).FirstOrDefault();
                    if (CodeConst.IsUseStock)
                    {
                        item.ProductStock = productStock == null ? Decimal.Zero : productStock.Value;
                    }
                    else
                    {
                        item.ProductStock = 1000;
                    }

                    while (classguid != null)
                    {
                        T_PRODUCTCLASSEntity productclass = db.IQueryable<T_PRODUCTCLASSEntity>(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == classguid).SingleOrDefault();
                        string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                        item.ProductClassList.Insert(0, classstr);
                        classguid = productclass.FK_ParentClass;
                    }

                    TMODELPRODUCTFruitsEntity fruitsEntity = fruitsEntities.FirstOrDefault(t => t.ImageNavigate1 == item.FK_Product);
                    if (fruitsEntity != null && fruitsEntity.time1.HasValue && fruitsEntity.time2.HasValue)//限时秒杀商品
                    {
                        if (fruitsEntity.num1 > fruitsEntity.num2)
                        {
                            DateTime now = DateTime.Now;
                            if (now > fruitsEntity.time2.Value && now < fruitsEntity.time1.Value)
                            {
                                item.ProductPrice = fruitsEntity.Price1 ?? 0;
                                item.MaxCount = 1;
                            }
                        }
                    }
                    TMODELPRODUCTEntity product = tMODELPRODUCTEntities.FirstOrDefault(t => t.ImageNavigate1 == item.FK_Product);
                    if (product != null)
                    {
                        item.MaxCount = 1;
                    }
                    else item.MaxCount = 10000;

                    if (item != null)
                    {
                        list.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(list);
        }

        /// <summary>
        /// 获取商品
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="islimit">是否是限时商品</param>
        /// <returns></returns>
        public string GetProductInfoByGUID(string guid, String islimit)
        {
            ResultData result = new ResultData();
            try
            {
                String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];

                Data.IDatabase db = DbFactory.Base();
                ProductInfo productInfo = new ProductPriceCache().GetCacheList().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO).Select(p => new ProductInfo
                {
                    ProductName = p.Name,
                    ProductDescribe = p.ProductParameter == null ? "0" : p.ProductParameter,
                    ProductUnit = p.ProductUnit,
                    ProductMainPicture = p.ProductMainPicture,
                    PackingSpecification = p.PackingSpecification == null ? 0 : p.PackingSpecification.Value,
                    DiscountPrice = p.DiscountPrice == null ? 0 : p.DiscountPrice.Value,
                    RetailPrice = p.RetailPrice == null ? 0 : p.RetailPrice.Value,
                    IsLimitTimeProduct = p.IsLimitTimeProduct == null ? 1 : p.IsLimitTimeProduct.Value,
                    LimitEndTime = p.LimitEndTime,
                    IsGroupProduct = p.IsGroupProduct == null ? 1 : p.IsGroupProduct.Value,
                    GroupPeople = p.GroupPeople == null ? 0 : p.GroupPeople.Value,
                    Remark = p.Remark == null ? "" : p.Remark,
                    FK_AppId = p.FK_AppId,
                    ProductKey = p.ProductKey
                }).FirstOrDefault();
                if (productInfo != null)
                {
                    decimal? productSale = db.IQueryable<VPRODUCTSALEEntity>().Where(p => p.GUID == guid).Select(p => p.ProductSale).SingleOrDefault();
                    if (productSale.HasValue)
                    {
                        productInfo.ProductDescribe = (Int32.Parse(productInfo.ProductDescribe) + Convert.ToInt32(productSale.Value)).ToString();
                    }
                    IList<T_PICTUREEntity> pictureList = db.IQueryable<T_PICTUREEntity>().Where(p => p.FK_GUID == guid && p.IsDel == CodeConst.IsDel.NO && (p.Type == CodeConst.PictureType.ProductPicture || p.Type == CodeConst.PictureType.ProductContent)).ToList();
                    productInfo.PictureList = pictureList.Where(p => p.Type == CodeConst.PictureType.ProductPicture).OrderBy(p => p.ImageName).Select(p => p.ImageURL).ToList();
                    productInfo.DetailPictureList = pictureList.Where(p => p.Type == CodeConst.PictureType.ProductContent).OrderBy(p => p.ImageName).Select(p => p.ImageURL).ToList();

                    for (int i = 0, j = productInfo.PictureList.Count; i < j; i++)
                    {
                        if (!String.IsNullOrEmpty(productInfo.PictureList[i]) && !productInfo.PictureList[i].StartsWith("http:") && !productInfo.PictureList[i].StartsWith("https:"))
                        {
                            productInfo.PictureList[i] = ImgUrl + productInfo.PictureList[i];
                        }
                    }
                    for (int i = 0, j = productInfo.DetailPictureList.Count; i < j; i++)
                    {
                        if (!String.IsNullOrEmpty(productInfo.DetailPictureList[i]) && !productInfo.DetailPictureList[i].StartsWith("http:") && !productInfo.DetailPictureList[i].StartsWith("https:"))
                        {
                            productInfo.DetailPictureList[i] = ImgUrl + productInfo.DetailPictureList[i];
                        }
                    }

                    List<T_PRODUCTCLASSEntity> classlist = new ProductClassCache().GetCacheList().Where(p => p.FK_Product == guid && p.IsDel == CodeConst.IsDel.NO).ToList();
                    List<string> list = classlist.OrderBy(p => p.CreateTime).Select(p => p.ProductClass).Distinct().ToList();

                    productInfo.ProductClassList = new List<ProductClass>();

                    Dictionary<string, List<string>> dic = new Dictionary<string, List<string>>();

                    IList<String> productSKUs = classlist.Select(t => t.ProductSKU).ToList();
                    IList<VSTOCKEntity> vstockList = null;
                    if (CodeConst.IsUseStock)
                        vstockList = db.IQueryable<VSTOCKEntity>().Where(t => productSKUs.Contains(t.SKU)).ToList();
                    foreach (string str in list)
                    {
                        ProductClass pc = new ProductClass();
                        pc.ClassType = str;
                        pc.ClassItemList = GetClassItemList(vstockList, classlist, ref dic, str);
                        productInfo.ProductClassList.Add(pc);
                    }

                    productInfo.ClassDic = new Dictionary<string, List<string>>();

                    foreach (string key in dic.Keys)
                    {
                        if (dic[key].Count > 0)
                        {
                            productInfo.ClassDic[key] = dic[key];
                        }
                    }

                    //限时商品
                    //if (islimit == "0")//获取限时商品数据
                    //{
                    productInfo.LimitEndTime = null;
                    TMODELPRODUCTFruitsEntity fruitsEntity = db.IQueryable<TMODELPRODUCTFruitsEntity>().FirstOrDefault(t => t.ImageNavigate1 == guid && t.IsDel == 0 && t.time1.HasValue);
                    if (fruitsEntity != null)
                    {
                        if (fruitsEntity.time1.HasValue && fruitsEntity.time2.HasValue)//限时秒杀商品
                        {
                            if (fruitsEntity.num1 > fruitsEntity.num2)
                            {
                                DateTime now = DateTime.Now;
                                if (now > fruitsEntity.time2.Value && now < fruitsEntity.time1.Value)
                                {
                                    productInfo.LimitEndTime = fruitsEntity.time1;
                                    productInfo.Remark = (fruitsEntity.num1 ?? 0).ToString();
                                    productInfo.Remark2 = (fruitsEntity.num2 ?? 0).ToString();
                                    productInfo.DiscountPrice = fruitsEntity.Price1 ?? 0;
                                }
                            }
                        }
                    }
                    //}

                    if (!String.IsNullOrEmpty(productInfo.ProductMainPicture) && !productInfo.ProductMainPicture.StartsWith("http:") && !productInfo.ProductMainPicture.StartsWith("https:"))
                    {
                        productInfo.ProductMainPicture = ImgUrl + productInfo.ProductMainPicture;
                    }

                    result.Data = JSONHelper.WXObjectToJson(productInfo);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取用户优惠券
        /// </summary>
        /// <param name="openId"></param>
        /// <param name="totalprice"></param>
        /// <param name="productlist"></param>
        /// <returns></returns>
        public string GetUserCanUseCard(string openId, decimal totalprice, string productlist)
        {
            ResultData result = new ResultData();
            result.msg = "0";
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<TUSERCARDEntity> list = db.IQueryable<TUSERCARDEntity>().Where(p => p.FK_OpenId == openId && p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.CardUse.NOUSE &&
                p.LeastCost <= totalprice && (p.CardType == CodeConst.CardType.XRQ || p.CardType == CodeConst.CardType.DJQ)).OrderByDescending(p => p.ReduceCost).ToList();

                bool IsMJ = true;
                string mjprice = "";
                if (!String.IsNullOrEmpty(productlist))
                {
                    List<List<string>> pdtlist = JSONHelper.WXDecodeObject<List<List<string>>>(productlist);
                    IList<String> classguids = new List<String>();
                    foreach (List<string> productclass in pdtlist)
                    {
                        classguids.Add(productclass[0]);
                    }
                    IList<T_PRODUCTCLASSEntity> productclassList = new ProductClassCache().GetCacheList().Where(t => classguids.Contains(t.GUID) && t.IsDel == CodeConst.IsDel.NO).ToList();
                    //查询产品分类信息
                    IList<String> productids = productclassList.Select(t => t.FK_Product).ToList();
                    IList<T_CATEGORY_PRODUCTEntity> categoryproductList = db.IQueryable<T_CATEGORY_PRODUCTEntity>().Where(t => productids.Contains(t.FK_Product)).ToList();
                    IList<String> categroyids = categoryproductList.Select(t => t.FK_Category).ToList();
                    IList<T_PRODUCTCATEGORYEntity> productcategoryList = db.IQueryable<T_PRODUCTCATEGORYEntity>().Where(p => p.Remark != null && categroyids.Contains(p.GUID) && p.IsDel == CodeConst.IsDel.NO).ToList();
                    foreach (List<string> productclass in pdtlist)
                    {
                        string classguid = productclass[0];
                        T_PRODUCTCLASSEntity stockclass = productclassList.Where(p => p.GUID == classguid).SingleOrDefault();

                        if (stockclass == null)
                        {
                            continue;
                        }

                        List<T_PRODUCTCATEGORYEntity> categorylist = categoryproductList.Where(p => p.FK_Product == stockclass.FK_Product)
                             .Join(productcategoryList, m => m.FK_Category, n => n.GUID, (m, n) => n).ToList();
                        if (categorylist.Where(p => p.Remark.Contains("不能使用优惠券")).Count() > 0 && list.Count > 0)
                        {
                            result.msg = "此订单包含活动特价商品，无法使用优惠券！";
                            return JSONHelper.WXObjectToJson(result);
                        }

                        if (IsMJ)
                        {
                            if (categorylist.Where(p => p.GUID == "ef2a36879c644e9eadc7a0bfe7569216").Count() == 1 && totalprice >= 38 && (String.IsNullOrEmpty(mjprice) || mjprice == "3"))
                            {
                                mjprice = "3";
                            }
                            else if (categorylist.Where(p => p.GUID == "1e1a045e621b43b592e6a0287f5c60ea").Count() == 1 && totalprice >= 68 && (String.IsNullOrEmpty(mjprice) || mjprice == "20"))
                            {
                                mjprice = "20";
                            }
                            else
                            {
                                IsMJ = false;
                            }
                        }
                    }

                }
                List<TUSERCARDEntity> resultlist = new List<TUSERCARDEntity>();
                db.BeginTrans();
                foreach (TUSERCARDEntity usercard in list)
                {
                    if (usercard.ValidDate < DateTime.Now.Date)
                    {
                        usercard.IsUse = CodeConst.CardUse.ISOUTTIME;
                        db.Update<TUSERCARDEntity>(usercard, null);
                        continue;
                    }
                    TUSERCARDEntity newusercard = new TUSERCARDEntity();
                    Str.UpdateObject(usercard, newusercard);
                    newusercard.ValidDateStr = usercard.ValidDate.Value.ToString("yyyy.MM.dd");
                    newusercard.CreatetimeStr = usercard.CreateTime.Value.ToString("yyyy.MM.dd");
                    newusercard.day = (usercard.ValidDate.Value - usercard.CreateTime.Value).TotalDays.ToString();
                    resultlist.Add(newusercard);
                }
                db.Commit();

                result.Data = JSONHelper.WXObjectToJson(resultlist);
                if (IsMJ)
                {
                    result.msg = mjprice;
                }
                result.IsOK = true;

            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取售后订单
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public string GetAfterSaleOrder(string openid, string organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                IList<TORDEREntity> orderList = db.IQueryable<TORDEREntity>().Where(p => p.FK_Openid == openid && p.OrderStatus == CodeConst.OrderStatus.COMPLETE && p.IsDel == CodeConst.IsDel.NO).ToList();
                IList<String> orderids = orderList.Select(t => t.GUID).ToList();
                IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && orderids.Contains(p.FK_Order)).ToList();

                //获取已经申请售后的订单
                IList<T_OrderAfterSaleEntity> afterList = db.IQueryable<T_OrderAfterSaleEntity>().Where(t => orderids.Contains(t.orderid) && t.status == CodeConst.OrderDetailStatus.AFTERSALE.ToString()).ToList();

                //分类
                IList<T_PRODUCTCLASSEntity> productClassList = new Cache.ProductClassCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == organizeid).ToList();
                IList<T_PRODUCTPRICEEntity> productPriceList = new Cache.ProductPriceCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == organizeid).ToList();
                //获取售后的订单
                List<OrderModel> list = orderList.Join(orderdetailList, m => m.GUID, n => n.FK_Order, (m, n) => m).Distinct()
                    .Select(p => new OrderModel
                    {
                        GUID = p.GUID,
                        OrderNumber = p.OrderNumber,
                        IsGroupOrder = p.IsGroupOrder == CodeConst.IsUse.YES,
                        GroupNumber = p.GroupNumber,
                        GroupResult = p.GroupResult == CodeConst.IsUse.YES,
                        OrderStatus = p.OrderStatus ?? 0,
                        DeliveryMode = p.DeliveryMode ?? 0,
                        Addressee = p.DeliveryMode == CodeConst.DeliveryMode.STORE ? p.FK_PlanStore : p.FK_Adress,
                        OrderPrice = p.OrderPrice ?? 0,
                        CreateTime = p.CreateTime,
                        RegimentTime = p.RegimentTime,
                        DeliverTime = p.DeliverTime,
                        CompleteTime = p.CompleteTime,
                        Remark = p.Remark,
                        ServiceMobile = p.FK_AppId
                    }).ToList();
                String ServiceMobile = db.IQueryable<TWXAPPEntity>().SingleOrDefault(t => t.GUID == CodeConst.WxAppKEY).Mobile;
                foreach (OrderModel order in list)
                {
                    order.type = "1";
                    order.ServiceMobile = ServiceMobile;
                    order.OrderProductList = orderdetailList.Where(p => p.FK_Order == order.GUID).
                        Join(productClassList, m => m.FK_ProductClass, n => n.GUID, (m, n) => new { m, n }).
                        Join(productPriceList, x => x.n.FK_Product, y => y.GUID, (x, y) => new ProductInfo
                        {
                            OrderDetailStatus = x.m.Status ?? 0,
                            ProductName = y.Name,
                            ProductParam = x.m.Remark,
                            ProductUnit = y.ProductUnit,
                            ProductMainPicture = y.ProductMainPicture,
                            DiscountPrice = x.m.Price ?? 0,
                            RetailPrice = x.m.OldPrice ?? 0,
                            ProductCount = x.m.Count ?? 0,
                            IsGroupProduct = y.IsGroupProduct.Value,
                            GroupPeople = y.GroupPeople.Value,
                            ProductClass = x.n.GUID,
                            type = y.FK_AppId == "B43E1A5E8A5D4386AE5B6555135BE513" ? "1" : "2"
                        }).ToList();

                    foreach (ProductInfo item in order.OrderProductList)
                    {
                        string classguid = item.ProductClass;
                        item.ProductDescribe = String.Empty;
                        while (classguid != null)
                        {
                            if (item.ProductDescribe != String.Empty)
                            {
                                item.ProductDescribe += Environment.NewLine;
                            }
                            T_PRODUCTCLASSEntity productclass = productClassList.Where(p => p.GUID == classguid).SingleOrDefault();
                            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                            item.ProductDescribe += classstr;
                            classguid = productclass.FK_ParentClass;
                        }
                        if (item.type == "2")
                        {
                            order.type = "2";
                        }
                    }

                    T_OrderAfterSaleEntity t_OrderAfter = afterList.FirstOrDefault(t => t.orderid == order.GUID);
                    if (t_OrderAfter != null)
                    {
                        order.hasAfter = true;
                    }
                    else
                        order.hasAfter = false;

                }
                if (list.Count > 0)
                {
                    result.Data = JSONHelper.WXObjectToJson(list);
                    result.IsOK = true;
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="wxopenId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string DeleteOrder(string wxopenId, string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO && p.OrderStatus > CodeConst.OrderStatus.COLLECT).SingleOrDefault();
                if (order != null)
                {
                    order.IsDel = CodeConst.IsDel.YES;
                    order.Deleter = wxopenId;
                    order.DeleteTime = DateTime.Now;
                    db.Update<TORDEREntity>(order, null);
                    result.IsOK = true;
                }
                else
                {
                    result.msg = "该订单无法删除！";
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 确认订单
        /// </summary>
        /// <param name="wxopenId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string ConfirmOrder(string wxopenId, string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO && p.OrderStatus == CodeConst.OrderStatus.COLLECT).SingleOrDefault();
                if (order != null)
                {
                    int integral = Int32.Parse((order.OrderPrice / CodeConst.Integral.PAYORDER).ToString());

                    TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FK_Openid && p.IsDel == CodeConst.IsDel.NO)
                            .Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();
                    if (user != null)
                    {
                        user.Integral += integral;
                        TORDERService tORDERService = new TORDERService();

                        TINTEGRALEntity tintegral = tORDERService.IntegralRecord(order.FK_Openid, order.FK_AppId, integral, user.Integral.Value, CodeConst.IntegralType.PAYORDER, CodeConst.IntegralRemark.PAYORDER);
                        db.Insert<TINTEGRALEntity>(tintegral);
                        db.Update<TUSEREntity>(user, null);
                    }

                    order.OrderStatus = CodeConst.OrderStatus.COMPLETE;
                    order.Completer = wxopenId;
                    order.CompleteTime = DateTime.Now;
                    order.Modifyer = wxopenId;
                    order.ModifyTime = DateTime.Now;
                    db.Update<TORDEREntity>(order, null);
                    result.IsOK = true;
                }
                else
                {
                    result.msg = "该订单无法确认！";
                }
                if (CodeConst.IsUseStock && result.IsOK)
                {
                    IList<TORDERSTOCKEntity> list = db.IQueryable<TORDERSTOCKEntity>().Where(p => p.FK_Order == guid && p.Status == CodeConst.OrderStockStatus.YTC).ToList();
                    IList<String> fk_Stocks = list.Select(t => t.FK_Stock).ToList();
                    IList<TSTOCKEntity> stocklist = db.IQueryable<TSTOCKEntity>().Where(t => fk_Stocks.Contains(t.GUID)).ToList();
                    foreach (TORDERSTOCKEntity orderstock in list)
                    {
                        orderstock.Status = CodeConst.OrderStockStatus.YWC;
                        TSTOCKEntity stockInfo = stocklist.Where(p => p.GUID == orderstock.FK_Stock).SingleOrDefault();

                        if (stockInfo != null)
                        {
                            stockInfo.ActualStock -= orderstock.StockCount;

                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                            stockdetail.ID = Guid.NewGuid().ToString();
                            stockdetail.FK_Stock = stockInfo.GUID;
                            stockdetail.ActualStock = -orderstock.StockCount;
                            stockdetail.Type = CodeConst.StockDetailType.CK;
                            stockdetail.Remark = "小程序订单完成";
                            stockdetail.Creater = wxopenId;
                            stockdetail.CreateTime = DateTime.Now;
                            stockdetail.organizeid = order.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdetail);

                            result.IsOK = true;
                        }
                    }

                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 售后申请
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string SaveAfterSale(T_OrderAfterSaleEntity entity)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();

                int i = db.IQueryable<T_OrderAfterSaleEntity>().Count(t => t.orderid == entity.orderid && t.status == CodeConst.OrderDetailStatus.AFTERSALE.ToString());
                if (i > 0)
                {
                    result.msg = "该订单已经在售后中";
                    return JSONHelper.WXObjectToJson(result);
                }

                TORDEREntity order = db.IQueryable<TORDEREntity>().FirstOrDefault(p => p.GUID == entity.orderid && p.FK_Openid == entity.adduser);
                if (order == null)
                {
                    result.msg = "订单不存在";
                    return JSONHelper.WXObjectToJson(result);
                }
                if (order.OrderStatus == CodeConst.OrderStatus.PAYMENT || order.OrderStatus == CodeConst.OrderStatus.FAILED)
                {
                    result.msg = "订单状态不正确";
                    return JSONHelper.WXObjectToJson(result);
                }
                if (String.IsNullOrEmpty(entity.type))
                {
                    result.msg = "请选择售后类型";
                    return JSONHelper.WXObjectToJson(result);
                }
                if (entity.type == "4")//退款
                {
                    //if (String.IsNullOrEmpty(entity.remark))
                    //{
                    //    result.msg = "请输入退款说明";
                    //    return JSONHelper.WXObjectToJson(result);
                    //}
                }
                else
                {
                    if (String.IsNullOrEmpty(entity.storeid))
                    {
                        result.msg = "请选择售后门店";
                        return JSONHelper.WXObjectToJson(result);
                    }
                }

                IList<TORDERDETAILEntity> orderdetails = db.IQueryable<TORDERDETAILEntity>().Where(t => t.FK_Order == entity.orderid && t.IsDel == 0).ToList();

                entity.status = (CodeConst.OrderDetailStatus.AFTERSALE).ToString();
                entity.addtime = DateTime.Now;
                db.Insert<T_OrderAfterSaleEntity>(entity);
                foreach (TORDERDETAILEntity detail in orderdetails)
                {
                    detail.Status = CodeConst.OrderDetailStatus.AFTERSALE;
                    if (entity.type == "4")
                        detail.Remark = entity.remark;
                    else if (entity.type == "1")
                        detail.Remark = "退货";
                    else if (entity.type == "2")
                        detail.Remark = "换货";
                    else if (entity.type == "3")
                        detail.Remark = "维修";
                    db.Update<TORDERDETAILEntity>(detail, null);
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            result.IsOK = true;
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="wxopenId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string CancellationOrder(string wxopenId, string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity order = null;

                order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO && p.OrderStatus < CodeConst.OrderStatus.COLLECT).SingleOrDefault();
                if (order != null)
                {
                    try
                    {

                        db.BeginTrans();
                        TUSERCARDEntity usercard = db.IQueryable<TUSERCARDEntity>().Where(p => p.GUID == order.Coupon && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                        if (usercard != null)
                        {
                            TORDEREntity neworder = db.IQueryable<TORDEREntity>().Where(p => p.GUID != order.GUID && p.Coupon == usercard.GUID && p.FK_Openid == wxopenId && p.OrderStatus > CodeConst.OrderStatus.PAYMENT && p.OrderStatus < CodeConst.OrderStatus.FAILED).FirstOrDefault();
                            if (neworder == null)
                            {
                                usercard.IsUse = CodeConst.CardUse.NOUSE;
                                db.Update<TUSERCARDEntity>(usercard, null);
                            }
                        }
                        order.OrderStatus = CodeConst.OrderStatus.CANCEL;
                        order.Modifyer = wxopenId;
                        order.ModifyTime = DateTime.Now;
                        db.Update<TORDEREntity>(order, null);
                        db.Commit();
                        result.IsOK = true;
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new Exception(e.ToString());
                    }
                }
                else
                {
                    result.msg = "该订单无法取消！";
                }

                if (order != null)
                {
                    OrderStockReserve(order);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        private void OrderStockReserve(TORDEREntity order)
        {
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                //秒杀商品恢复
                IList<TORDERDETAILEntity> oRDERDETAILList = db.IQueryable<TORDERDETAILEntity>().Where(t => order.GUID == t.FK_Order && t.IsDel == 0).ToList();
                IList<String> productclassids = oRDERDETAILList.Select(t => t.FK_ProductClass).ToList();
                IList<T_PRODUCTCLASSEntity> t_PRODUCTCLASSList = new ProductClassCache().GetCacheList().Where(t => t.IsDel == 0 && productclassids.Contains(t.GUID)).ToList();
                IList<T_PRODUCTPRICEEntity> t_PRODUCTPriceList = new ProductPriceCache().GetCacheList();

                foreach (TORDERDETAILEntity tORDERDETAIL in oRDERDETAILList)
                {
                    T_PRODUCTCLASSEntity pdtclass = t_PRODUCTCLASSList.FirstOrDefault(t => t.GUID == tORDERDETAIL.FK_ProductClass);
                    T_PRODUCTPRICEEntity price = t_PRODUCTPriceList.FirstOrDefault(t => t.GUID == pdtclass.FK_Product);
                    if (price != null)
                    {
                        TMODELPRODUCTFruitsEntity fruitsEntity = db.IQueryable<TMODELPRODUCTFruitsEntity>().FirstOrDefault(t => t.ImageNavigate1 == price.GUID && t.time1.HasValue && t.IsDel == 0);
                        fruitsEntity.num2 = fruitsEntity.num2 - Convert.ToInt32(tORDERDETAIL.Count);
                        db.Update<TMODELPRODUCTFruitsEntity>(fruitsEntity, null);
                    }
                }

                if (CodeConst.IsUseStock)
                {

                    bool isStockOut = true;
                    List<TORDERSTOCKEntity> list = db.IQueryable<TORDERSTOCKEntity>().Where(p => p.Status == CodeConst.OrderStockStatus.YTC && p.FK_Order == order.GUID).ToList();
                    if (list.Count == 0)
                    {
                        isStockOut = false;
                    }
                    else
                    {
                        IList<String> fk_Stocks = list.Select(t => t.FK_Stock).ToList();
                        IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(t => t.IsDel == 0 && fk_Stocks.Contains(t.GUID)).ToList();
                        IList<TSTOCKDEAILEntity> stockdetailList = new List<TSTOCKDEAILEntity>();
                        foreach (TORDERSTOCKEntity orderstockInfo in list)
                        {
                            orderstockInfo.Status = CodeConst.OrderStockStatus.YQX;
                            db.Update<TORDERSTOCKEntity>(orderstockInfo, null);

                            TSTOCKEntity stockInfo = stockList.Where(p => p.GUID == orderstockInfo.FK_Stock).SingleOrDefault();
                            if (stockInfo != null)
                            {
                                stockInfo.Stock += orderstockInfo.StockCount;
                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                stockdetail.ID = Guid.NewGuid().ToString();
                                stockdetail.FK_Stock = stockInfo.GUID;
                                stockdetail.Stock = orderstockInfo.StockCount;
                                stockdetail.Type = CodeConst.StockDetailType.RK;
                                stockdetail.Remark = String.Format("订单：{0}取消", order.OrderNumber);
                                stockdetail.Creater = "系统操作";
                                stockdetail.CreateTime = DateTime.Now;
                                stockdetail.organizeid = order.organizeid;
                                stockdetailList.Add(stockdetail);
                            }
                        }
                        db.Update<TSTOCKDEAILEntity>(stockdetailList, null);
                    }

                    if (!isStockOut)
                    {
                        Hashtable sqlListHashtable = new Hashtable();
                        var productclasslist = db.IQueryable<TORDERDETAILEntity>().Where(p => p.FK_Order == order.GUID).Join(db.IQueryable<T_PRODUCTCLASSEntity>(), m => m.FK_ProductClass, n => n.GUID, (m, n) => new
                        {
                            SKU = n.ProductSKU,
                            pdtCount = m.Count * n.ProductStock.Value
                        }).ToList();
                        int nctnumber = 1;

                        //库存信息
                        IList<String> productskus = productclasslist.Select(t => t.SKU).ToList();
                        IList<VSTOCKEntity> vstockList = db.IQueryable<VSTOCKEntity>().Where(t => productskus.Contains(t.SKU)).ToList();
                        productskus = vstockList.Select(t => t.ActualSKU).ToList();
                        IList<VPRODUCTCOMBEntity> vproductcombList = db.IQueryable<VPRODUCTCOMBEntity>().Where(t => productskus.Contains(t.SKU)).ToList();

                        foreach (var pdtclass in productclasslist)
                        {
                            VSTOCKEntity stockInfo = vstockList.Where(p => p.SKU == pdtclass.SKU).FirstOrDefault();
                            List<VPRODUCTCOMBEntity> comblist = vproductcombList.Where(p => p.SKU == stockInfo.ActualSKU).ToList();
                            if (comblist.Count > 0)
                            {
                                foreach (VPRODUCTCOMBEntity comb in comblist)
                                {
                                    sqlListHashtable = WSCSql.GetUpdateProductSql(comb.ChildSKU, (comb.ChildCount.Value * -pdtclass.pdtCount * stockInfo.PdtCount.Value) ?? 0, nctnumber++, sqlListHashtable);
                                }
                            }
                            else
                            {
                                sqlListHashtable = WSCSql.GetUpdateProductSql(stockInfo.ActualSKU, (-pdtclass.pdtCount * stockInfo.PdtCount.Value) ?? 0, nctnumber++, sqlListHashtable);
                            }
                        }
                        foreach (DictionaryEntry myDE in sqlListHashtable)
                        {
                            string cmdText = myDE.Key.ToString();
                            SqlParameter[] cmdParms = (SqlParameter[])myDE.Value;
                            db.ExecuteBySql(cmdText, cmdParms);
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }
        }

        /// <summary>
        /// 申请售后
        /// </summary>
        /// <param name="detailguid"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public string SetAfterSale(string detailguid, string remark)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDERDETAILEntity orderdetail = db.IQueryable<TORDERDETAILEntity>().Where(p => p.GUID == detailguid && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (orderdetail != null)
                {
                    orderdetail.Status = CodeConst.OrderDetailStatus.AFTERSALE;
                    orderdetail.Remark = remark;
                    orderdetail.ModifyTime = DateTime.Now;
                    db.Update<TORDERDETAILEntity>(orderdetail, null);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public string GetZCCount(string productguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                VORDERPRODUCTEntity item = db.IQueryable<VORDERPRODUCTEntity>().Where(p => p.GUID == productguid).SingleOrDefault();
                if (item != null)
                {
                    result.IsOK = true;
                    result.Data = item.ProductCount.ToString();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取团购订单
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="classguid"></param>
        /// <returns></returns>
        public string GetGroupOrder(string openid, string classguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDERService tORDERService = new TORDERService();
                tORDERService.DeleteTimeOutGroupOrder(openid);

                List<VGROUPORDEREntity> resultlist = db.IQueryable<VGROUPORDEREntity>().Where(p => p.FK_ProductClass == classguid && p.GroupRole == CodeConst.GroupRole.INITIATOR && p.OrderStatus == CodeConst.OrderStatus.DELIVER && p.GroupResult == CodeConst.IsUse.NO).ToList();

                if (resultlist.Count > 0)
                {
                    IList<String> groupNumbers = resultlist.Select(t => t.GroupNumber).ToList();
                    IList<TORDEREntity> orderList = db.IQueryable<TORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PayResult == CodeConst.IsUse.YES && groupNumbers.Contains(p.GroupNumber)).OrderBy(t => t.CreateTime).ToList();
                    IList<String> openids = orderList.Select(t => t.FK_Openid).ToList();
                    IList<TUSEREntity> userList = db.IQueryable<TUSEREntity>().Where(p => openids.Contains(p.Openid)).ToList();

                    var list = resultlist.Select(p => new
                    {
                        p.OrderGUID,
                        p.FK_ProductClass,
                        p.CreateTime,
                        p.OrderNumber,
                        p.FK_Openid,
                        p.GroupNumber,
                        p.OrderStatus,
                        p.OrderStatusStr,
                        p.ProductMainPicture,
                        p.Name,
                        p.ProductParameter,
                        p.ProductCount,
                        p.GroupPeople,
                        p.People,
                        p.ClassPrice,
                        p.ClassOldPrice,
                        p.ProductUnit,
                        p.LimitEndTime,
                        p.GroupResult,
                        p.GroupRole,
                        p.AvatarUrl,
                        GroupPeopleImg = GetGroupMember(orderList, userList, p.GroupNumber)
                    }).ToList();

                    result.Data = JSONHelper.WXObjectToJson(list);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取团购订单
        /// </summary>
        /// <param name="openid"></param>
        /// <returns></returns>
        public string GetMyGroupOrder(string openid, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDERService tORDERService = new TORDERService();
                tORDERService.SystemDeleteTimeOutOrder(openid);
                tORDERService.DeleteTimeOutGroupOrder(openid);

                List<VGROUPORDEREntity> resultlist = db.IQueryable<VGROUPORDEREntity>().Where(p => p.FK_Openid == openid).ToList();
                if (resultlist.Count > 0)
                {
                    IList<String> groupNumbers = resultlist.Select(t => t.GroupNumber).ToList();
                    IList<TORDEREntity> orderList = db.IQueryable<TORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PayResult == CodeConst.IsUse.YES && groupNumbers.Contains(p.GroupNumber)).OrderBy(t => t.CreateTime).ToList();
                    IList<String> openids = orderList.Select(t => t.FK_Openid).ToList();
                    IList<TUSEREntity> userList = db.IQueryable<TUSEREntity>().Where(p => openids.Contains(p.Openid)).ToList();
                    IList<T_PRODUCTCLASSEntity> productclassList = new ProductClassCache().GetCacheList().Where(t => t.IsDel == 0 && t.organizeid == organizeid).ToList();
                    var list = resultlist.Select(p => new
                    {
                        p.OrderGUID,
                        p.FK_ProductClass,
                        p.CreateTime,
                        p.OrderNumber,
                        p.FK_Openid,
                        p.GroupNumber,
                        p.OrderStatus,
                        p.OrderStatusStr,
                        p.ProductMainPicture,
                        p.Name,
                        ProductParameter = GetParameterStr(productclassList, p.FK_ProductClass, p.ProductCount),
                        p.ProductCount,
                        p.GroupPeople,
                        p.People,
                        p.ClassPrice,
                        p.ClassOldPrice,
                        p.ProductUnit,
                        p.LimitEndTime,
                        p.GroupResult,
                        p.GroupRole,
                        p.AvatarUrl,
                        GroupPeopleImg = GetGroupMember(orderList, userList, p.GroupNumber)
                    }).ToList();

                    result.Data = JSONHelper.WXObjectToJson(list);
                    result.IsOK = true;
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        private string GetGroupMember(IList<TORDEREntity> orderList, IList<TUSEREntity> userList, string groupNumber)
        {
            List<string> result = new List<string>();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                result = orderList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.PayResult == CodeConst.IsUse.YES && p.GroupNumber == groupNumber).
                    Join(userList, m => m.FK_Openid, n => n.Openid, (m, n) => new { m, n }).OrderBy(p => p.m.CreateTime).Take(10).Select(p => p.n.AvatarUrl).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取订单成员
        /// </summary>
        /// <param name="groupnumber"></param>
        /// <returns></returns>
        public string GetGroupMember(string groupnumber)
        {
            List<string> result = new List<string>();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                result = db.IQueryable<TORDEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PayResult == CodeConst.IsUse.YES && p.GroupNumber == groupnumber).
                    Join(db.IQueryable<TUSEREntity>(), m => m.FK_Openid, n => n.Openid, (m, n) => new { m, n }).OrderBy(p => p.m.CreateTime).Take(10).Select(p => p.n.AvatarUrl).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取订单详情
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetOrderDetail(string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity orderentity = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (orderentity != null)
                {
                    OrderModel order = new OrderModel
                    {
                        GUID = orderentity.GUID,
                        OrderNumber = orderentity.OrderNumber,
                        IsGroupOrder = orderentity.IsGroupOrder == CodeConst.IsUse.YES,
                        GroupNumber = orderentity.GroupNumber,
                        GroupResult = orderentity.GroupResult == CodeConst.IsUse.YES,
                        OrderStatus = orderentity.OrderStatus ?? 0,
                        DeliveryMode = orderentity.DeliveryMode ?? 0,
                        PlanPickUpTime = orderentity.PlanPickUpTime,
                        Addressee = orderentity.DeliveryMode == CodeConst.DeliveryMode.STORE ? orderentity.FK_PlanStore : orderentity.FK_Adress,
                        OrderPrice = orderentity.OrderPrice ?? 0,
                        CreateTime = orderentity.CreateTime,
                        RegimentTime = orderentity.RegimentTime,
                        DeliverTime = orderentity.DeliverTime,
                        CompleteTime = orderentity.CompleteTime,
                        Remark = orderentity.Remark == null ? String.Empty : orderentity.Remark,
                        ServiceMobile = orderentity.FK_AppId,
                        ExpressNumber = orderentity.ExpressNumber == null ? String.Empty : orderentity.ExpressNumber,
                        FKOpenId = orderentity.FK_Openid,
                        Freight = orderentity.Freight ?? 0
                    };

                    order.OrderUseTime = order.CreateTime.Value.AddMinutes(CodeConst.PayMinutes);
                    order.ServiceMobile = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == order.ServiceMobile).Select(p => p.Mobile).SingleOrDefault();
                    order.OrderStatusStr = statuslist[order.OrderStatus];
                    if (order.OrderStatus == CodeConst.OrderStatus.DELIVER && order.IsGroupOrder && !order.GroupResult)
                    {
                        order.OrderStatusStr = "待成团";
                    }
                    order.DeliveryTime = order.DeliverTime == null ? DateTime.Now : order.DeliverTime.Value.AddDays(DeliveryTime);
                    if (order.DeliveryMode == CodeConst.DeliveryMode.EXPRESS)
                    {
                        TADRESSEntity adress = db.IQueryable<TADRESSEntity>().Where(p => p.GUID == order.Addressee).FirstOrDefault();
                        if (adress != null)
                        {
                            order.Addressee = adress.UserName;
                            order.Address = adress.MoreAdress + adress.Adress;
                            order.Mobile = adress.Mobile;
                        }
                    }
                    else
                    {
                        if (order.OrderStatus == CodeConst.OrderStatus.DELIVER)
                        {
                            order.OrderStatusStr = "待取货";
                        }
                        order.GetGoodsTime = order.PlanPickUpTime == null ? "" : order.PlanPickUpTime.Value.ToString("yyyy-MM-dd 8:30-21:30");
                        T_STOREEntity store = db.IQueryable<T_STOREEntity>().Where(p => p.GUID == order.Addressee).FirstOrDefault();
                        if (store != null)
                        {
                            order.Addressee = store.StoreName;
                            order.Address = store.StoreAdress;
                            order.Mobile = store.StoreMobile;
                        }
                    }

                    bool isNoMember = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FKOpenId && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>().Where(t => t.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).SingleOrDefault() == null;

                    IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Order == order.GUID).ToList();

                    IList<String> productclassids = orderdetailList.Select(t => t.FK_ProductClass).ToList();
                    IList<T_PRODUCTCLASSEntity> productclassList = new ProductClassCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == orderentity.organizeid).ToList();
                    IList<T_PRODUCTPRICEEntity> productpriceList = new ProductPriceCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == orderentity.organizeid).ToList();

                    List<ProductInfo> productlist = orderdetailList.
                        Join(productclassList, m => m.FK_ProductClass, n => n.GUID, (m, n) => new { m, n }).
                        Join(productpriceList, x => x.n.FK_Product, y => y.GUID, (x, y) => new ProductInfo
                        {
                            OrderDetailGUID = x.m.GUID,
                            OrderStatus = order.OrderStatus,
                            OrderDetailStatus = x.m.Status ?? 0,
                            GUID = x.n.GUID,
                            ProductGUID = x.n.FK_Product,
                            ProductName = y.Name,
                            ProductDescribe = y.ProductParameter,
                            ProductClass = x.n.GUID,
                            ProductUnit = y.ProductUnit,
                            ProductMainPicture = y.ProductMainPicture,
                            DiscountPrice = (isNoMember ? x.m.OldPrice : x.m.Price) ?? 0,
                            RetailPrice = isNoMember ? 0 : (x.m.OldPrice ?? 0),
                            ProductCount = x.m.Count ?? 0,
                            IsLimitTimeProduct = y.IsLimitTimeProduct.Value,
                            LimitEndTime = y.LimitEndTime,
                            IsGroupProduct = y.IsGroupProduct.Value,
                            GroupPeople = y.GroupPeople.Value
                        }).ToList();

                    String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];

                    foreach (ProductInfo item in productlist)
                    {
                        item.Remark = new string[] { "正常状态", "售后处理", "审核通过", "审核不通过", "已完成" }[item.OrderDetailStatus];
                        item.ProductDetailList = new List<string>();
                        string classguid = item.GUID;
                        string productdescribe = String.Empty;
                        while (classguid != null)
                        {
                            T_PRODUCTCLASSEntity productclass = productclassList.Where(p => p.GUID == classguid).SingleOrDefault();
                            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                            item.ProductDetailList.Insert(0, classstr);
                            productdescribe += String.Format("{0}:{1}", productclass.ProductClass, productclass.ClassValue);
                            classguid = productclass.FK_ParentClass;
                        }
                        item.ProductParam = productdescribe;

                        if (!String.IsNullOrEmpty(item.ProductMainPicture) && !item.ProductMainPicture.StartsWith("http:") && !item.ProductMainPicture.StartsWith("https:"))
                        {
                            item.ProductMainPicture = ImgUrl + item.ProductMainPicture;
                        }
                    }

                    order.OrderProductList = productlist;

                    result.Data = JSONHelper.WXObjectToJson(order);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取订单详情少量信息使用
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetOrderDetail2(string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity orderentity = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                if (orderentity != null)
                {
                    OrderModel order = new OrderModel
                    {
                        GUID = orderentity.GUID,
                        OrderNumber = orderentity.OrderNumber,
                        IsGroupOrder = orderentity.IsGroupOrder == CodeConst.IsUse.YES,
                        GroupNumber = orderentity.GroupNumber,
                        GroupResult = orderentity.GroupResult == CodeConst.IsUse.YES,
                        OrderStatus = orderentity.OrderStatus ?? 0,
                        DeliveryMode = orderentity.DeliveryMode ?? 0,
                        PlanPickUpTime = orderentity.PlanPickUpTime,
                        Addressee = orderentity.DeliveryMode == CodeConst.DeliveryMode.STORE ? orderentity.FK_PlanStore : orderentity.FK_Adress,
                        OrderPrice = orderentity.OrderPrice ?? 0,
                        CreateTime = orderentity.CreateTime,
                        RegimentTime = orderentity.RegimentTime,
                        DeliverTime = orderentity.DeliverTime,
                        CompleteTime = orderentity.CompleteTime,
                        Remark = orderentity.Remark == null ? String.Empty : orderentity.Remark,
                        ServiceMobile = orderentity.FK_AppId,
                        ExpressNumber = orderentity.ExpressNumber == null ? String.Empty : orderentity.ExpressNumber,
                        FKOpenId = orderentity.FK_Openid
                    };

                    IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Order == order.GUID).ToList();

                    IList<String> productclassids = orderdetailList.Select(t => t.FK_ProductClass).ToList();
                    IList<T_PRODUCTCLASSEntity> productclassList = new ProductClassCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == orderentity.organizeid).ToList();
                    IList<T_PRODUCTPRICEEntity> productpriceList = new ProductPriceCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == orderentity.organizeid).ToList();

                    List<ProductInfo> productlist = orderdetailList.
                        Join(productclassList, m => m.FK_ProductClass, n => n.GUID, (m, n) => new { m, n }).
                        Join(productpriceList, x => x.n.FK_Product, y => y.GUID, (x, y) => new ProductInfo
                        {
                            OrderDetailGUID = x.m.GUID,
                            OrderStatus = order.OrderStatus,
                            OrderDetailStatus = x.m.Status ?? 0,
                            GUID = x.n.GUID,
                            ProductGUID = x.n.FK_Product,
                            ProductName = y.Name,
                            ProductDescribe = y.ProductParameter,
                            ProductClass = x.n.GUID,
                            ProductUnit = y.ProductUnit,
                            ProductMainPicture = y.ProductMainPicture,
                            ProductCount = x.m.Count ?? 0,
                            IsLimitTimeProduct = y.IsLimitTimeProduct.Value,
                            LimitEndTime = y.LimitEndTime,
                            IsGroupProduct = y.IsGroupProduct.Value,
                            GroupPeople = y.GroupPeople.Value
                        }).ToList();

                    String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];

                    foreach (ProductInfo item in productlist)
                    {
                        item.Remark = new string[] { "正常状态", "售后处理", "审核通过", "审核不通过", "已完成" }[item.OrderDetailStatus];
                        item.ProductDetailList = new List<string>();
                        string classguid = item.GUID;
                        string productdescribe = String.Empty;
                        while (classguid != null)
                        {
                            T_PRODUCTCLASSEntity productclass = productclassList.Where(p => p.GUID == classguid).SingleOrDefault();
                            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                            item.ProductDetailList.Insert(0, classstr);
                            productdescribe += String.Format("{0}:{1}", productclass.ProductClass, productclass.ClassValue);
                            classguid = productclass.FK_ParentClass;
                        }
                        item.ProductParam = productdescribe;

                        if (!String.IsNullOrEmpty(item.ProductMainPicture) && !item.ProductMainPicture.StartsWith("http:") && !item.ProductMainPicture.StartsWith("https:"))
                        {
                            item.ProductMainPicture = ImgUrl + item.ProductMainPicture;
                        }
                    }

                    order.OrderProductList = productlist;

                    result.Data = JSONHelper.WXObjectToJson(order);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取订单信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="wxopenId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public string GetOrderList(int pageIndex, string wxopenId, int status)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                //TORDERService tORDERService = new TORDERService();
                //tORDERService.SystemDeleteTimeOutOrder(wxopenId);

                var list = db.IQueryable<TORDEREntity>().Where(p => p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO && (p.IsGroupOrder == CodeConst.IsUse.NO || p.GroupResult == CodeConst.IsUse.YES));
                if (status != 0)
                {
                    list = list.Where(p => p.OrderStatus == status);
                }
                List<TORDEREntity> resultList3 = list.OrderByDescending(p => p.CreateTime).Skip(pageIndex * 3).Take(3).ToList();
                List<OrderModel> resultList = resultList3.Select(p => new OrderModel
                {
                    GUID = p.GUID,
                    OrderNumber = p.OrderNumber,
                    OrderStatus = p.OrderStatus ?? 0,
                    DeliveryMode = p.DeliveryMode ?? 0,
                    OrderPrice = p.OrderPrice ?? 0,
                    CreateTime = p.CreateTime,
                    CanCancel = p.FK_ActualStore == null
                }).ToList();

                if (resultList.Count == 0)
                {
                    return JSONHelper.WXObjectToJson(result);
                }

                bool isNoMember = db.IQueryable<TUSEREntity>().Where(p => p.Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>().Where(t => t.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).SingleOrDefault() == null;

                IList<String> orderids = resultList.Select(t => t.GUID).ToList();
                IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(t => orderids.Contains(t.FK_Order) && t.IsDel == CodeConst.IsDel.NO).ToList();
                IList<String> productClasss = orderdetailList.Select(t => t.FK_ProductClass).ToList();

                IList<T_PRODUCTCLASSEntity> productClassList = new ProductClassCache().GetCacheList().Where(t => productClasss.Contains(t.GUID)).ToList();
                //IList<T_PRODUCTPRICEEntity> productPriceList = new ProductPriceCache().GetCacheList().Where(t => productClassList.Select(y => y.FK_Product).Contains(t.GUID)).ToList();


                var datalist = productClassList
                        .Join(new ProductPriceCache().GetCacheList(), x => x.FK_Product, y => y.GUID, (x, y) => new
                        {
                            ItemCode = x.GUID,
                            ImageURL = y.ProductMainPicture,
                            ProductName = y.Name,
                            ProductDescribe = y.ProductParameter
                        }).ToList();


                foreach (OrderModel model in resultList)
                {
                    model.OrderStatusStr = statuslist[model.OrderStatus];

                    if (model.DeliveryMode == CodeConst.DeliveryMode.STORE && model.OrderStatus == CodeConst.OrderStatus.DELIVER)
                    {
                        model.OrderStatusStr = "待取货";
                    }

                    model.ProductList = orderdetailList.Where(p => p.FK_Order == model.GUID && p.IsDel == CodeConst.IsDel.NO)
                        .Join(datalist, m => m.FK_ProductClass, n => n.ItemCode, (m, n) => new ShopItem
                        {
                            ItemCode = n.ItemCode,
                            ImageURL = n.ImageURL,
                            ProductName = n.ProductName,
                            ProductDescribe = n.ProductDescribe,
                            ProductPrice = (isNoMember ? m.OldPrice : m.Price) ?? 0,
                            OldPrice = isNoMember ? 0 : (m.OldPrice ?? 0),
                            ProductCount = m.Count ?? 0
                        }).ToList();

                    String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];
                    foreach (ShopItem item in model.ProductList)
                    {
                        item.ProductClassList = new List<string>();
                        string classguid = item.ItemCode;
                        while (classguid != null)
                        {
                            T_PRODUCTCLASSEntity productclass = productClassList.Where(p => p.GUID == classguid).SingleOrDefault();
                            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                            item.ProductClassList.Insert(0, classstr);
                            classguid = productclass.FK_ParentClass;
                        }

                        if (!String.IsNullOrEmpty(item.ImageURL) && !item.ImageURL.StartsWith("http:") && !item.ImageURL.StartsWith("https:"))
                        {
                            item.ImageURL = ImgUrl + item.ImageURL;
                        }
                    }
                    if (model.OrderStatus >= 4)
                    {
                        //model.hasEval = db.IQueryable<T_CommentEntity>().Count(t => t.orderid == model.GUID) > 0;
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(resultList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public ResultData GetOrderListShouyin(string wxopenId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TMEMBEREntity member = db.IQueryable<TMEMBEREntity>().FirstOrDefault(t => t.usercode == wxopenId);

                var list = db.IQueryable<TORDEREntity>().Where(p => p.FK_Openid == member.FK_UnionId && p.IsDel == CodeConst.IsDel.NO && (p.IsGroupOrder == CodeConst.IsUse.NO || p.GroupResult == CodeConst.IsUse.YES));
                list = list.Where(p => (p.OrderStatus == 2 || p.OrderStatus == 3));
                List<TORDEREntity> resultList3 = list.OrderByDescending(p => p.CreateTime).ToList();
                List<OrderModel> resultList = resultList3.Select(p => new OrderModel
                {
                    GUID = p.GUID,
                    OrderNumber = p.OrderNumber,
                    OrderStatus = p.OrderStatus ?? 0,
                    DeliveryMode = p.DeliveryMode ?? 0,
                    OrderPrice = p.OrderPrice ?? 0,
                    CreateTime = p.CreateTime,
                    CanCancel = p.FK_ActualStore == null,
                    Count = p.Count,
                    DeliverName = p.DeliveryName,
                    DeliverMobile = p.DeliveryMobile
                }).ToList();

                if (resultList.Count == 0)
                {
                    return result;
                }

                //bool isNoMember = db.IQueryable<TUSEREntity>().Where(p => p.Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>().Where(t => t.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).SingleOrDefault() == null;

                //IList<String> orderids = resultList.Select(t => t.GUID).ToList();
                //IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(t => orderids.Contains(t.FK_Order) && t.IsDel == CodeConst.IsDel.NO).ToList();
                //IList<String> productClasss = orderdetailList.Select(t => t.FK_ProductClass).ToList();

                //IList<T_PRODUCTCLASSEntity> productClassList = new ProductClassCache().GetCacheList().Where(t => productClasss.Contains(t.GUID)).ToList();

                //var datalist = productClassList
                //        .Join(new ProductPriceCache().GetCacheList(), x => x.FK_Product, y => y.GUID, (x, y) => new
                //        {
                //            ItemCode = x.GUID,
                //            ImageURL = y.ProductMainPicture,
                //            ProductName = y.Name,
                //            ProductDescribe = y.ProductParameter
                //        }).ToList();


                //foreach (OrderModel model in resultList)
                //{
                //    model.OrderStatusStr = statuslist[model.OrderStatus];

                //    if (model.DeliveryMode == CodeConst.DeliveryMode.STORE && model.OrderStatus == CodeConst.OrderStatus.DELIVER)
                //    {
                //        model.OrderStatusStr = "待取货";
                //    }

                //    model.ProductList = orderdetailList.Where(p => p.FK_Order == model.GUID && p.IsDel == CodeConst.IsDel.NO)
                //        .Join(datalist, m => m.FK_ProductClass, n => n.ItemCode, (m, n) => new ShopItem
                //        {
                //            ItemCode = n.ItemCode,
                //            ImageURL = n.ImageURL,
                //            ProductName = n.ProductName,
                //            ProductDescribe = n.ProductDescribe,
                //            ProductPrice = (isNoMember ? m.OldPrice : m.Price) ?? 0,
                //            OldPrice = isNoMember ? 0 : (m.OldPrice ?? 0),
                //            ProductCount = m.Count ?? 0
                //        }).ToList();

                //    String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];
                //    foreach (ShopItem item in model.ProductList)
                //    {
                //        item.ProductClassList = new List<string>();
                //        string classguid = item.ItemCode;
                //        while (classguid != null)
                //        {
                //            T_PRODUCTCLASSEntity productclass = productClassList.Where(p => p.GUID == classguid).SingleOrDefault();
                //            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                //            item.ProductClassList.Insert(0, classstr);
                //            classguid = productclass.FK_ParentClass;
                //        }

                //        if (!String.IsNullOrEmpty(item.ImageURL) && !item.ImageURL.StartsWith("http:") && !item.ImageURL.StartsWith("https:"))
                //        {
                //            item.ImageURL = ImgUrl + item.ImageURL;
                //        }
                //    }
                //}
                result.Data = JSONHelper.WXObjectToJson(resultList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return result;
        }

        public ResultData ComOrderShouyinDetail(String warehourse, string orderguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == orderguid && p.IsDel == CodeConst.IsDel.NO && (p.OrderStatus == CodeConst.OrderStatus.COLLECT || p.OrderStatus == CodeConst.OrderStatus.DELIVER)).SingleOrDefault();
                if (order != null)
                {
                    int integral = Convert.ToInt32(order.OrderPrice / CodeConst.Integral.PAYORDER);

                    TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FK_Openid && p.IsDel == CodeConst.IsDel.NO)
                            .Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();
                    if (user != null)
                    {
                        user.Integral += integral;
                        TORDERService tORDERService = new TORDERService();

                        TINTEGRALEntity tintegral = tORDERService.IntegralRecord(order.FK_Openid, order.FK_AppId, integral, user.Integral.Value, CodeConst.IntegralType.PAYORDER, CodeConst.IntegralRemark.PAYORDER);
                        db.Insert<TINTEGRALEntity>(tintegral);
                        db.Update<TUSEREntity>(user, null);
                    }

                    order.OrderStatus = CodeConst.OrderStatus.COMPLETE;
                    order.Completer = warehourse;
                    order.FK_ActualStore = warehourse;
                    order.CompleteTime = DateTime.Now;
                    order.Modifyer = warehourse;
                    order.ModifyTime = DateTime.Now;
                    db.Update<TORDEREntity>(order, null);
                    result.IsOK = true;
                }
                else
                {
                    result.msg = "该订单无法确认！";
                }
                if (CodeConst.IsUseStock && result.IsOK)
                {
                    IList<TORDERSTOCKEntity> list = db.IQueryable<TORDERSTOCKEntity>().Where(p => p.FK_Order == orderguid && p.Status == CodeConst.OrderStockStatus.YTC).ToList();
                    IList<String> fk_Stocks = list.Select(t => t.FK_Stock).ToList();
                    IList<TSTOCKEntity> stocklist = db.IQueryable<TSTOCKEntity>().Where(t => fk_Stocks.Contains(t.GUID)).ToList();
                    foreach (TORDERSTOCKEntity orderstock in list)
                    {
                        orderstock.Status = CodeConst.OrderStockStatus.YWC;
                        TSTOCKEntity stockInfo = stocklist.Where(p => p.GUID == orderstock.FK_Stock).SingleOrDefault();

                        if (stockInfo != null)
                        {
                            stockInfo.ActualStock -= orderstock.StockCount;

                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                            stockdetail.ID = Guid.NewGuid().ToString();
                            stockdetail.FK_Stock = stockInfo.GUID;
                            stockdetail.ActualStock = -orderstock.StockCount;
                            stockdetail.Type = CodeConst.StockDetailType.CK;
                            stockdetail.Remark = "小程序订单完成";
                            stockdetail.Creater = warehourse;
                            stockdetail.CreateTime = DateTime.Now;
                            stockdetail.organizeid = order.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdetail);

                            result.IsOK = true;
                        }
                    }

                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return result;
        }

        public ResultData GetOrderShouyinDetail(string orderguid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity list = db.IQueryable<TORDEREntity>().FirstOrDefault(p => p.GUID == orderguid);

                OrderModel resultList = new OrderModel
                {
                    GUID = list.GUID,
                    OrderNumber = list.OrderNumber,
                    OrderStatus = list.OrderStatus ?? 0,
                    DeliveryMode = list.DeliveryMode ?? 0,
                    OrderPrice = list.OrderPrice ?? 0,
                    CreateTime = list.CreateTime,
                    CanCancel = list.FK_ActualStore == null,
                    Count = list.Count
                };

                bool isNoMember = db.IQueryable<TUSEREntity>().Where(p => p.Openid == list.Creater && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>().Where(t => t.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).SingleOrDefault() == null;

                IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(t => t.FK_Order == orderguid && t.IsDel == CodeConst.IsDel.NO).ToList();
                IList<String> productClasss = orderdetailList.Select(t => t.FK_ProductClass).ToList();

                IList<T_PRODUCTCLASSEntity> productClassList = new ProductClassCache().GetCacheList().Where(t => productClasss.Contains(t.GUID)).ToList();

                var datalist = productClassList
                        .Join(new ProductPriceCache().GetCacheList(), x => x.FK_Product, y => y.GUID, (x, y) => new
                        {
                            ItemCode = x.GUID,
                            ImageURL = y.ProductMainPicture,
                            ProductName = y.Name,
                            ProductDescribe = y.ProductParameter
                        }).ToList();


                resultList.OrderStatusStr = statuslist[resultList.OrderStatus];

                if (resultList.DeliveryMode == CodeConst.DeliveryMode.STORE && resultList.OrderStatus == CodeConst.OrderStatus.DELIVER)
                {
                    resultList.OrderStatusStr = "待取货";
                }

                resultList.ProductList = orderdetailList.Where(p => p.FK_Order == resultList.GUID && p.IsDel == CodeConst.IsDel.NO)
                    .Join(datalist, m => m.FK_ProductClass, n => n.ItemCode, (m, n) => new ShopItem
                    {
                        ItemCode = n.ItemCode,
                        ImageURL = n.ImageURL,
                        ProductName = n.ProductName,
                        ProductDescribe = n.ProductDescribe,
                        ProductPrice = (isNoMember ? m.OldPrice : m.Price) ?? 0,
                        OldPrice = isNoMember ? 0 : (m.OldPrice ?? 0),
                        ProductCount = m.Count ?? 0,
                        TotalProductPrice = Convert.ToDecimal((((isNoMember ? m.OldPrice : m.Price) ?? 0) * (m.Count ?? 0)).ToString("0.00"))
                    }).ToList();

                String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];
                foreach (ShopItem item in resultList.ProductList)
                {
                    item.ProductClassList = new List<string>();
                    string classguid = item.ItemCode;
                    while (classguid != null)
                    {
                        T_PRODUCTCLASSEntity productclass = productClassList.Where(p => p.GUID == classguid).SingleOrDefault();
                        string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                        item.ProductClassList.Insert(0, classstr);
                        classguid = productclass.FK_ParentClass;
                        item.ProductDescribe += classstr + ",";
                        item.ProductSku = productclass.ProductSKU;
                    }
                    item.ProductName = item.ProductName + "[" + item.ProductDescribe.TrimEnd(',') + "]";
                    if (!String.IsNullOrEmpty(item.ImageURL) && !item.ImageURL.StartsWith("http:") && !item.ImageURL.StartsWith("https:"))
                    {
                        item.ImageURL = ImgUrl + item.ImageURL;
                    }
                }

                result.Data = JSONHelper.WXObjectToJson(resultList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return result;
        }


        /// <summary>
        /// 保存订单
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="wxopenId"></param>
        /// <param name="productlist"></param>
        /// <param name="mode"></param>
        /// <param name="classcount"></param>
        /// <param name="orderprice"></param>
        /// <param name="modeguid"></param>
        /// <param name="isgrouporder"></param>
        /// <param name="groupnumber"></param>
        /// <param name="remark"></param>
        /// <param name="freight"></param>
        /// <param name="cardguid"></param>
        /// <param name="getGoodsDate"></param>
        /// <returns></returns>
        public string SaveOrderInfo(string appId, string wxopenId, string productlist, int mode, int classcount, decimal orderprice, string modeguid, int isgrouporder, string groupnumber, string remark, decimal freight, string cardguid, string getGoodsDate, String username, String usertel, string organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<List<string>> list = JSONHelper.WXDecodeObject<List<List<string>>>(productlist);
                Hashtable sqlListHashtable = new Hashtable();
                TORDEREntity orderInfo = new TORDEREntity();

                db.BeginTrans();
                {
                    TORDEREntity newtimeorder = db.IQueryable<TORDEREntity>().Where(p => p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO).OrderByDescending(p => p.CreateTime).FirstOrDefault();

                    if (newtimeorder != null)
                    {
                        int timeoutcnt = DateTime.Now.AddSeconds(-5).CompareTo(newtimeorder.CreateTime.Value);
                        if (timeoutcnt < 0)
                        {
                            result.msg = "订单提交太频繁！";
                            return JSONHelper.WXObjectToJson(result);
                        }
                    }

                    bool IsMember = db.IQueryable<TMEMBEREntity>().Where(p => p.FK_UnionId == wxopenId).Count() > 0;

                    int isBuy = db.IQueryable<TORDEREntity>().Where(p => p.FK_Openid == wxopenId && p.GroupNumber == groupnumber && p.OrderStatus > CodeConst.OrderStatus.PAYMENT && p.OrderStatus < CodeConst.OrderStatus.FAILED).Count();
                    TORDEREntity peopleorder = db.IQueryable<TORDEREntity>().Where(p => p.GroupNumber == groupnumber && p.IsDel == CodeConst.IsDel.NO && p.GroupResult == CodeConst.IsUse.YES && p.GroupRole == CodeConst.GroupRole.INITIATOR).SingleOrDefault();
                    if (isBuy > 0)
                    {
                        result.msg = "您已参与该次拼团！";
                    }
                    else if (peopleorder != null)
                    {
                        result.msg = "该次拼团已结束！";
                    }
                    else
                    {
                        Random rd = new Random();
                        string guid = Guid.NewGuid().ToString();
                        TORDEREntity order = new TORDEREntity();
                        order.GUID = guid;
                        order.FK_AppId = appId;
                        order.OrderNumber = DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0');
                        order.OrderStatus = CodeConst.OrderStatus.PAYMENT;
                        order.FK_Openid = wxopenId;
                        order.Count = classcount;
                        order.PayWay = CodeConst.PayWay.WXPAY;
                        order.PayResult = CodeConst.IsUse.NO;
                        order.OrderPrice = orderprice;
                        order.organizeid = organizeid;
                        if (!String.IsNullOrEmpty(cardguid))
                        {
                            order.Coupon = cardguid;
                            TUSERCARDEntity usercard = db.IQueryable<TUSERCARDEntity>().Where(p => p.GUID == cardguid && p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.CardUse.NOUSE).FirstOrDefault();
                            if (usercard != null)
                            {
                                if (usercard.ValidDate < DateTime.Now.Date)
                                {
                                    result.msg = "该优惠券已过期！";
                                    return JSONHelper.WXObjectToJson(result);
                                }
                                usercard.IsUse = CodeConst.CardUse.ISUSE;
                            }
                            else
                            {
                                result.msg = "该优惠券无法使用！";
                                return JSONHelper.WXObjectToJson(result);
                            }
                        }
                        bool IsDeivery = false;
                        order.DeliveryMode = mode;
                        if (mode.Equals(CodeConst.DeliveryMode.STORE))
                        {
                            order.FK_PlanStore = modeguid;
                            if (!String.IsNullOrEmpty(getGoodsDate))
                                order.PlanPickUpTime = DateTime.Parse(getGoodsDate);
                            order.DeliveryName = username;
                            order.DeliveryMobile = usertel;
                        }
                        else
                        {
                            TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == appId).SingleOrDefault();
                            IsDeivery = true;
                            order.FK_Adress = modeguid;
                            TADRESSEntity orderadress = db.IQueryable<TADRESSEntity>().Where(p => p.GUID == modeguid).SingleOrDefault();
                            decimal distance = RoadDistance.algorithm(orderadress.Longitude.Value, orderadress.Latitude.Value, wxapp.Longitude.Value, wxapp.Latitude.Value);
                            if (distance > 3)
                            {
                                result.msg = "3公里外不配送！";
                                return JSONHelper.WXObjectToJson(result);
                            }
                            if (orderprice < CodeConst.FreightSend)
                            {
                                result.msg = String.Format("订单满{0}元起送！", CodeConst.FreightSend);
                                return JSONHelper.WXObjectToJson(result);
                            }

                            order.DeliveryAdress = orderadress.MoreAdress + orderadress.Adress;
                            order.DeliveryName = orderadress.UserName;
                            order.DeliveryMobile = orderadress.Mobile;
                            order.Freight = freight;
                        }
                        order.IsGroupOrder = isgrouporder;
                        if (isgrouporder.Equals(CodeConst.IsUse.YES))
                        {
                            order.GroupEndTime = DateTime.Now.AddMinutes(30);
                            order.GroupResult = CodeConst.IsUse.NO;
                        }
                        if (!String.IsNullOrEmpty(groupnumber))
                        {
                            order.GroupNumber = groupnumber;
                            order.GroupRole = CodeConst.GroupRole.PARTICIPANT;
                        }
                        else
                        {
                            order.GroupNumber = Guid.NewGuid().ToString("N");
                            order.GroupRole = CodeConst.GroupRole.INITIATOR;
                        }
                        order.Remark = remark;
                        order.IsDel = CodeConst.IsDel.NO;
                        order.Creater = wxopenId;
                        order.CreateTime = DateTime.Now;
                        order.Modifyer = wxopenId;
                        order.ModifyTime = DateTime.Now;
                        order.OrderType = "1";
                        db.Insert<TORDEREntity>(order);

                        int nctnumber = 1;
                        //获取商品信息
                        IList<String> classguids = new List<String>();
                        foreach (List<string> productclass in list)
                        {
                            classguids.Add(productclass[0]);
                        }

                        IList<T_PRODUCTCLASSEntity> productclassList = new ProductClassCache().GetCacheList().Where(x => x.IsDel == 0 && classguids.Contains(x.GUID)).ToList();
                        IList<String> productids = productclassList.Select(t => t.FK_Product).ToList();

                        //判断是否有特殊商品
                        if (IsDeivery)
                        {
                            int cnt = db.IQueryable<T_CATEGORY_PRODUCTEntity>().Where(p => productids.Contains(p.FK_Product))
                                 .Join(db.IQueryable<T_PRODUCTCATEGORYEntity>().Where(p => p.Remark.Contains("到店取货")), m => m.FK_Category, n => n.GUID, (m, n) => n).Count();
                            if (cnt > 0)
                            {
                                result.msg = "此订单包含特殊商品，只能选择到店取货！";
                                return JSONHelper.WXObjectToJson(result);
                            }
                        }

                        //获取已经购买的商品
                        IList<String> sproductclassids = productclassList.Select(t => t.GUID).ToList();
                        DateTime nowdate = DateTime.Now.Date;
                        List<TORDERDETAILEntity> orderdetaillist = db.IQueryable<TORDEREntity>().Where(p => p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO && p.OrderStatus <= CodeConst.OrderStatus.COMPLETE && p.CreateTime >= nowdate)
                                    .Join(db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && sproductclassids.Contains(p.FK_ProductClass)), m => m.GUID, n => n.FK_Order, (m, n) => n).ToList();

                        IList<T_PRODUCTPRICEEntity> productpriceList = new ProductPriceCache().GetCacheList().Where(t => t.IsDel == 0 && productids.Contains(t.GUID)).ToList();
                        //库存信息
                        IList<String> productskus = productclassList.Select(t => t.ProductSKU).ToList();
                        IList<VSTOCKEntity> vstockList = db.IQueryable<VSTOCKEntity>().Where(t => productskus.Contains(t.SKU)).ToList();
                        productskus = vstockList.Select(t => t.ActualSKU).ToList();
                        IList<VPRODUCTCOMBEntity> vproductcombList = db.IQueryable<VPRODUCTCOMBEntity>().Where(t => productskus.Contains(t.SKU)).ToList();

                        Boolean boTiYan = false;
                        TUSEREntity user = db.IQueryable<TUSEREntity>().FirstOrDefault(p => p.Openid == wxopenId);
                        foreach (List<string> productclass in list)
                        {
                            string classguid = productclass[0];
                            T_PRODUCTCLASSEntity stockclass = productclassList.Where(p => p.GUID == classguid).SingleOrDefault();

                            T_PRODUCTPRICEEntity productpriceInfo = productpriceList.Where(p => p.GUID == stockclass.FK_Product).FirstOrDefault();
                            if (productpriceInfo != null)
                            {
                                if (productpriceInfo.IsDel == CodeConst.IsDel.YES || productpriceInfo.IsSale != CodeConst.IsSale.IsShelves)
                                {
                                    result.msg = String.Format("商品{0}已下架，无法购买！", productpriceInfo.Name);
                                    return JSONHelper.WXObjectToJson(result);
                                }

                                //判断是否是限时商品
                                if (productpriceInfo.IsLimitTimeProduct == CodeConst.IsUse.YES)
                                {
                                    int c = Int32.Parse(productclass[1]);
                                    if (c > 1)
                                    {
                                        result.msg = String.Format("抱歉！秒杀商品只能购买1个！");
                                        return JSONHelper.WXObjectToJson(result);
                                    }
                                    TMODELPRODUCTFruitsEntity fruitsEntity = db.IQueryable<TMODELPRODUCTFruitsEntity>().FirstOrDefault(t => t.ImageNavigate1 == productpriceInfo.GUID && t.IsDel == 0 && t.time1.HasValue);
                                    if (fruitsEntity != null)
                                    {
                                        if (fruitsEntity.time1 < DateTime.Now)
                                        {
                                            result.msg = String.Format("商品{0}已超出购买时限，无法购买！", productpriceInfo.Name);
                                            return JSONHelper.WXObjectToJson(result);
                                        }
                                        if (fruitsEntity.num1 < (fruitsEntity.num2 ?? 0) + Int32.Parse(productclass[1]))
                                        {
                                            result.msg = String.Format("秒杀商品{0}已销售完成！", productpriceInfo.Name);
                                            return JSONHelper.WXObjectToJson(result);
                                        }
                                        fruitsEntity.num2 = (fruitsEntity.num2 ?? 0) + Int32.Parse(productclass[1]);
                                        db.Update<TMODELPRODUCTFruitsEntity>(fruitsEntity, null);
                                    }
                                    else
                                    {
                                        //if (productpriceInfo.LimitEndTime < DateTime.Now)
                                        //{
                                        //    result.msg = String.Format("商品{0}已超出购买时限，无法购买！", productpriceInfo.Name);
                                        //    return JSONHelper.WXObjectToJson(result);
                                        //}
                                    }
                                }

                                List<TORDERDETAILEntity> detaillist = orderdetaillist.Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_ProductClass == stockclass.GUID).ToList();

                                decimal buyCount = Decimal.Zero;

                                if (detaillist.Count > 0)
                                {
                                    buyCount = detaillist.Sum(p => p.Count) ?? 0;
                                }

                                //if (buyCount + Decimal.Parse(productclass[1]) > productpriceInfo.LimitCount)
                                //{
                                //    result.msg = String.Format("此订单包含限量商品{0}，只能购买{1}{2}！", productpriceInfo.Name, productpriceInfo.LimitCount - buyCount, productpriceInfo.ProductUnit);
                                //    return JSONHelper.WXObjectToJson(result);
                                //}

                                //判断是否是新人限购商品
                                TMODELPRODUCTEntity tMODELPRODUCTEntity = db.IQueryable<TMODELFUNCTIONEntity>().Where(t => t.GUID == "3928a1785517460183a4d531530fdd3b").Join(db.IQueryable<TMODELPRODUCTEntity>().Where(t => t.ImageNavigate1 == productpriceInfo.GUID), m => m.GUID, n => n.FK_ModelFunction, (m, n) => n).FirstOrDefault();
                                if (tMODELPRODUCTEntity != null)//新人限购商品
                                {
                                    int c = Int32.Parse(productclass[1]);
                                    if (c > 1)
                                    {
                                        result.msg = String.Format("抱歉！首单体验只能购买1个！");
                                        return JSONHelper.WXObjectToJson(result);
                                    }
                                    //判断是否已经购买了
                                    if (user.EventDraw == 1)
                                    {
                                        result.msg = String.Format("抱歉！您已参与过首单体验，无法再次体验！");
                                        return JSONHelper.WXObjectToJson(result);
                                    }
                                    boTiYan = true;
                                }
                            }
                            if (boTiYan)
                            {
                                user.EventDraw = 1;
                                user.EventDrawDate = DateTime.Now;
                                db.Update<TUSEREntity>(user, null);
                            }

                            TORDERDETAILEntity detail = new TORDERDETAILEntity();
                            detail.GUID = Guid.NewGuid().ToString();
                            detail.FK_Order = guid;
                            detail.FK_ProductClass = classguid;
                            detail.Count = Decimal.Parse(productclass[1]);
                            if (detail.Count == Decimal.Zero)
                            {
                                result.msg = String.Format("订单提交失败，请尝试重新提交！");
                                return JSONHelper.WXObjectToJson(result);
                            }
                            detail.OldPrice = stockclass.ClassOldPrice;
                            detail.Price = stockclass.ClassPrice;
                            detail.ActualPrice = IsMember ? stockclass.ClassPrice : stockclass.ClassOldPrice;
                            detail.Status = CodeConst.OrderDetailStatus.NORMAL;
                            detail.IsDel = CodeConst.IsDel.NO;
                            detail.Creater = wxopenId;
                            detail.CreateTime = DateTime.Now;
                            detail.Modifyer = wxopenId;
                            detail.ModifyTime = DateTime.Now;
                            detail.organizeid = organizeid;
                            db.Insert<TORDERDETAILEntity>(detail);

                            if (CodeConst.IsUseStock)
                            {
                                VSTOCKEntity stockInfo = vstockList.Where(p => p.SKU == stockclass.ProductSKU).FirstOrDefault();
                                if (stockInfo == null || stockInfo.Stock < detail.Count * stockclass.ProductStock.Value)
                                {
                                    result.msg = String.Format("{0}库存不足！", stockInfo.PdtName);
                                    return JSONHelper.WXObjectToJson(result);
                                }

                                List<VPRODUCTCOMBEntity> comblist = vproductcombList.Where(p => p.SKU == stockInfo.ActualSKU).ToList();
                                if (comblist.Count > 0)
                                {
                                    foreach (VPRODUCTCOMBEntity comb in comblist)
                                    {
                                        sqlListHashtable = WSCSql.GetUpdateProductSql(comb.ChildSKU, (comb.ChildCount.Value * detail.Count * stockclass.ProductStock.Value * stockInfo.PdtCount.Value) ?? 0, nctnumber++, sqlListHashtable);
                                    }
                                }
                                else
                                {
                                    sqlListHashtable = WSCSql.GetUpdateProductSql(stockInfo.ActualSKU, (detail.Count * stockclass.ProductStock.Value * stockInfo.PdtCount.Value) ?? 0, nctnumber++, sqlListHashtable);
                                }
                            }
                        }
                        result.IsOK = true;
                        result.Data = order.GUID;
                        orderInfo = order;
                    }
                }
                foreach (DictionaryEntry myDE in sqlListHashtable)
                {
                    string cmdText = myDE.Key.ToString();
                    SqlParameter[] cmdParms = (SqlParameter[])myDE.Value;
                    db.ExecuteBySql(cmdText, cmdParms);
                }

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                result.msg = "系统错误，请尝试重新打开小程序！";
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取运费信息
        /// </summary>
        /// <param name="appguId"></param>
        /// <param name="adressguId"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetFreight(string appguId, string adressguId, string organizeid)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();
                TADRESSEntity adress = db.IQueryable<TADRESSEntity>().Where(p => p.GUID == adressguId && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
                TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == appguId).SingleOrDefault();
                if (adress != null && wxapp != null)
                {
                    decimal distance = RoadDistance.algorithm(adress.Longitude.Value, adress.Latitude.Value, wxapp.Longitude.Value, wxapp.Latitude.Value);
                    T_FREIGHTEntity freight = db.IQueryable<T_FREIGHTEntity>().Where(p => p.organizeid == organizeid && p.Distance >= distance && p.IsDel == CodeConst.IsDel.NO).OrderBy(p => p.Distance).FirstOrDefault();
                    if (freight != null)
                    {
                        result.Data = freight.Freight.ToString();
                        result.msg = CodeConst.FreightFree;
                        result.IsOK = true;
                    }
                    else
                    {
                        result.Data = CodeConst.Freight;
                        result.msg = CodeConst.FreightFree;
                        result.IsOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取地址信息
        /// </summary>
        /// <param name="adresstype"></param>
        /// <param name="wxopenId"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetAdressList(int adresstype, string wxopenId, string organizeid)
        {
            List<AdressModel> result = new List<AdressModel>();
            try
            {
                Data.IDatabase db = DbFactory.Base();
                if (adresstype.Equals(CodeConst.DeliveryMode.STORE))
                {
                    result = db.IQueryable<T_STOREEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.organizeid == organizeid).Select(p => new AdressModel
                    {
                        GUID = p.GUID,
                        AdressName = p.StoreName,
                        AdressPlace = p.StoreAdress,
                        Mobile = p.StoreMobile,
                        Longitude = p.Longitude,
                        Latitude = p.Latitude
                    }).ToList();

                    return JSONHelper.WXObjectToJson(result);
                }
                else
                {
                    result = db.IQueryable<TADRESSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_OpenId == wxopenId).Select(p => new AdressModel
                    {
                        GUID = p.GUID,
                        AdressName = p.UserName,
                        AdressPlace = p.MoreAdress + p.Adress,
                        Mobile = p.Mobile,
                        IsUse = p.IsUse ?? 0
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 新增点赞
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string UserZan(string guid, string openId)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();

                TZANEntity zan = db.IQueryable<TZANEntity>().Where(p => p.FK_Product == guid && p.FK_OpenId == openId).SingleOrDefault();
                if (zan != null)
                {
                    zan.IsZan = 1 - zan.IsZan;
                }
                else
                {
                    TZANEntity newzan = new TZANEntity();
                    newzan.GUID = Guid.NewGuid().ToString();
                    newzan.FK_Product = guid;
                    newzan.FK_OpenId = openId;
                    newzan.IsZan = CodeConst.IsUse.YES;
                    newzan.ZanCount = 1;
                    db.Insert<TZANEntity>(newzan);
                }
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 获取点赞信息
        /// </summary>
        /// <param name="guid">商品id</param>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string GetZanCountByGUID(string guid, string openId)
        {
            ResultData result = new ResultData();
            try
            {
                Data.IDatabase db = DbFactory.Base();
                int ZanCount = 0;
                List<TZANEntity> list = db.IQueryable<TZANEntity>().Where(p => p.FK_Product == guid && p.IsZan == CodeConst.IsUse.YES).ToList();
                if (list.Count > 0)
                {
                    ZanCount = list.Sum(p => p.ZanCount) ?? 0;
                }
                int IsZan = 1;
                TZANEntity zan = db.IQueryable<TZANEntity>().Where(p => p.FK_Product == guid && p.FK_OpenId == openId).SingleOrDefault();
                if (zan != null)
                {
                    IsZan = zan.IsZan ?? 0;
                }
                if (ZanCount < 1000)
                {
                    result.Data = ZanCount.ToString();
                }
                else if (ZanCount < 100000)
                {
                    result.Data = String.Format("{0}K", (ZanCount / 1000).ToString());
                }
                else
                {
                    result.Data = String.Format("{0}W", (ZanCount / 10000).ToString());
                }

                result.msg = IsZan.ToString();
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public String SavePingJia(String OrderGUID, String OrderNumber, String openid, Int32 m_des, Int32 m_du, Int32 m_wuliu, Boolean n_name, String codeid, String eval, String typeNumber, String productParam, String productCount, String productname)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {

                if (String.IsNullOrEmpty(OrderGUID))
                {
                    result.msg = "请输入订单信息";
                    return JSONHelper.WXObjectToJson(result);
                }
                if (String.IsNullOrEmpty(openid))
                {
                    result.msg = "请先登录";
                    return JSONHelper.WXObjectToJson(result);
                }
                T_CommentEntity t_Comment = db.IQueryable<T_CommentEntity>().FirstOrDefault(t => t.FK_OpenId == openid && t.orderid == OrderGUID);
                if (t_Comment != null)
                {
                    result.msg = "该订单已评价";
                    return JSONHelper.WXObjectToJson(result);
                }
                db.BeginTrans();

                IList<T_CommentDetailEntity> comments = new List<T_CommentDetailEntity>();
                String[] codeids = codeid.Split(';');
                String[] typeNumbers = typeNumber.Split(';');
                String[] evals = eval.Split(';');
                String[] productParams = productParam.Split(';');
                String[] productCounts = productCount.Split(';');
                String[] productnames = productname.Split(';');
                for (Int32 i = 0, j = codeids.Length; i < j; i++)
                {
                    if (String.IsNullOrEmpty(codeids[i]))
                    {
                        continue;
                    }
                    T_CommentDetailEntity comd = new T_CommentDetailEntity();
                    comd.Create();
                    comd.ProductName = productnames[i];
                    comd.Count = Convert.ToInt32(productCounts[i]);
                    comd.FK_Product = codeids[i];
                    comd.FK_OpenId = openid;
                    comd.orderid = OrderGUID;
                    comd.ordernumber = OrderNumber;
                    comd.type = typeNumbers[i];
                    comd.content = evals[i];
                    comd.addtime = DateTime.Now;
                    comd.adduser = openid;
                    comd.zancount = 0;
                    comd.guige = productParams[i];
                    comd.isniming = n_name ? "1" : "0";
                    comments.Add(comd);
                }
                db.Insert<T_CommentDetailEntity>(comments);

                T_CommentEntity com = new T_CommentEntity();
                com.Create();
                com.FK_OpenId = openid;
                com.orderid = OrderGUID;
                com.ordernumber = OrderNumber;
                com.addtime = DateTime.Now;
                com.adduser = openid;
                com.zancount = 0;
                com.star1 = m_des;
                com.star2 = m_wuliu;
                com.star3 = m_du;
                com.isniming = n_name ? "1" : "0";

                db.Insert<T_CommentEntity>(com);

                db.Commit();

                result.msg = "";
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                db.Rollback();
                result.msg = "保存失败";
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        private List<ClassInfo> GetClassItemList(IList<VSTOCKEntity> vstockList, List<T_PRODUCTCLASSEntity> list, ref Dictionary<string, List<string>> dic, string classtype = null)
        {
            try
            {
                List<ClassInfo> resultlist = list.Where(p => p.ProductClass == classtype).Select(p => new ClassInfo
                {
                    ClassGUID = p.GUID,
                    ClassValue = p.ClassValue,
                    ClassPrice = p.ClassPrice ?? 0,
                    ClassOldPrice = p.ClassOldPrice ?? 0
                }).ToList();

                List<string> valuelist = new List<string>();

                foreach (ClassInfo classinfo in resultlist)
                {
                    T_PRODUCTCLASSEntity newpdtclass = list.Where(p => p.GUID == classinfo.ClassGUID).FirstOrDefault();

                    if (newpdtclass != null)
                    {
                        classinfo.ClassStockNum = newpdtclass.ProductStock.Value;

                        if (!CodeConst.IsUseStock)
                        {
                            classinfo.ClassStock = 1000;
                        }
                        else
                        {
                            classinfo.ClassStock = vstockList.Where(p => p.SKU == newpdtclass.ProductSKU).Select(p => p.Stock).FirstOrDefault();
                            if (classinfo.ClassStock == null)
                            {
                                classinfo.ClassStock = Decimal.Zero;
                            }
                        }
                    }

                    if (valuelist.Contains(classinfo.ClassValue))
                    {
                        classinfo.ClassShow = true;
                    }
                    else
                    {
                        valuelist.Add(classinfo.ClassValue);
                        classinfo.ClassShow = false;
                    }
                    List<string> strlist = new List<string>();
                    string productclass = list.Where(p => p.FK_ParentClass == classinfo.ClassGUID).Select(p => p.ProductClass).FirstOrDefault();
                    if (productclass != null)
                    {
                        List<ClassInfo> allvaluelist = list.Where(p => p.ProductClass == productclass).Select(p => new ClassInfo
                        {
                            ClassGUID = p.GUID,
                            ClassValue = p.ClassValue
                        }).ToList();
                        List<string> chooselist = list.Where(p => p.FK_ParentClass == classinfo.ClassGUID).Select(p => p.GUID).ToList();
                        foreach (ClassInfo info in allvaluelist)
                        {
                            if (chooselist.Contains(info.ClassGUID))
                            {
                                strlist.Add(info.ClassGUID);
                            }
                        }
                    }

                    dic[classinfo.ClassGUID] = strlist;
                }

                return resultlist;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return new List<ClassInfo>();
            }
        }

        private string GetParameterStr(IList<T_PRODUCTCLASSEntity> productclassList, string productclass, decimal pdtcount)
        {
            string parameterStr = String.Empty;
            T_PRODUCTCLASSEntity newclass = productclassList.Where(p => p.GUID == productclass && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
            while (newclass != null)
            {
                if (parameterStr == String.Empty)
                {
                    parameterStr = String.Format("{0}:{1}", newclass.ProductClass, newclass.ClassValue);
                }
                else
                {
                    parameterStr = String.Format("{0}:{1}", newclass.ProductClass, newclass.ClassValue) + Environment.NewLine + parameterStr;
                }
                parameterStr = String.Format("数量:{0}", pdtcount) + Environment.NewLine + parameterStr;
                newclass = productclassList.Where(p => p.GUID == newclass.FK_ParentClass && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
            }
            return parameterStr;
        }

    }
}
