﻿using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Application.Service.Cache;
using MMM.Application.Service.Order;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WSC
{
    public class WXManageDAL
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        public string KFUserUpdate(string appguid, string userId, string password)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                UserEntity userEntity = db.IQueryable<UserEntity>().Where(p => p.Account == userId).FirstOrDefault();
                if (userEntity != null)
                {
                    string jmpassword = Md5Helper.MD5(password, 32);
                    string dbPassword = Md5Helper.MD5(DESEncrypt.Encrypt(jmpassword.ToLower(), userEntity.Secretkey).ToLower(), 32).ToLower();
                    if (dbPassword == userEntity.Password)
                        result.IsOK = true;
                }

            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 优惠券是否使用
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public string IsCardUse(string card)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                T_OUTCARDEntity outcard = db.IQueryable<T_OUTCARDEntity>().Where(p => p.GUID == card && p.IsUse == CodeConst.IsUse.NO && p.IsDel == CodeConst.IsDel.NO).FirstOrDefault();
                if (outcard != null)
                {
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 使用优惠券
        /// </summary>
        /// <param name="card"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string UseCard(string card, string userId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TUSERCARDEntity usercard = db.IQueryable<TUSERCARDEntity>().Where(p => p.GUID == card && p.IsUse == CodeConst.CardUse.NOUSE && p.IsDel == CodeConst.IsDel.NO).FirstOrDefault();
                if (usercard != null)
                {
                    usercard.IsUse = CodeConst.CardUse.ISUSE;
                    usercard.Modifyer = userId;
                    usercard.ModifyTime = DateTime.Now;
                    db.Update<TUSERCARDEntity>(usercard, null);
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 订单
        /// </summary>
        /// <param name="wxopenId"></param>
        /// <returns></returns>
        public string GetStoreOrderList(string wxopenId, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {

                IList<TORDEREntity> orderList = db.IQueryable<TORDEREntity>().Where(p => p.organizeid == organizeid && p.FK_Openid == wxopenId && p.IsDel == CodeConst.IsDel.NO && p.DeliveryMode == CodeConst.DeliveryMode.STORE && p.OrderStatus == CodeConst.OrderStatus.DELIVER && (p.GroupResult == null || p.GroupResult == CodeConst.IsUse.YES)).OrderByDescending(p => p.CreateTime).ToList();
                List<OrderModel> resultList = orderList.Select(p => new OrderModel
                {
                    GUID = p.GUID,
                    OrderNumber = p.OrderNumber,
                    OrderStatus = p.OrderStatus ?? 0,
                    DeliveryMode = p.DeliveryMode ?? 0,
                    OrderPrice = p.OrderPrice ?? 0,
                    CreateTime = p.CreateTime
                }).ToList();

                IList<String> orderids = resultList.Select(t => t.GUID).ToList();
                IList<TORDERDETAILEntity> orderdetailList = db.IQueryable<TORDERDETAILEntity>().Where(t => t.IsDel == CodeConst.IsDel.NO && orderids.Contains(t.FK_Order)).ToList();
                IList<T_PRODUCTCLASSEntity> productclassList = new ProductClassCache().GetCacheList().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == organizeid).ToList();
                IList<String> productids = productclassList.Select(t => t.FK_Product).ToList();
                IList<VPRODUCTEntity> vproductList = db.IQueryable<VPRODUCTEntity>().Where(t => t.organizeid == organizeid && productids.Contains(t.ProductGUID)).ToList();
                foreach (OrderModel model in resultList)
                {
                    model.ProductList = orderdetailList.Where(p => p.FK_Order == model.GUID)
                        .Join(productclassList
                        .Join(vproductList, x => x.FK_Product, y => y.ProductGUID, (x, y) => new
                        {
                            ItemCode = x.GUID,
                            ImageURL = y.ImageURL,
                            ProductName = y.ProductName,
                            ProductDescribe = y.ProductDescribe,
                            ProductPrice = y.NewPrice,
                            OldPrice = y.OldPrice
                        }), m => m.FK_ProductClass, n => n.ItemCode, (m, n) => new ShopItem
                        {
                            ItemCode = n.ItemCode,
                            ImageURL = n.ImageURL,
                            ProductName = n.ProductName,
                            ProductDescribe = n.ProductDescribe,
                            ProductPrice = n.ProductPrice,
                            OldPrice = n.OldPrice,
                            ProductCount = m.Count ?? 0
                        }).ToList();

                    foreach (ShopItem item in model.ProductList)
                    {
                        item.ProductClassList = new List<string>();
                        string classguid = item.ItemCode;
                        while (classguid != null)
                        {
                            T_PRODUCTCLASSEntity productclass = productclassList.Where(p => p.GUID == classguid).SingleOrDefault();
                            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                            item.ProductClassList.Insert(0, classstr);
                            classguid = productclass.FK_ParentClass;
                        }
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(resultList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public string GetStoreOrderPHList(string userId, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                userId = userId.ToUpper();
                List<OrderModel> resultList = db.IQueryable<TORDEREntity>().Where(p => p.Distribution == userId && p.DistributionTime == null && p.IsDel == CodeConst.IsDel.NO && p.OrderStatus == CodeConst.OrderStatus.DELIVER).Select(p => new OrderModel
                {
                    GUID = p.GUID,
                    OrderNumber = p.OrderNumber,
                    OrderStatus = p.OrderStatus ?? 0,
                    DeliveryMode = p.DeliveryMode ?? 0,
                    OrderPrice = p.OrderPrice ?? 0,
                    CreateTime = p.CreateTime
                }).OrderByDescending(p => p.CreateTime).ToList();

                IList<String> orderids = resultList.Select(t => t.GUID).ToList();
                IList<T_PRODUCTCLASSEntity> productclassList = db.IQueryable<T_PRODUCTCLASSEntity>().Where(t => t.IsDel == CodeConst.IsDel.NO && t.organizeid == organizeid).ToList();

                var productList = db.IQueryable<TORDERDETAILEntity>().Where(p => orderids.Contains(p.FK_Order) && p.IsDel == CodeConst.IsDel.NO)
                        .Join(db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO)
                        .Join(db.IQueryable<VPRODUCTEntity>(), x => x.FK_Product, y => y.ProductGUID, (x, y) => new
                        {
                            ItemCode = x.GUID,
                            ImageURL = y.ImageURL,
                            ProductName = y.ProductName,
                            ProductDescribe = y.ProductDescribe,
                            ProductPrice = y.NewPrice,
                            OldPrice = y.OldPrice
                        }), m => m.FK_ProductClass, n => n.ItemCode, (m, n) => new ShopItem
                        {
                            ItemCode = n.ItemCode,
                            ImageURL = n.ImageURL,
                            ProductName = n.ProductName,
                            ProductDescribe = n.ProductDescribe,
                            ProductPrice = n.ProductPrice,
                            OldPrice = n.OldPrice,
                            ProductCount = m.Count ?? 0,
                            orderid = m.FK_Order
                        }).ToList();
                foreach (OrderModel model in resultList)
                {
                    model.ProductList = productList.Where(t => model.GUID == t.orderid).ToList();

                    foreach (ShopItem item in model.ProductList)
                    {
                        item.ProductClassList = new List<string>();
                        string classguid = item.ItemCode;
                        while (classguid != null)
                        {
                            T_PRODUCTCLASSEntity productclass = productclassList.Where(p => p.GUID == classguid).SingleOrDefault();
                            string classstr = String.Format("{0}：{1}", productclass.ProductClass, productclass.ClassValue);
                            item.ProductClassList.Insert(0, classstr);
                            classguid = productclass.FK_ParentClass;
                        }
                    }
                }
                result.Data = JSONHelper.WXObjectToJson(resultList);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 完成订单
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="guid"></param>
        /// <param name="warehouseNo"></param>
        /// <returns></returns>
        public string KFConfirmOrder(string userId, string guid, string warehouseNo)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                db.BeginTrans();
                userId = userId.ToUpper();
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO && p.DeliveryMode == CodeConst.DeliveryMode.STORE && p.OrderStatus == CodeConst.OrderStatus.DELIVER).SingleOrDefault();
                if (order != null)
                {
                    int integral = Convert.ToInt32(order.OrderPrice / CodeConst.Integral.PAYORDER);

                    TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FK_Openid && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();
                    if (user != null)
                    {
                        user.Integral += integral;
                        TORDERService tORDERService = new TORDERService();
                        TINTEGRALEntity tintegral = tORDERService.IntegralRecord(order.FK_Openid, order.FK_AppId, integral, user.Integral.Value, CodeConst.IntegralType.PAYORDER, CodeConst.IntegralRemark.PAYORDER);
                        db.Insert<TINTEGRALEntity>(tintegral);
                        db.Update<TUSEREntity>(user, null);
                    }
                    order.FK_ActualStore = warehouseNo;
                    order.ActualPickUpTime = DateTime.Now;
                    order.OrderStatus = CodeConst.OrderStatus.COMPLETE;
                    order.Completer = userId + "(小程序)";
                    order.CompleteTime = DateTime.Now;
                    order.Modifyer = userId + "(小程序)";
                    order.ModifyTime = DateTime.Now;
                    db.Update<TORDEREntity>(order, null);
                    result.IsOK = true;
                }
                else
                {
                    result.msg = "该订单无法确认！";
                }

                if (CodeConst.IsUseStock && result.IsOK)
                {
                    List<TORDERSTOCKEntity> list = db.IQueryable<TORDERSTOCKEntity>().Where(p => p.FK_Order == guid && p.Status == CodeConst.OrderStockStatus.YTC).ToList();
                    IList<String> stockids = list.Select(t => t.FK_Stock).ToList();
                    IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(t => t.IsDel == 0 && stockids.Contains(t.GUID)).ToList();
                    foreach (TORDERSTOCKEntity orderstock in list)
                    {
                        orderstock.Status = CodeConst.OrderStockStatus.YWC;
                        db.Update<TORDERSTOCKEntity>(orderstock, null);

                        TSTOCKEntity stockInfo = stockList.Where(p => p.GUID == orderstock.FK_Stock).SingleOrDefault();

                        if (stockInfo != null)
                        {
                            stockInfo.ActualStock -= orderstock.StockCount;
                            db.Update<TSTOCKEntity>(stockInfo, null);

                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                            stockdetail.ID = Guid.NewGuid().ToString();
                            stockdetail.FK_Stock = stockInfo.GUID;
                            stockdetail.ActualStock = -orderstock.StockCount;
                            stockdetail.Type = CodeConst.StockDetailType.CK;
                            stockdetail.Remark = "小程序订单完成(小程序)";
                            stockdetail.Creater = userId;
                            stockdetail.CreateTime = DateTime.Now;
                            stockdetail.organizeid = order.organizeid;
                            db.Insert<TSTOCKDEAILEntity>(stockdetail);

                            result.IsOK = true;
                        }
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        /// <summary>
        /// 订单确认
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string KFPHConfirmOrder(string userId, string guid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity order = db.IQueryable<TORDEREntity>().Where(p => p.GUID == guid && p.IsDel == CodeConst.IsDel.NO && p.OrderStatus == CodeConst.OrderStatus.DELIVER).SingleOrDefault();
                if (order != null)
                {
                    order.DistributionTime = DateTime.Now;
                    order.Modifyer = userId;
                    order.ModifyTime = DateTime.Now;
                    db.Update<TORDEREntity>(order, null);
                    result.IsOK = true;
                }
                else
                {
                    result.msg = "该订单无法确认！";
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public string GetProductInfoByCode(string sku, String organizeid)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                ProductManage productmanage = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.ProductSKU == sku)
                    .Join(db.IQueryable<T_PRODUCTPRICEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.FK_Product, n => n.GUID, (m, n) => new ProductManage
                    {
                        ProductName = n.Name,
                        ProductUnit = n.ProductUnit,
                        ProductCategory = n.GUID,
                        ProductClass = m.GUID,
                        ProductOldPrice = m.ClassOldPrice ?? 0,
                        ProductPrice = m.ClassPrice ?? 0
                    }).FirstOrDefault();

                VSTOCKEntity stockInfo = db.IQueryable<VSTOCKEntity>().Where(p => p.SKU == sku).FirstOrDefault();

                productmanage.ProductStock = stockInfo.Stock == null ? Decimal.Zero : stockInfo.Stock.Value;
                productmanage.ProductSaleCount = stockInfo.ActualStock == null ? Decimal.Zero : stockInfo.ActualStock.Value;

                List<string> categorylist = db.IQueryable<T_CATEGORY_PRODUCTEntity>().Where(p => p.FK_Product == productmanage.ProductCategory)
.Join(db.IQueryable<T_PRODUCTCATEGORYEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.FK_Category, n => n.GUID, (m, n) => n.CategoryName).ToList();
                productmanage.ProductCategory = String.Empty;
                foreach (string categorystr in categorylist)
                {
                    productmanage.ProductCategory += String.Format("{0},", categorystr);
                }
                if (productmanage.ProductCategory.Length > 0)
                {
                    productmanage.ProductCategory = productmanage.ProductCategory.Substring(0, productmanage.ProductCategory.Length - 1);
                }
                else
                {
                    productmanage.ProductCategory = "暂无分类信息";
                }

                productmanage.ClassList = new List<ClassManage>();
                string classguid = productmanage.ProductClass;
                while (classguid != null)
                {
                    T_PRODUCTCLASSEntity productclass = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == classguid).SingleOrDefault();
                    if (productclass != null)
                    {
                        ClassManage item = new ClassManage();
                        item.ClassName = productclass.ProductClass;
                        item.ClassValue = productclass.ClassValue;
                        productmanage.ClassList.Add(item);
                        classguid = productclass.FK_ParentClass;
                    }
                    else
                    {
                        classguid = null;
                    }
                }

                result.Data = JSONHelper.WXObjectToJson(productmanage);
                result.IsOK = true;

            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }
    }
}
