﻿using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.SystemManage;
using MMM.Application.Entity.UserInfo;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using WxPayAPI;

namespace MMM.Application.Service.WSC
{
    public class WXPayDAL
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        public string GetWXPayOrder(string appguid, string openId, string orderguid, int isUseMemberCard)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TORDEREntity orderInfo = db.IQueryable<TORDEREntity>().FirstOrDefault(p => p.GUID == orderguid);
                if (orderInfo == null)
                {
                    result.msg = "订单不存在！";
                    return JSONHelper.WXObjectToJson(result);
                }

                TWXAPPEntity wxapp = null;

                VWXPAYEntity wxpay = db.IQueryable<VWXPAYEntity>().Where(p => p.out_trade_no == orderguid).SingleOrDefault();
                if (wxpay == null) wxpay = new VWXPAYEntity();

                string groupnumber = orderInfo.GroupNumber;
                TORDEREntity peopleorder = db.IQueryable<TORDEREntity>().Where(p => p.GroupNumber == groupnumber && p.IsDel == CodeConst.IsDel.NO && p.GroupResult == CodeConst.IsUse.YES && p.GroupRole == CodeConst.GroupRole.INITIATOR).SingleOrDefault();
                if (peopleorder != null)
                {
                    return String.Empty;
                }
                wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == appguid).SingleOrDefault();

                if (isUseMemberCard == CodeConst.IsUse.YES)
                {
                    try
                    {
                        TMEMBEREntity member = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId).Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).FirstOrDefault();
                        if (member != null)
                        {
                            db.BeginTrans();
                            TMONEYDETAILEntity moneydetail = new TMONEYDETAILEntity();
                            moneydetail.ID = Guid.NewGuid().ToString();
                            moneydetail.FK_Member = member.GUID;
                            moneydetail.Remark = String.Format("订单[{0}]消费", orderInfo.OrderNumber);
                            moneydetail.Creater = openId;
                            moneydetail.CreateTime = DateTime.Now;
                            moneydetail.organizeid = orderInfo.organizeid;
                            if (member.Money >= orderInfo.OrderPrice)
                            {
                                member.Money -= orderInfo.OrderPrice;
                                moneydetail.MoneyChange = -orderInfo.OrderPrice;
                                wxpay.total_fee = null;
                                orderInfo.OrderStatus = CodeConst.OrderStatus.DELIVER;
                                orderInfo.PayWay = CodeConst.PayWay.CARDPAY;
                                orderInfo.PayResult = CodeConst.PayResult.PayYes;
                            }
                            else
                            {
                                wxpay.total_fee -= member.Money * 100;
                                moneydetail.MoneyChange = -member.Money;
                                member.Money = Decimal.Zero;
                                orderInfo.PayWay = CodeConst.PayWay.HHPAY;
                            }
                            db.Update<TORDEREntity>(orderInfo, null);
                            db.Insert<TMONEYDETAILEntity>(moneydetail);
                            db.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        db.Rollback();
                        throw new Exception(e.ToString());
                    }
                }

                if (wxpay.total_fee == null)
                {
                    result.IsOK = true;
                    return JSONHelper.WXObjectToJson(result);
                }

                WxPayData wxpaydata = new WxPayData();
                wxpaydata.SetValue("out_trade_no", orderInfo.OrderNumber);
                wxpaydata.SetValue("appid", wxapp.AppId);
                wxpaydata.SetValue("sign_type", "MD5");
                wxpaydata.SetValue("body", wxpay.body);
                wxpaydata.SetValue("detail", wxpay.detail);
                wxpaydata.SetValue("total_fee", Convert.ToInt32(wxpay.total_fee.Value));
                wxpaydata.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));
                wxpaydata.SetValue("time_expire", DateTime.Now.AddMinutes(CodeConst.PayMinutes).ToString("yyyyMMddHHmmss"));
                wxpaydata.SetValue("trade_type", "JSAPI");
                wxpaydata.SetValue("openid", openId);

                WxPayAPI.JsApiPay jsApiPay = new JsApiPay();
                WxPayData unifiedOrderResult = jsApiPay.GetUnifiedOrderResult(wxpaydata, wxapp);

                return jsApiPay.GetJsApiParameters(orderguid, openId, wxapp);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        /// <summary>
        /// 会员支付
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="openId"></param>
        /// <returns></returns>
        public string MemberPay(string appId, string openId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                //判断是否是商城会员
                TMEMBEREntity member = db.IQueryable<TMEMBEREntity>().FirstOrDefault(t => t.FK_UnionId == openId);
                if (member != null)
                {
                    result.msg = "您已经是会员！";
                    result.IsOK = true;
                    return JSONHelper.WXObjectToJson(result);
                }

                TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == appId).SingleOrDefault();

                Random rd = new Random();
                String number = "HY" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + rd.Next(9999).ToString().PadLeft(4, '0'); ;

                DataItemDetailEntity dataItemDetail = db.IQueryable<DataItemDetailEntity>().FirstOrDefault(t => t.ItemName == "NewPrice");
                int money = Int32.Parse((Double.Parse(dataItemDetail.ItemValue) * 100).ToString());

                WxPayData wxpaydata = new WxPayData();
                wxpaydata.SetValue("out_trade_no", number);
                wxpaydata.SetValue("appid", wxapp.AppId);
                wxpaydata.SetValue("sign_type", "MD5");
                wxpaydata.SetValue("body", "有恋会员");
                wxpaydata.SetValue("detail", "会员升级");
                wxpaydata.SetValue("total_fee", money);
                wxpaydata.SetValue("time_start", DateTime.Now.ToString("yyyyMMddHHmmss"));
                wxpaydata.SetValue("time_expire", DateTime.Now.AddMinutes(CodeConst.PayMinutes).ToString("yyyyMMddHHmmss"));
                wxpaydata.SetValue("trade_type", "JSAPI");
                wxpaydata.SetValue("openid", openId);

                WxPayAPI.JsApiPay jsApiPay = new JsApiPay();
                WxPayData unifiedOrderResult = jsApiPay.GetUnifiedOrderResult(wxpaydata, wxapp);

                return jsApiPay.GetJsApiParametersMember(openId, wxapp);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        public string PayWXPayOrder(string appguid, string openId, string orderguid)
        {
            Data.IDatabase db = DbFactory.Base();
            try
            {
                string groupnumber = db.IQueryable<TORDEREntity>().Where(p => p.GUID == orderguid).Select(p => p.GroupNumber).SingleOrDefault();
                TORDEREntity peopleorder = db.IQueryable<TORDEREntity>().Where(p => p.GroupNumber == groupnumber && p.IsDel == CodeConst.IsDel.NO && p.GroupResult == CodeConst.IsUse.YES && p.GroupRole == CodeConst.GroupRole.INITIATOR).SingleOrDefault();
                if (peopleorder != null)
                {
                    return String.Empty;
                }

                JsApiPay jsApiPay = new JsApiPay();

                TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == appguid).SingleOrDefault();
                return jsApiPay.GetJsApiParametersByOrderGUID(wxapp.AppId, openId, orderguid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        public void ResultNotify(Page page)
        {
            ResultNotify resultNotify = new ResultNotify(page);
            resultNotify.ProcessNotify();
        }
    }
}
