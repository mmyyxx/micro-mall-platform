﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WSC
{
    public class AdressModel
    {
        public string GUID { set; get; }

        public string AdressName { set; get; }

        public string AdressPlace { set; get; }

        public string MoreAdress { set; get; }

        public string Mobile { set; get; }

        public decimal? Longitude { set; get; }

        public decimal? Latitude { set; get; }

        public int IsUse { set; get; }
    }
}
