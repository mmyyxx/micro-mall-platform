﻿using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.ShoppingMall;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WSC
{
    public class WXClassDAL
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");
        private int PageSize = 8;

        /// <summary>
        /// 项目搜索
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="categoryId"></param>
        /// <param name="searchstr"></param>
        /// <param name="orderstr"></param>
        /// <param name="ordertype"></param>
        /// <param name="pageIndex"></param>
        /// <param name="isLimit"></param>
        /// <param name="isGroup"></param>
        /// <returns></returns>
        public string GetProductSearchData(string appId, string categoryId, string searchstr, string orderstr, string ordertype, int pageIndex, int isLimit, int isGroup)
        {
            Data.IDatabase db = DbFactory.Base();
            List<string> productGuidList = null;
            IQueryable<VPRODUCTEntity> productQuery = db.IQueryable<VPRODUCTEntity>();
            if (!String.IsNullOrEmpty(categoryId))
            {
                productGuidList = db.IQueryable<T_PRODUCTCATEGORYEntity>().Where(t => (t.GUID == categoryId || t.FK_ParentCategory == categoryId) && t.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<T_CATEGORY_PRODUCTEntity>(), m => m.GUID, n => n.FK_Category, (m, n) => n.FK_Product).ToList();

                productQuery = productQuery.Where(t => productGuidList.Contains(t.ProductGUID));
            }

            //if (isLimit.ToString().Equals(CodeConst.IsUse.NO.ToString()))
            //{
            //    productQuery = productQuery.Where(t => t.IsLimitTimeProduct == isLimit);
            //}
            //else
            //{
            //    productQuery = productQuery.Where(t => t.LimitStartTime <= DateTime.Now && t.LimitEndTime >= DateTime.Now);
            //}
            productQuery = productQuery.Where(t => t.IsGroupProduct == isGroup);
            if (!String.IsNullOrEmpty(searchstr))
            {
                productQuery = productQuery.Where(t => (t.ProductKey.Contains(searchstr) || t.ProductName.Contains(searchstr)));
            }
            productQuery = productQuery.Where(t => t.IsSale == CodeConst.IsSale.IsShelves);

            var result = productQuery.Select(p => new SearchItem
            {
                ProductGUID = p.ProductGUID,
                ProductName = p.ProductName,
                ImageURL = p.ImageURL,
                ProductDescribe = p.ProductDescribe,
                NewPrice = p.NewPrice,
                OldPrice = p.OldPrice,
                ProductComprehensive = p.ProductName,
                ProductSale = 0,
                ProductZan = p.ProductZan,
                GroupPeople = p.GroupPeople,
                ProductUnit = p.ProductUnit,
                IsLimit = p.IsLimitTimeProduct.Value
            });

            if (ordertype == CodeConst.OrderType.DESC)
            {
                if (orderstr == "NewPrice")
                {
                    result = result.OrderByDescending(p => p.NewPrice).Skip(pageIndex * PageSize).Take(PageSize);
                }
                else if (orderstr == "ProductSale")
                {
                    result = result.OrderByDescending(p => p.ProductSale).Skip(pageIndex * PageSize).Take(PageSize);
                }
                else if (orderstr == "Hot")
                {
                    result = result.OrderByDescending(p => p.ProductZan).Skip(pageIndex * PageSize).Take(PageSize);
                }
                else
                {
                    result = result.OrderByDescending(p => p.ProductComprehensive).Skip(pageIndex * PageSize).Take(PageSize);
                }
            }
            else
            {
                if (orderstr == "NewPrice")
                {
                    result = result.OrderBy(p => p.NewPrice).Skip(pageIndex * PageSize).Take(PageSize);
                }
                else if (orderstr == "ProductSale")
                {
                    result = result.OrderBy(p => p.ProductSale).Skip(pageIndex * PageSize).Take(PageSize);
                }
                else if (orderstr == "Hot")
                {
                    result = result.OrderByDescending(p => p.ProductZan).Skip(pageIndex * PageSize).Take(PageSize);
                }
                else
                {
                    result = result.OrderBy(p => p.ProductComprehensive).Skip(pageIndex * PageSize).Take(PageSize);
                }
            }

            String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];
            List<SearchItem> resultList = result.ToList();

            IList<String> ids = result.Select(t => t.ProductGUID).ToList();

            IList<TMODELPRODUCTFruitsEntity> fruitsEntities = db.IQueryable<TMODELPRODUCTFruitsEntity>().Where(t => ids.Contains(t.ImageNavigate1) && t.IsDel == 0 && t.time1.HasValue).ToList();

            foreach (SearchItem item in resultList)
            {
                item.ProductUnit = String.IsNullOrEmpty(item.ProductUnit) ? "" : item.ProductUnit;

                if (!String.IsNullOrEmpty(item.ImageURL) && !item.ImageURL.StartsWith("http:") && !item.ImageURL.StartsWith("https:"))
                {
                    item.ImageURL = ImgUrl + item.ImageURL;
                }

                TMODELPRODUCTFruitsEntity fruitsEntity = fruitsEntities.FirstOrDefault(t => t.ImageNavigate1 == item.ProductGUID);
                if (fruitsEntity != null && fruitsEntity.time1.HasValue && fruitsEntity.time2.HasValue)//限时秒杀商品
                {
                    if (fruitsEntity.num1 > fruitsEntity.num2)
                    {
                        DateTime now = DateTime.Now;
                        if (now > fruitsEntity.time2.Value && now < fruitsEntity.time1.Value)
                        {
                            item.NewPrice = fruitsEntity.Price1 ?? 0;
                        }
                    }
                }
            }
            return JSONHelper.WXObjectToJson(resultList);
        }

        /// <summary>
        /// 获取分类class信息
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetClassData(string appId, String organizeid)
        {
            Data.IDatabase db = DbFactory.Base();

            try
            {
                IList<T_PRODUCTCATEGORYEntity> t_PRODUCTCATEGORYEntities = db.IQueryable<T_PRODUCTCATEGORYEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.IsUse.YES).ToList();
                IList<T_PICTUREEntity> t_PICTUREEntities = db.IQueryable<T_PICTUREEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Type == CodeConst.PictureType.Category).ToList();

                List<CategoryModel> resultList = t_PRODUCTCATEGORYEntities.Where(p => p.FK_ParentCategory == null).OrderBy(p => p.ModifyTime).Select(p => new CategoryModel
                {
                    isChecked = false,
                    CategoryCode = p.GUID,
                    CategoryName = p.CategoryName,
                    OrderBy = p.OrderBy ?? 0
                }).OrderBy(p => p.OrderBy).ToList();

                bool isFirst = true;

                String ImgUrl = System.Configuration.ConfigurationManager.AppSettings["ImgUrl"];
                foreach (CategoryModel categorymodel in resultList)
                {
                    categorymodel.ChildCategoryList = t_PRODUCTCATEGORYEntities.Where(p => p.FK_ParentCategory == categorymodel.CategoryCode).OrderBy(p => p.CategoryName)
                        .Join(t_PICTUREEntities, m => m.GUID, n => n.FK_GUID, (m, n) => new CategoryModel
                        {
                            CategoryCode = m.GUID,
                            CategoryName = m.CategoryName,
                            ImageURL = n.ImageURL,
                            OrderBy = m.OrderBy ?? 0
                        }).OrderBy(p => p.OrderBy).ToList();

                    if (!String.IsNullOrEmpty(categorymodel.ImageURL) && !categorymodel.ImageURL.StartsWith("http:") && !categorymodel.ImageURL.StartsWith("https:"))
                    {
                        categorymodel.ImageURL = ImgUrl + categorymodel.ImageURL;
                    }
                    foreach (CategoryModel second in categorymodel.ChildCategoryList)
                    {
                        if (!String.IsNullOrEmpty(second.ImageURL) && !second.ImageURL.StartsWith("http:") && !second.ImageURL.StartsWith("https:"))
                        {
                            second.ImageURL = ImgUrl + second.ImageURL;
                        }

                        second.ChildCategoryList = t_PRODUCTCATEGORYEntities.Where(p => p.FK_ParentCategory == second.CategoryCode).OrderBy(p => p.CategoryName)
                            .Join(t_PICTUREEntities, m => m.GUID, n => n.FK_GUID, (m, n) => new CategoryModel
                            {
                                CategoryCode = m.GUID,
                                CategoryName = m.CategoryName,
                                ImageURL = n.ImageURL,
                                OrderBy = m.OrderBy ?? 0
                            }).OrderBy(p => p.OrderBy).ToList();
                        foreach (CategoryModel thrid in second.ChildCategoryList)
                        {
                            if (!String.IsNullOrEmpty(thrid.ImageURL) && !thrid.ImageURL.StartsWith("http:") && !thrid.ImageURL.StartsWith("https:"))
                            {
                                thrid.ImageURL = ImgUrl + thrid.ImageURL;
                            }
                        }
                    }

                    if (isFirst)
                    {
                        categorymodel.isChecked = true;
                        isFirst = false;
                    }
                }

                return JSONHelper.WXObjectToJson(resultList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        /// <summary>
        /// 获取首页商品
        /// </summary>
        /// <returns></returns>
        public string GetIndexProduct()
        {
            try
            {
                return GetCategoryProduct("c6bb4990f36744248838f7604b93e6fd", null);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return "";
            }
        }

        /// <summary>
        /// 获取分类数据
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetCategoryDataByGUID(string guid)
        {
            try
            {
                Data.IDatabase db = DbFactory.Base();

                IList<T_PRODUCTCATEGORYEntity> t_PRODUCTCATEGORYEntities = db.IQueryable<T_PRODUCTCATEGORYEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.IsUse == CodeConst.IsUse.YES && p.FK_ParentCategory == guid).ToList();
                IList<T_PICTUREEntity> t_PICTUREEntities = db.IQueryable<T_PICTUREEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Type == CodeConst.PictureType.Category).ToList();

                List<CategoryModel> result = t_PRODUCTCATEGORYEntities
                    .Join(t_PICTUREEntities, m => m.GUID, n => n.FK_GUID, (m, n) => new CategoryModel
                    {
                        CategoryCode = m.GUID,
                        CategoryName = m.CategoryName,
                        ImageURL = n.ImageURL,
                        OrderBy = m.OrderBy ?? 0
                    }).OrderBy(p => p.OrderBy).ToList();

                return JSONHelper.WXObjectToJson(result);
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// 根据分类获取商品
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="nowtime"></param>
        /// <returns></returns>
        public string GetCategoryProduct(string guid, DateTime? nowtime)
        {
            Data.IDatabase db = DbFactory.Base();

            IQueryable<T_PRODUCTPRICEEntity> query = db.IQueryable<T_PRODUCTPRICEEntity>(p => p.IsDel == CodeConst.IsDel.NO && p.IsSale == CodeConst.IsSale.IsShelves);

            if (nowtime.HasValue)
            {
                query = query.Where(p => p.LimitStartTime <= nowtime && p.LimitEndTime >= nowtime);
            }

            IList<T_PRODUCTPRICEEntity> result = query.Join(db.IQueryable<T_CATEGORY_PRODUCTEntity>().Where(p => p.FK_Category == guid), m => m.GUID, n => n.FK_Product, (m, n) => m).ToList();

            IList<String> pdtGuids = result.Select(t => t.GUID).ToList();
            IList<T_PRODUCTCLASSEntity> pRODUCTCLASSEntities = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && pdtGuids.Contains(p.FK_Product)).ToList();

            IList<String> skus = pRODUCTCLASSEntities.Select(t => t.ProductSKU).ToList();
            IList<VSTOCKEntity> stockList = db.IQueryable<VSTOCKEntity>().Where(p => skus.Contains(p.SKU)).ToList();
            foreach (T_PRODUCTPRICEEntity pdt in result)
            {
                decimal? stock = stockList.Join(pRODUCTCLASSEntities.Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Product == pdt.GUID), m => m.SKU, n => n.ProductSKU, (m, n) => m).Sum(p => p.Stock);
                if (stock != null && stock.Value > Decimal.Zero)
                {
                    pdt.Remark = "0";
                }
                else
                {
                    pdt.Remark = CodeConst.IsUseStock ? "1" : "0";
                }
            }

            return JSONHelper.WXObjectToJson(result);
        }
    }
}
