﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WSC
{
    public class WSCSql
    {
        internal static Hashtable GetUpdateProductSql(string SKU, decimal pdtCount, int cnt, Hashtable result)
        {
            try
            {
                string sql = String.Format(@"UPDATE T_PRODUCT
                                         SET SaleCount += @SaleCount
                                         WHERE SKU = '{0}' AND {1} = {1}", SKU, cnt);
                SqlParameter[] param = new SqlParameter[]{
                new SqlParameter("@SaleCount", SqlDbType.Decimal) { Value = pdtCount}
            };
                result.Add(sql, param);
            }
            catch (Exception ex)
            {
            }
            return result;
        }
    }
}
