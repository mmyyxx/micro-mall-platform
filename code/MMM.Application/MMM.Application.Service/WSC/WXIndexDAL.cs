﻿using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Entity.WebServiceSmall;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.WSC
{
    /// <summary>
    /// 小程序逻辑
    /// </summary>
    public class WXIndexDAL
    {
        private int PageSize = 3;
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        public Boolean GetGoldEggStart()
        {
            string dateStr = DateTime.Now.ToString("yyyy-MM-dd");
            if (dateStr.Equals("2018-11-10") || dateStr.Equals("2018-11-11"))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 获取轮播图片
        /// </summary>
        /// <param name="appguid"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetSwiperPictures(string appguid)
        {
            try
            {
                Data.IDatabase db = DbFactory.Base();

                List<string> list = db.IQueryable<T_PICTUREEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.Type == CodeConst.PictureType.Swiper).OrderBy(p => p.ImageName).Select(p => p.ImageURL).ToList();
                return JSONHelper.WXObjectToJson(list);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        /// <summary>
        /// 获取项目信息
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="appguid"></param>
        /// <param name="organizeid"></param>
        /// <returns></returns>
        public string GetLimitedProducts(int pageIndex, string appguid, String organizeid)
        {
            try
            {
                Data.IDatabase db = DbFactory.Base();

                List<LimitProduct> resultList = db.IQueryable<T_PRODUCTPRICEEntity>().Where(p => p.organizeid == organizeid && p.IsLimitTimeProduct == CodeConst.IsUse.YES && p.IsGroupProduct == CodeConst.IsUse.NO).Select(p => new LimitProduct
                {
                    ImageURL = p.ProductMainPicture,
                    ProductName = p.Name,
                    ProductDescribe = p.ProductParameter,
                    NewPrice = p.DiscountPrice.ToString(),
                    OldPrice = p.RetailPrice.ToString()
                }).OrderBy(p => p.SurplusStock).Skip(pageIndex * PageSize).Take(PageSize).ToList();

                return JSONHelper.WXObjectToJson(resultList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        /// <summary>
        /// 订单标题
        /// </summary>
        /// <param name="appguid"></param>
        /// <returns></returns>
        public string GetOrderTitle(string appguid)
        {
            try
            {
                return CodeConst.OrderTitle.YLSG;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                return String.Empty;
            }
        }

        /// <summary>
        /// 获取库存数量
        /// </summary>
        /// <param name="guidlist"></param>
        /// <returns></returns>
        public string GetProductStockByGUID(string guidlist)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<decimal> resultlist = new List<decimal>();
                List<string> list = JSONHelper.WXDecodeObject<List<string>>(guidlist);
                IList<T_PRODUCTCLASSEntity> productclassList = db.IQueryable<T_PRODUCTCLASSEntity>().Where(t => list.Contains(t.FK_Product) && t.IsDel == CodeConst.IsDel.NO).ToList();
                IList<string> skuidslist = productclassList.Select(p => p.ProductSKU).ToList();
                IList<VSTOCKEntity> stockList = db.IQueryable<VSTOCKEntity>().Where(t => skuidslist.Contains(t.SKU)).ToList();
                foreach (string guid in list)
                {
                    List<string> skulist = productclassList.Where(p => p.FK_Product == guid).Select(p => p.ProductSKU).Distinct().ToList();
                    decimal? stock = stockList.Where(p => skulist.Contains(p.SKU)).Sum(p => p.Stock);
                    resultlist.Add(stock.Value);
                }
                result.Data = JSONHelper.WXObjectToJson(resultlist);
                result.IsOK = true;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }

        public string UserShareGoldEggs(string openId)
        {
            ResultData result = new ResultData();
            Data.IDatabase db = DbFactory.Base();
            try
            {
                TUSEREntity userInfo = db.IQueryable<TUSEREntity>().Where(p => p.Openid == openId && p.IsShare == CodeConst.IsUse.NO).SingleOrDefault();
                if (userInfo != null)
                {
                    result.IsOK = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            return JSONHelper.WXObjectToJson(result);
        }
    }
}
