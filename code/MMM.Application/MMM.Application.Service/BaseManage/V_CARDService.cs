using MMM.Application.Entity.BaseManage;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.BaseManage
{
    /// <summary>
    /// 版 本 6.1

    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：T_CARD
    /// </summary>
    public class V_CARDService : RepositoryFactory<V_CARDEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryParam">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<V_CARDEntity> GetPageList(Pagination pagination, JObject queryParam, String organizeList)
        {
            var expression = LinqExtensions.True<V_CARDEntity>();
            //查询条件
            //if (organizeList != null && organizeList.Count > 0)
            //{
            //IList<String> organizeIds = organizeList.Select(x => x.OrganizeId).ToList();
            if (!String.IsNullOrEmpty(organizeList))
                expression = expression.And(t => t.organizeid == organizeList);
            //}
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<V_CARDEntity> GetList(string queryJson)
        {

            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public V_CARDEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion
    }
}
