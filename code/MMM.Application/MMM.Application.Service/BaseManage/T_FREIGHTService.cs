using MMM.Application.Entity.BaseManage;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using System;
using MMM.Util.Extension;

namespace MMM.Application.Service.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-19 11:23
    /// 描 述：T_FREIGHT
    /// </summary>
    public class T_FREIGHTService : RepositoryFactory<T_FREIGHTEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_FREIGHTEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_FREIGHTEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_FREIGHTEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        public IEnumerable<T_FREIGHTEntity> GetPageList(Pagination pagination, JObject queryJson, String organizeList)
        {
            var expression = LinqExtensions.True<T_FREIGHTEntity>();
            //查询条件
            //if (organizeList != null && organizeList.Count > 0)
            //{
            //    IList<String> organizeIds = organizeList.Select(x => x.OrganizeId).ToList();
            //    expression = expression.And(t => organizeIds.Contains(t.organizeid));
            //}
            if(!String.IsNullOrEmpty(organizeList))
                expression = expression.And(t => organizeList == t.organizeid);
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        #endregion
    }
}
