using MMM.Application.Entity.BaseManage;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;

namespace MMM.Application.Service.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-28 21:28
    /// 描 述：图片
    /// </summary>
    public class T_PICTUREService : RepositoryFactory<T_PICTUREEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_PICTUREEntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="type">查询参数</param>
        /// <param name="fK_GUID">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_PICTUREEntity> GetList(int? type, string fK_GUID)
        {
            IQueryable<T_PICTUREEntity> query = this.BaseRepository().IQueryable();
            if (type.HasValue)
                query = query.Where(x => x.Type == type);
            if (!String.IsNullOrEmpty(fK_GUID))
                query = query.Where(x => x.FK_GUID == fK_GUID);
            return query.Where(x => x.IsDel == 0).OrderBy(x => x.CreateTime).ToList();
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_PICTUREEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_PICTUREEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, T_PICTUREEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
