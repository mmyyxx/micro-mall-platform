using MMM.Application.Entity.BaseManage;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-24 22:44
    /// 描 述：门店
    /// </summary>
    public class T_STOREService : RepositoryFactory<T_STOREEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_STOREEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            var expression = LinqExtensions.True<T_STOREEntity>();

            if (!queryJson["StoreName"].IsEmpty())
            {
                String StoreName = queryJson["StoreName"].ToString();
                expression = expression.And(t => t.StoreName.Contains(StoreName));
            }

            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="organizeId">查询参数</param>
        /// <param name="storeid">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_STOREEntity> GetList(string organizeId, String storeid = "")
        {
            IQueryable<T_STOREEntity> query = this.BaseRepository().IQueryable();
            if (!String.IsNullOrEmpty(organizeId))
                query = query.Where(x => x.organizeid == organizeId);
            if(!String.IsNullOrEmpty(storeid))
                query = query.Where(x => x.GUID == storeid);

            return query.Where(x => x.IsDel == 0).ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_STOREEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="StoreId">仓库id</param>
        /// <returns></returns>
        public T_STOREEntity GetEntityByStoreId(string StoreId)
        {
            return this.BaseRepository().IQueryable().FirstOrDefault(x => x.StoreId == StoreId);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_STOREEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, T_STOREEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
