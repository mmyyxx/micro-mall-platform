using MMM.Application.Entity.BaseManage;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.BaseManage
{
    /// <summary>
    /// 描 述：T_CARD
    /// </summary>
    public class T_CARDService : RepositoryFactory<T_CARDEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryParam">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_CARDEntity> GetPageList(Pagination pagination, JObject queryParam, IList<OrganizeEntity> organizeList)
        {
            var expression = LinqExtensions.True<T_CARDEntity>();
            //查询条件
            if (organizeList != null && organizeList.Count > 0)
            {
                IList<String> organizeIds = organizeList.Select(x => x.OrganizeId).ToList();
                expression = expression.And(t => organizeIds.Contains(t.organizeid));
            }
            expression = expression.And(t => t.IsDel == 0);
            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_CARDEntity> GetList(string queryJson)
        {

            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_CARDEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_CARDEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }
        #endregion
    }
}
