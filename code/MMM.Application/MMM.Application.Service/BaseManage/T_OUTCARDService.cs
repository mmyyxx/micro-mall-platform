using MMM.Application.Entity.BaseManage;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-18 22:59
    /// 描 述：T_OUTCARD
    /// </summary>
    public class T_OUTCARDService : RepositoryFactory<T_OUTCARDEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_OUTCARDEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_OUTCARDEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 根据Cardid外键获取数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public IList<T_OUTCARDEntity> ExportCard(string keyValue)
        {
            return this.BaseRepository().IQueryable(x=>x.CardId==keyValue).ToList();
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, Int32 CardCount, String FK_AppId)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                List<T_OUTCARDEntity> entityList = new List<T_OUTCARDEntity>();
                for (int i = 0; i < CardCount; i++)
                {
                    T_OUTCARDEntity entity = new T_OUTCARDEntity();
                    entity.Create();
                    entity.CardId = keyValue;
                    entity.FK_AppId = FK_AppId;
                    entityList.Add(entity);
                }
                this.BaseRepository().Insert(entityList);
            }
        }
        #endregion
    }
}
