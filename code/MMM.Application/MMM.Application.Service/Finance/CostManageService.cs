using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Finance;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MMM.Application.Service.Finance
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class CostManageService
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<CostModel> GetPageList(Pagination pagination, JObject queryJson)
        {
            Data.IDatabase db;
            List<CostModel> list;
            GetList(queryJson, out db, out list);

            pagination.records = list.Count;

            List<CostModel> newlist = list.OrderBy(p => p.SKU).Skip((pagination.page - 1) * pagination.rows).Take(pagination.rows).ToList();
            List<String> skus = newlist.Select(t => t.SKU).ToList();
            List<VCOSTINFOEntity> costList = db.IQueryable<VCOSTINFOEntity>().Where(t => skus.Contains(t.SKU)).ToList();
            foreach (CostModel item in newlist)
            {
                VCOSTINFOEntity costinfo = costList.FirstOrDefault(p => p.SKU == item.SKU);
                if (costinfo != null)
                {
                    item.ProductName = costinfo.ProductName;
                    item.CostPrice = costinfo.Price;
                }
                item.SaleTotal = Decimal.Round(item.SalePrice * item.SaleCount, 2);
                item.CostTotal = Decimal.Round(item.CostPrice * item.DetailCount, 2);
                item.Maori = item.SaleTotal - item.CostTotal;
                if (item.SaleTotal == Decimal.Zero)
                {
                    item.GrossInterestRate = Decimal.Zero;
                }
                else
                {
                    item.GrossInterestRate = Decimal.Round(item.Maori / item.SaleTotal * 100, 2);
                }
            }

            return newlist;
        }

        /// <summary>
        /// 获取列表数据
        /// </summary>
        /// <param name="j"></param>
        /// <returns></returns>
        public IList<CostModel> GetList(JObject j)
        {
            Data.IDatabase db;
            List<CostModel> list;
            GetList(j, out db, out list);
            List<String> skus = list.Select(t => t.SKU).ToList();
            List<VCOSTINFOEntity> costList = db.IQueryable<VCOSTINFOEntity>().Where(t => skus.Contains(t.SKU)).ToList();
            foreach (CostModel item in list)
            {
                VCOSTINFOEntity costinfo = costList.FirstOrDefault(p => p.SKU == item.SKU);
                if (costinfo != null)
                {
                    item.ProductName = costinfo.ProductName;
                    item.CostPrice = costinfo.Price;
                }
                item.SaleTotal = Decimal.Round(item.SalePrice * item.SaleCount, 2);
                item.CostTotal = Decimal.Round(item.CostPrice * item.DetailCount, 2);
                item.Maori = item.SaleTotal - item.CostTotal;
                if (item.SaleTotal == Decimal.Zero)
                {
                    item.GrossInterestRate = Decimal.Zero;
                }
                else
                {
                    item.GrossInterestRate = Decimal.Round(item.Maori / item.SaleTotal * 100, 2);
                }
            }
            return list;
        }

        private static void GetList(JObject queryJson, out Data.IDatabase db, out List<CostModel> list)
        {
            db = DbFactory.Base();
            IQueryable<TORDEREntity> orderquery = db.IQueryable<TORDEREntity>().Where(t => t.IsDel == CodeConst.IsDel.NO);

            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                orderquery = orderquery.Where(t => t.organizeid == organizeid);
            }
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                orderquery = orderquery.Where(t => t.FK_PlanStore == storeGUID);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime starttime = Convert.ToDateTime(queryJson["startdate"]);
                orderquery = orderquery.Where(t => t.CreateTime >= starttime);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime endtime = Convert.ToDateTime(queryJson["enddate"]).AddDays(1);
                orderquery = orderquery.Where(t => t.CreateTime < endtime);
            }

            list = new List<CostModel>();
            List<CostModel> storelist = orderquery.Where(t => t.OrderType == "0")
                .Join(db.IQueryable<TORDERDETAILEntity>(), m => m.GUID, n => n.FK_Order, (m, n) => new
                {
                    Count = n.Count,
                    Price = n.Price,
                    ProductSKU = n.ProductSKU,
                    Creater = m.Creater
                })
                .GroupBy(p => new { p.ProductSKU, p.Price }).Select(p => new CostModel
                {
                    SKU = p.Key.ProductSKU,
                    ProductName = "未入库",
                    SaleCount = p.Sum(z => z.Count) ?? 0,
                    DetailCount = p.Sum(z => z.Count) ?? 0,
                    SalePrice = p.Key.Price ?? 0,
                    CostPrice = Decimal.Zero,
                    Creater = p.Max(z => z.Creater)
                }).ToList();
            List<CostModel> wxlist = orderquery.Where(p => p.OrderType == "1" && p.OrderStatus <= 4 && p.OrderStatus > 1)
                .Join(db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.GUID, n => n.FK_Order, (m, n) => new
                {
                    GUID = n.GUID,
                    Count = n.Count,
                    Price = n.Price,
                    FK_ProductClass = n.FK_ProductClass,
                    ActualPrice = n.ActualPrice,
                    Creater = m.Completer
                })
                .Join(db.IQueryable<T_PRODUCTCLASSEntity>(), a => a.FK_ProductClass, b => b.GUID, (a, b) => new { Creater = a.Creater,a.GUID, b.ProductSKU, Count = a.Count * b.ProductStock.Value, DetailCount = a.Count, a.ActualPrice })
                .GroupBy(p => new { p.ProductSKU, p.ActualPrice }).Select(p => new CostModel
                {
                    SKU = p.Key.ProductSKU,
                    ProductName = "未入库",
                    SaleCount = p.Sum(z => z.DetailCount) ?? 0,
                    DetailCount = p.Sum(z => z.Count) ?? 0,
                    SalePrice = p.Key.ActualPrice ?? 0,
                    CostPrice = Decimal.Zero,
                    Creater = p.Max(z => z.Creater)
                }).ToList();

            list.AddRange(wxlist);
            list.AddRange(storelist);

            list = list.GroupBy(p => new { p.SKU, p.SalePrice }).Select(p => new CostModel
            {
                SKU = p.Key.SKU,
                ProductName = p.Max(m => m.ProductName),
                SaleCount = Decimal.Round(p.Sum(m => m.SaleCount), 2),
                DetailCount = Decimal.Round(p.Sum(m => m.DetailCount), 2),
                SalePrice = p.Key.SalePrice,
                CostPrice = Decimal.Zero,
                Creater = p.Max(m => m.Creater)
            }).ToList();
        }
        #endregion
    }
}
