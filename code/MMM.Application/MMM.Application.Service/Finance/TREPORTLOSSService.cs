using MMM.Application.Entity.Finance;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Code;

namespace MMM.Application.Service.Finance
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-20 16:38
    /// 描 述：报损
    /// </summary>
    public class TREPORTLOSSService : RepositoryFactory<TREPORTLOSSEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<ReportLossModel> GetPageList(Pagination pagination, JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();
            IQueryable<TREPORTLOSSEntity> reportlossQuery = db.IQueryable<TREPORTLOSSEntity>().Where(t => t.IsDel == CodeConst.IsDel.NO);
            IQueryable<TPRODUCTEntity> productQuery = db.IQueryable<TPRODUCTEntity>().Where(t => t.IsDel == CodeConst.IsDel.NO);
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                reportlossQuery = reportlossQuery.Where(t => t.FK_WarehouseNo == storeGUID);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime starttime = Convert.ToDateTime(queryJson["startdate"]);
                reportlossQuery = reportlossQuery.Where(t => t.CreateTime >= starttime);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime endtime = Convert.ToDateTime(queryJson["enddate"]).AddDays(1);
                reportlossQuery = reportlossQuery.Where(t => t.CreateTime < endtime);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                reportlossQuery = reportlossQuery.Where(t => t.organizeid == organizeid);
                productQuery = productQuery.Where(t => t.organizeid == organizeid);
            }
            if (!queryJson["sku"].IsEmpty())
            {
                String sku = Convert.ToString(queryJson["sku"]);
                productQuery = productQuery.Where(t => t.SKU.Contains(sku));
            }

            var list = reportlossQuery.Join(productQuery, m => m.FK_Product, n => n.GUID, (m, n) => new ReportLossModel
            {
                ID = m.ID,
                SKU = n.SKU,
                PdtName = n.PdtName,
                PdtCount = m.PdtCount ?? 0,
                CostPrice = m.CostPrice ?? 0,
                CostTotal = m.CostTotal ?? 0,
                IsStockOut = m.IsStockOut ?? 1,
                Remark = m.Remark,
                Creater = m.Creater,
                CreateTime = m.CreateTime,
                Modifyer = m.Modifyer,
                ModifyTime = m.ModifyTime,
                organizeid = m.organizeid
            }).OrderByDescending(p => p.CreateTime);

            pagination.records = list.Count();
            IList<ReportLossModel> newlist = list.OrderBy(p => p.SKU).Skip((pagination.page - 1) * pagination.rows).Take(pagination.rows).ToList();
            return newlist;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<ReportLossModel> GetList(JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();
            IQueryable<TREPORTLOSSEntity> reportlossQuery = db.IQueryable<TREPORTLOSSEntity>().Where(t => t.IsDel == CodeConst.IsDel.NO);
            IQueryable<TPRODUCTEntity> productQuery = db.IQueryable<TPRODUCTEntity>().Where(t => t.IsDel == CodeConst.IsDel.NO);
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                reportlossQuery = reportlossQuery.Where(t => t.FK_WarehouseNo == storeGUID);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime starttime = Convert.ToDateTime(queryJson["startdate"]);
                reportlossQuery = reportlossQuery.Where(t => t.CreateTime >= starttime);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime endtime = Convert.ToDateTime(queryJson["enddate"]).AddDays(1);
                reportlossQuery = reportlossQuery.Where(t => t.CreateTime < endtime);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                reportlossQuery = reportlossQuery.Where(t => t.organizeid == organizeid);
                productQuery = productQuery.Where(t => t.organizeid == organizeid);
            }
            if (!queryJson["sku"].IsEmpty())
            {
                String sku = Convert.ToString(queryJson["sku"]);
                productQuery = productQuery.Where(t => t.SKU.Contains(sku));
            }
            var list = reportlossQuery.Join(productQuery, m => m.FK_Product, n => n.GUID, (m, n) => new ReportLossModel
            {
                ID = m.ID,
                SKU = n.SKU,
                PdtName = n.PdtName,
                PdtCount = m.PdtCount ?? 0,
                CostPrice = m.CostPrice ?? 0,
                CostTotal = m.CostTotal ?? 0,
                IsStockOut = m.IsStockOut ?? 1,
                Remark = m.Remark,
                Creater = m.Creater,
                CreateTime = m.CreateTime,
                Modifyer = m.Modifyer,
                ModifyTime = m.ModifyTime
            }).OrderByDescending(p => p.CreateTime);
            return list.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TREPORTLOSSEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TREPORTLOSSEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 移除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TREPORTLOSSEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 报损出库信息
        /// </summary>
        /// <param name="type">1-出库，-1-取消出库</param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public string SaveOrderSend(Int32 type, string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            TREPORTLOSSEntity reportlossInfo = db.FindEntity<TREPORTLOSSEntity>(keyValue);
            if (reportlossInfo.IsDel == 1) return "该报损已经删除!";

            if (type == 1 && reportlossInfo.IsStockOut.Equals(CodeConst.IsYesNO.YES))
            {
                return ("该报损数据已出库，无法再次出库！");
            }
            else if (type == -1 && reportlossInfo.IsStockOut.Equals(CodeConst.IsYesNO.NO))
            {
                return "该报损未出库,不需要取消！";
            }
            //门店信息
            T_STOREEntity store = db.FindEntity<T_STOREEntity>(reportlossInfo.FK_WarehouseNo);

            //取出所有产品以及组合
            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(x => x.IsDel == 0 && x.organizeid == reportlossInfo.organizeid).ToList();
            IList<TPRODUCTCOMBEntity> productcombList = db.IQueryable<TPRODUCTCOMBEntity>().Where(x => x.IsDel == 0).ToList();

            IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(t => t.IsDel == 0 && t.FK_WarehouseNo == store.StoreId).ToList();

            try
            {
                bool isResult = true;
                String str = "";
                db.BeginTrans();
                var combllist = productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == reportlossInfo.FK_Product)
            .Join(productcombList.Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.GUID, n => n.FK_Product, (m, n) => new { m, n })
            .Join(productList.Where(p => p.IsDel == CodeConst.IsDel.NO), x => x.n.FK_Child, y => y.GUID, (x, y) => new
            {
                ProductSKU = y.SKU,
                ProductCount = reportlossInfo.PdtCount * x.n.ChildCount.Value
            }).ToList();

                if (combllist.Count > 0)
                {
                    foreach (var combpdt in combllist)
                    {
                        List<TSTOCKEntity> stocklist = stockList
                                   .Join(productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == combpdt.ProductSKU), a => a.FK_Product, b => b.GUID, (a, b) => a)
                                   .OrderBy(p => p.FK_Location).Select(p => p).ToList();
                        if (stocklist.Count > 0)
                        {
                            decimal totalstock = stocklist.Sum(p => p.Stock) ?? 0;
                            if (totalstock >= combpdt.ProductCount * type)
                            {
                                TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= combpdt.ProductCount * type).FirstOrDefault();
                                if (stockinfo != null)
                                {
                                    stockinfo.Stock -= combpdt.ProductCount * type;
                                    stockinfo.ActualStock -= combpdt.ProductCount * type;

                                    db.Update<TSTOCKEntity>(stockinfo, null);

                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                    stockdetail.ID = Guid.NewGuid().ToString();
                                    stockdetail.FK_Stock = stockinfo.GUID;
                                    stockdetail.Stock = -combpdt.ProductCount * type;
                                    stockdetail.ActualStock = -combpdt.ProductCount * type;
                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                    stockdetail.Remark = String.Format("{1}出库：{0}", reportlossInfo.ID, type == 1 ? "报损" : "取消");
                                    stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                    stockdetail.CreateTime = DateTime.Now;
                                    stockdetail.organizeid = reportlossInfo.organizeid;

                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                }
                                else
                                {
                                    decimal outstock = combpdt.ProductCount ?? 0;
                                    while (outstock > 0)
                                    {
                                        foreach (TSTOCKEntity stockitem in stocklist)
                                        {
                                            decimal itemstock = outstock;
                                            if (stockitem.Stock >= outstock)
                                            {
                                                stockitem.Stock -= outstock;
                                                stockitem.ActualStock -= outstock;
                                                outstock = Decimal.Zero;
                                            }
                                            else
                                            {
                                                outstock -= stockitem.Stock ?? 0;
                                                stockitem.Stock -= stockitem.Stock;
                                                stockitem.ActualStock -= stockitem.Stock;
                                                stockitem.Stock = Decimal.Zero;
                                            }
                                            db.Update<TSTOCKEntity>(stockitem, null);

                                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                            stockdetail.FK_Stock = stockitem.GUID;
                                            stockdetail.Stock = outstock - itemstock;
                                            stockdetail.ActualStock = outstock - itemstock;
                                            stockdetail.Type = CodeConst.StockDetailType.CK;
                                            stockdetail.Remark = String.Format("报损出库：{0}", reportlossInfo.ID);
                                            stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                            stockdetail.CreateTime = DateTime.Now;
                                            stockdetail.organizeid = reportlossInfo.organizeid;

                                            db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                        }
                                    }
                                }
                                reportlossInfo.IsStockOut = type == 1 ? CodeConst.IsYesNO.YES : CodeConst.IsYesNO.NO;

                                db.Update<TREPORTLOSSEntity>(reportlossInfo, null);
                            }
                            else
                            {
                                isResult = false;
                                str = "库存不足！";
                            }
                        }
                        else
                        {
                            isResult = false;
                            str = "库存不存在！";
                        }
                    }
                }
                else
                {
                    List<TSTOCKEntity> stocklist = stockList
                               .Join(productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.GUID == reportlossInfo.FK_Product), a => a.FK_Product, b => b.GUID, (a, b) => a)
                               .OrderBy(p => p.FK_Location).Select(p => p).ToList();
                    
                    if (stocklist.Count > 0)
                    {
                        decimal totalstock = stocklist.Sum(p => p.Stock)??0;
                        if (totalstock >= reportlossInfo.PdtCount * type)
                        {
                            TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= reportlossInfo.PdtCount * type).FirstOrDefault();
                            if (stockinfo != null)
                            {
                                stockinfo.Stock -= reportlossInfo.PdtCount * type;
                                stockinfo.ActualStock -= reportlossInfo.PdtCount * type;

                                db.Update<TSTOCKEntity>(stockinfo, null);

                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                stockdetail.ID = Guid.NewGuid().ToString();
                                stockdetail.FK_Stock = stockinfo.GUID;
                                stockdetail.Stock = -reportlossInfo.PdtCount * type;
                                stockdetail.ActualStock = -reportlossInfo.PdtCount * type;
                                stockdetail.Type = CodeConst.StockDetailType.CK;
                                stockdetail.Remark = String.Format("{1}出库：{0}", reportlossInfo.ID, type == 1 ? "报损" : "取消");
                                stockdetail.Creater = OperatorProvider.Provider.Current().CompanyId;
                                stockdetail.CreateTime = DateTime.Now;
                                stockdetail.organizeid = reportlossInfo.organizeid;

                                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                            }
                            else
                            {
                                decimal outstock = reportlossInfo.PdtCount??0;
                                while (outstock > 0)
                                {
                                    foreach (TSTOCKEntity stockitem in stocklist)
                                    {
                                        decimal itemstock = outstock;
                                        if (stockitem.Stock >= outstock)
                                        {
                                            stockitem.Stock -= outstock;
                                            stockitem.ActualStock -= outstock;
                                            outstock = Decimal.Zero;
                                        }
                                        else
                                        {
                                            outstock -= stockitem.Stock??0;
                                            stockitem.Stock -= stockitem.Stock;
                                            stockitem.ActualStock -= stockitem.Stock;
                                            stockitem.Stock = Decimal.Zero;
                                        }
                                        db.Update<TSTOCKEntity>(stockitem, null);

                                        TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                        stockdetail.ID = Guid.NewGuid().ToString();
                                        stockdetail.FK_Stock = stockitem.GUID;
                                        stockdetail.Stock = outstock - itemstock;
                                        stockdetail.ActualStock = outstock - itemstock;
                                        stockdetail.Type = CodeConst.StockDetailType.CK;
                                        stockdetail.Remark = String.Format("报损出库：{0}", reportlossInfo.ID);
                                        stockdetail.Creater = OperatorProvider.Provider.Current().CompanyId;
                                        stockdetail.CreateTime = DateTime.Now;
                                        stockdetail.organizeid = reportlossInfo.organizeid;

                                        db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                    }
                                }
                            }
                            reportlossInfo.IsStockOut = type == 1 ? CodeConst.IsYesNO.YES : CodeConst.IsYesNO.NO;

                            db.Update<TREPORTLOSSEntity>(reportlossInfo, null);
                        }
                        else
                        {
                            isResult = false;
                            str = "库存不足！";
                        }
                    }
                    else
                    {
                        isResult = false;
                        str = "库存不存在！";
                    }
                }
                if (isResult)
                    db.Commit();
                else
                {
                    db.Rollback();
                    return str;
                }
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }
        #endregion
    }
}
