using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VAFTERSALEService : RepositoryFactory<VAFTERSALEEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<VAFTERSALEEntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<VAFTERSALEEntity>();
            if (!queryJson["ordernumber"].IsEmpty())
            {
                String OrderNumber = Convert.ToString(queryJson["ordernumber"]);
                expression = expression.And(t => t.OrderNumber.Contains(OrderNumber));
            }
            if (!queryJson["orderstatus"].IsEmpty())
            {
                Int32 orderstatus = Convert.ToInt32(queryJson["orderstatus"]);
                expression = expression.And(t => t.Status == orderstatus);
            }
            else
            {
                Int32 orderstatus = CodeConst.OrderDetailStatus.COMPLETED;
                expression = expression.And(t => t.Status < orderstatus);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                //expression = expression.And(t => t.fk_ == storeGUID);
            }

            return BaseRepository().FindList(expression, pagination).ToList();
        }
        #endregion
    }
}
