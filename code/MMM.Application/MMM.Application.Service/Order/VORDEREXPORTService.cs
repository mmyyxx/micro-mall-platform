using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VORDEREXPORTService : RepositoryFactory<VORDEREXPORTEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<VORDEREXPORTEntity> GetPageList(JObject queryJson)
        {
            var expression = BaseRepository().IQueryable();
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime startdate = Convert.ToDateTime(queryJson["startdate"]);
                expression = expression.Where(t => t.CompleteTime >= startdate);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime enddate = Convert.ToDateTime(queryJson["enddate"]).AddDays(1);
                expression = expression.Where(t => t.CompleteTime < enddate);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.Where(t => t.organizeid == organizeid);
            }
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                expression = expression.Where(t => t.FK_ActualStore == storeGUID);
            }

            return expression.ToList();
        }
        #endregion
    }
}
