using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.WebService;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-23 21:39
    /// 描 述：T_ORDERReturnDetail
    /// </summary>
    public class TORDERReturnDetailService : RepositoryFactory<TORDERReturnDetailEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TORDERReturnDetailEntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TORDERReturnDetailEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TORDERReturnDetailEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 获取门店订单信息
        /// </summary>
        /// <param name="orderguid"></param>
        /// <returns></returns>
        public IList<ProductItem> GetStoreOrderDetail(string orderguid)
        {
            Data.IDatabase db = DbFactory.Base();

            IList<TORDERReturnDetailEntity> list = db.IQueryable<TORDERReturnDetailEntity>().Where(p => p.FK_Return == orderguid).ToList();

            IList<String> skus = list.Select(t => t.ProductSKU).ToList();
            IList<TPRODUCTEntity> productList = db.IQueryable<TPRODUCTEntity>().Where(t => skus.Contains(t.SKU)).ToList();

            IList<ProductItem> productItemList = new List<ProductItem>();
            foreach (TORDERReturnDetailEntity p in list)
            {
                TPRODUCTEntity product = productList.FirstOrDefault(t => t.SKU == p.ProductSKU);

                ProductItem item = new ProductItem();
                item.ProductSKU = p.ProductSKU;
                if (product != null)
                    item.ProductName = product.PdtName;
                item.ProductCount = p.Count ?? 0;
                item.ProductPrice = p.Price ?? 0;
                item.ProductTotal = (p.Count ?? 0) * (p.Price ?? 0);
                item.OrderTime = p.CreateTime.Value.ToString("yyyy-MM-dd");
                productItemList.Add(item);
            }
            return productItemList;
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TORDERReturnDetailEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }
        #endregion
    }
}
