using MMM.Application.Entity.Order;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using MMM.Application.Entity.BaseManage;
using MMM.Util.Extension;
using Newtonsoft.Json.Linq;
using MMM.Application.Entity.UserInfo;
using MMM.Application.Code;
using WxPayAPI;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Warehouse;
using MMM.Application.Entity.Product;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using MMM.Application.Service.WSC;
using MMM.Util.Log;
using MMM.Application.Entity.WebService;
using MMM.Application.Service.Cache;
using MMM.Application.Entity.ShoppingMall;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-14 19:23
    /// 描 述：订单
    /// </summary>
    public class TORDERService : RepositoryFactory<TORDEREntity>
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("webservice");
        private object _lock = new object();

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TORDEREntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();

            IList<String> userIds = new List<String>();
            IList<VWXUSEREntity> vWxUser = null;
            IList<UserEntity> cysUser = null;
            String searchName = "";
            if (!queryJson["searchName"].IsEmpty())
            {
                searchName = queryJson["searchName"].ToString();

                vWxUser = db.IQueryable<VWXUSEREntity>().Where(p => (p.NickName.Contains(searchName) || p.UserName.Contains(searchName)) && p.IsDel == 0).ToList();
                userIds = vWxUser.Select(x => x.Openid).ToList();

                cysUser = db.IQueryable<UserEntity>().Where(p => p.NickName.Contains(searchName) || p.Account.Contains(searchName)).ToList();
                userIds = userIds.Concat(cysUser.Select(x => x.Account).ToList()).ToList();
            }
            var expression = LinqExtensions.True<TORDEREntity>();
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                expression = expression.And(dic => dic.FK_PlanStore != null && dic.FK_PlanStore.Equals(storeGUID));
            }
            if (!queryJson["ordernumber"].IsEmpty())
            {
                String ordernumber = Convert.ToString(queryJson["ordernumber"]);
                expression = expression.And(dic => dic.OrderNumber == ordernumber);
            }
            if (!queryJson["deliverymode"].IsEmpty())
            {
                Int32 DeliveryMode = Convert.ToInt32(queryJson["DeliveryMode"]);
                expression = expression.And(dic => dic.DeliveryMode == DeliveryMode);
            }
            if (!queryJson["isgroup"].IsEmpty())
            {
                Int32 isgroup = Convert.ToInt32(queryJson["isgroup"]);
                expression = expression.And(dic => dic.IsGroupOrder == isgroup);
            }
            if (!queryJson["payresult"].IsEmpty())
            {
                Int32 payresult = Convert.ToInt32(queryJson["payresult"]);
                expression = expression.And(dic => dic.PayResult == payresult);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime startdate = Convert.ToDateTime(queryJson["startdate"]);
                expression = expression.And(dic => dic.CreateTime >= startdate);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime enddate = Convert.ToDateTime(queryJson["enddate"]).AddDays(1);
                expression = expression.And(dic => dic.CreateTime < enddate);
            }
            if (userIds.Count > 0)
            {
                expression = expression.And(x => userIds.Contains(x.FK_Openid));
            }
            if (!queryJson["OrderType"].IsEmpty())
            {
                String OrderType = Convert.ToString(queryJson["OrderType"]);
                expression = expression.And(dic => dic.OrderType == OrderType);
            }
            if (!queryJson["OrderStatus"].IsEmpty())
            {
                Int32 OrderStatus = Convert.ToInt32(queryJson["OrderStatus"]);
                expression = expression.And(dic => dic.OrderStatus == OrderStatus);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = Convert.ToString(queryJson["organizeid"]);
                expression = expression.And(dic => dic.organizeid == organizeid);
            }
            IList<TORDEREntity> orderList = this.BaseRepository().FindList(expression, pagination).ToList();

            IList<String> planStoreids = orderList.Where(x => !String.IsNullOrEmpty(x.FK_PlanStore)).Select(x => x.FK_PlanStore).ToList();
            IList<T_STOREEntity> storeList = db.IQueryable<T_STOREEntity>().Where(p => planStoreids.Contains(p.GUID)).ToList();

            //查询Coupon
            IList<String> couponIds = orderList.Where(x => !String.IsNullOrEmpty(x.Coupon)).Select(x => x.Coupon).ToList();
            IList<TUSERCARDEntity> userCardList = db.IQueryable<TUSERCARDEntity>().Where(p => couponIds.Contains(p.GUID)).ToList();

            //查询用户
            if (String.IsNullOrEmpty(searchName))
            {
                IList<String> orderUserIds = orderList.Select(x => x.FK_Openid).ToList();

                vWxUser = db.IQueryable<VWXUSEREntity>().Where(p => orderUserIds.Contains(p.Openid)).ToList();
                //cysUser = db.IQueryable<UserEntity>().Where(p => orderUserIds.Contains(p.UserId)).ToList();
            }
            foreach (TORDEREntity m in orderList)
            {
                VWXUSEREntity wxUser = vWxUser.FirstOrDefault(x => x.Openid == m.FK_Openid);
                if (wxUser != null)
                    m.Creater = wxUser.NickName + "(" + wxUser.UserName + ")";
                //else
                //{
                //    UserEntity temCysUser = cysUser.FirstOrDefault(x => x.UserId == m.FK_Openid);
                //    if (temCysUser != null) m.Creater = temCysUser.Account;
                //}
                if (!String.IsNullOrEmpty(m.Coupon))
                {
                    TUSERCARDEntity usercard = userCardList.Where(p => p.GUID == m.Coupon).SingleOrDefault();
                    if (usercard != null)
                    {
                        m.CouponMoney = usercard.ReduceCost;
                    }
                }

                T_STOREEntity storeInfo = storeList.SingleOrDefault(p => p.GUID == m.FK_PlanStore);
                if (storeInfo != null)
                {
                    m.PlanStoreStr = storeInfo.StoreName;
                    m.PlanStoreId = storeInfo.StoreId;
                }
            }

            return orderList;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TORDEREntity> GetList(JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();
            var expression = db.IQueryable<TORDEREntity>().Where(x => x.IsDel == 0);
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                expression = expression.Where(dic => dic.FK_PlanStore == storeGUID);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime startdate = Convert.ToDateTime(queryJson["startdate"]);
                expression = expression.Where(dic => dic.CreateTime >= startdate);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime enddate = Convert.ToDateTime(queryJson["enddate"]);
                expression = expression.Where(dic => dic.CreateTime < enddate);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = Convert.ToString(queryJson["organizeid"]);
                expression = expression.Where(dic => dic.organizeid == organizeid);
            }
            IList<TORDEREntity> orderList = expression.OrderByDescending(t => t.CreateTime).ToList();
            return orderList;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="other"></param>
        /// <returns></returns>
        public TORDEREntity GetEntity(string keyValue, Boolean other = true)
        {
            Data.IDatabase db = DbFactory.Base();
            TORDEREntity order = db.FindEntity<TORDEREntity>(keyValue);
            if (other)
            {
                if (order.FK_Openid.Length >= 20)
                {
                    TUSEREntity user = db.FindEntity<TUSEREntity>(order.FK_Openid);
                    if (user != null) order.Creater = user.NickName;
                }
                if (!String.IsNullOrEmpty(order.FK_ActualStore))
                {
                    T_STOREEntity store = db.FindEntity<T_STOREEntity>(order.FK_ActualStore);
                    if (store != null) order.ActualStoreStr = store.StoreName;
                }
                if (!String.IsNullOrEmpty(order.FK_PlanStore))
                {
                    T_STOREEntity store = db.FindEntity<T_STOREEntity>(order.FK_PlanStore);
                    if (store != null) order.PlanStoreStr = store.StoreName;
                }
                order.GroupEndTimeStr = order.GroupEndTime == null ? "暂无记录" : order.GroupEndTime.Value.ToString("yyyy-MM-dd HH:mm");
                order.CreateTimeStr = order.CreateTime == null ? "暂无记录" : order.CreateTime.Value.ToString("yyyy-MM-dd HH:mm");
                order.RegimentTimeStr = order.RegimentTime == null ? "暂无记录" : order.RegimentTime.Value.ToString("yyyy-MM-dd HH:mm");
                order.DeliverTimeStr = order.DeliverTime == null ? "暂无记录" : order.DeliverTime.Value.ToString("yyyy-MM-dd HH:mm");
                order.CompleteTimeStr = order.CompleteTime == null ? "暂无记录" : order.CompleteTime.Value.ToString("yyyy-MM-dd HH:mm");
            }
            return order;
        }

        /// <summary>
        /// 获取门店订单导出列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<ShopOrderExport> GetExportList(JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();
            var query = db.IQueryable<TORDEREntity>();

            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                query = query.Where(dic => dic.FK_PlanStore != null && dic.FK_PlanStore.Equals(storeGUID));
            }
            if (!queryJson["ordertype"].IsEmpty())
            {
                String ordertype = queryJson["ordertype"].ToString();
                query = query.Where(dic => dic.OrderType == ordertype);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                query = query.Where(dic => dic.organizeid == organizeid);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime startdate = Convert.ToDateTime(queryJson["startdate"]);
                query = query.Where(dic => dic.CreateTime >= startdate);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime enddate = Convert.ToDateTime(queryJson["enddate"]).AddDays(1);
                query = query.Where(dic => dic.CreateTime < enddate);
            }

            IList<ShopOrderExport> list = query.Where(p => p.IsDel == CodeConst.IsDel.NO)
                        .Join(db.IQueryable<TORDERDETAILEntity>(), m => m.GUID, n => n.FK_Order, (m, n) => new { m, n }).Join(db.IQueryable<VPRODUCTSTOCKEntity>(), x => x.n.ProductSKU, y => y.SKU, (x, y) => new ShopOrderExport
                        {
                            FK_PlanStore = x.m.FK_PlanStore,
                            OrderNumber = x.m.OrderNumber,
                            PayWay = x.m.PayWay,
                            OrderPrice = x.m.OrderPrice,
                            OrderRemark = x.m.OrderRemark,
                            Creater = x.m.Creater,
                            CreateTime = x.m.CreateTime,
                            ProductSKU = x.n.ProductSKU,
                            PdtName = y.PdtName,
                            Count = x.n.Count,
                            Price = x.n.Price
                        }).ToList();
            IList<String> ids = list.Select(x => x.FK_PlanStore).ToList();
            IList<T_STOREEntity> storeList = db.IQueryable<T_STOREEntity>().Where(x => ids.Contains(x.GUID)).ToList();
            foreach (ShopOrderExport order in list)
            {
                T_STOREEntity store = storeList.FirstOrDefault(x => x.GUID == order.FK_PlanStore);
                if (store != null)
                    order.PlanStoreStr = store.StoreId;
            }
            return list;
        }

        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TORDEREntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 保存订单以及详情
        /// </summary>
        /// <param name="orderentity"></param>
        /// <param name="detailentityList"></param>
        public void SaveOrderDetail(TORDEREntity orderentity, IList<TORDERDETAILEntity> detailentityList)
        {
            Data.IDatabase db = DbFactory.Base();
            T_STOREEntity store = db.FindEntity<T_STOREEntity>(orderentity.FK_PlanStore);
            orderentity.organizeid = store.organizeid;
            try
            {
                db.BeginTrans();
                db.Insert<TORDEREntity>(orderentity);
                foreach (TORDERDETAILEntity orderdetail in detailentityList)
                {
                    orderdetail.organizeid = store.organizeid;
                }
                db.Insert<TORDERDETAILEntity>(detailentityList);
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
        }


        /// <summary>
        /// 删除订单
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, TORDEREntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        /// <summary>
        /// 到店收货
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public string OrderPic(TORDEREntity order)
        {
            int integral = Convert.ToInt32((order.OrderPrice / CodeConst.Integral.PAYORDER));

            order.ActualPickUpTime = DateTime.Now;
            order.OrderStatus = CodeConst.OrderStatus.COMPLETE;
            order.Completer = OperatorProvider.Provider.Current().Account;
            order.CompleteTime = DateTime.Now;

            Data.IDatabase db = DbFactory.Base();

            //取出会员
            TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FK_Openid && p.IsDel == CodeConst.IsDel.NO).Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();

            try
            {
                db.BeginTrans();
                db.Update<TORDEREntity>(order, null);
                if (user != null)
                {
                    user.SignInDate = DateTime.Now;
                    user.Integral += integral;

                    TINTEGRALEntity record = new TINTEGRALEntity();
                    record.GUID = Guid.NewGuid().ToString();
                    record.FK_Openid = order.FK_Openid;
                    record.FK_AppId = order.FK_AppId;
                    record.Integral = integral;
                    record.SurplusIntegral = user.Integral.Value;
                    record.IntegralRemark = CodeConst.IntegralRemark.PAYORDER;
                    record.IntegralType = CodeConst.IntegralType.PAYORDER;
                    record.CreateTime = DateTime.Now;

                    db.Update<TUSEREntity>(user, null);
                    db.Insert<TINTEGRALEntity>(record);
                }
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }

        /// <summary>
        /// 订单退款
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public string RefundSave(string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            TORDEREntity order = db.FindEntity<TORDEREntity>(keyValue);
            if (order.IsDel != 0) return "订单已经删除!";
            if (order.PayResult.ToString() == CodeConst.IsUse.NO.ToString())
            {
                return "该订单未付款!";
            }
            if (order.OrderStatus < CodeConst.OrderStatus.COMPLETE)
            {
                return "订单状态不正确，无法退款!";
            }

            try
            {
                db.BeginTrans();

                if (order.PayWay == CodeConst.PayWay.WXPAY)
                {
                    TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == order.FK_AppId).SingleOrDefault();
                    double price = (double)order.OrderPrice;
                    string orderprice = ((price) * 100).ToString();
                    Refund.Run(wxapp, order.OrderNumber, orderprice, orderprice, String.IsNullOrEmpty(order.OrderRemark) ? "订单退款" : order.OrderRemark);
                    order.PayResult = 2;
                    order.Modifyer = OperatorProvider.Provider.Current().Account;
                    order.ModifyTime = DateTime.Now;

                    db.Update<TORDEREntity>(order, null);
                }
                else
                {
                    TMEMBEREntity memberInfo = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FK_Openid).Join(db.IQueryable<TMEMBEREntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.Openid, n => n.FK_UnionId, (m, n) => n).SingleOrDefault();
                    if (memberInfo != null)
                    {
                        memberInfo.Money += order.OrderPrice;
                        TMONEYDETAILEntity moneydetail = new TMONEYDETAILEntity();
                        moneydetail.Create();
                        moneydetail.FK_Member = memberInfo.GUID;
                        moneydetail.MoneyChange = order.OrderPrice;
                        moneydetail.Remark = String.Format("订单[{0}]退款", order.OrderNumber);

                        order.PayResult = 2;
                        order.Modifyer = OperatorProvider.Provider.Current().Account;
                        order.ModifyTime = DateTime.Now;

                        db.Update<TMEMBEREntity>(memberInfo, null);
                        db.Insert<TMONEYDETAILEntity>(moneydetail);
                        db.Update<TORDEREntity>(order, null);
                    }
                }
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }

        /// <summary>
        /// 门店订单手动出库
        /// </summary>
        /// <param name="storeGUID">门店GUID</param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public string SaveOrderSend(String storeGUID, string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            TORDEREntity order = db.FindEntity<TORDEREntity>(keyValue);
            if (order.IsDel == 1)
            {
                return "该订单已经删除!";
            }
            if (order.OrderStatus != 1)
            {
                return "该订单已经出库";
            }
            //门店信息
            T_STOREEntity store = db.FindEntity<T_STOREEntity>(storeGUID);

            IList<TORDERDETAILEntity> list = db.IQueryable<TORDERDETAILEntity>().Where(p => p.FK_Order == keyValue && p.Status == CodeConst.IsYesNO.NO).ToList();
            IDictionary<string, decimal?> dic = new Dictionary<string, decimal?>();
            foreach (TORDERDETAILEntity detail in list)
            {
                dic[detail.ProductSKU] = detail.Count;
            }

            IList<String> skus = dic.Keys.ToList();

            IList<String> projectGuids = new List<String>();
            //取出组合商品
            IList<ProductInfoAll> productInfoList = db.IQueryable<TPRODUCTEntity>().Where(p => p.organizeid == order.organizeid && p.IsDel == CodeConst.IsDel.NO && skus.Contains(p.SKU))
            .Join(db.IQueryable<TPRODUCTCOMBEntity>().Where(x => x.IsDel == 0), m => m.GUID, n => n.FK_Product, (m, n) => new { m, n })
            .Join(db.IQueryable<TPRODUCTEntity>().Where(x => x.IsDel == 0 && x.organizeid == order.organizeid), x => x.n.FK_Child, y => y.GUID, (x, y) => new ProductInfoAll
            {
                SKUOld = x.m.SKU,
                GUID = y.GUID,
                ProductNo = y.ProductNo,
                SKU = y.SKU,
                ChildCount = x.n.ChildCount.Value
            }).ToList();

            projectGuids = projectGuids.Concat(productInfoList.Select(t => t.GUID)).ToList();
            //过滤不是组合商品的sku
            skus = new List<String>();
            foreach (String key in dic.Keys)
            {
                if (productInfoList.Count(t => t.SKUOld == key) <= 0)
                    skus.Add(key);
            }

            IList<ProductInfoAll> packageList = null;
            IList<TPRODUCTEntity> otherProjectGuids = null;
            if (skus.Count > 0)
            {
                //包装商品
                packageList = db.IQueryable<TPACKAGEEntity>(t => t.IsDel == 0 && t.organizeid == order.organizeid).Where(p => skus.Contains(p.PackageSKU)).Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.organizeid == order.organizeid && p.IsDel == CodeConst.IsDel.NO), m => m.FK_Product, n => n.GUID, (m, n) => new ProductInfoAll
                {
                    SKUOld = m.PackageSKU,
                    GUID = n.GUID,
                    SKU = n.SKU,
                    ChildCount = m.PdtCount.Value
                }).ToList();
                projectGuids = projectGuids.Concat(packageList.Select(t => t.GUID)).ToList();
                IList<String> temskus = new List<String>();
                foreach (String p in skus)
                {
                    if (packageList.Count(t => t.SKUOld == p) <= 0)
                        temskus.Add(p);
                }

                if (temskus.Count > 0)//存在单独的商品
                {
                    otherProjectGuids = db.IQueryable<TPRODUCTEntity>().Where(p => p.organizeid == order.organizeid && p.IsDel == CodeConst.IsDel.NO && temskus.Contains(p.SKU)).ToList();

                    projectGuids = projectGuids.Concat(otherProjectGuids.Select(t => t.GUID)).ToList();
                }
            }
            //获取门店下的location和库存信息
            IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(t => t.IsDel == 0 && projectGuids.Contains(t.FK_Product) && t.FK_WarehouseNo == store.StoreId).ToList();

            Boolean isResult = true;
            try
            {
                db.BeginTrans();
                foreach (string sku in dic.Keys)
                {
                    decimal pdtcount = dic[sku] ?? 0;
                    if (pdtcount == 0) continue;

                    var combllist = productInfoList.Where(p => p.SKUOld == sku)
                        .Select(t => new
                        {
                            ProductSKU = t.SKU,
                            ProductCount = pdtcount * t.ChildCount
                        }).ToList();

                    if (combllist.Count > 0)
                    {
                        foreach (var combpdt in combllist)
                        {
                            List<TSTOCKEntity> stocklist = stockList
                                       .Join(productInfoList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == combpdt.ProductSKU), a => a.FK_Product, b => b.GUID, (a, b) => a)
                                       .OrderBy(p => p.FK_Location).Select(p => p).ToList();
                            if (stocklist.Count > 0)
                            {
                                decimal? totalstock = stocklist.Sum(p => p.Stock);
                                if (totalstock >= combpdt.ProductCount)
                                {
                                    TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= combpdt.ProductCount).FirstOrDefault();
                                    if (stockinfo != null)
                                    {
                                        stockinfo.Stock -= combpdt.ProductCount;
                                        stockinfo.ActualStock -= combpdt.ProductCount;
                                        db.Update<TSTOCKEntity>(stockinfo, null);

                                        TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                        stockdetail.ID = Guid.NewGuid().ToString();
                                        stockdetail.FK_Stock = stockinfo.GUID;
                                        stockdetail.Stock = -combpdt.ProductCount;
                                        stockdetail.ActualStock = -combpdt.ProductCount;
                                        stockdetail.Type = CodeConst.StockDetailType.CK;
                                        stockdetail.Remark = String.Format("门店出库：{0}", keyValue);
                                        stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                        stockdetail.CreateTime = DateTime.Now;
                                        stockdetail.organizeid = order.organizeid;

                                        db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                    }
                                    else
                                    {
                                        decimal outstock = combpdt.ProductCount;
                                        while (outstock > 0)
                                        {
                                            foreach (TSTOCKEntity stockitem in stocklist)
                                            {
                                                decimal itemstock = outstock;
                                                if (stockitem.Stock >= outstock)
                                                {
                                                    stockitem.Stock -= outstock;
                                                    stockitem.ActualStock -= outstock;
                                                    outstock = Decimal.Zero;
                                                }
                                                else
                                                {
                                                    outstock -= stockitem.Stock ?? 0;
                                                    stockitem.Stock -= stockitem.Stock;
                                                    stockitem.ActualStock -= stockitem.Stock;
                                                    stockitem.Stock = Decimal.Zero;
                                                }
                                                db.Update<TSTOCKEntity>(stockitem, null);

                                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                stockdetail.ID = Guid.NewGuid().ToString();
                                                stockdetail.FK_Stock = stockitem.GUID;
                                                stockdetail.Stock = outstock - itemstock;
                                                stockdetail.ActualStock = outstock - itemstock;
                                                stockdetail.Type = CodeConst.StockDetailType.CK;
                                                stockdetail.Remark = String.Format("门店出库：{0}", keyValue);
                                                stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                                stockdetail.CreateTime = DateTime.Now;
                                                stockdetail.organizeid = order.organizeid;

                                                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                            }
                                        }
                                    }
                                    foreach (TORDERDETAILEntity orderdetailInfo in list)
                                    {
                                        if (orderdetailInfo.ProductSKU == sku)
                                        {
                                            orderdetailInfo.Status = CodeConst.IsYesNO.YES;
                                            db.Update<TORDERDETAILEntity>(orderdetailInfo, null);
                                        }
                                    }
                                }
                                else
                                {
                                    isResult = false;
                                    break;
                                }
                            }
                            else
                            {
                                isResult = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        string skuStr = sku;
                        List<TSTOCKEntity> stocklist = null;
                        if (packageList != null)
                        {
                            var packageInfo = packageList.Where(p => p.SKUOld == sku).Select(t => new
                            {
                                Guid = t.GUID,
                                PdtSKU = t.SKU,
                                PdtCount = t.ChildCount
                            }).SingleOrDefault();

                            if (packageInfo != null)
                            {
                                skuStr = packageInfo.PdtSKU;
                                pdtcount = packageInfo.PdtCount;

                                stocklist = stockList.Where(t => t.FK_Product == packageInfo.Guid)
                                           .OrderBy(p => p.FK_Location).Select(p => p).ToList();
                            }
                        }
                        if (stocklist == null || stocklist.Count == 0)
                            stocklist = stockList.Join(otherProjectGuids.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == skuStr), a => a.FK_Product, b => b.GUID, (a, b) => a)
                                       .OrderBy(p => p.FK_Location).Select(p => p).ToList();

                        if (stocklist.Count > 0)
                        {
                            decimal? totalstock = stocklist.Sum(p => p.Stock);
                            if (totalstock >= pdtcount)
                            {
                                TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= pdtcount).FirstOrDefault();
                                if (stockinfo != null)
                                {
                                    stockinfo.Stock -= pdtcount;
                                    stockinfo.ActualStock -= pdtcount;

                                    db.Update<TSTOCKEntity>(stockinfo, null);

                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                    stockdetail.ID = Guid.NewGuid().ToString();
                                    stockdetail.FK_Stock = stockinfo.GUID;
                                    stockdetail.Stock = -pdtcount;
                                    stockdetail.ActualStock = -pdtcount;
                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                    stockdetail.Remark = String.Format("门店出库：{0}", keyValue);
                                    stockdetail.Creater = OperatorProvider.Provider.Current().CompanyId;
                                    stockdetail.CreateTime = DateTime.Now;
                                    stockdetail.organizeid = order.organizeid;

                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                }
                                else
                                {
                                    decimal outstock = pdtcount;
                                    while (outstock > 0)
                                    {
                                        foreach (TSTOCKEntity stockitem in stocklist)
                                        {
                                            decimal itemstock = outstock;
                                            if (stockitem.Stock >= outstock)
                                            {
                                                stockitem.Stock -= outstock;
                                                stockitem.ActualStock -= outstock;
                                                outstock = Decimal.Zero;
                                            }
                                            else
                                            {
                                                outstock -= stockitem.Stock ?? 0;
                                                stockitem.Stock -= stockitem.Stock;
                                                stockitem.ActualStock -= stockitem.Stock;
                                                stockitem.Stock = Decimal.Zero;
                                            }
                                            db.Update<TSTOCKEntity>(stockitem, null);

                                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                            stockdetail.ID = Guid.NewGuid().ToString();
                                            stockdetail.FK_Stock = stockitem.GUID;
                                            stockdetail.Stock = outstock - itemstock;
                                            stockdetail.ActualStock = outstock - itemstock;
                                            stockdetail.Type = CodeConst.StockDetailType.CK;
                                            stockdetail.Remark = String.Format("门店出库：{0}", keyValue);
                                            stockdetail.Creater = OperatorProvider.Provider.Current().CompanyId;
                                            stockdetail.CreateTime = DateTime.Now;
                                            stockdetail.organizeid = order.organizeid;

                                            db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                        }
                                    }
                                }
                                foreach (TORDERDETAILEntity orderdetailInfo in list)
                                {
                                    if (orderdetailInfo.ProductSKU == sku)
                                    {
                                        orderdetailInfo.Status = CodeConst.IsYesNO.YES;
                                        db.Update<TORDERDETAILEntity>(orderdetailInfo, null);
                                    }
                                }
                            }
                            else
                            {
                                isResult = false;
                                continue;
                            }
                        }
                        else
                        {
                            isResult = false;
                            continue;
                        }
                    }
                }

                if (isResult)
                {
                    order.OrderStatus = CodeConst.IsYesNO.YES;
                    order.FK_ActualStore = storeGUID;
                    db.Update<TORDEREntity>(order, null);
                    db.Commit();
                }
                else
                {
                    db.Rollback();
                    return "库存不足,出库失败!";
                }
            }
            catch (Exception ex)
            {
                db.Rollback();
                throw (new Exception(ex.ToString()));
            }
            return "";
        }

        /// <summary>
        /// 小程序自动完成订单
        /// </summary>
        public void ConfirmOrders()
        {
            lock (_lock)
            {
                Data.IDatabase db = DbFactory.Base();

                DateTime endtime = DateTime.Now.AddDays(-CodeConst.DeliveryDays);
                IList<TORDEREntity> orderlist = db.IQueryable<TORDEREntity>().Where(p => p.IsDel == 0 && p.OrderStatus == CodeConst.OrderStatus.COLLECT && p.DeliverTime < endtime).ToList();
                IList<String> orderids = orderlist.Select(t => t.GUID).ToList();
                IList<TORDERSTOCKEntity> orderstockList = new List<TORDERSTOCKEntity>();
                IList<TSTOCKEntity> stockList = new List<TSTOCKEntity>();
                if (CodeConst.IsUseStock)
                {
                    orderstockList = db.IQueryable<TORDERSTOCKEntity>().Where(p => p.Status == CodeConst.OrderStockStatus.YTC && orderids.Contains(p.FK_Order)).ToList();
                    IList<String> orderstockIds = orderstockList.Select(t => t.FK_Stock).ToList();
                    stockList = db.IQueryable<TSTOCKEntity>(t => orderstockIds.Contains(t.GUID)).ToList();
                }
                try
                {
                    db.BeginTrans();

                    foreach (TORDEREntity order in orderlist)
                    {
                        int integral = Convert.ToInt32(order.OrderPrice / CodeConst.Integral.PAYORDER);

                        TUSEREntity user = db.IQueryable<TUSEREntity>().Where(p => p.Openid == order.FK_Openid && p.IsDel == CodeConst.IsDel.NO)
                            .Join(db.IQueryable<TMEMBEREntity>(), m => m.Openid, n => n.FK_UnionId, (m, n) => m).SingleOrDefault();
                        if (user != null)
                        {
                            user.Integral += integral;
                            TINTEGRALEntity tintegral = IntegralRecord(order.FK_Openid, order.FK_AppId, integral, user.Integral.Value, CodeConst.IntegralType.PAYORDER, CodeConst.IntegralRemark.PAYORDER);
                            db.Insert<TINTEGRALEntity>(tintegral);
                            db.Update<TUSEREntity>(user, null);
                        }

                        order.OrderStatus = CodeConst.OrderStatus.COMPLETE;
                        order.Completer = "系统操作";
                        order.CompleteTime = DateTime.Now;
                        order.Modifyer = "系统操作";
                        order.ModifyTime = DateTime.Now;
                        db.Update<TORDEREntity>(order, null);

                        if (CodeConst.IsUseStock)
                        {
                            List<TORDERSTOCKEntity> list = orderstockList.Where(p => p.FK_Order == order.GUID && p.Status == CodeConst.OrderStockStatus.YTC).ToList();

                            foreach (TORDERSTOCKEntity orderstock in list)
                            {
                                orderstock.Status = CodeConst.OrderStockStatus.YWC;
                                db.Update<TORDERSTOCKEntity>(orderstock, null);

                                TSTOCKEntity stockInfo = stockList.Where(p => p.GUID == orderstock.FK_Stock).SingleOrDefault();

                                if (stockInfo != null)
                                {
                                    stockInfo.ActualStock -= orderstock.StockCount;
                                    db.Update<TSTOCKEntity>(stockInfo, null);

                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                    stockdetail.ID = Guid.NewGuid().ToString();
                                    stockdetail.FK_Stock = stockInfo.GUID;
                                    stockdetail.ActualStock = -orderstock.StockCount;
                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                    stockdetail.Remark = "小程序订单完成";
                                    stockdetail.Creater = "系统操作";
                                    stockdetail.CreateTime = DateTime.Now;
                                    stockdetail.organizeid = order.organizeid;
                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                }
                            }
                        }
                    }

                    db.Commit();
                }
                catch (Exception e)
                {
                    db.Rollback();
                    throw new Exception(e.ToString());
                }
            }
        }

        public TINTEGRALEntity IntegralRecord(string openId, string appguid, int integral, int surplusintegral, int type, string remark)
        {
            TINTEGRALEntity record = new TINTEGRALEntity();
            record.GUID = Guid.NewGuid().ToString();
            record.FK_Openid = openId;
            record.FK_AppId = appguid;
            record.Integral = integral;
            record.SurplusIntegral = surplusintegral;
            record.IntegralRemark = remark;
            record.IntegralType = type;
            record.CreateTime = DateTime.Now;
            return record;
        }

        /// <summary>
        /// 删除团购订单
        /// </summary>
        /// <param name="wxopenId"></param>
        public void DeleteTimeOutGroupOrder(string wxopenId)
        {
            Data.IDatabase db = DbFactory.Base();
            try
            {
                List<string> guidlist = db.IQueryable<VOUTGROUPEntity>().Select(p => p.GUID).ToList();
                List<TORDEREntity> orderlist = db.IQueryable<TORDEREntity>().Where(p => guidlist.Contains(p.GUID) && p.IsDel == CodeConst.IsDel.NO).ToList();

                TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == CodeConst.WxAppKEY).SingleOrDefault();

                IList<String> orderids = orderlist.Select(t => t.GUID).ToList();
                IList<TORDERDETAILEntity> oRDERDETAILList = db.IQueryable<TORDERDETAILEntity>().Where(t => orderids.Contains(t.FK_Order) && t.IsDel == 0).ToList();
                IList<String> productclassids = oRDERDETAILList.Select(t => t.FK_ProductClass).ToList();
                IList<T_PRODUCTCLASSEntity> t_PRODUCTCLASSList = db.IQueryable<T_PRODUCTCLASSEntity>().Where(t => t.IsDel == 0 && productclassids.Contains(t.GUID)).ToList();

                db.BeginTrans();
                foreach (TORDEREntity order in orderlist)
                {
                    try
                    {
                        string orderprice = (order.OrderPrice * 100).ToString();
                        order.PayResult = CodeConst.PayResult.PayReturn;
                        order.OrderStatus = CodeConst.OrderStatus.FAILED;
                        order.Modifyer = wxopenId;
                        order.ModifyTime = DateTime.Now;
                        db.Update<TORDEREntity>(order, null);

                        Hashtable sqlListHashtable = new Hashtable();
                        var productclasslist = oRDERDETAILList.Where(p => p.FK_Order == order.GUID)
                            .Join(t_PRODUCTCLASSList, m => m.FK_ProductClass, n => n.GUID, (m, n) => new
                            {
                                SKU = n.ProductSKU,
                                pdtCount = m.Count * n.ProductStock.Value
                            }).ToList();
                        int nctnumber = 1;

                        IList<String> SKUs = productclasslist.Select(t => t.SKU).ToList();
                        IList<VSTOCKEntity> vstockList = db.IQueryable<VSTOCKEntity>().Where(x => SKUs.Contains(x.SKU)).ToList();
                        IList<String> aSKUs = vstockList.Select(t => t.ActualSKU).ToList();
                        IList<VPRODUCTCOMBEntity> vproductcombList = db.IQueryable<VPRODUCTCOMBEntity>().Where(x => aSKUs.Contains(x.SKU)).ToList();
                        foreach (var pdtclass in productclasslist)
                        {
                            VSTOCKEntity stockInfo = vstockList.Where(p => p.SKU == pdtclass.SKU).FirstOrDefault();
                            List<VPRODUCTCOMBEntity> comblist = vproductcombList.Where(p => p.SKU == stockInfo.ActualSKU).ToList();
                            if (comblist.Count > 0)
                            {
                                foreach (VPRODUCTCOMBEntity comb in comblist)
                                {
                                    sqlListHashtable = WSCSql.GetUpdateProductSql(comb.ChildSKU, (comb.ChildCount.Value * -pdtclass.pdtCount * stockInfo.PdtCount.Value) ?? 0, nctnumber++, sqlListHashtable);
                                }
                            }
                            else
                            {
                                sqlListHashtable = WSCSql.GetUpdateProductSql(stockInfo.ActualSKU, (-pdtclass.pdtCount * stockInfo.PdtCount.Value) ?? 0, nctnumber++, sqlListHashtable);
                            }
                        }
                        foreach (DictionaryEntry myDE in sqlListHashtable)
                        {
                            string cmdText = myDE.Key.ToString();
                            SqlParameter[] cmdParms = (SqlParameter[])myDE.Value;
                            db.ExecuteBySql(cmdText, cmdParms);
                        }
                        Refund.Run(wxapp, order.GUID, orderprice, orderprice, "团购失败退款");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }
                }
                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// 删除超时的订单
        /// </summary>
        public void SystemDeleteTimeOutOrder(string wxopenId)
        {

            Data.IDatabase db = DbFactory.Base();
            List<TORDEREntity> orderlist = new List<TORDEREntity>();

            DateTime nowtime = DateTime.Now.AddMinutes(-CodeConst.PayMinutes);
            IQueryable<TORDEREntity> orderquery = db.IQueryable<TORDEREntity>().Where(p => p.OrderStatus == CodeConst.OrderStatus.PAYMENT && p.IsDel == CodeConst.IsDel.NO && p.PayResult == CodeConst.IsUse.NO && p.CreateTime < nowtime);
            if (!String.IsNullOrEmpty(wxopenId))
            {
                orderquery = orderquery.Where(t => t.FK_Openid == wxopenId);
            }
            orderlist = orderquery.ToList();
            if (orderlist.Count == 0)
            {
                return;
            }
            IList<String> couponids = orderlist.Select(t => t.Coupon).ToList();
            IList<String> orderids = orderlist.Select(t => t.GUID).ToList();
            List<TUSERCARDEntity> cardList = db.IQueryable<TUSERCARDEntity>().Where(p => couponids.Contains(p.GUID) && p.IsDel == CodeConst.IsDel.NO).ToList();

            IList<TORDEREntity> newOrderList = null;
            if (cardList.Count > 0)
                newOrderList = db.IQueryable<TORDEREntity>().Where(p => p.OrderStatus > CodeConst.OrderStatus.PAYMENT && p.OrderStatus < CodeConst.OrderStatus.FAILED && couponids.Contains(p.Coupon)).ToList();

            try
            {
                IList<TORDERDETAILEntity> oRDERDETAILList = db.IQueryable<TORDERDETAILEntity>().Where(t => orderids.Contains(t.FK_Order) && t.IsDel == 0).ToList();
                IList<String> productclassids = oRDERDETAILList.Select(t => t.FK_ProductClass).ToList();
                IList<T_PRODUCTCLASSEntity> t_PRODUCTCLASSList = new ProductClassCache().GetCacheList().Where(t => t.IsDel == 0 && productclassids.Contains(t.GUID)).ToList();
                IList<T_PRODUCTPRICEEntity> t_PRODUCTPriceList = new ProductPriceCache().GetCacheList();
                db.BeginTrans();
                foreach (TORDEREntity order in orderlist)
                {
                    TUSERCARDEntity usercard = cardList.FirstOrDefault(p => p.GUID == order.Coupon && p.FK_OpenId == order.FK_Openid && p.IsDel == CodeConst.IsDel.NO);
                    if (usercard != null)
                    {
                        TORDEREntity neworder = newOrderList.FirstOrDefault(p => p.FK_Openid == order.FK_Openid && p.Coupon == usercard.GUID && p.OrderStatus > CodeConst.OrderStatus.PAYMENT && p.OrderStatus < CodeConst.OrderStatus.FAILED);
                        if (neworder == null)
                        {
                            usercard.IsUse = CodeConst.CardUse.NOUSE;
                            db.Update<TUSERCARDEntity>(usercard, null);
                        }
                    }
                    order.IsDel = CodeConst.IsDel.YES;
                    order.Deleter = "系统操作";
                    order.DeleteTime = DateTime.Now;
                    db.Update<TORDEREntity>(order, null);

                    Hashtable sqlListHashtable = new Hashtable();
                    var productclasslist = oRDERDETAILList.Where(p => p.FK_Order == order.GUID).Join(t_PRODUCTCLASSList, m => m.FK_ProductClass, n => n.GUID, (m, n) => new
                    {
                        SKU = n.ProductSKU,
                        pdtCount = m.Count * n.ProductStock.Value,
                        productclass = n.FK_Product,
                        Count = m.Count,
                    }).ToList();
                    int nctnumber = 1;

                    IList<String> SKUs = productclasslist.Select(t => t.SKU).ToList();
                    IList<VSTOCKEntity> vstockList = db.IQueryable<VSTOCKEntity>().Where(x => SKUs.Contains(x.SKU)).ToList();
                    IList<String> aSKUs = vstockList.Select(t => t.ActualSKU).ToList();
                    IList<VPRODUCTCOMBEntity> vproductcombList = db.IQueryable<VPRODUCTCOMBEntity>().Where(x => aSKUs.Contains(x.SKU)).ToList();
                    foreach (var pdtclass in productclasslist)
                    {
                        T_PRODUCTPRICEEntity price = t_PRODUCTPriceList.FirstOrDefault(t => t.GUID == pdtclass.productclass);
                        if (price != null)
                        {
                            TMODELPRODUCTFruitsEntity fruitsEntity = db.IQueryable<TMODELPRODUCTFruitsEntity>().FirstOrDefault(t => t.ImageNavigate1 == price.GUID && t.IsDel == 0 && t.time1.HasValue);
                            if (fruitsEntity != null)
                            {
                                fruitsEntity.num2 = fruitsEntity.num2 - Convert.ToInt32(pdtclass.Count);
                                db.Update<TMODELPRODUCTFruitsEntity>(fruitsEntity, null);
                            }
                        }
                        if (CodeConst.IsUseStock)
                        {
                            VSTOCKEntity stockInfo = vstockList.Where(p => p.SKU == pdtclass.SKU).FirstOrDefault();
                            List<VPRODUCTCOMBEntity> comblist = vproductcombList.Where(p => p.SKU == stockInfo.ActualSKU).ToList();
                            if (comblist.Count > 0)
                            {
                                foreach (VPRODUCTCOMBEntity comb in comblist)
                                {
                                    sqlListHashtable = WSCSql.GetUpdateProductSql(comb.ChildSKU, comb.ChildCount.Value * -(pdtclass.pdtCount ?? 0) * stockInfo.PdtCount.Value, nctnumber++, sqlListHashtable);
                                }
                            }
                            else
                            {
                                sqlListHashtable = WSCSql.GetUpdateProductSql(stockInfo.ActualSKU, -(pdtclass.pdtCount ?? 0) * stockInfo.PdtCount.Value, nctnumber++, sqlListHashtable);
                            }
                        }
                    }
                    foreach (DictionaryEntry myDE in sqlListHashtable)
                    {
                        string cmdText = myDE.Key.ToString();
                        SqlParameter[] cmdParms = (SqlParameter[])myDE.Value;
                        db.ExecuteBySql(cmdText, cmdParms);
                    }
                }

                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
        }

        /// <summary>
        /// 门店订单自动出库
        /// </summary>
        /// <param name="warehouseNo"></param>
        /// <param name="operatorId"></param>
        /// <param name="orderId"></param>
        /// <param name="dic"></param>
        /// <returns></returns>
        public bool ProductOutStock(string warehouseNo, string operatorId, string orderId, Dictionary<string, decimal> dic)
        {
            bool isResult = true;

            Data.IDatabase db = DbFactory.Base();
            TORDEREntity order = db.IQueryable<TORDEREntity>().FirstOrDefault(t => t.OrderNumber == orderId);
            if (order.IsDel == 1)
            {
                return false;
            }
            if (order.OrderStatus != 1)
            {
                return false;
            }
            IList<TORDERDETAILEntity> list = db.IQueryable<TORDERDETAILEntity>().Where(p => p.FK_Order == order.GUID && p.Status == CodeConst.IsYesNO.NO).ToList();

            T_STOREEntity store = db.IQueryable<T_STOREEntity>().FirstOrDefault(t => t.GUID == warehouseNo);

            IList<String> skus = dic.Keys.ToList();

            IList<String> projectGuids = new List<String>();
            //取出组合商品
            IList<ProductInfoAll> productInfoList = db.IQueryable<TPRODUCTEntity>().Where(p => p.organizeid == order.organizeid && p.IsDel == CodeConst.IsDel.NO && skus.Contains(p.SKU))
            .Join(db.IQueryable<TPRODUCTCOMBEntity>().Where(x => x.IsDel == 0), m => m.GUID, n => n.FK_Product, (m, n) => new { m, n })
            .Join(db.IQueryable<TPRODUCTEntity>().Where(x => x.IsDel == 0 && x.organizeid == order.organizeid), x => x.n.FK_Child, y => y.GUID, (x, y) => new ProductInfoAll
            {
                SKUOld = x.m.SKU,
                GUID = y.GUID,
                ProductNo = y.ProductNo,
                SKU = y.SKU,
                ChildCount = x.n.ChildCount.Value
            }).ToList();

            projectGuids = projectGuids.Concat(productInfoList.Select(t => t.GUID)).ToList();
            //过滤不是组合商品的sku
            skus = new List<String>();
            foreach (String key in dic.Keys)
            {
                if (productInfoList.Count(t => t.SKUOld == key) <= 0)
                    skus.Add(key);
            }

            IList<ProductInfoAll> packageList = null;
            IList<TPRODUCTEntity> otherProjectGuids = null;
            if (skus.Count > 0)
            {
                //包装商品
                packageList = db.IQueryable<TPACKAGEEntity>(t => t.IsDel == 0 && t.organizeid == order.organizeid).Where(p => skus.Contains(p.PackageSKU)).Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.organizeid == order.organizeid && p.IsDel == CodeConst.IsDel.NO), m => m.FK_Product, n => n.GUID, (m, n) => new ProductInfoAll
                {
                    SKUOld = m.PackageSKU,
                    GUID = n.GUID,
                    SKU = n.SKU,
                    ChildCount = m.PdtCount.Value
                }).ToList();
                projectGuids = projectGuids.Concat(packageList.Select(t => t.GUID)).ToList();
                IList<String> temskus = new List<String>();
                foreach (String p in skus)
                {
                    if (packageList.Count(t => t.SKUOld == p) <= 0)
                        temskus.Add(p);
                }

                if (temskus.Count > 0)//存在单独的商品
                {
                    otherProjectGuids = db.IQueryable<TPRODUCTEntity>().Where(p => p.organizeid == order.organizeid && p.IsDel == CodeConst.IsDel.NO && temskus.Contains(p.SKU)).ToList();

                    projectGuids = projectGuids.Concat(otherProjectGuids.Select(t => t.GUID)).ToList();
                }
            }
            //获取门店下的location和库存信息
            IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(t => t.IsDel == 0 && projectGuids.Contains(t.FK_Product) && t.FK_WarehouseNo == store.StoreId).ToList();

            try
            {
                db.BeginTrans();
                foreach (string sku in dic.Keys)
                {
                    TORDERDETAILEntity orderdetailInfo = list.Where(p => p.ProductSKU == sku).FirstOrDefault();
                    decimal pdtcount = dic[sku];

                    var combllist = productInfoList.Where(p => p.SKUOld == sku)
                    .Select(t => new
                    {
                        ProductSKU = t.SKU,
                        ProductCount = pdtcount * t.ChildCount
                    }).ToList();

                    if (combllist.Count > 0)
                    {
                        foreach (var combpdt in combllist)
                        {
                            List<TSTOCKEntity> stocklist = stockList
                                       .Join(productInfoList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == combpdt.ProductSKU), a => a.FK_Product, b => b.GUID, (a, b) => a)
                                       .OrderBy(p => p.FK_Location).Select(p => p).ToList();

                            if (stocklist.Count > 0)
                            {
                                decimal totalstock = stocklist.Sum(p => p.Stock) ?? 0;
                                if (totalstock >= combpdt.ProductCount)
                                {
                                    TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= combpdt.ProductCount).FirstOrDefault();
                                    if (stockinfo != null)
                                    {
                                        stockinfo.Stock -= combpdt.ProductCount;
                                        stockinfo.ActualStock -= combpdt.ProductCount;
                                        db.Update<TSTOCKEntity>(stockinfo, null);

                                        TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                        stockdetail.ID = Guid.NewGuid().ToString();
                                        stockdetail.FK_Stock = stockinfo.GUID;
                                        stockdetail.Stock = -combpdt.ProductCount;
                                        stockdetail.ActualStock = -combpdt.ProductCount;
                                        stockdetail.Type = CodeConst.StockDetailType.CK;
                                        stockdetail.Remark = String.Format("门店出库：{0}", orderId);
                                        stockdetail.Creater = operatorId;
                                        stockdetail.CreateTime = DateTime.Now;
                                        stockdetail.organizeid = order.organizeid;

                                        db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                    }
                                    else
                                    {
                                        decimal outstock = combpdt.ProductCount;
                                        while (outstock > 0)
                                        {
                                            foreach (TSTOCKEntity stockitem in stocklist)
                                            {
                                                decimal itemstock = outstock;
                                                if (stockitem.Stock >= outstock)
                                                {
                                                    stockitem.Stock -= outstock;
                                                    stockitem.ActualStock -= outstock;
                                                    outstock = Decimal.Zero;
                                                }
                                                else
                                                {
                                                    outstock -= stockitem.Stock ?? 0;
                                                    stockitem.Stock -= stockitem.Stock;
                                                    stockitem.ActualStock -= stockitem.Stock;
                                                    stockitem.Stock = Decimal.Zero;
                                                }
                                                db.Update<TSTOCKEntity>(stockitem, null);

                                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                stockdetail.FK_Stock = stockitem.GUID;
                                                stockdetail.Stock = outstock - itemstock;
                                                stockdetail.ActualStock = outstock - itemstock;
                                                stockdetail.Type = CodeConst.StockDetailType.CK;
                                                stockdetail.Remark = String.Format("门店出库：{0}", orderId);
                                                stockdetail.Creater = operatorId;
                                                stockdetail.CreateTime = DateTime.Now;
                                                stockdetail.ID = Guid.NewGuid().ToString();
                                                stockdetail.organizeid = order.organizeid;
                                                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                            }
                                        }
                                    }
                                    orderdetailInfo.Status = CodeConst.IsYesNO.YES;
                                    db.Update<TORDERDETAILEntity>(orderdetailInfo, null);
                                }
                                else
                                {
                                    isResult = false;
                                    break;
                                }
                            }
                            else
                            {
                                isResult = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        string skuStr = sku;
                        List<TSTOCKEntity> stocklist = null;
                        if (packageList != null)
                        {
                            var packageInfo = packageList.Where(p => p.SKUOld == sku).Select(t => new
                            {
                                Guid = t.GUID,
                                PdtSKU = t.SKU,
                                PdtCount = t.ChildCount
                            }).SingleOrDefault();

                            if (packageInfo != null)
                            {
                                skuStr = packageInfo.PdtSKU;
                                pdtcount = packageInfo.PdtCount;

                                stocklist = stockList.Where(t => t.FK_Product == packageInfo.Guid)
                                           .OrderBy(p => p.FK_Location).Select(p => p).ToList();
                            }
                        }
                        if (stocklist == null || stocklist.Count == 0)
                            stocklist = stockList.Join(otherProjectGuids.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == skuStr), a => a.FK_Product, b => b.GUID, (a, b) => a)
                                       .OrderBy(p => p.FK_Location).Select(p => p).ToList();

                        if (stocklist.Count > 0)
                        {
                            decimal totalstock = stocklist.Sum(p => p.Stock) ?? 0;
                            if (totalstock >= pdtcount)
                            {
                                TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= pdtcount).FirstOrDefault();
                                if (stockinfo != null)
                                {
                                    stockinfo.Stock -= pdtcount;
                                    stockinfo.ActualStock -= pdtcount;
                                    db.Update<TSTOCKEntity>(stockinfo, null);

                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                    stockdetail.FK_Stock = stockinfo.GUID;
                                    stockdetail.Stock = -pdtcount;
                                    stockdetail.ActualStock = -pdtcount;
                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                    stockdetail.Remark = String.Format("门店出库：{0}", orderId);
                                    stockdetail.Creater = operatorId;
                                    stockdetail.CreateTime = DateTime.Now;
                                    stockdetail.ID = Guid.NewGuid().ToString();
                                    stockdetail.organizeid = order.organizeid;
                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                }
                                else
                                {
                                    decimal outstock = pdtcount;
                                    while (outstock > 0)
                                    {
                                        foreach (TSTOCKEntity stockitem in stocklist)
                                        {
                                            decimal itemstock = outstock;
                                            if (stockitem.Stock >= outstock)
                                            {
                                                stockitem.Stock -= outstock;
                                                stockitem.ActualStock -= outstock;
                                                outstock = Decimal.Zero;
                                            }
                                            else
                                            {
                                                outstock -= stockitem.Stock ?? 0;
                                                stockitem.Stock -= stockitem.Stock;
                                                stockitem.ActualStock -= stockitem.Stock;
                                                stockitem.Stock = Decimal.Zero;
                                            }
                                            db.Update<TSTOCKEntity>(stockitem, null);

                                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                            stockdetail.FK_Stock = stockitem.GUID;
                                            stockdetail.Stock = outstock - itemstock;
                                            stockdetail.ActualStock = outstock - itemstock;
                                            stockdetail.Type = CodeConst.StockDetailType.CK;
                                            stockdetail.Remark = String.Format("门店出库：{0}", orderId);
                                            stockdetail.Creater = operatorId;
                                            stockdetail.CreateTime = DateTime.Now;
                                            stockdetail.ID = Guid.NewGuid().ToString();
                                            stockdetail.organizeid = order.organizeid;
                                            db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                        }
                                    }
                                }
                                orderdetailInfo.Status = CodeConst.IsYesNO.YES;
                                db.Update<TORDERDETAILEntity>(orderdetailInfo, null);
                            }
                            else
                            {
                                isResult = false;
                                continue;
                            }
                        }
                        else
                        {
                            isResult = false;
                            continue;
                        }
                    }
                }
                if (isResult)
                {
                    order.OrderStatus = CodeConst.IsYesNO.YES;
                    db.Update<TORDEREntity>(order, null);
                }
                else
                {
                }
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return isResult;
        }
        #endregion
    }
}
