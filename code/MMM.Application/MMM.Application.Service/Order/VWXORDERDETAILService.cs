using MMM.Application.Code;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Product;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：小程序订单明细
    /// </summary>
    public class VWXORDERDETAILService : RepositoryFactory<VWXORDERDETAILEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<VWXORDERDETAILEntity> GetPageList(JObject queryJson)
        {
            var expression = BaseRepository().IQueryable();
            if (!queryJson["FK_Order"].IsEmpty())
            {
                String FK_Order = queryJson["FK_Order"].ToString();
                expression = expression.Where(t => t.FK_Order == FK_Order);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.Where(t => t.organizeid == organizeid);
            }

            return expression.ToList();
        }

        /// <summary>
        /// 删除小程序订单明细
        /// </summary>
        /// <param name="keyValue"></param>
        public void RemoveForm(string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();

            string sku = String.Empty;
            decimal pdtcount = Decimal.Zero;

            TORDERDETAILEntity detail = db.IQueryable<TORDERDETAILEntity>().Where(p => p.GUID == keyValue && p.IsDel == CodeConst.IsDel.NO).SingleOrDefault();
            if (detail != null)
            {
                try
                {
                    db.BeginTrans();
                    pdtcount = detail.Count ?? 0;
                    sku = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.GUID == detail.FK_ProductClass).Select(p => p.ProductSKU).SingleOrDefault();

                    detail.IsDel = CodeConst.IsDel.YES;
                    detail.Deleter = OperatorProvider.Provider.Current().Account;
                    detail.DeleteTime = DateTime.Now;
                    db.Update<TORDERDETAILEntity>(detail, null);

                    TORDEREntity orderInfo = db.IQueryable<TORDEREntity>().SingleOrDefault(p => p.GUID == detail.FK_Order);
                    if (orderInfo != null)
                    {
                        orderInfo.OrderPrice = orderInfo.OrderPrice - detail.ActualPrice * detail.Count;//db.IQueryable<TORDERDETAILEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_Order == detail.FK_Order).Sum(p => p.ActualPrice * p.Count);
                        db.Update<TORDEREntity>(orderInfo, null);
                    }

                    TPRODUCTEntity product = db.IQueryable<TPRODUCTEntity>().FirstOrDefault(p => p.SKU == sku && p.IsDel == CodeConst.IsDel.NO);
                    if (product != null)
                    {
                        product.SaleCount -= pdtcount;
                        db.Update<TPRODUCTEntity>(product, null);
                    }
                    db.Commit();
                }
                catch (Exception e)
                {
                    db.Rollback();
                    throw new Exception(e.ToString());
                }
            }

        }

        /// <summary>
        /// 保存小程序订单明细
        /// </summary>
        /// <param name="FK_Order"></param>
        /// <param name="sku"></param>
        /// <param name="price"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public string SaveDetailForm(String FK_Order, string sku, decimal price, decimal count)
        {
            Data.IDatabase db = DbFactory.Base();

            TPRODUCTEntity product = db.IQueryable<TPRODUCTEntity>().FirstOrDefault(p => p.SKU == sku && p.IsDel == CodeConst.IsDel.NO);
            if (product != null)
            {
                product.SaleCount += count;
            }
            else
            {
                return "仓储条码不存在！";
            }

            T_PRODUCTCLASSEntity productclass = db.IQueryable<T_PRODUCTCLASSEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.ProductSKU == sku).OrderBy(p => p.ProductStock).FirstOrDefault();
            if (productclass == null)
            {
                return "小程序条码不存在！";
            }
            TORDERDETAILEntity detailInfo = new TORDERDETAILEntity();
            detailInfo.GUID = Guid.NewGuid().ToString();
            detailInfo.FK_Order = FK_Order;
            detailInfo.FK_ProductClass = productclass.GUID;
            detailInfo.OldPrice = productclass.ClassOldPrice;
            detailInfo.Price = productclass.ClassPrice;
            detailInfo.ActualPrice = price;
            detailInfo.Count = count;
            detailInfo.Status = 0;
            detailInfo.IsDel = CodeConst.IsDel.NO;
            detailInfo.Creater = OperatorProvider.Provider.Current().Account;
            detailInfo.CreateTime = DateTime.Now;
            detailInfo.organizeid = productclass.organizeid;

            TORDEREntity orderInfo = db.IQueryable<TORDEREntity>().SingleOrDefault(p => p.GUID == FK_Order);
            if (orderInfo != null)
            {
                orderInfo.OrderPrice += detailInfo.ActualPrice * detailInfo.Count;
            }
            try
            {
                db.BeginTrans();
                db.Update<TPRODUCTEntity>(product, null);
                db.Insert<TORDERDETAILEntity>(detailInfo);
                db.Update<TORDEREntity>(orderInfo, null);
                db.Commit();
            }
            catch (Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
            return "";
        }
        #endregion
    }
}
