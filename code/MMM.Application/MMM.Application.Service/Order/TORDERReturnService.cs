using MMM.Application.Entity;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.WebService;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-23 21:32
    /// 描 述：退货单
    /// </summary>
    public class TORDERReturnService : RepositoryFactory<TORDERReturnEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TORDERReturnEntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TORDERReturnEntity> GetList(JObject queryJson)
        {
            var expression = this.BaseRepository().IQueryable().Where(x => x.IsDel == 0);
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                expression = expression.Where(dic => dic.FK_WarehouseNo == storeGUID);
            }
            if (!queryJson["startdate"].IsEmpty())
            {
                DateTime startdate = Convert.ToDateTime(queryJson["startdate"]);
                expression = expression.Where(dic => dic.CreateTime >= startdate);
            }
            if (!queryJson["enddate"].IsEmpty())
            {
                DateTime enddate = Convert.ToDateTime(queryJson["enddate"]);
                expression = expression.Where(dic => dic.CreateTime < enddate);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = Convert.ToString(queryJson["organizeid"]);
                expression = expression.Where(dic => dic.organizeid == organizeid);
            }
            IList<TORDERReturnEntity> orderList = expression.OrderByDescending(t => t.CreateTime).ToList();
            return orderList;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TORDERReturnEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TORDERReturnEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 门店订单退货
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public ResultData ProductReturnStore(InputData input)
        {
            ResultData result = new ResultData();

            Data.IDatabase db = DbFactory.Base();

            TORDERReturnEntity orderreturn = JSONHelper.DecodeObject<TORDERReturnEntity>(input.value);
            orderreturn.CreateTime = DateTime.Now;

            //查询订单是否存在
            TORDEREntity order = db.IQueryable<TORDEREntity>().FirstOrDefault(t => t.OrderNumber == orderreturn.OrderNumber);
            if (order == null)
            {
                result.msg = "订单不存在!";
                return result;
            }
            if (order.OrderStatus == CodeConst.IsYesNO.NO)
            {
                result.msg = "订单未出库!";
                return result;
            }
            T_STOREEntity store = db.IQueryable<T_STOREEntity>().FirstOrDefault(t=>t.GUID== orderreturn.FK_WarehouseNo);
            IList<TORDERDETAILEntity> orderDetailList = db.IQueryable<TORDERDETAILEntity>().Where(t => t.FK_Order == order.GUID).ToList();

            List<TORDERReturnDetailEntity> returndetaillist = JSONHelper.DecodeObject<List<TORDERReturnDetailEntity>>(input.valuelist);

            //查询是否存在已经退过商品
            IList<TORDERReturnEntity> oldorderreturnList = db.IQueryable<TORDERReturnEntity>().Where(t => t.OrderNumber == order.OrderNumber).ToList();
            IList<String> orderreturnIds = oldorderreturnList.Select(t => t.GUID).ToList();
            IList<TORDERReturnDetailEntity> oldorderreturndetailList = db.IQueryable<TORDERReturnDetailEntity>().Where(t => orderreturnIds.Contains(t.FK_Return)).ToList();

            Dictionary<string, decimal> dic = new Dictionary<string, decimal>();
            foreach (TORDERReturnDetailEntity returndetail in returndetaillist)
            {
                dic[returndetail.ProductSKU] = returndetail.Count ?? 0;
            }
            try
            {
                db.BeginTrans();

                foreach (string sku in dic.Keys)
                {
                    decimal pdtcount = dic[sku];
                    decimal saleCount = orderDetailList.Where(t => t.ProductSKU == sku).Sum(t => t.Count) ?? 0;
                    decimal returnCount = oldorderreturndetailList.Where(t => t.ProductSKU == sku).Sum(t => t.Count) ?? 0;
                    if (pdtcount + returnCount > saleCount)
                    {
                        result.msg = "退货数量不能大于销售数量!";
                        return result;
                    }

                    var combllist = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == sku)
                    .Join(db.IQueryable<TPRODUCTCOMBEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), m => m.GUID, n => n.FK_Product, (m, n) => new { m, n })
                    .Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO), x => x.n.FK_Child, y => y.GUID, (x, y) => new
                    {
                        ProductSKU = y.SKU,
                        ProductCount = pdtcount * x.n.ChildCount.Value
                    }).ToList();

                    if (combllist.Count > 0)//如果是组合商品
                    {
                        foreach (var combpdt in combllist)
                        {
                            TPRODUCTEntity productInfo = db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == combpdt.ProductSKU).FirstOrDefault();

                            TSTOCKEntity stock = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == store.StoreId && p.FK_Product == productInfo.GUID).FirstOrDefault();

                            if (stock != null)
                            {
                                stock.Stock += combpdt.ProductCount;
                                stock.ActualStock += combpdt.ProductCount;
                                db.Update<TSTOCKEntity>(stock, null);

                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                stockdetail.ID = Guid.NewGuid().ToString();
                                stockdetail.FK_Stock = stock.GUID;
                                stockdetail.Stock = combpdt.ProductCount;
                                stockdetail.ActualStock = pdtcount;
                                stockdetail.Type = CodeConst.StockDetailType.RK;
                                stockdetail.Remark = String.Format("门店仓库{0}退货入库", store.StoreId);
                                stockdetail.Creater = orderreturn.Creater;
                                stockdetail.CreateTime = DateTime.Now;
                                stockdetail.organizeid = order.organizeid;
                                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                            }
                            else
                            {
                                result.msg = "库存不存在！";
                                return result;
                            }
                        }
                    }
                    else
                    {
                        //判断是否是包装商品
                        var packageInfo = db.IQueryable<TPACKAGEEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.PackageSKU == sku).Join(db.IQueryable<TPRODUCTEntity>(), m => m.FK_Product, n => n.GUID, (m, n) => new
                        {
                            PdtSKU = n.SKU,
                            PdtCount = m.PdtCount
                        }).SingleOrDefault();

                        string skuStr = sku;

                        if (packageInfo != null)
                        {
                            skuStr = packageInfo.PdtSKU;
                            pdtcount = packageInfo.PdtCount ?? 0;
                        }

                        List<TSTOCKEntity> stocklist = db.IQueryable<TSTOCKEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.FK_WarehouseNo == store.StoreId)
.Join(db.IQueryable<TPRODUCTEntity>().Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == skuStr), a => a.FK_Product, b => b.GUID, (a, b) => a).OrderBy(p => p.FK_Location).ToList();

                        if (stocklist.Count > 0)
                        {
                            TSTOCKEntity stockinfo = stocklist.FirstOrDefault();
                            if (stockinfo != null)
                            {
                                stockinfo.Stock += pdtcount;
                                stockinfo.ActualStock += pdtcount;
                                db.Update<TSTOCKEntity>(stockinfo, null);

                                TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                stockdetail.ID = Guid.NewGuid().ToString();
                                stockdetail.FK_Stock = stockinfo.GUID;
                                stockdetail.Stock = pdtcount;
                                stockdetail.ActualStock = pdtcount;
                                stockdetail.Type = CodeConst.StockDetailType.RK;
                                stockdetail.Remark = String.Format("门店仓库{0}退货入库", store.StoreId);
                                stockdetail.Creater = orderreturn.Creater;
                                stockdetail.CreateTime = DateTime.Now;
                                stockdetail.organizeid = order.organizeid;
                                db.Insert<TSTOCKDEAILEntity>(stockdetail);
                            }
                        }
                        else
                        {
                            result.msg = "库存不存在！";
                            return result;
                        }
                    }
                }

                //在订单表中添加退货信息，价格是负数
                TORDEREntity rorder = new TORDEREntity();
                rorder.GUID = Guid.NewGuid().ToString();
                rorder.OrderNumber = orderreturn.ReturnNumber;
                rorder.FK_PlanStore = orderreturn.FK_WarehouseNo;
                rorder.PayWay = order.PayWay;
                rorder.OrderPrice = -orderreturn.OrderPrice;
                rorder.OrderRemark = orderreturn.OrderRemark;
                rorder.OrderStatus = CodeConst.IsYesNO.NO;
                rorder.IsDel = orderreturn.IsDel;
                rorder.Creater = orderreturn.Creater;
                rorder.CreateTime = orderreturn.CreateTime;
                rorder.organizeid = order.organizeid;
                rorder.FK_Openid = orderreturn.Creater;
                rorder.IsGroupOrder = 0;
                rorder.Count = 1;
                db.Insert<TORDEREntity>(rorder);

                foreach (TORDERReturnDetailEntity returndetail in returndetaillist)
                {
                    TORDERDETAILEntity detail = new TORDERDETAILEntity();
                    detail.GUID = Guid.NewGuid().ToString();
                    detail.FK_Order = rorder.GUID;
                    detail.ProductSKU = returndetail.ProductSKU;
                    detail.Count = returndetail.Count;
                    detail.Price = -returndetail.Price;
                    detail.Status = CodeConst.IsYesNO.NO;
                    detail.Creater = returndetail.Creater;
                    detail.CreateTime = returndetail.CreateTime;
                    detail.organizeid = order.organizeid;
                    detail.IsDel = orderreturn.IsDel;
                    db.Insert<TORDERDETAILEntity>(detail);

                    returndetail.guid = Guid.NewGuid().ToString();
                    returndetail.organizeid = order.organizeid;
                }

                orderreturn.organizeid = order.organizeid;
                db.Insert<TORDERReturnEntity>(orderreturn);
                db.Insert<TORDERReturnDetailEntity>(returndetaillist);

                db.Commit();
            }
            catch (Exception ex)
            {
                db.Rollback();
                throw new Exception(ex.ToString());
            }
            result.IsOK = true;
            return result;
        }
        #endregion
    }
}
