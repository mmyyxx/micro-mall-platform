using MMM.Application.Entity.Order;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Code;
using WxPayAPI;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;
using MMM.Application.Entity.Materiel;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-16 16:31
    /// 描 述：订单明细
    /// </summary>
    public class TORDERDETAILService : RepositoryFactory<TORDERDETAILEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<TORDERDETAILEntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<TORDERDETAILEntity> GetList(JObject queryJson)
        {
            Data.IDatabase db = DbFactory.Base();
            var expression = db.IQueryable<TORDERDETAILEntity>();
            if (!queryJson["FK_Order"].IsEmpty())
            {
                String FK_Order = queryJson["FK_Order"].ToString();
                expression = expression.Where(t => t.FK_Order == FK_Order);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.Where(t => t.organizeid == organizeid);
            }
            IList<TORDERDETAILEntity> list = expression.ToList();
            IList<String> skus = list.Select(x => x.ProductSKU).ToList();
            IList<VPRODUCTSTOCKEntity> productstockList = db.IQueryable<VPRODUCTSTOCKEntity>().Where(x => skus.Contains(x.SKU)).ToList();
            foreach(TORDERDETAILEntity detail in list)
            {
                VPRODUCTSTOCKEntity stock = productstockList.FirstOrDefault(x => x.SKU == detail.ProductSKU);
                if (stock != null) detail.ProductName = stock.PdtName;
            }
            return list;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public TORDERDETAILEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, TORDERDETAILEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 明细退款
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="price"></param>
        /// <returns></returns>
        public string RestPaySave(string keyValue, decimal? price)
        {
            Data.IDatabase db = DbFactory.Base();
            TORDERDETAILEntity detail = db.FindEntity<TORDERDETAILEntity>(keyValue);
            if (detail == null)
            {
                return "退款失败!";
            }
            if (detail.Price < price)
            {
                return "退款金额不能大于明细金额!";
            }
            TORDEREntity order = db.FindEntity<TORDEREntity>(detail.FK_Order);

            TWXAPPEntity wxapp = db.IQueryable<TWXAPPEntity>().Where(p => p.GUID == order.FK_AppId).SingleOrDefault();
            string detailprice = (price * 100).ToString();
            string orderprice = (order.OrderPrice * 100).ToString();
            try
            {
                db.BeginTrans();

                detail.Status = 4;
                detail.Modifyer = OperatorProvider.Provider.Current().CompanyId;
                detail.ModifyTime = DateTime.Now;
                db.Update<TORDERDETAILEntity>(detail,null);
                db.Commit();
                Refund.Run(wxapp, detail.GUID, orderprice, detailprice, "售后审核退款");
            }
            catch (Exception ex)
            {
                db.Rollback();
                throw new Exception(ex.ToString());
            }
            return "";
        }
        #endregion
    }
}
