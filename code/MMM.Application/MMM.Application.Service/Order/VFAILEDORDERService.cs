using MMM.Application.Code;
using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Materiel;
using MMM.Application.Entity.Order;
using MMM.Application.Entity.Warehouse;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MMM.Application.Service.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VFAILEDORDERService : RepositoryFactory<VFAILEDORDEREntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<VFAILEDORDEREntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            var expression = LinqExtensions.True<VFAILEDORDEREntity>();
            if (!queryJson["ordernumber"].IsEmpty())
            {
                String OrderNumber = Convert.ToString(queryJson["ordernumber"]);
                expression = expression.And(t => t.OrderNumber.Contains(OrderNumber));
            }
            if (!queryJson["orderStatus"].IsEmpty())
            {
                Int32 OrderStatus = Convert.ToInt32(queryJson["orderStatus"]);
                expression = expression.And(t => t.OrderStatus == OrderStatus);
            }
            if (!queryJson["stockstatus"].IsEmpty())
            {
                Int32 StockStatus = Convert.ToInt32(queryJson["stockstatus"]);
                expression = expression.And(t => t.StockStatus == StockStatus);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            if (!queryJson["people"].IsEmpty())
            {
                String people = queryJson["people"].ToString();
                expression = expression.And(t => t.DeliveryName.Contains(people));
            }
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                expression = expression.And(t => t.FK_PlanStore == storeGUID);
            }
            if (!queryJson["starttime"].IsEmpty())
            {
                DateTime starttime = Convert.ToDateTime(queryJson["starttime"]);
                expression = expression.And(t => t.CreateTime >= starttime);
            }
            if (!queryJson["endtime"].IsEmpty())
            {
                DateTime endtime = Convert.ToDateTime(queryJson["endtime"]).AddDays(1);
                expression = expression.And(t => t.CreateTime < endtime);
            }
            expression = expression.And(t => t.IsDel == 0);
            return BaseRepository().FindList(expression, pagination).ToList();
        }

        public IList<VFAILEDORDEREntity> GetList(JObject queryJson)
        {
            var expression = DbFactory.Base().IQueryable<VFAILEDORDEREntity>();
            if (!queryJson["ordernumber"].IsEmpty())
            {
                String OrderNumber = Convert.ToString(queryJson["ordernumber"]);
                expression = expression.Where(t => t.OrderNumber.Contains(OrderNumber));
            }
            if (!queryJson["orderStatus"].IsEmpty())
            {
                Int32 OrderStatus = Convert.ToInt32(queryJson["orderStatus"]);
                expression = expression.Where(t => t.OrderStatus == OrderStatus);
            }
            if (!queryJson["stockstatus"].IsEmpty())
            {
                Int32 StockStatus = Convert.ToInt32(queryJson["stockstatus"]);
                expression = expression.Where(t => t.StockStatus == StockStatus);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.Where(t => t.organizeid == organizeid);
            }
            if (!queryJson["people"].IsEmpty())
            {
                String people = queryJson["people"].ToString();
                expression = expression.Where(t => t.DeliveryName.Contains(people));
            }
            if (!queryJson["storeGUID"].IsEmpty())
            {
                String storeGUID = queryJson["storeGUID"].ToString();
                expression = expression.Where(t => t.FK_PlanStore == storeGUID);
            }
            if (!queryJson["starttime"].IsEmpty())
            {
                DateTime starttime = Convert.ToDateTime(queryJson["starttime"]);
                expression = expression.Where(t => t.CreateTime >= starttime);
            }
            if (!queryJson["endtime"].IsEmpty())
            {
                DateTime endtime = Convert.ToDateTime(queryJson["endtime"]).AddDays(1);
                expression = expression.Where(t => t.CreateTime < endtime);
            }
            expression = expression.Where(t => t.IsDel == 0);
            return expression.OrderByDescending(t=>t.CreateTime).ToList();
        }

        /// <summary>
        /// 小程序订单手动出库
        /// </summary>
        /// <param name="storeid"></param>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public string SaveOrderSend(string storeid, string keyValue)
        {
            Data.IDatabase db = DbFactory.Base();
            VFAILEDORDEREntity wxOrder = db.FindEntity<VFAILEDORDEREntity>(keyValue);
            if (wxOrder.OrderStatus != CodeConst.WXOrderStatus.COMPLETE)
            {
                return "无法进行出库操作！";
            }
            if (wxOrder.StockStatus <= CodeConst.OrderStockStatus.YTC)
            {
                try
                {
                    db.BeginTrans();
                    if (wxOrder.StockStatus == CodeConst.OrderStockStatus.WCK)
                    {
                        //门店信息
                        T_STOREEntity store = db.FindEntity<T_STOREEntity>(storeid);

                        List<VWXORDERDETAILEntity> orderdetaillist = db.IQueryable<VWXORDERDETAILEntity>().Where(p => p.FK_Order == keyValue).ToList();

                        IList<String> skus = orderdetaillist.Select(t => t.ProductSKU).ToList();
                        IList<TPACKAGEEntity> packageList = db.IQueryable<TPACKAGEEntity>().Where(t => t.IsDel == 0 && skus.Contains(t.PackageSKU) && t.organizeid == wxOrder.organizeid).ToList();

                        IQueryable<TPRODUCTEntity> productQuery = db.IQueryable<TPRODUCTEntity>().Where(x => x.IsDel == 0 && x.organizeid == wxOrder.organizeid);
                        if (packageList.Count > 0)
                        {
                            //判断不是package的sku
                            IList<String> tempSku = new List<String>();
                            foreach (String key in skus)
                            {
                                if (packageList.Count(t => t.PackageSKU == key) <= 0)
                                {
                                    tempSku.Add(key);
                                }
                            }
                            //package的productguid
                            IList<String> productIds = packageList.Select(t => t.FK_Product).ToList();
                            if (tempSku.Count > 0)
                                productQuery = productQuery.Where(t => productIds.Contains(t.GUID) || tempSku.Contains(t.SKU));
                            else
                                productQuery = productQuery.Where(t => productIds.Contains(t.GUID));
                        }

                        IList<TPRODUCTEntity> productList = productQuery.ToList();
                        IList<String> productGuids = productList.Select(t => t.GUID).ToList();
                        //获取门店下的库存信息
                        IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(t => t.IsDel == 0 && productGuids.Contains(t.FK_Product) && t.FK_WarehouseNo == store.StoreId).ToList();

                        foreach (VWXORDERDETAILEntity pdtInfo in orderdetaillist)
                        {
                            TPACKAGEEntity package = packageList.Where(p => p.PackageSKU == pdtInfo.ProductSKU).FirstOrDefault();

                            string SKUStr = pdtInfo.ProductSKU;

                            if (package != null)
                            {
                                SKUStr = productList.Where(p => p.GUID == package.FK_Product).Select(p => p.SKU).FirstOrDefault();
                            }

                            string stockSKU = SKUStr.Replace("YD", "");

                            List<TSTOCKEntity> stocklist = stockList.Where(p => p.IsDel == CodeConst.IsDel.NO)
                                       .Join(productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == stockSKU), a => a.FK_Product, b => b.GUID, (a, b) => a)
                                       .OrderBy(p => p.FK_Location).Select(p => p).ToList();
                            if (stocklist.Count > 0)
                            {
                                decimal totalstock = stocklist.Sum(p => p.Stock) ?? 0;
                                decimal productCount = pdtInfo.Count.Value;
                                if (package != null)
                                {
                                    productCount = (pdtInfo.Count.Value * package.PdtCount) ?? 0;
                                }
                                if (productCount == 0)
                                {
                                    return String.Format("数据错误，商品{0}出库明细数量为0！", pdtInfo.PdtName);
                                }
                                if (totalstock >= productCount)
                                {
                                    TSTOCKEntity stockinfo = stocklist.Where(p => p.Stock >= productCount).FirstOrDefault();
                                    if (stockinfo != null)
                                    {
                                        stockinfo.Stock -= productCount;
                                        db.Update<TSTOCKEntity>(stockinfo, null);

                                        TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                        orderstock.ID = Guid.NewGuid().ToString();
                                        orderstock.FK_Order = keyValue;
                                        orderstock.FK_Stock = stockinfo.GUID;
                                        orderstock.StockCount = productCount;
                                        orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                        orderstock.Creater = OperatorProvider.Provider.Current().Account;
                                        orderstock.CreateTime = DateTime.Now;

                                        db.Insert<TORDERSTOCKEntity>(orderstock);

                                        TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                        stockdetail.ID = Guid.NewGuid().ToString();
                                        stockdetail.FK_Stock = stockinfo.GUID;
                                        stockdetail.Stock = -productCount;
                                        stockdetail.Type = CodeConst.StockDetailType.CK;
                                        stockdetail.Remark = String.Format("小程序订单手动提出：{0}", wxOrder.OrderNumber);
                                        stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                        stockdetail.CreateTime = DateTime.Now;

                                        db.Insert<TSTOCKDEAILEntity>(stockdetail);
                                    }
                                    else
                                    {
                                        decimal outstock = productCount;
                                        while (outstock > 0)
                                        {
                                            foreach (TSTOCKEntity stockitem in stocklist)
                                            {
                                                if (stockitem.Stock >= outstock)
                                                {
                                                    stockitem.Stock -= outstock;
                                                    db.Update<TSTOCKEntity>(stockitem, null);

                                                    TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                                    orderstock.ID = Guid.NewGuid().ToString();
                                                    orderstock.FK_Order = keyValue;
                                                    orderstock.FK_Stock = stockitem.GUID;
                                                    orderstock.StockCount = outstock;
                                                    orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                                    orderstock.Creater = OperatorProvider.Provider.Current().Account;
                                                    orderstock.CreateTime = DateTime.Now;

                                                    db.Insert<TORDERSTOCKEntity>(orderstock);

                                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                    stockdetail.ID = Guid.NewGuid().ToString();
                                                    stockdetail.FK_Stock = stockitem.GUID;
                                                    stockdetail.Stock = -outstock;
                                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                                    stockdetail.Remark = String.Format("小程序订单手动提出：{0}", wxOrder.OrderNumber);
                                                    stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                                    stockdetail.CreateTime = DateTime.Now;

                                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                                    outstock = Decimal.Zero;
                                                }
                                                else
                                                {
                                                    outstock -= stockitem.Stock ?? 0;
                                                    TORDERSTOCKEntity orderstock = new TORDERSTOCKEntity();
                                                    orderstock.ID = Guid.NewGuid().ToString();
                                                    orderstock.FK_Order = keyValue;
                                                    orderstock.FK_Stock = stockitem.GUID;
                                                    orderstock.StockCount = stockitem.Stock;
                                                    orderstock.Status = CodeConst.OrderStockStatus.YTC;
                                                    orderstock.Creater = OperatorProvider.Provider.Current().Account;
                                                    orderstock.CreateTime = DateTime.Now;

                                                    db.Insert<TORDERSTOCKEntity>(orderstock);

                                                    TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                                                    stockdetail.ID = Guid.NewGuid().ToString();
                                                    stockdetail.FK_Stock = stockitem.GUID;
                                                    stockdetail.Stock = -stockitem.Stock;
                                                    stockdetail.Type = CodeConst.StockDetailType.CK;
                                                    stockdetail.Remark = String.Format("小程序订单手动提出：{0}", wxOrder.OrderNumber);
                                                    stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                                                    stockdetail.CreateTime = DateTime.Now;

                                                    db.Insert<TSTOCKDEAILEntity>(stockdetail);

                                                    stockitem.Stock = Decimal.Zero;
                                                }
                                            }
                                        }
                                    }

                                    TPRODUCTEntity productInfo = productList.Where(p => p.IsDel == CodeConst.IsDel.NO && p.SKU == SKUStr).FirstOrDefault();

                                    if (productInfo != null)
                                    {
                                        productInfo.SaleCount -= productCount;
                                        db.Update<TPRODUCTEntity>(productInfo, null);
                                    }

                                }
                                else
                                {
                                    return "库存不足，无法出库！";
                                }
                            }
                            else
                            {
                                return "库存不足，无法出库！";
                            }
                        }
                    }
                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    throw new Exception(ex.ToString());
                }
                try
                {
                    db = DbFactory.Base();
                    db.BeginTrans();
                    List<TORDERSTOCKEntity> list = db.IQueryable<TORDERSTOCKEntity>().Where(p => p.FK_Order == keyValue && p.Status == CodeConst.OrderStockStatus.YTC).ToList();
                    IList<String> stockIds = list.Select(x => x.FK_Stock).ToList();
                    IList<TSTOCKEntity> stockList = db.IQueryable<TSTOCKEntity>().Where(x => stockIds.Contains(x.GUID)).ToList();
                    foreach (TORDERSTOCKEntity orderstock in list)
                    {
                        orderstock.Status = CodeConst.OrderStockStatus.YWC;
                        db.Update<TORDERSTOCKEntity>(orderstock, null);

                        TSTOCKEntity stockInfo = stockList.Where(p => p.GUID == orderstock.FK_Stock).SingleOrDefault();
                        if (stockInfo != null)
                        {
                            stockInfo.ActualStock -= orderstock.StockCount;
                            db.Update<TSTOCKEntity>(stockInfo, null);

                            TSTOCKDEAILEntity stockdetail = new TSTOCKDEAILEntity();
                            stockdetail.ID = Guid.NewGuid().ToString();
                            stockdetail.FK_Stock = stockInfo.GUID;
                            stockdetail.ActualStock = -orderstock.StockCount;
                            stockdetail.Type = CodeConst.StockDetailType.CK;
                            stockdetail.Remark = String.Format("小程序订单手动出库：{0}", wxOrder.OrderNumber); ;
                            stockdetail.Creater = OperatorProvider.Provider.Current().Account;
                            stockdetail.CreateTime = DateTime.Now;

                            db.Insert<TSTOCKDEAILEntity>(stockdetail);
                        }
                    }

                    db.Commit();
                }
                catch (Exception ex)
                {
                    db.Rollback();
                    throw new Exception(ex.ToString());
                }
            }
            else
                return "无法进行出库操作！";

            return "";
        }
        #endregion
    }
}
