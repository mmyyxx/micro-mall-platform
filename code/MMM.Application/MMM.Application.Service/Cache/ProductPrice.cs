﻿using MMM.Application.Entity.Product;
using MMM.Application.Service.Product;
using MMM.Cache.Factory;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.Cache
{
    public class ProductPriceCache
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        public string cacheKey = "ProductPriceCache";

        private T_PRODUCTPRICEService t_productpriceservice = new T_PRODUCTPRICEService();

        public IList<T_PRODUCTPRICEEntity> GetCacheList()
        {
            var cacheList = CacheFactory.Cache().GetCache<IList<T_PRODUCTPRICEEntity>>(cacheKey);
            if (cacheList == null)
            {
                var watch = CommonHelper.TimerStart();
                var data = t_productpriceservice.GetList("");
                _logger.Warn(DateTime.Now.ToString("yyyy-MM-dd HH:mm:s.ffff") + "::" +CommonHelper.TimerEnd(watch) + "加载price缓存完成\r\n");

                CacheFactory.Cache().WriteCache(data, cacheKey,DateTime.Now.AddYears(1));
                
                return data;
            }
            else
            {
                return cacheList;
            }
        }
    }
}
