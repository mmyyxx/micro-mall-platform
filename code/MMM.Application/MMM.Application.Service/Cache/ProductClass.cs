﻿using MMM.Application.Entity.Product;
using MMM.Application.Service.Product;
using MMM.Cache.Factory;
using MMM.Util;
using MMM.Util.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Service.Cache
{
    public class ProductClassCache
    {
        MMM.Util.Log.Log _logger = LogFactory.GetLogger("hmcys");

        public string cacheKey = "ProductClassCache";

        private T_PRODUCTCLASSService t_productclassservice = new T_PRODUCTCLASSService();

        public IList<T_PRODUCTCLASSEntity> GetCacheList()
        {
            var cacheList = CacheFactory.Cache().GetCache<IList<T_PRODUCTCLASSEntity>>(cacheKey);
            if (cacheList == null)
            {
                var watch = CommonHelper.TimerStart();
                var data = t_productclassservice.GetList("", "");
                _logger.Warn(DateTime.Now.ToString("yyyy-MM-dd HH:mm:s.ffff") + "::" + CommonHelper.TimerEnd(watch) + "加载class缓存\r\n");

                CacheFactory.Cache().WriteCache(data, cacheKey, DateTime.Now.AddYears(1));
                
                return data;
            }
            else
            {
                return cacheList;
            }
        }
    }
}
