using MMM.Application.Entity.Product;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using Newtonsoft.Json.Linq;
using MMM.Util.Extension;

namespace MMM.Application.Service.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 11:03
    /// 描 述：商品
    /// </summary>
    public class T_PRODUCTPRICEService : RepositoryFactory<T_PRODUCTPRICEEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_PRODUCTPRICEEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            var expression = LinqExtensions.True<T_PRODUCTPRICEEntity>();
            if (!queryJson["Name"].IsEmpty())
            {
                String Name = queryJson["Name"].ToString();
                expression = expression.And(t => t.Name.Contains(Name));
            }
            if (!queryJson["Creater"].IsEmpty())
            {
                String Creater = queryJson["Creater"].ToString();
                expression = expression.And(t => t.Creater.Contains(Creater));
            }
            if (!queryJson["ProductKey"].IsEmpty())
            {
                String ProductKey = queryJson["ProductKey"].ToString();
                expression = expression.And(t => t.ProductKey.Contains(ProductKey));
            }
            if (!queryJson["IsGroupProduct"].IsEmpty())
            {
                Int32 IsGroupProduct = Convert.ToInt32(queryJson["IsGroupProduct"].ToString());
                expression = expression.And(t => t.IsGroupProduct == IsGroupProduct);
            }
            if (!queryJson["IsLimitTimeProduct"].IsEmpty())
            {
                Int32 IsLimitTimeProduct = Convert.ToInt32(queryJson["IsLimitTimeProduct"].ToString());
                expression = expression.And(t => t.IsLimitTimeProduct == IsLimitTimeProduct);
            }
            if (!queryJson["IsSale"].IsEmpty())
            {
                Int32 IsSale = Convert.ToInt32(queryJson["IsSale"].ToString());
                expression = expression.And(t => t.IsSale == IsSale);
            }

            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);

            return this.BaseRepository().FindList(expression, pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_PRODUCTPRICEEntity> GetList(string queryJson)
        {
            return this.BaseRepository().IQueryable().Where(x => x.IsDel == 0).ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_PRODUCTPRICEEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_PRODUCTPRICEEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 批量保存表单
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(List<T_PRODUCTPRICEEntity> entitys)
        {
            this.BaseRepository().Update(entitys);
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, T_PRODUCTPRICEEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }

        public void RemoveFormAll(List<T_PRODUCTPRICEEntity> entity)
        {
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
