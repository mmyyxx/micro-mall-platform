using MMM.Application.Entity.Product;
using MMM.Data.Repository;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品类别
    /// </summary>
    public class T_PRODUCTCLASSService : RepositoryFactory<T_PRODUCTCLASSEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid"></param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<T_PRODUCTCLASSEntity> GetPageList(Pagination pagination, JObject queryJson)
        {
            IRepository<T_PRODUCTCLASSEntity> db = this.BaseRepository();
            var expression = LinqExtensions.True<T_PRODUCTCLASSEntity>();

            IList<T_PRODUCTCLASSEntity> list = db.IQueryable().ToList();

            if (!queryJson["FK_Product"].IsEmpty())
            {
                String FK_Product = queryJson["FK_Product"].ToString();
                expression = expression.And(t => t.FK_Product.Contains(FK_Product));
            }
            
            expression = expression.And(t => t.IsDel == 0);

            IList<T_PRODUCTCLASSEntity> retList = db.FindList(expression, pagination).ToList();
            foreach (T_PRODUCTCLASSEntity entity in retList)
            {
                if (!String.IsNullOrEmpty(entity.FK_ParentClass))
                {
                    T_PRODUCTCLASSEntity tem = list.FirstOrDefault(x => x.GUID == entity.FK_ParentClass);
                    entity.FK_ParentClassName = tem.ProductClass+"("+tem.ClassValue+")";
                }
            }

            return retList;
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="oid">查询参数</param>
        /// <param name="productGuid">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_PRODUCTCLASSEntity> GetList(string oid,String productGuid)
        {
            var expression = this.BaseRepository().IQueryable();
            if (!String.IsNullOrEmpty(oid))
                expression = expression.Where(t => t.organizeid == oid);
            if(!String.IsNullOrEmpty(productGuid))
                expression = expression.Where(t => t.FK_Product == productGuid);
            expression = expression.Where(t => t.IsDel == 0);
            return expression.OrderBy(t=>t.xorder).ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_PRODUCTCLASSEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }

        /// <summary>
        /// 获取子分类
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public IList<T_PRODUCTCLASSEntity> GetChild(string keyValue)
        {
            IRepository<T_PRODUCTCLASSEntity> db = this.BaseRepository();
            return db.IQueryable().Where(x => x.FK_ParentClass == keyValue && x.IsDel == 0).ToList();
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_PRODUCTCLASSEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }
        #endregion
    }
}
