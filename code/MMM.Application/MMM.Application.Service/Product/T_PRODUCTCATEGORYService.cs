using MMM.Application.Entity.Product;
using MMM.Data.Repository;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System.Linq;
using System;
using MMM.Util.Extension;
using Newtonsoft.Json.Linq;

namespace MMM.Application.Service.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-27 19:51
    /// 描 述：商品分类
    /// </summary>
    public class T_PRODUCTCATEGORYService : RepositoryFactory<T_PRODUCTCATEGORYEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="organizeid">公司id</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IList<T_PRODUCTCATEGORYEntity> GetPageList(Pagination pagination, String organizeid, JObject queryJson)
        {
            IRepository<T_PRODUCTCATEGORYEntity> db = this.BaseRepository();
            var expression = LinqExtensions.True<T_PRODUCTCATEGORYEntity>();

            IList<T_PRODUCTCATEGORYEntity> list = db.IQueryable().ToList();

            if (!queryJson["FK_ParentCategoryName"].IsEmpty())
            {
                String FK_ParentCategoryName = queryJson["FK_ParentCategoryName"].ToString();
                IList<String> pids = list.Where(x => x.CategoryName.Contains(FK_ParentCategoryName)).Select(x => x.GUID).ToList();
                expression = expression.And(t => pids.Contains(t.FK_ParentCategory));
            }

            if (!queryJson["CategoryName"].IsEmpty())
            {
                String CategoryName = queryJson["CategoryName"].ToString();
                expression = expression.And(t => t.CategoryName.Contains(CategoryName));
            }

            if (!queryJson["CategoryName"].IsEmpty())
            {
                String CategoryName = queryJson["CategoryName"].ToString();
                expression = expression.And(t => t.CategoryName.Contains(CategoryName));
            }

            if (!queryJson["CategoryLevel"].IsEmpty())
            {
                Int32 CategoryLevel = Convert.ToInt32(queryJson["CategoryLevel"].ToString());
                expression = expression.And(t => t.CategoryLevel == CategoryLevel);
            }

            if (!String.IsNullOrEmpty(organizeid))
                expression = expression.And(t => t.organizeid == organizeid);
            expression = expression.And(t => t.IsDel == 0);
            IList<T_PRODUCTCATEGORYEntity> retList = db.FindList(expression, pagination).ToList();
            foreach (T_PRODUCTCATEGORYEntity entity in retList)
            {
                if (!String.IsNullOrEmpty(entity.FK_ParentCategory))
                {
                    T_PRODUCTCATEGORYEntity tem = list.FirstOrDefault(x => x.GUID == entity.FK_ParentCategory);
                    entity.FK_ParentCategoryName = tem.CategoryName;
                }
            }
            return retList;
        }

        /// <summary>
        /// 获取与分类有关的商品
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="search"></param>
        /// <returns></returns>
        public IList<T_PRODUCTPRICEEntity> GetPageListProduct(Pagination pagination, JObject search)
        {
            Data.IDatabase db = DbFactory.Base();
            IQueryable<T_PRODUCTPRICEEntity> query = null;
            if (!search["type"].IsEmpty())
            {
                String type = search["type"].ToString();
                String categoryGUID = search["categoryGUID"].ToString();
                if (type == "0") query = db.IQueryable<T_PRODUCTPRICEEntity>().Where(p => p.IsDel == 0).Join(db.IQueryable<T_CATEGORY_PRODUCTEntity>().Where(p => p.FK_Category == categoryGUID), m => m.GUID, n => n.FK_Product, (m, n) => m).OrderByDescending(p => p.CreateTime);
                else {
                    IList<T_CATEGORY_PRODUCTEntity> cateproductList = db.IQueryable<T_CATEGORY_PRODUCTEntity>().Where(n => n.FK_Category == categoryGUID).ToList();
                    IList<String> cateproductIds = cateproductList.Select(x => x.FK_Product).ToList();
                    query = db.IQueryable<T_PRODUCTPRICEEntity>().Where(p => p.IsDel == 0 && !cateproductIds.Contains(p.GUID)).OrderByDescending(p => p.CreateTime);
                }
            }
            pagination.records = query.Count();
            return query.Skip(pagination.rows * (pagination.page - 1)).Take(pagination.rows).ToList();
        }

        /// <summary>
        /// 获取子分类
        /// </summary>
        /// <param name="keyValue"></param>
        /// <returns></returns>
        public IList<T_PRODUCTCATEGORYEntity> GetChild(string keyValue)
        {
            IRepository<T_PRODUCTCATEGORYEntity> db = this.BaseRepository();
            return db.IQueryable().Where(x => x.FK_ParentCategory == keyValue && x.IsDel == 0).ToList();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="oid">公司id</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_PRODUCTCATEGORYEntity> GetList(String oid, string queryJson)
        {
            var expression = this.BaseRepository().IQueryable();
            if (!String.IsNullOrEmpty(oid))
                expression = expression.Where(t => t.organizeid == oid);
            expression = expression.Where(t => t.IsDel == 0);
            return expression.ToList();
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_PRODUCTCATEGORYEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_PRODUCTCATEGORYEntity entity)
        {
            if (!string.IsNullOrEmpty(keyValue))
            {
                entity.Modify(keyValue);
                this.BaseRepository().Update(entity);
            }
            else
            {
                entity.Create();
                this.BaseRepository().Insert(entity);
            }
        }

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue"></param>
        /// <param name="entity"></param>
        public void RemoveForm(string keyValue, T_PRODUCTCATEGORYEntity entity)
        {
            entity.Remove(keyValue);
            this.BaseRepository().Update(entity);
        }
        #endregion
    }
}
