using MMM.Application.Entity.BaseManage;
using MMM.Application.Entity.Product;
using MMM.Data.Repository;
using MMM.Util;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Linq.Expressions;

namespace MMM.Application.Service.Product
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：
    /// </summary>
    public class VPRODUCTStoreService : RepositoryFactory<VPRODUCTStoreEntity>
    {
        #region 获取数据

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public VPRODUCTStoreEntity GetEntity(JObject queryJson)
        {
            var expression = this.BaseRepository().IQueryable();
            if (!queryJson["sku"].IsEmpty())
            {
                String sku = queryJson["sku"].ToString();
                expression = expression.Where(t => t.SKU == sku);
            }
            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.Where(t => t.organizeid == organizeid);
            }

            return expression.FirstOrDefault();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson"></param>
        /// <returns></returns>
        public List<VPRODUCTStoreEntity> GetList(Pagination pagination,JObject queryJson)
        {
            var expression = LinqExtensions.True<VPRODUCTStoreEntity>();

            if (!queryJson["organizeid"].IsEmpty())
            {
                String organizeid = queryJson["organizeid"].ToString();
                expression = expression.And(t => t.organizeid == organizeid);
            }
            if (!queryJson["sku"].IsEmpty())
            {
                String sku = queryJson["sku"].ToString();
                expression = expression.And(t => t.SKU.Contains(sku));
            }
            if (!queryJson["PdtName"].IsEmpty())
            {
                String PdtName = queryJson["PdtName"].ToString();
                expression = expression.And(t => t.PdtName.Contains(PdtName));
            }
            return this.BaseRepository().FindList(expression, pagination).ToList();
        }


        #endregion
    }
}
