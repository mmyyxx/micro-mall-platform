using MMM.Application.Entity.Product;
using MMM.Data.Repository;
using MMM.Util.Extension;
using MMM.Util.WebControl;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMM.Application.Service.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品分类
    /// </summary>
    public class T_CATEGORY_PRODUCTService : RepositoryFactory<T_CATEGORY_PRODUCTEntity>
    {
        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_CATEGORY_PRODUCTEntity> GetPageList(Pagination pagination, string queryJson)
        {
            return this.BaseRepository().FindList(pagination);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_CATEGORY_PRODUCTEntity> GetList(JObject queryJson)
        {
            IQueryable<T_CATEGORY_PRODUCTEntity> query = this.BaseRepository().IQueryable();
            if (!queryJson["FK_Product"].IsEmpty())
            {
                string FK_Product = queryJson["FK_Product"].ToString();
                query = query.Where(x => x.FK_Product == FK_Product);
            }
            return query.ToList();
        }

        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_CATEGORY_PRODUCTEntity GetEntity(string keyValue)
        {
            return this.BaseRepository().FindEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            this.BaseRepository().Delete(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="fk_product">商品主键</param>
        /// <param name="entitys">实体对象</param>
        /// <returns></returns>
        public void SaveForm(String fk_product, List<T_CATEGORY_PRODUCTEntity> entitys)
        {
            IRepository<T_CATEGORY_PRODUCTEntity> db = this.BaseRepository();
            //首先删除
            try
            {
                db.BeginTrans();
                db.Delete(x => x.FK_Product == fk_product);

                db.Insert(entitys);
                db.Commit();
            }
            catch(Exception e)
            {
                db.Rollback();
                throw new Exception(e.ToString());
            }
        }

        /// <summary>
        /// 保存分类商品
        /// </summary>
        /// <param name="ids">商品id</param>
        /// <param name="fK_Category">分类id</param>
        public void UpdateCateGoryProduct(IList<string> ids, string fK_Category)
        {
            IRepository<T_CATEGORY_PRODUCTEntity> db = this.BaseRepository();
            List<T_CATEGORY_PRODUCTEntity> list = new List<T_CATEGORY_PRODUCTEntity>();
            foreach (String s in ids)
            {
                T_CATEGORY_PRODUCTEntity entity = new T_CATEGORY_PRODUCTEntity();
                entity.Create();
                entity.FK_Category = fK_Category;
                entity.FK_Product = s;
                list.Add(entity);
            }

            db.Insert(list);
        }

        /// <summary>
        /// 移除分类商品
        /// </summary>
        /// <param name="ids">商品id</param>
        /// <param name="fK_Category">分类id</param>
        public void RemoveCateGoryProduct(IList<string> ids, string fK_Category)
        {
            IRepository<T_CATEGORY_PRODUCTEntity> db = this.BaseRepository();
            db.Delete(x => ids.Contains(x.FK_Product) && x.FK_Category == fK_Category);
        }
        #endregion
    }
}
