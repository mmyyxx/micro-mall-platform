﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Mapping.WMS
{
    public class ItemInfo
    {
        public string GUID { set; get; }

        public string Tag { set; get; }

        public string ImgURL { set; get; }

        public string TitleName { set; get; }

        public string Title { set; get; }

        public List<ItemDetailInfo> DetailList { set; get; }
    }

    public class ItemDetailInfo
    {
        public string Name1 { set; get; }

        public string Value1 { set; get; }

        public string Name2 { set; get; }

        public string Value2 { set; get; }
    }

    public class ProductDB
    {
        public string SKU { set; get; }

        public string PdtName { set; get; }

        public string PackageSKU { set; get; }

        public string PackageName { set; get; }

        public decimal PackageChildCount { set; get; }

        public decimal? CostPrice { set; get; }

        public decimal StockCount { set; get; }

        public decimal PackageStockCount { set; get; }
    }
}
