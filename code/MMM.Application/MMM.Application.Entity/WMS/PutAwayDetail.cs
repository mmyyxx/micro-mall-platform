﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WMS
{
    public class PutAwayDetail
    {
        public string GUID { get; set; }
        public string FK_PurchaseDetail { get; set; }
        public string FK_Location { get; set; }
        public string ActualLocation { get; set; }
        public string BatchNumber { get; set; }
        public int Status { get; set; }
        public string Operator { get; set; }
        public Nullable<System.DateTime> CompleteTime { get; set; }
        public string Remark { get; set; }
        public int IsDel { get; set; }
        public string Creater { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Modifyer { get; set; }
        public Nullable<System.DateTime> ModifyTime { get; set; }
        public string Deleter { get; set; }
        public Nullable<System.DateTime> DeleteTime { get; set; }

        public string PurchaseOrderGUID { set; get; }

        public string PdtName { set; get; }

        public string MainUnit { set; get; }

        public decimal PdtCount { set; get; }

        public string AuxiliaryUnit { set; get; }

        public decimal? UnitCount { set; get; }

        public string LocationNo { set; get; }

        public string ActualLocationNo { set; get; }
    }
}
