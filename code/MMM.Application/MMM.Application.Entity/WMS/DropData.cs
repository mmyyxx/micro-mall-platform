﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WMS
{
    [Serializable]
    public class DropData
    {
        public DropData()
        {

        }

        public string Code { set; get; }

        public int NumCode { set; get; }

        public string Name { set; get; }
    }
}
