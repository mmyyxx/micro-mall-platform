﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WMS
{
    public class OutGoingInfo
    {
        public string GUID { get; set; }
        public string FK_WarehouseNo { get; set; }
        public string FK_Product { get; set; }
        public string BatchNumber { get; set; }
        public decimal Count { get; set; }
        public decimal OutCount { get; set; }
        public decimal StockCount { get; set; }
        public int Status { get; set; }
        public string Operator { get; set; }
        public Nullable<System.DateTime> CompleteTime { get; set; }
        public string Remark { get; set; }
        public int IsDel { get; set; }
        public string Creater { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Modifyer { get; set; }
        public Nullable<System.DateTime> ModifyTime { get; set; }
        public string Deleter { get; set; }
        public Nullable<System.DateTime> DeleteTime { get; set; }
        public string SKU { set; get; }

        public string PdtName { set; get; }

        public string Unit { set; get; }
    }

    public class OutGoingStockInfo
    {
        public decimal Count { set; get; }

        public decimal LocationCount { set; get; }
    }

    public class OutStockDetailInfo
    {
        public string DetailGUID { set; get; }

        public string OutStockOrderNo { set; get; }

        public string SKU { set; get; }

        public string ProductName { set; get; }

        public decimal Count { set; get; }

        public string PlatName { set; get; }
    }
}
