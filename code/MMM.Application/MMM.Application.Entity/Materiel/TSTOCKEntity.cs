using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// 描 述：库存
    /// </summary>
    public class TSTOCKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品库存，主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }

        public String FK_WarehouseNo { get; set; }
        /// <summary>
        /// 储位
        /// </summary>
        /// <returns></returns>
        public string FK_Location { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        public string BatchNumber { get; set; }
        /// <summary>
        /// 商品库存
        /// </summary>
        /// <returns></returns>
        public decimal? Stock { get; set; }
        /// <summary>
        /// 实际库存
        /// </summary>
        /// <returns></returns>
        public decimal? ActualStock { get; set; }
        /// <summary>
        /// 生产日期
        /// </summary>
        /// <returns></returns>
        public DateTime? ProductionDate { get; set; }
        /// <summary>
        /// 保质期
        /// </summary>
        /// <returns></returns>
        public int? QualityDay { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
                    }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}