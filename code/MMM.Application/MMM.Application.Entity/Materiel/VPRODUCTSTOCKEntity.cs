using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    public class VPRODUCTSTOCKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public String FK_Product { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductNo { get; set; }
        /// <summary>
        /// 产品id
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        public string PdtName { get; set; }
        public decimal? UseStock { get; set; }
        public decimal? Stock { get; set; }
        public decimal? ActualStock { get; set; }
        public string organizeid { get; set; }

        #endregion

    }
}