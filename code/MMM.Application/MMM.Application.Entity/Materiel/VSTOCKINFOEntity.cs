using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    public class VSTOCKINFOEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public String StockGUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string WarehouseNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string OutGoingGUID { get; set; }
        public string SKU { get; set; }
        public string PdtName { get; set; }
        public string LocationNo { get; set; }
        public decimal Stock { get; set; }
        public DateTime? ProductionDate { get; set; }
        public int QualityDate { get; set; }

        #endregion

    }
}