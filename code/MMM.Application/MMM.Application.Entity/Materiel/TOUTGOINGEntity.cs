using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-06 21:48
    /// 描 述：出库分配
    /// </summary>
    public class TOUTGOINGEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 出库单
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_WarehouseNo
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseNo { get; set; }
        /// <summary>
        /// 物料主键
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        public string BatchNumber { get; set; }
        /// <summary>
        /// 需要数量
        /// </summary>
        /// <returns></returns>
        public decimal? Count { get; set; }
        /// <summary>
        /// 提出数量
        /// </summary>
        /// <returns></returns>
        public decimal? OutCount { get; set; }
        /// <summary>
        /// StockCount
        /// </summary>
        /// <returns></returns>
        public decimal? StockCount { get; set; }
        /// <summary>
        /// 出库状态(1.未提出 2.已提出 3.已完成)
        /// </summary>
        /// <returns></returns>
        public int? Status { get; set; }
        /// <summary>
        /// 出库人
        /// </summary>
        /// <returns></returns>
        public string Operator { get; set; }
        /// <summary>
        /// 出库完成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CompleteTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        public string FK_WarehouseNoName { get; set; }
        public string PdtName { get; set; }
        public string SKU { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
                    }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}