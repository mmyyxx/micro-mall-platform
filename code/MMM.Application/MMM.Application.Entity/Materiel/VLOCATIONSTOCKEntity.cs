using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    public class VLOCATIONSTOCKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 仓库编号
        /// </summary>
        /// <returns></returns>
        public String FK_WarehouseNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_Location { get; set; }
        /// <summary>
        /// 储位号
        /// </summary>
        /// <returns></returns>
        public string LocationNo { get; set; }
        /// <summary>
        /// 储位体积
        /// </summary>
        /// <returns></returns>
        public decimal? Volume { get; set; }

        /// <summary>
        /// 库存
        /// </summary>
        public decimal? Stock { get; set; }

        /// <summary>
        /// 实际库存
        /// </summary>
        public decimal? ActualStock { get; set; }

        public String organizeid { get; set; }

        public String WarehouseName { get; set; }
        #endregion

    }
}