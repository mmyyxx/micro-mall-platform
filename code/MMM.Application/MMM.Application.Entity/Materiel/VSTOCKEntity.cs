using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    public class VSTOCKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public String FK_Product { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductNo { get; set; }
        /// <summary>
        /// 产品id
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        public string ActualSKU { get; set; }
        public string PdtName { get; set; }
        public decimal? PdtCount { get; set; }
        public decimal? Stock { get; set; }
        public decimal? ActualStock { get; set; }

        #endregion

    }
}