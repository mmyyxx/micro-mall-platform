using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 00:18
    /// 描 述：库存明细
    /// </summary>
    public class TSTOCKDEAILEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品库存明细表，自增主键
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 库存主键，t_stock.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Stock { get; set; }
        /// <summary>
        /// 库存变化
        /// </summary>
        /// <returns></returns>
        public decimal? Stock { get; set; }
        /// <summary>
        /// ActualStock
        /// </summary>
        /// <returns></returns>
        public decimal? ActualStock { get; set; }
        /// <summary>
        /// 库存变化类型(1.入库 2.出库 3.库内)
        /// </summary>
        /// <returns></returns>
        public int? Type { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}