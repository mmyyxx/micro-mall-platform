using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// 描 述：库存不足
    /// </summary>
    public class VOUTSTOCKDETAILEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public Guid GUID { get; set; }
        /// <summary>
        /// 仓库编号
        /// </summary>
        /// <returns></returns>
        public string WarehouseNo { get; set; }
        /// <summary>
        /// 物料名称
        /// </summary>
        /// <returns></returns>
        public string ProductName { get; set; }
        /// <summary>
        /// 产品id
        /// </summary>
        /// <returns></returns>
        public string ProductGUID { get; set; }
        public string organizeid { get; set; }

        #endregion

    }
}