using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    public class VPUTAWAYEntity : BaseEntity
    {
        #region 实体成员

        public Guid GUID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String FK_WarehouseNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 产品id
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        public string ProductName { get; set; }
        public decimal PdtCount { get; set; }
        public string Unit { get; set; }
        public decimal? UnitCount { get; set; }
        public string AuxiliaryUnit { get; set; } 

        #endregion

    }
}