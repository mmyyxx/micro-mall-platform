using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-02 12:43
    /// 描 述：T_OUTGOINGSTOCK
    /// </summary>
    public class TOUTGOINGSTOCKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 自增主键
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 分配主键
        /// </summary>
        /// <returns></returns>
        public string FK_OutGoing { get; set; }
        /// <summary>
        /// 库存主键
        /// </summary>
        /// <returns></returns>
        public string FK_Stock { get; set; }
        /// <summary>
        /// 提出数量
        /// </summary>
        /// <returns></returns>
        public decimal? StockCount { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}