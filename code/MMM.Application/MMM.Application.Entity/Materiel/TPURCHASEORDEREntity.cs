using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 10:02
    /// 描 述：入库单
    /// </summary>
    public class TPURCHASEORDEREntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品采购入库单，主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 入库单号
        /// </summary>
        /// <returns></returns>
        public string PurchaseOrderNo { get; set; }
        /// <summary>
        /// 仓库编码
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseNo { get; set; }
        /// <summary>
        /// 供应商外键
        /// </summary>
        /// <returns></returns>
        public string FK_Supplier { get; set; }
        /// <summary>
        /// 订单状态(0.未上架 1.已上架 2.已入库)
        /// </summary>
        /// <returns></returns>
        public int? Status { get; set; }
        /// <summary>
        /// 订单进货价
        /// </summary>
        /// <returns></returns>
        public decimal? PurchaseOrderPrice { get; set; }
        /// <summary>
        /// 订单类型(1.进货入库 2.退货入库 3.调拨入库)
        /// </summary>
        /// <returns></returns>
        public int? OrderType { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        public string FK_SupplierName { get; set; }
        public string FK_WarehouseNoName { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.Status = 0;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
            this.FK_SupplierName = null;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}