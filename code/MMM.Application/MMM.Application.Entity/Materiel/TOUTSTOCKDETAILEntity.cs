using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-05 21:41
    /// 描 述：出库单明细
    /// </summary>
    public class TOUTSTOCKDETAILEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 出库单明细，主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 出库单，t_outstockdetail
        /// </summary>
        /// <returns></returns>
        public string FK_OutStockOrder { get; set; }
        /// <summary>
        /// FK_Product
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 0.已完成 1.未完成
        /// </summary>
        /// <returns></returns>
        public int? DetailStatus { get; set; }
        /// <summary>
        /// 物料SKU
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        /// <summary>
        /// 物料名称
        /// </summary>
        /// <returns></returns>
        public string ProductName { get; set; }
        /// <summary>
        /// 物料主数量
        /// </summary>
        /// <returns></returns>
        public decimal? Count { get; set; }
        /// <summary>
        /// 主单位
        /// </summary>
        /// <returns></returns>
        public string MainUnit { get; set; }
        /// <summary>
        /// 物料辅数量
        /// </summary>
        /// <returns></returns>
        public decimal? UnitCount { get; set; }
        /// <summary>
        /// 辅单位
        /// </summary>
        /// <returns></returns>
        public string AuxiliaryUnit { get; set; }
        /// <summary>
        /// 主辅数量换算
        /// </summary>
        /// <returns></returns>
        public decimal? UnitConversion { get; set; }
        /// <summary>
        /// 物料单价
        /// </summary>
        /// <returns></returns>
        public decimal? Price { get; set; }
        /// <summary>
        /// 物料折扣
        /// </summary>
        /// <returns></returns>
        public decimal? Discount { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.DetailStatus = 1;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}