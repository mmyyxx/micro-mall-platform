using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Materiel
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-02 22:07
    /// 描 述：上架
    /// </summary>
    public class TPUTAWAYEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 上架，主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 入库单明细
        /// </summary>
        /// <returns></returns>
        public string FK_PurchaseDetail { get; set; }
        /// <summary>
        /// 分配储位
        /// </summary>
        /// <returns></returns>
        public string FK_Location { get; set; }
        /// <summary>
        /// 实际储位
        /// </summary>
        /// <returns></returns>
        public string ActualLocation { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        public string BatchNumber { get; set; }
        /// <summary>
        /// 上架状态(0.已完成 1.未完成)
        /// </summary>
        /// <returns></returns>
        public int? Status { get; set; }
        /// <summary>
        /// 上架人
        /// </summary>
        /// <returns></returns>
        public string Operator { get; set; }
        /// <summary>
        /// 上架完成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CompleteTime { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }

        public string LocationNo { set; get; }

        public string SKU { set; get; }

        public string ProductName { set; get; }

        public decimal? Count { set; get; }

        public string MainUnit { set; get; }

        public decimal? UnitCount { set; get; }

        public string AuxiliaryUnit { set; get; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}