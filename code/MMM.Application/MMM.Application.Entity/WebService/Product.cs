﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebService
{
    /// <summary>
    /// 出库时使用的商品类
    /// </summary>
    public class ProductInfoAll
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 物料编号
        /// </summary>
        /// <returns></returns>
        public string ProductNo { get; set; }
        /// <summary>
        /// 物料条码
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        public string SKUOld { get; set; }
        /// <summary>
        /// 物料名称
        /// </summary>
        /// <returns></returns>
        public string PdtName { get; set; }
        /// <summary>
        /// 单位
        /// </summary>
        /// <returns></returns>
        public string PdtUnit { get; set; }
        /// <summary>
        /// 规格
        /// </summary>
        /// <returns></returns>
        public string Specifications { get; set; }
        /// <summary>
        /// 品牌
        /// </summary>
        /// <returns></returns>
        public string Brand { get; set; }
        /// <summary>
        /// 产地
        /// </summary>
        /// <returns></returns>
        public string Place { get; set; }
        /// <summary>
        /// 物料储存方式：0.普通储存 1.冷藏储存
        /// </summary>
        /// <returns></returns>
        public int? StorageMode { get; set; }
        /// <summary>
        /// 是否易碎：0.易碎 1.不易碎
        /// </summary>
        /// <returns></returns>
        public int? IsFragile { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        /// <returns></returns>
        public int? Type { get; set; }
        /// <summary>
        /// 保质期(单位：天)
        /// </summary>
        /// <returns></returns>
        public int? QualityDate { get; set; }
        /// <summary>
        /// SaleCount
        /// </summary>
        /// <returns></returns>
        public decimal? SaleCount { get; set; }
        /// <summary>
        /// ReserveCount
        /// </summary>
        /// <returns></returns>
        public decimal? ReserveCount { get; set; }
        /// <summary>
        /// 物料单价
        /// </summary>
        /// <returns></returns>
        public decimal? Price { get; set; }
        /// <summary>
        /// OrderBy
        /// </summary>
        /// <returns></returns>
        public int? OrderBy { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }

        public decimal ChildCount { get; set; }
        #endregion
    }

}
