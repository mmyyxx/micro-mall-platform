﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebService
{
    public class T_ORDERDETAIL
    {
        public string FK_Order { get; set; }
        public string ProductSKU { get; set; }
        public decimal Count { get; set; }
        public decimal Price { get; set; }
        public int IsStockOut { get; set; }
        public string Creater { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
