﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebService
{
    [Serializable]
    public class InputData
    {
        public string value { set; get; }

        public string valuelist { set; get; }


        public InputData()
        {

        }

        public InputData(string pvalue)
        {
            value = pvalue;
        }
    }
}
