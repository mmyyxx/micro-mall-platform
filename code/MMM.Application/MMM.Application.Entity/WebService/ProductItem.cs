﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebService
{
    public class ProductItem
    {
        public string ProductSKU { set; get; }

        public string ProductName { set; get; }

        public decimal ProductCount { set; get; }

        public decimal ProductPrice { set; get; }

        public decimal ProductTotal { set; get; }

        public string OrderTime { set; get; }
    }
}
