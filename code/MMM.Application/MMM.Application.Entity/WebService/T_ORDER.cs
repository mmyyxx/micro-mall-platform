﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebService
{
    /// <summary>
    /// 原wms库的订单表，
    /// </summary>
    public class T_ORDER
    {
        public string GUID { get; set; }
        public string FK_WarehouseNo { get; set; }
        public string OrderNumber { get; set; }
        public int PayWay { get; set; }
        public decimal OrderPrice { get; set; }
        public string OrderRemark { get; set; }
        public int IsStockOut { get; set; }
        public int IsDel { get; set; }
        public string Creater { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
    }
}
