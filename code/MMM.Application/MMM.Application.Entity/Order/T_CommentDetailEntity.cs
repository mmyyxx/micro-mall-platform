using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-09-12 15:21
    /// 描 述：T_CommentDetail
    /// </summary>
    public class T_CommentDetailEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// id
        /// </summary>
        /// <returns></returns>
        public string id { get; set; }
        /// <summary>
        /// ProductName
        /// </summary>
        /// <returns></returns>
        public string ProductName { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        /// <returns></returns>
        public int? Count { get; set; }
        /// <summary>
        /// 商品集赞表
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 用户，t_user.openid
        /// </summary>
        /// <returns></returns>
        public string FK_OpenId { get; set; }
        /// <summary>
        /// orderid
        /// </summary>
        /// <returns></returns>
        public string orderid { get; set; }
        /// <summary>
        /// ordernumber
        /// </summary>
        /// <returns></returns>
        public string ordernumber { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        /// <returns></returns>
        public string type { get; set; }
        /// <summary>
        /// content
        /// </summary>
        /// <returns></returns>
        public string content { get; set; }
        /// <summary>
        /// addtime
        /// </summary>
        /// <returns></returns>
        public DateTime? addtime { get; set; }
        /// <summary>
        /// adduser
        /// </summary>
        /// <returns></returns>
        public string adduser { get; set; }
        /// <summary>
        /// zancount
        /// </summary>
        /// <returns></returns>
        public int? zancount { get; set; }
        /// <summary>
        /// guige
        /// </summary>
        /// <returns></returns>
        public string guige { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public Int32 IsDel { get; set; }
        /// <summary>
        /// edittime
        /// </summary>
        /// <returns></returns>
        public DateTime? edittime { get; set; }
        /// <summary>
        /// edituser
        /// </summary>
        /// <returns></returns>
        public string edituser { get; set; }
        /// <summary>
        /// isniming
        /// </summary>
        /// <returns></returns>
        public string isniming { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.id = Guid.NewGuid().ToString();
                                    this.IsDel = 0;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.id = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.id = keyValue;
            this.IsDel = 1;
        }
        #endregion
    }
}