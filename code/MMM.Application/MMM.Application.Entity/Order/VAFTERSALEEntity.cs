using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VAFTERSALEEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_ProductClass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal? Count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        public string OrderNumber { get; set; }
        public decimal? DetailPrice { get; set; }
        public decimal OrderPrice { get; set; }
        public string DeliveryName { get; set; }
        public string DeliveryMobile { get; set; }
        public int IsGroupOrder { get; set; }
        public string NickName { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        
        public string UserName { get; set; }
        public string Mobile { get; set; }

        public string StatusStr { get; set; }
        public string ProductClass { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}