using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-14 19:23
    /// 描 述：订单
    /// </summary>
    public class TORDEREntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// GUID
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// f_wxapp.guid
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        /// <returns></returns>
        public string OrderNumber { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        /// <returns></returns>
        public int? OrderStatus { get; set; }
        /// <summary>
        /// 用户t_user.openid
        /// </summary>
        /// <returns></returns>
        public string FK_Openid { get; set; }
        /// <summary>
        /// 商品数量
        /// </summary>
        /// <returns></returns>
        public decimal? Count { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        /// <returns></returns>
        public int? PayWay { get; set; }
        /// <summary>
        /// 支付结果
        /// </summary>
        /// <returns></returns>
        public int? PayResult { get; set; }
        /// <summary>
        /// 团购信息
        /// </summary>
        /// <returns></returns>
        public string Coupon { get; set; }
        /// <summary>
        /// 邮费
        /// </summary>
        /// <returns></returns>
        public decimal? Freight { get; set; }
        /// <summary>
        /// 订单总价
        /// </summary>
        /// <returns></returns>
        public decimal? OrderPrice { get; set; }
        /// <summary>
        /// 寄送方式，1表示快递
        /// </summary>
        /// <returns></returns>
        public int? DeliveryMode { get; set; }
        /// <summary>
        /// FK_Adress
        /// </summary>
        /// <returns></returns>
        public string FK_Adress { get; set; }
        /// <summary>
        /// DeliveryAdress
        /// </summary>
        /// <returns></returns>
        public string DeliveryAdress { get; set; }
        /// <summary>
        /// DeliveryName
        /// </summary>
        /// <returns></returns>
        public string DeliveryName { get; set; }
        /// <summary>
        /// DeliveryMobile
        /// </summary>
        /// <returns></returns>
        public string DeliveryMobile { get; set; }
        /// <summary>
        /// ExpressNumber
        /// </summary>
        /// <returns></returns>
        public string ExpressNumber { get; set; }
        /// <summary>
        /// 是否团购订单？
        /// </summary>
        /// <returns></returns>
        public int? IsGroupOrder { get; set; }
        /// <summary>
        /// 团购角色，团长或成员
        /// </summary>
        /// <returns></returns>
        public int? GroupRole { get; set; }
        /// <summary>
        /// 团购结束时间
        /// </summary>
        /// <returns></returns>
        public DateTime? GroupEndTime { get; set; }
        /// <summary>
        /// 团购码
        /// </summary>
        /// <returns></returns>
        public string GroupNumber { get; set; }
        /// <summary>
        /// 团购结果
        /// </summary>
        /// <returns></returns>
        public int? GroupResult { get; set; }
        /// <summary>
        /// RegimentTime
        /// </summary>
        /// <returns></returns>
        public DateTime? RegimentTime { get; set; }
        /// <summary>
        /// DeliverTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeliverTime { get; set; }
        /// <summary>
        /// Distribution
        /// </summary>
        /// <returns></returns>
        public string Distribution { get; set; }
        /// <summary>
        /// DistributionTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DistributionTime { get; set; }
        /// <summary>
        /// Completer
        /// </summary>
        /// <returns></returns>
        public string Completer { get; set; }
        /// <summary>
        /// CompleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CompleteTime { get; set; }
        /// <summary>
        /// 拟出单仓库
        /// </summary>
        /// <returns></returns>
        public string FK_PlanStore { get; set; }
        /// <summary>
        /// 预计提货时间
        /// </summary>
        /// <returns></returns>
        public DateTime? PlanPickUpTime { get; set; }
        /// <summary>
        /// 实际出单仓库
        /// </summary>
        /// <returns></returns>
        public string FK_ActualStore { get; set; }
        /// <summary>
        /// 实际提货时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ActualPickUpTime { get; set; }
        /// <summary>
        /// ？？？？？
        /// </summary>
        /// <returns></returns>
        public int? IsShare { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// OrderRemark
        /// </summary>
        /// <returns></returns>
        public string OrderRemark { get; set; }
        /// <summary>
        /// 订单类型：0-门店订单；1-小程序订单
        /// </summary>
        /// <returns></returns>
        public string OrderType { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        public string organizeid { get;  set; }

        
        public string OrderStatusStr { set; get; }

        public string PayWayStr { set; get; }

        public string PayResultStr { set; get; }

        public string OpenIdName { set; get; }

        public string IsGroupOrderStr { set; get; }

        public decimal? CouponMoney { set; get; }

        public string GroupRoleStr { set; get; }

        public string GroupResultStr { set; get; }

        public string GroupEndTimeStr { set; get; }

        public string CreateTimeStr { set; get; }

        public string RegimentTimeStr { set; get; }

        public string DeliverTimeStr { set; get; }

        public string CompleteTimeStr { set; get; }

        public string DeliveryModeStr { set; get; }

        public string PlanStoreId { set; get; }

        public string PlanStoreStr { set; get; }

        public string ActualStoreStr { set; get; }

        public string DeliverAdress { set; get; }

        public string IsDelStr { set; get; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
                    }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}