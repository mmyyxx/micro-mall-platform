using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VGROUPORDEREntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string OrderGUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_ProductClass { get; set; }
        public DateTime? CreateTime { get; set; }
        public string OrderNumber { get; set; }
        public string FK_Openid { get; set; }
        public string GroupNumber { get; set; }
        public Int32 OrderStatus { get; set; }
        public string OrderStatusStr { get; set; }
        public string ProductMainPicture { get; set; }
        public string Name { get; set; }
        public string ProductParameter { get; set; }
        public decimal ProductCount { get; set; }
        public Int32? GroupPeople { get; set; }
        public Int32? People { get; set; }
        public decimal ClassPrice { get; set; }
        public decimal ClassOldPrice { get; set; }
        public String ProductUnit { get; set; }
        public DateTime? LimitEndTime { get; set; }
        public Int32 GroupResult { get; set; }
        public Int32? GroupRole { get; set; }
        public String AvatarUrl { get; set; }
        public String organizeid { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}