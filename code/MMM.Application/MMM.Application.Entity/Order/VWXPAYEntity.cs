using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VWXPAYEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string out_trade_no { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Int32 OrderStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string body { get; set; }
        public decimal? total_fee { get; set; }
        public string detail { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}