using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VWXORDERDETAILEntity : BaseEntity
    {
        #region 实体成员
        public Guid ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_Order { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductSKU { get; set; }
        public string PdtName { get; set; }
        public decimal? Count { get; set; }
        public decimal? Price { get; set; }
        public decimal? OldPrice { get; set; }
        public decimal? ActualPrice { get; set; }
        public decimal? Stock { get; set; }
        
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}