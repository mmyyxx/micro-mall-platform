using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-23 21:39
    /// 描 述：T_ORDERReturnDetail
    /// </summary>
    public class TORDERReturnDetailEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// Guid
        /// </summary>
        /// <returns></returns>
        public string guid { get; set; }
        /// <summary>
        /// FK_Return
        /// </summary>
        /// <returns></returns>
        public string FK_Return { get; set; }
        /// <summary>
        /// ProductSKU
        /// </summary>
        /// <returns></returns>
        public string ProductSKU { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        /// <returns></returns>
        public decimal? Count { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        /// <returns></returns>
        public decimal? Price { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.guid = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.guid = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.guid = keyValue;
        }
        #endregion
    }
}