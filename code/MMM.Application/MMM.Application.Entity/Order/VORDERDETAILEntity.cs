using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：订单详情视图
    /// </summary>
    public class VORDERDETAILEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_Order { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_ProductClass { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal Count { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal Price { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Name { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }

        public String ProductStatusStr { get; set; }
        public string ProductParameter { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}