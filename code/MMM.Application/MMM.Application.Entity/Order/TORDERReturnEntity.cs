using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-23 21:32
    /// 描 述：退货单
    /// </summary>
    public class TORDERReturnEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// GUID
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_WarehouseNo
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseNo { get; set; }
        /// <summary>
        /// OrderNumber
        /// </summary>
        /// <returns></returns>
        public string OrderNumber { get; set; }
        /// <summary>
        /// ReturnNumber
        /// </summary>
        /// <returns></returns>
        public string ReturnNumber { get; set; }
        /// <summary>
        /// OrderPrice
        /// </summary>
        /// <returns></returns>
        public decimal? OrderPrice { get; set; }
        /// <summary>
        /// OrderRemark
        /// </summary>
        /// <returns></returns>
        public string OrderRemark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.IsDel = 1;
        }
        #endregion
    }
}