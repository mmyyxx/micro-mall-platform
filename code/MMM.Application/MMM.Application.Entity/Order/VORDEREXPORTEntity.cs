using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VORDEREXPORTEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string OrderGUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string OrderNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductSKU { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public decimal Price { get; set; }
        public decimal OldPrice { get; set; }
        public decimal ActualPrice { get; set; }
        public decimal Count { get; set; }
        public decimal ProductDetailCount { get; set; }
        public decimal YHPrice { get; set; }
        public decimal Freight { get; set; }
        public decimal OrderPrice { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Name { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }

        public DateTime CreateTime { get; set; }
        public string ExpressNumber { get; set; }
        public DateTime? DeliverTime { get; set; }
        public DateTime? CompleteTime { get; set; }
        public string FK_ActualStore { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}