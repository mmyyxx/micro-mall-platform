using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-09-16 10:21
    /// 描 述：售后状态
    /// </summary>
    public class T_OrderAfterSaleEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// id
        /// </summary>
        /// <returns></returns>
        public string id { get; set; }
        /// <summary>
        /// orderid
        /// </summary>
        /// <returns></returns>
        public string orderid { get; set; }
        /// <summary>
        /// 售后类型：1-退货，2-换货，3-维修，4-退款
        /// </summary>
        /// <returns></returns>
        public string type { get; set; }
        /// <summary>
        /// 售后门店
        /// </summary>
        /// <returns></returns>
        public string storeid { get; set; }
        /// <summary>
        /// 售后状态
        /// </summary>
        /// <returns></returns>
        public string status { get; set; }
        /// <summary>
        /// 退款说明
        /// </summary>
        /// <returns></returns>
        public string remark { get; set; }
        /// <summary>
        /// 审核说明
        /// </summary>
        /// <returns></returns>
        public string exremark { get; set; }
        /// <summary>
        /// addtime
        /// </summary>
        /// <returns></returns>
        public DateTime? addtime { get; set; }
        /// <summary>
        /// adduser
        /// </summary>
        /// <returns></returns>
        public string adduser { get; set; }
        /// <summary>
        /// edittime
        /// </summary>
        /// <returns></returns>
        public DateTime? edittime { get; set; }
        /// <summary>
        /// edituser
        /// </summary>
        /// <returns></returns>
        public string edituser { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.id = Guid.NewGuid().ToString();
            //                        this.IsDel = 0;
            //this.organizeid = OperatorProvider.Provider.Current().CompanyId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.id = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.id = keyValue;
            //this.Deleter = OperatorProvider.Provider.Current().Account;
            //this.DeleteTime = DateTime.Now;
            //this.IsDel = 1;
        }
        #endregion
    }
}