using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-09-11 14:12
    /// 描 述：商品评论
    /// </summary>
    public class T_CommentEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// id
        /// </summary>
        /// <returns></returns>
        public string id { get; set; }

        public int IsDel { get; set; }

        /// <summary>
        /// 用户，t_user.openid
        /// </summary>
        /// <returns></returns>
        public string FK_OpenId { get; set; }
        /// <summary>
        /// orderid
        /// </summary>
        /// <returns></returns>
        public string orderid { get; set; }

        public string ordernumber { get; set; }
        /// <summary>
        /// addtime
        /// </summary>
        /// <returns></returns>
        public DateTime? addtime { get; set; }
        /// <summary>
        /// adduser
        /// </summary>
        /// <returns></returns>
        public string adduser { get; set; }
        /// <summary>
        /// zancount
        /// </summary>
        /// <returns></returns>
        public int? zancount { get; set; }
        /// <summary>
        /// star1
        /// </summary>
        /// <returns></returns>
        public int? star1 { get; set; }
        /// <summary>
        /// star2
        /// </summary>
        /// <returns></returns>
        public int? star2 { get; set; }
        /// <summary>
        /// star3
        /// </summary>
        /// <returns></returns>
        public int? star3 { get; set; }
        /// <summary>
        /// isniming
        /// </summary>
        /// <returns></returns>
        public string isniming { get; set; }
        /// <summary>
        /// edittime
        /// </summary>
        /// <returns></returns>
        public DateTime? edittime { get; set; }
        /// <summary>
        /// edituser
        /// </summary>
        /// <returns></returns>
        public string edituser { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.id = Guid.NewGuid().ToString();
                                    this.IsDel = 0;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.id = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.id = keyValue;
            this.IsDel = 1;
        }
        #endregion
    }
}