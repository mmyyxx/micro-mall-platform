using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-19 17:20
    /// 描 述：订单库存
    /// </summary>
    public class TORDERSTOCKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 订单所存放的位置？
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 订单号，t_order.orderNumber
        /// </summary>
        /// <returns></returns>
        public string FK_Order { get; set; }
        /// <summary>
        /// 仓库,t_stock.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Stock { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        public decimal? StockCount { get; set; }
        /// <summary>
        /// 1.已提出 2.已完成 3.已取消
        /// </summary>
        /// <returns></returns>
        public int? Status { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}