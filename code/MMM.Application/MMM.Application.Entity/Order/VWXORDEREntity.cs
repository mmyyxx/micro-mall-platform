using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Order
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-21 19:06
    /// 描 述：
    /// </summary>
    public class VWXORDEREntity : BaseEntity
    {
        #region 实体成员

        /// <summary>
        /// GUID
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// f_wxapp.guid
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 订单编号
        /// </summary>
        /// <returns></returns>
        public string OrderNumber { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        /// <returns></returns>
        public int? OrderStatus { get; set; }
        /// <summary>
        /// 用户t_user.openid
        /// </summary>
        /// <returns></returns>
        public string FK_Openid { get; set; }
        /// <summary>
        /// 商品数量
        /// </summary>
        /// <returns></returns>
        public decimal? Count { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        /// <returns></returns>
        public int? PayWay { get; set; }
        /// <summary>
        /// 支付结果
        /// </summary>
        /// <returns></returns>
        public int? PayResult { get; set; }
        /// <summary>
        /// 团购信息
        /// </summary>
        /// <returns></returns>
        public string Coupon { get; set; }
        /// <summary>
        /// 邮费
        /// </summary>
        /// <returns></returns>
        public decimal? Freight { get; set; }
        /// <summary>
        /// 订单总价
        /// </summary>
        /// <returns></returns>
        public decimal? OrderPrice { get; set; }
        /// <summary>
        /// 寄送方式，1表示快递
        /// </summary>
        /// <returns></returns>
        public int? DeliveryMode { get; set; }
        /// <summary>
        /// FK_Adress
        /// </summary>
        /// <returns></returns>
        public string FK_Adress { get; set; }
        /// <summary>
        /// DeliveryAdress
        /// </summary>
        /// <returns></returns>
        public string DeliveryAdress { get; set; }
        /// <summary>
        /// DeliveryName
        /// </summary>
        /// <returns></returns>
        public string DeliveryName { get; set; }
        /// <summary>
        /// DeliveryMobile
        /// </summary>
        /// <returns></returns>
        public string DeliveryMobile { get; set; }
        /// <summary>
        /// ExpressNumber
        /// </summary>
        /// <returns></returns>
        public string ExpressNumber { get; set; }
        /// <summary>
        /// 是否团购订单？
        /// </summary>
        /// <returns></returns>
        public int? IsGroupOrder { get; set; }
        /// <summary>
        /// 团购角色，团长或成员
        /// </summary>
        /// <returns></returns>
        public int? GroupRole { get; set; }
        /// <summary>
        /// 团购结束时间
        /// </summary>
        /// <returns></returns>
        public DateTime? GroupEndTime { get; set; }
        /// <summary>
        /// 团购码
        /// </summary>
        /// <returns></returns>
        public string GroupNumber { get; set; }
        /// <summary>
        /// 团购结果
        /// </summary>
        /// <returns></returns>
        public int? GroupResult { get; set; }
        /// <summary>
        /// RegimentTime
        /// </summary>
        /// <returns></returns>
        public DateTime? RegimentTime { get; set; }
        /// <summary>
        /// DeliverTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeliverTime { get; set; }
        /// <summary>
        /// Distribution
        /// </summary>
        /// <returns></returns>
        public string Distribution { get; set; }
        /// <summary>
        /// DistributionTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DistributionTime { get; set; }
        /// <summary>
        /// Completer
        /// </summary>
        /// <returns></returns>
        public string Completer { get; set; }
        /// <summary>
        /// CompleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CompleteTime { get; set; }
        /// <summary>
        /// 拟出单仓库
        /// </summary>
        /// <returns></returns>
        public string FK_PlanStore { get; set; }
        /// <summary>
        /// 预计提货时间
        /// </summary>
        /// <returns></returns>
        public DateTime? PlanPickUpTime { get; set; }
        /// <summary>
        /// 实际出单仓库
        /// </summary>
        /// <returns></returns>
        public string FK_ActualStore { get; set; }
        /// <summary>
        /// 实际提货时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ActualPickUpTime { get; set; }
        /// <summary>
        /// ？？？？？
        /// </summary>
        /// <returns></returns>
        public int? IsShare { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// OrderRemark
        /// </summary>
        /// <returns></returns>
        public string OrderRemark { get; set; }
        /// <summary>
        /// 订单类型：0-门店订单；1-小程序订单
        /// </summary>
        /// <returns></returns>
        public string OrderType { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        public string organizeid { get; set; }
        public string StoreId { get; set; }
        #endregion

        #region 扩展操作

        #endregion
    }
}