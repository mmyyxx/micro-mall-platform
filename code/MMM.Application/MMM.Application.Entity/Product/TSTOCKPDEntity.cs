using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-07-02 14:27
    /// 描 述：TSTOCKPD
    /// </summary>
    public class TSTOCKPDEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 库存商品存放库存表
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 库位，t_store.StoreId
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseNo { get; set; }
        /// <summary>
        /// 商品sku
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        /// <summary>
        /// ProductDate
        /// </summary>
        /// <returns></returns>
        public DateTime? ProductDate { get; set; }
        /// <summary>
        /// 存放数量
        /// </summary>
        /// <returns></returns>
        public decimal? Stock { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}