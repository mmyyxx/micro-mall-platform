using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 11:03
    /// 描 述：商品
    /// </summary>
    public class T_PRODUCTPRICEEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品售价表？？？
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_AppId
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// Company
        /// </summary>
        /// <returns></returns>
        public string Company { get; set; }
        /// <summary>
        /// SKU
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        /// <returns></returns>
        public string Name { get; set; }
        /// <summary>
        /// EnName
        /// </summary>
        /// <returns></returns>
        public string EnName { get; set; }
        /// <summary>
        /// ProductBrand
        /// </summary>
        /// <returns></returns>
        public string ProductBrand { get; set; }
        /// <summary>
        /// ProductModel
        /// </summary>
        /// <returns></returns>
        public string ProductModel { get; set; }
        /// <summary>
        /// ProductColor
        /// </summary>
        /// <returns></returns>
        public string ProductColor { get; set; }
        /// <summary>
        /// ProductMaterial
        /// </summary>
        /// <returns></returns>
        public string ProductMaterial { get; set; }
        /// <summary>
        /// ProductParameter
        /// </summary>
        /// <returns></returns>
        public string ProductParameter { get; set; }
        /// <summary>
        /// ProductUnit
        /// </summary>
        /// <returns></returns>
        public string ProductUnit { get; set; }
        /// <summary>
        /// ProductVolume
        /// </summary>
        /// <returns></returns>
        public string ProductVolume { get; set; }
        /// <summary>
        /// ProductSize
        /// </summary>
        /// <returns></returns>
        public string ProductSize { get; set; }
        /// <summary>
        /// ProductWeight
        /// </summary>
        /// <returns></returns>
        public string ProductWeight { get; set; }
        /// <summary>
        /// ProductAuthentication
        /// </summary>
        /// <returns></returns>
        public string ProductAuthentication { get; set; }
        /// <summary>
        /// ProductPackage
        /// </summary>
        /// <returns></returns>
        public string ProductPackage { get; set; }
        /// <summary>
        /// PackingSpecification
        /// </summary>
        /// <returns></returns>
        public int? PackingSpecification { get; set; }
        /// <summary>
        /// LimitCount
        /// </summary>
        /// <returns></returns>
        public decimal? LimitCount { get; set; }
        /// <summary>
        /// IsBluetooth
        /// </summary>
        /// <returns></returns>
        public int? IsBluetooth { get; set; }
        /// <summary>
        /// DiscountPrice
        /// </summary>
        /// <returns></returns>
        public decimal? DiscountPrice { get; set; }
        /// <summary>
        /// RetailPrice
        /// </summary>
        /// <returns></returns>
        public decimal? RetailPrice { get; set; }
        /// <summary>
        /// ProductMainPicture
        /// </summary>
        /// <returns></returns>
        public string ProductMainPicture { get; set; }
        /// <summary>
        /// ProductKey
        /// </summary>
        /// <returns></returns>
        public string ProductKey { get; set; }
        /// <summary>
        /// IsLimitTimeProduct
        /// </summary>
        /// <returns></returns>
        public int? IsLimitTimeProduct { get; set; }
        /// <summary>
        /// LimitStartTime
        /// </summary>
        /// <returns></returns>
        public DateTime? LimitStartTime { get; set; }
        /// <summary>
        /// LimitEndTime
        /// </summary>
        /// <returns></returns>
        public DateTime? LimitEndTime { get; set; }
        /// <summary>
        /// IsGroupProduct
        /// </summary>
        /// <returns></returns>
        public int? IsGroupProduct { get; set; }
        /// <summary>
        /// GroupPeople
        /// </summary>
        /// <returns></returns>
        public int? GroupPeople { get; set; }
        /// <summary>
        /// IsSale
        /// </summary>
        /// <returns></returns>
        public int? IsSale { get; set; }
        /// <summary>
        /// SaleUser
        /// </summary>
        /// <returns></returns>
        public string SaleUser { get; set; }
        /// <summary>
        /// SaleTime
        /// </summary>
        /// <returns></returns>
        public DateTime? SaleTime { get; set; }
        /// <summary>
        /// DeliveryTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeliveryTime { get; set; }
        /// <summary>
        /// TransactionTime
        /// </summary>
        /// <returns></returns>
        public DateTime? TransactionTime { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.IsSale = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
                    }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}