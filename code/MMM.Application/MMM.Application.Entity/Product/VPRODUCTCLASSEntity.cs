using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    [Serializable]
    public class VPRODUCTCLASSEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Guid ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductSKU { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ClassName { get; set; }

        public String organizeid { get; set; }
        #endregion

    }
}