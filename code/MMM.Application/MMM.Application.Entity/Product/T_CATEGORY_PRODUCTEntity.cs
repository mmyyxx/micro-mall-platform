using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品分类
    /// </summary>
    public class T_CATEGORY_PRODUCTEntity : BaseEntity
    {
        #region 实体成员
        public String GUID { get; set; }
        /// <summary>
        /// 分类_商品表，分类id,T_PRODUCTCATEGORY.Guid
        /// </summary>
        /// <returns></returns>
        public string FK_Category { get; set; }
        /// <summary>
        /// 商品编号，t_product.Guid
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// t_cysuser.userid
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.FK_Product = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.FK_Product = keyValue;
        }
        #endregion
    }
}