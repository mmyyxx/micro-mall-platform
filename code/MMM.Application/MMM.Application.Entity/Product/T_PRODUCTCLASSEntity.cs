using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-30 18:21
    /// 描 述：商品类别
    /// </summary>
    public class T_PRODUCTCLASSEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品属性值
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 商品，t_product.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 属性名称
        /// </summary>
        /// <returns></returns>
        public string ProductClass { get; set; }
        /// <summary>
        /// 所属分类，自关联,t_productClass.guid
        /// </summary>
        /// <returns></returns>
        public string FK_ParentClass { get; set; }
        /// <summary>
        /// 属性值
        /// </summary>
        /// <returns></returns>
        public string ClassValue { get; set; }
        /// <summary>
        /// ClassPrice
        /// </summary>
        /// <returns></returns>
        public decimal? ClassPrice { get; set; }
        /// <summary>
        /// ClassOldPrice
        /// </summary>
        /// <returns></returns>
        public decimal? ClassOldPrice { get; set; }
        /// <summary>
        /// 商品编号SKU
        /// </summary>
        /// <returns></returns>
        public string ProductSKU { get; set; }
        /// <summary>
        /// 存储数量
        /// </summary>
        /// <returns></returns>
        public decimal? ProductStock { get; set; }
        /// <summary>
        /// 是否包或箱类商品
        /// </summary>
        /// <returns></returns>
        public int? IsPackage { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }

        public Int32? xorder { get; set; }

        public string FK_ParentClassName { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
                    }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}