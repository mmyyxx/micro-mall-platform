using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-07-02 15:02
    /// 描 述：T_ALLOCATION
    /// </summary>
    public class TALLOCATIONEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品调拨表
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 商品编号t_product.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 批次号
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        /// <returns></returns>
        public string Name { get; set; }
        /// <summary>
        /// 出T_Store.storeid
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseFrom { get; set; }
        /// <summary>
        /// 入T_Store.storeid
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseTo { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        /// <returns></returns>
        public decimal? AllocationCount { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        /// <returns></returns>
        public decimal? Price { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        public string organizeid { get; set; }

        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}