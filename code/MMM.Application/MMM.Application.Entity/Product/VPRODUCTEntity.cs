using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Product
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    [Serializable]
    public class VPRODUCTEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public String ProductGUID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        public String ProductKey { get; set; }
        public String ProductName { get; set; }
        public String ImageURL { get; set; }
        public String ProductDescribe { get; set; }
        public String ProductUnit { get; set; }
        public Int32? IsLimitTimeProduct { get; set; }
        public Int32 PackingSpecification { get; set; }
        public Int32 IsGroupProduct { get; set; }
        public Int32 GroupPeople { get; set; }
        public DateTime? LimitStartTime { get; set; }
        public DateTime? LimitEndTime { get; set; }
        public Decimal NewPrice { get; set; }
        public Decimal OldPrice { get; set; }
        public Int32? IsSale { get; set; }
        //public Int32? ProductSale { get; set; }
        public Int32 ProductZan { get; set; }

        public String organizeid { get; set; }
        #endregion

    }
}