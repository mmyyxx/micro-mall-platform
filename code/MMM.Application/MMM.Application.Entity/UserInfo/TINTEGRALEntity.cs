using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-16 21:49
    /// 描 述：积分明细
    /// </summary>
    public class TINTEGRALEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 用户积分
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 小程序用户openid
        /// </summary>
        /// <returns></returns>
        public string FK_Openid { get; set; }
        /// <summary>
        /// 所属app，t_wxapp.guid
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 积分数量
        /// </summary>
        /// <returns></returns>
        public int? Integral { get; set; }
        /// <summary>
        /// 盈余数量？？？
        /// </summary>
        /// <returns></returns>
        public int? SurplusIntegral { get; set; }
        /// <summary>
        /// OperatorId
        /// </summary>
        /// <returns></returns>
        public string OperatorId { get; set; }
        /// <summary>
        /// 积分说明
        /// </summary>
        /// <returns></returns>
        public string IntegralRemark { get; set; }
        /// <summary>
        /// 积分类型
        /// </summary>
        /// <returns></returns>
        public int? IntegralType { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }

        public String CreateDateStr { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
        }
        #endregion
    }
}