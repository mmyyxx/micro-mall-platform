using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-29 09:49
    /// 描 述：点赞
    /// </summary>
    public class TZANEntity : BaseEntity
    {
        #region 实体成员
        public string GUID { get; set; }
        /// <summary>
        /// 商品集赞表
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 用户，t_user.openid
        /// </summary>
        /// <returns></returns>
        public string FK_OpenId { get; set; }
        /// <summary>
        /// IsZan
        /// </summary>
        /// <returns></returns>
        public int? IsZan { get; set; }
        /// <summary>
        /// 当前商品的第几个赞
        /// </summary>
        /// <returns></returns>
        public int? ZanCount { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.FK_OpenId = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.FK_OpenId = keyValue;
        }
        #endregion
    }
}