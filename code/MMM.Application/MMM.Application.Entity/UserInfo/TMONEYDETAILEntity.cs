using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 23:28
    /// 描 述：余额明细
    /// </summary>
    public class TMONEYDETAILEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 会员金额变动记录表
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 成员t_member.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Member { get; set; }
        /// <summary>
        /// 金额变动
        /// </summary>
        /// <returns></returns>
        public decimal? MoneyChange { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 变动之前余额
        /// </summary>
        /// <returns></returns>
        public decimal? BeforeMoney { get; set; }
        /// <summary>
        /// 变动之后余额
        /// </summary>
        /// <returns></returns>
        public decimal? AfterMoney { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
        }
        #endregion
    }
}