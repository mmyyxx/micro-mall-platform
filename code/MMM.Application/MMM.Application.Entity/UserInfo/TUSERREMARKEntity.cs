using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-28 15:57
    /// 描 述：用户反馈
    /// </summary>
    public class TUSERREMARKEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 用户评论表
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_AppId
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 用户,t_user.openid
        /// </summary>
        /// <returns></returns>
        public string FK_OpenId { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        public string organizeid { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        /// <returns></returns>
        public bool? IsShow { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
        }
        #endregion
    }
}