using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.UserInfo
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-10 14:33
    /// 描 述：客户
    /// </summary>
    public class VWXUSEREntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 小程序用户表
        /// </summary>
        /// <returns></returns>
        public string Openid { get; set; }
        /// <summary>
        /// 对应应用，t_wxapp.guid
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 小程序sessionkey
        /// </summary>
        /// <returns></returns>
        public string SessionKey { get; set; }
        /// <summary>
        /// 小程序unionid,通过code请求解密得到
        /// </summary>
        /// <returns></returns>
        public string Unionid { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        /// <returns></returns>
        public string NickName { get; set; }
        /// <summary>
        /// 用户图标
        /// </summary>
        /// <returns></returns>
        public string AvatarUrl { get; set; }
        /// <summary>
        /// 性别
        /// </summary>
        /// <returns></returns>
        public string Gender { get; set; }
        /// <summary>
        /// 所在城市
        /// </summary>
        /// <returns></returns>
        public string City { get; set; }
        /// <summary>
        /// 所在省份
        /// </summary>
        /// <returns></returns>
        public string Province { get; set; }
        /// <summary>
        /// 民在国家
        /// </summary>
        /// <returns></returns>
        public string Country { get; set; }
        /// <summary>
        /// 使用语言
        /// </summary>
        /// <returns></returns>
        public string Language { get; set; }
        /// <summary>
        /// 注册日期
        /// </summary>
        /// <returns></returns>
        public DateTime? SignInDate { get; set; }
        /// <summary>
        /// 积分
        /// </summary>
        /// <returns></returns>
        public int? Integral { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 金蛋？？
        /// </summary>
        /// <returns></returns>
        public int? GoldEgg { get; set; }
        /// <summary>
        /// 金蛋数量？？
        /// </summary>
        /// <returns></returns>
        public int? GoldEggCount { get; set; }
        /// <summary>
        /// EventDraw
        /// </summary>
        /// <returns></returns>
        public int? EventDraw { get; set; }
        /// <summary>
        /// EventDrawDate
        /// </summary>
        /// <returns></returns>
        public DateTime? EventDrawDate { get; set; }
        /// <summary>
        /// IsShare
        /// </summary>
        /// <returns></returns>
        public int? IsShare { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }

        /// <summary>
        /// 公司id
        /// </summary>
        public String organizeid { get; set; }

        public String UserName { get; set; }
        public String Mobile { get; set; }
        #endregion
        
    }
}