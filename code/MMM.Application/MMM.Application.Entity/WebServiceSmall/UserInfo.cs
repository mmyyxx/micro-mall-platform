﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class GetUserInfo
    {
        public UserInfo userInfo { set; get; }

        public string rawData { set; get; }

        public string signature { set; get; }

        public string encryptedData { set; get; }

        public string iv { set; get; }
    }

    public class UserInfo
    {
        public string nickName { set; get; }

        public string avatarUrl { set; get; }

        public string gender { set; get; }

        public string city { set; get; }

        public string province { set; get; }

        public string country { set; get; }

        public string language { set; get; }
    }

    public class EncryptedData
    {
        public string openId { set; get; }

        public string nickName { set; get; }

        public string unionId { set; get; }

        public string avatarUrl { set; get; }

        public string gender { set; get; }

        public string city { set; get; }

        public string province { set; get; }

        public string country { set; get; }

        public string language { set; get; }

        public object watermark { set; get; }
    }
}
