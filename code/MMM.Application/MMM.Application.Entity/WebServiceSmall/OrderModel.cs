﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class OrderModel
    {
        public string GUID { set; get; }

        public string OrderNumber { set; get; }

        public int OrderStatus { set; get; }

        public bool IsGroupOrder { set; get; }

        public string GroupNumber { set; get; }

        public bool GroupResult { set; get; }

        public int DeliveryMode { set; get; }

        public DateTime? PlanPickUpTime { set; get; }

        public string GetGoodsTime { set; get; }

        public string OrderStatusStr { set; get; }

        public string Addressee { set; get; }

        public string Address { set; get; }

        public string Mobile { set; get; }

        public decimal OrderPrice { set; get; }

        public decimal Freight { get; set; }

        public DateTime DeliveryTime { set; get; }

        public DateTime OrderUseTime { set; get; }

        public DateTime? CreateTime { set; get; }

        public DateTime? RegimentTime { set; get; }

        public DateTime? DeliverTime { set; get; }

        public String DeliverName { set; get; }

        public String DeliverMobile { set; get; }

        public DateTime? CompleteTime { set; get; }

        public string Remark { set; get; }

        public List<ShopItem> ProductList { set; get; }

        public List<ProductInfo> OrderProductList { set; get; }

        public string ServiceMobile { set; get; }

        public string ExpressNumber { set; get; }

        public string FKOpenId { set; get; }

        public bool CanCancel { set; get; }

        public bool hasEval { get; set; }

        public bool hasAfter { get; set; }

        public String type { get; set; }

        public decimal? Count { get; set; }
    }
    public class ShopItem
    {
        public string orderid { get; set; }

        public string ItemCode { set; get; }

        public bool isCheck { set; get; }

        public string ImageURL { set; get; }

        public string ProductName { set; get; }
        public String ProductSku { get; set; }
        public string ProductDescribe { set; get; }

        public List<string> ProductClassList { set; get; }

        public string ProductUnit { set; get; }

        public decimal ProductCount { set; get; }

        public decimal ProductPrice { set; get; }
        public decimal TotalProductPrice { set; get; }

        public decimal OldPrice { set; get; }

        public int MinCount { set; get; }
        public int MaxCount { set; get; }

        public decimal ProductStock { set; get; }
        public String FK_Product { get; set; }
    }
}
