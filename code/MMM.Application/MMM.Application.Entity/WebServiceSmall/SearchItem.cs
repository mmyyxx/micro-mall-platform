﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class SearchItem
    {
        public string ProductGUID { set; get; }

        public string ProductName { set; get; }

        public string ImageURL { set; get; }

        public string ProductDescribe { set; get; }

        public string ProductUnit { set; get; }

        public decimal NewPrice { set; get; }

        public decimal OldPrice { set; get; }

        public int ProductSale { set; get; }

        public int ProductZan { set; get; }

        public int IsLimit { set; get; }

        public int GroupPeople { set; get; }

        public decimal Stock { set; get; }

        public string ProductComprehensive { set; get; }
    }
}
