﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class ProductInfo
    {
        public string GUID { set; get; }

        public string ProductGUID { set; get; }

        public string OrderDetailGUID { set; get; }

        public int OrderStatus { set; get; }

        public int OrderDetailStatus { set; get; }

        public string ProductName { set; get; }

        public string ProductDescribe { set; get; }

        public string ProductParam { set; get; }

        public string ProductUnit { set; get; }

        public string ProductMainPicture { set; get; }

        public string ProductClass { set; get; }

        public decimal ProductCount { set; get; }

        public int PackingSpecification { set; get; }

        public decimal DiscountPrice { set; get; }

        public decimal RetailPrice { set; get; }

        public List<string> PictureList { set; get; }

        public int IsLimitTimeProduct { set; get; }

        public DateTime? LimitEndTime { set; get; }

        public int IsGroupProduct { set; get; }

        public int GroupPeople { set; get; }

        public List<string> DetailPictureList { set; get; }

        public List<ProductClass> ProductClassList { set; get; }

        public List<string> ProductDetailList { set; get; }

        public Dictionary<string, List<string>> ClassDic { set; get; }

        public string Remark { set; get; }
        public string Remark2 { set; get; }

        public string FK_AppId { set; get; }

        /// <summary>
        /// 类型判断是数码还是零食,1是零食，2是数码
        /// </summary>
        public String type { get; set; }

        public String ProductKey { get; set; }
    }

    public class ProductClass
    {
        public string ClassType { set; get; }

        public List<ClassInfo> ClassItemList { set; get; }
    }

    public class ClassInfo
    {
        public bool ClassShow { set; get; }

        public string ClassGUID { set; get; }

        public string ClassValue { set; get; }

        public decimal? ClassStock { set; get; }

        public decimal ClassStockNum { set; get; }

        public decimal ClassPrice { set; get; }

        public decimal ClassOldPrice { set; get; }
    }
    public class ProductManage
    {
        public string ProductName { set; get; }

        public string ProductUnit { set; get; }

        public string ProductCategory { set; get; }

        public string ProductClass { set; get; }

        public decimal ProductStock { set; get; }

        public decimal? ProductSaleCount { set; get; }

        public decimal ProductPrice { set; get; }

        public decimal ProductOldPrice { set; get; }

        public List<ClassManage> ClassList { set; get; }
    }

    public class ClassManage
    {
        public string ClassName { set; get; }

        public string ClassValue { set; get; }
    }
}
