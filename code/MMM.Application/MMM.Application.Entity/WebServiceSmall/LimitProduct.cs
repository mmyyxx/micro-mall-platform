﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class LimitProduct
    {
        public string ImageURL { set; get; }

        public string ProductName { set; get; }

        public string ProductDescribe { set; get; }

        public string NewPrice { set; get; }

        public string OldPrice { set; get; }

        public decimal? SurplusStock { set; get; }
    }
}
