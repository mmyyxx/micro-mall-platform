﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class CategoryModel
    {
        public bool isChecked { set; get; }

        public string CategoryCode { set; get; }

        public string CategoryName { set; get; }

        public string ImageURL { set; get; }

        public int OrderBy { set; get; }

        public List<CategoryModel> ChildCategoryList { set; get; }
    }
}
