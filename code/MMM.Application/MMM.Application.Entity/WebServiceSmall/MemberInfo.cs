﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class MemberInfo
    {
        public string GUID { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string Mobile { get; set; }
        public string Adress { get; set; }
        public string QQNumber { get; set; }
        public string FK_UnionId { get; set; }
        public string Profession { get; set; }
        public string Email { get; set; }
        public decimal Money { get; set; }
        public string Remark { get; set; }
        public int IsDel { get; set; }
        public string Creater { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Modifyer { get; set; }
        public Nullable<System.DateTime> ModifyTime { get; set; }
        public string Deleter { get; set; }
        public Nullable<System.DateTime> DeleteTime { get; set; }

        public string AvatarUrl { set; get; }

        public string CreateTimeStr { set; get; }

        public string BirthdayStr { set; get; }

        public Int32 eventdraw { get; set; }

        public Int32 integral { get; set; }
    }

    public class MoneyDetail
    {
        public String ID { get; set; }
        public string FK_Member { get; set; }
        public decimal MoneyChange { get; set; }
        public string Remark { get; set; }
        public string Creater { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string CreateDateStr { set; get; }
    }
}
