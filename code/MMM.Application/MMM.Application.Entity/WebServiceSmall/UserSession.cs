﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class UserSession
    {
        public string openid { set; get; }

        public string session_key { set; get; }

        public string unionid { set; get; }

        public string errcode { set; get; }

        public string errmsg { set; get; }
    }
}
