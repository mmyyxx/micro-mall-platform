﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.WebServiceSmall
{
    public class ModelList
    {
        public string GUID { set; get; }

        public int? ModelOrder { set; get; }

        public int WindowWidth { set; get; }

        public string ModelName { set; get; }

        public string ModelTitle { set; get; }

        public string ModelURL { set; get; }

        public string DateTime { set; get; }

        public string Week { set; get; }

        public String remark { get; set; }

        public List<List<ProductModel>> ProductList3 { get; set; }

        public List<ProductModel> ProductList { set; get; }
    }

    public class ProductModel
    {
        public int ProductOrder { set; get; }

        public string ProductDescribes { set; get; }

        public string[] ProductDescribe { set; get; }

        public int width { set; get; }

        public string productName { set; get; }

        public string image { set; get; }

        public string imageurl { set; get; }

        public string productName2 { set; get; }

        public string image2 { set; get; }

        public string imageurl2 { set; get; }

        public string price1 { set; get; }

        public string price2 { set; get; }
    }


    public class ModelListTimer
    {
        public string GUID { set; get; }

        public int? ModelOrder { set; get; }

        public int WindowWidth { set; get; }

        public string ModelName { set; get; }

        public string ModelTitle { set; get; }

        public string ModelURL { set; get; }

        public string DateTime { set; get; }

        public string Week { set; get; }

        public String remark { get; set; }

        public List<List<ProductModelTimer>> ProductList3 { get; set; }

        public List<ProductModelTimer> ProductList { set; get; }
    }

    public class ProductModelTimer
    {
        public int ProductOrder { set; get; }

        public string ProductDescribes { set; get; }

        public string[] ProductDescribe { set; get; }

        public int width { set; get; }

        public string productName { set; get; }

        public string image { set; get; }

        public string imageurl { set; get; }

        public string productName2 { set; get; }

        public string image2 { set; get; }

        public string imageurl2 { set; get; }

        public string price1 { set; get; }

        public string price2 { set; get; }

        public Int32? num1 { get; set; }

        public Int32? num2 { get; set; }

        public String Timer1 { get; set; }
    }
}
