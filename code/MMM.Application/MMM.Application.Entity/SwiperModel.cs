﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity
{
    public class SwiperModel
    {
        public string dotscolor { set; get; }

        public string selectdotcolor { set; get; }

        public int interval { set; get; }

        public int duration { set; get; }

        public int width { set; get; }

        public int height { set; get; }

        public string appguid { set; get; }
    }
}
