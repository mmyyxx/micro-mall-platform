﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.Finance
{
    public class CostModel
    {
        public string SKU { set; get; }

        public string ProductName { set; get; }

        public decimal SaleCount { set; get; }

        public decimal DetailCount { set; get; }

        public decimal SalePrice { set; get; }

        public decimal SaleTotal { set; get; }

        public decimal CostPrice { set; get; }

        public decimal CostTotal { set; get; }

        public decimal Maori { set; get; }

        public decimal GrossInterestRate { set; get; }

        public String Creater { get; set; }
    }
}
