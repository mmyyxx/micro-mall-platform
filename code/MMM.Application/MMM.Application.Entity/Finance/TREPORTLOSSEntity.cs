using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Finance
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-20 16:38
    /// 描 述：报损
    /// </summary>
    public class TREPORTLOSSEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 报损表
        /// </summary>
        /// <returns></returns>
        public string ID { get; set; }
        /// <summary>
        /// 门店id,t_store.storeID
        /// </summary>
        /// <returns></returns>
        public string FK_WarehouseNo { get; set; }
        /// <summary>
        /// 商品，t_product.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Product { get; set; }
        /// <summary>
        /// 报损数量
        /// </summary>
        /// <returns></returns>
        public decimal? PdtCount { get; set; }
        /// <summary>
        /// 商品单价
        /// </summary>
        /// <returns></returns>
        public decimal? CostPrice { get; set; }
        /// <summary>
        /// 小计
        /// </summary>
        /// <returns></returns>
        public decimal? CostTotal { get; set; }
        /// <summary>
        /// 是否已经出库
        /// </summary>
        /// <returns></returns>
        public int? IsStockOut { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.ID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            this.IsStockOut = 1;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.ID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
            this.FK_WarehouseNo = null;
            this.organizeid = null;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.ID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}