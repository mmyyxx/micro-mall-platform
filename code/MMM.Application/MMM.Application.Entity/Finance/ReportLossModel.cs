﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity.Finance
{
    public class ReportLossModel
    {
        public String ID { get; set; }
        public string FK_WarehouseNo { get; set; }
        public string FK_Product { get; set; }
        public decimal PdtCount { get; set; }
        public decimal CostPrice { get; set; }
        public decimal CostTotal { get; set; }
        public int IsStockOut { get; set; }
        public string Remark { get; set; }
        public int IsDel { get; set; }
        public string Creater { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public string Modifyer { get; set; }
        public Nullable<System.DateTime> ModifyTime { get; set; }
        public string Deleter { get; set; }
        public Nullable<System.DateTime> DeleteTime { get; set; }

        public string SKU { set; get; }

        public string PdtName { set; get; }

        public string Renark { set; get; }
        public String organizeid { get; set; }
    }
}
