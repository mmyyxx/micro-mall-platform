using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.ShoppingMall
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-24 11:01
    /// 描 述：模块商品
    /// </summary>
    public class TMODELPRODUCTEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 各模块的具体商品
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_ModelFunction
        /// </summary>
        /// <returns></returns>
        public string FK_ModelFunction { get; set; }
        /// <summary>
        /// Product1
        /// </summary>
        /// <returns></returns>
        public string Product1 { get; set; }
        /// <summary>
        /// Product2
        /// </summary>
        /// <returns></returns>
        public string Product2 { get; set; }
        /// <summary>
        /// ImageNavigate1
        /// </summary>
        /// <returns></returns>
        public string ImageNavigate1 { get; set; }
        /// <summary>
        /// ImageNavigate2
        /// </summary>
        /// <returns></returns>
        public string ImageNavigate2 { get; set; }
        /// <summary>
        /// Price1
        /// </summary>
        /// <returns></returns>
        public decimal? Price1 { get; set; }
        /// <summary>
        /// Price2
        /// </summary>
        /// <returns></returns>
        public decimal? Price2 { get; set; }
        /// <summary>
        /// ImageAdress1
        /// </summary>
        /// <returns></returns>
        public string ImageAdress1 { get; set; }
        /// <summary>
        /// ImageAdress2
        /// </summary>
        /// <returns></returns>
        public string ImageAdress2 { get; set; }
        /// <summary>
        /// ProductOrder
        /// </summary>
        /// <returns></returns>
        public int? ProductOrder { get; set; }
        /// <summary>
        /// ProductDescribe
        /// </summary>
        /// <returns></returns>
        public string ProductDescribe { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        public string organizeid { get; set; }

        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}