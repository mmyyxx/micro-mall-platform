using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-06-03 15:03
    /// </summary>
    public class VSTOCKDETAILEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public String GUID { get; set; }
        public String LocationGUID { get; set;}
        public String ProductGUID { get; set;}
        public String WarehouseName { get; set;}
        public String LocationNo { get; set; }
        /// <summary>
        /// 仓库编号
        /// </summary>
        /// <returns></returns>
        public string WarehouseNo { get; set; }
        public string BatchNumber { get; set; }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public string SKU { get; set; }
        public string PdtName { get; set; }
        public DateTime? ProductionDate { get; set; }
        public int QualityDay { get; set; }
        public Decimal? Stock { get; set; }
        public Decimal? ActualStock { get; set; }

        #endregion

    }
}