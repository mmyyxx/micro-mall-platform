using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.Warehouse
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-22 11:59
    /// 描 述：储位
    /// </summary>
    public class T_LOCATIONEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }

        /// <summary>
        /// 所属仓库
        /// </summary>
        public String FK_WarehouseNo { get; set; }

        /// <summary>
        /// 所属库区，t_area.guid
        /// </summary>
        /// <returns></returns>
        public string FK_Area { get; set; }
        /// <summary>
        /// 储位编码
        /// </summary>
        /// <returns></returns>
        public string LocationNo { get; set; }
        /// <summary>
        /// 储位长（米）
        /// </summary>
        /// <returns></returns>
        public decimal? Length { get; set; }
        /// <summary>
        /// 储位宽（米）
        /// </summary>
        /// <returns></returns>
        public decimal? Width { get; set; }
        /// <summary>
        /// 储位高（米）
        /// </summary>
        /// <returns></returns>
        public decimal? Height { get; set; }
        /// <summary>
        /// 储位体积（立方米）
        /// </summary>
        /// <returns></returns>
        public decimal? Volume { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// 删除FLG
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// 作成者
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// 作成时间
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// 更新者
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// 删除者
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// 删除时间
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
            this.FK_Area = null;
            this.FK_WarehouseNo = null;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}