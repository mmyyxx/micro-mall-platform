using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.BaseManage
{
    /// <summary>
    /// 版 本
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：T_CARD
    /// </summary>
    public class T_CARDEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 卡券
        /// </summary>
        /// <returns></returns>
        public string CardId { get; set; }
        /// <summary>
        /// T_WXApp.guid，类型id，区分零食和数码
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }

        public string FK_AppIdName { get; set; }
        /// <summary>
        /// 类型｛0表示满减，1｝？？
        /// </summary>
        /// <returns></returns>
        public int? CardType { get; set; }

        public String CardTypeName { get; set; }
        /// <summary>
        /// 满多少
        /// </summary>
        /// <returns></returns>
        public decimal? LeastCost { get; set; }
        /// <summary>
        /// 抵销多少
        /// </summary>
        /// <returns></returns>
        public decimal? ReduceCost { get; set; }
        /// <summary>
        /// 卡券名称
        /// </summary>
        /// <returns></returns>
        public string Explain { get; set; }
        /// <summary>
        /// 有效期几天
        /// </summary>
        /// <returns></returns>
        public int? ValidDate { get; set; }
        /// <summary>
        /// ？？？
        /// </summary>
        /// <returns></returns>
        public int? Integral { get; set; }
        /// <summary>
        /// 止券图片1
        /// </summary>
        /// <returns></returns>
        public string ImgURL { get; set; }
        /// <summary>
        /// 止券图片2
        /// </summary>
        /// <returns></returns>
        public string ImgURL2 { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// t_cysuser.userid
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }

        public String organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            Operator o = OperatorProvider.Provider.Current();
            this.Creater = o.Account;
            this.CreateTime = DateTime.Now;
            this.CardId = Guid.NewGuid().ToString();
            this.IsDel = 0;
            this.organizeid = o.CompanyId;
        }

        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.CardId = keyValue;
            Operator o = OperatorProvider.Provider.Current();
            this.Modifyer = o.Account;
            this.ModifyTime = DateTime.Now;
        }

        public override void Remove(string keyValue)
        {
            this.CardId = keyValue;
            Operator o = OperatorProvider.Provider.Current();
            this.Deleter = o.Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}