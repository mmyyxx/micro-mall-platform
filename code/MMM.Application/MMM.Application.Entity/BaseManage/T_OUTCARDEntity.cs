using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-18 22:59
    /// 描 述：T_OUTCARD
    /// </summary>
    public class T_OUTCARDEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 暂时不用
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_OpenId
        /// </summary>
        /// <returns></returns>
        public string FK_OpenId { get; set; }
        /// <summary>
        /// CardId
        /// </summary>
        /// <returns></returns>
        public string CardId { get; set; }
        /// <summary>
        /// FK_AppId
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// IsUse
        /// </summary>
        /// <returns></returns>
        public int? IsUse { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            Operator o = OperatorProvider.Provider.Current();
            this.Creater = o.Account;
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.IsDel = 0;
            this.IsUse = 0;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
        }
        #endregion
    }
}