using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-19 11:23
    /// 描 述：T_FREIGHT
    /// </summary>
    public class T_FREIGHTEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 邮费记录
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 所属哪个应用程序t_wxApp.guid
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 距离
        /// </summary>
        /// <returns></returns>
        public decimal? Distance { get; set; }
        /// <summary>
        /// 费用
        /// </summary>
        /// <returns></returns>
        public decimal? Freight { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// 公司id
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            Operator o = OperatorProvider.Provider.Current();
            this.Creater = o.Account;
            this.CreateTime = DateTime.Now;
            this.GUID = Guid.NewGuid().ToString();
            this.IsDel = 0;
            this.organizeid = o.CompanyId;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            Operator o = OperatorProvider.Provider.Current();
            this.Modifyer = o.Account;
            this.ModifyTime = DateTime.Now;
        }

        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            Operator o = OperatorProvider.Provider.Current();
            this.Deleter = o.Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}