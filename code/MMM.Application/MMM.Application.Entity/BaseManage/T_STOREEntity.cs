using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-24 22:44
    /// 描 述：门店
    /// </summary>
    public class T_STOREEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商店表
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// FK_AppId
        /// </summary>
        /// <returns></returns>
        public string FK_AppId { get; set; }
        /// <summary>
        /// 仓库编号，唯一
        /// </summary>
        /// <returns></returns>
        public string StoreId { get; set; }
        /// <summary>
        /// 仓库名称
        /// </summary>
        /// <returns></returns>
        public string StoreName { get; set; }
        /// <summary>
        /// 仓库地址
        /// </summary>
        /// <returns></returns>
        public string StoreAdress { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        /// <returns></returns>
        public string StoreMobile { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        /// <returns></returns>
        public decimal? Longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        /// <returns></returns>
        public decimal? Latitude { get; set; }
        /// <summary>
        /// Remark
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Modifyer
        /// </summary>
        /// <returns></returns>
        public string Modifyer { get; set; }
        /// <summary>
        /// ModifyTime
        /// </summary>
        /// <returns></returns>
        public DateTime? ModifyTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        /// <summary>
        /// organizeid
        /// </summary>
        /// <returns></returns>
        public string organizeid { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
        }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
            this.ModifyTime = DateTime.Now;
            this.Modifyer = OperatorProvider.Provider.Current().Account;
        }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}