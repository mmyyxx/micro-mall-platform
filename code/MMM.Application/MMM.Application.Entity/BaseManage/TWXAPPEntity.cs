using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.BaseManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-06-16 16:33
    /// 描 述：TWXAPP
    /// </summary>
    public class TWXAPPEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 主键
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 小程序ID
        /// </summary>
        /// <returns></returns>
        public string AppId { get; set; }
        /// <summary>
        /// 小程序名称
        /// </summary>
        /// <returns></returns>
        public string AppName { get; set; }
        /// <summary>
        /// 小程序密匙
        /// </summary>
        /// <returns></returns>
        public string AppSecret { get; set; }
        /// <summary>
        /// 小程序商品类别
        /// </summary>
        /// <returns></returns>
        public string AppDescribe { get; set; }
        /// <summary>
        /// AccessToken
        /// </summary>
        /// <returns></returns>
        public string AccessToken { get; set; }
        /// <summary>
        /// AccessTokenTime
        /// </summary>
        /// <returns></returns>
        public DateTime? AccessTokenTime { get; set; }
        /// <summary>
        /// 客服电话
        /// </summary>
        /// <returns></returns>
        public string Mobile { get; set; }
        /// <summary>
        /// Longitude
        /// </summary>
        /// <returns></returns>
        public decimal? Longitude { get; set; }
        /// <summary>
        /// Latitude
        /// </summary>
        /// <returns></returns>
        public decimal? Latitude { get; set; }
        /// <summary>
        /// MCHID
        /// </summary>
        /// <returns></returns>
        public string MCHID { get; set; }
        /// <summary>
        /// PayKey
        /// </summary>
        /// <returns></returns>
        public string PayKey { get; set; }
        /// <summary>
        /// SSLCERTPATH
        /// </summary>
        /// <returns></returns>
        public string SSLCERTPATH { get; set; }
        /// <summary>
        /// SSLCERTPASSWORD
        /// </summary>
        /// <returns></returns>
        public string SSLCERTPASSWORD { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Number
        /// </summary>
        /// <returns></returns>
        public int? Number { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
                                    this.IsDel = 0;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.IsDel = 1;
        }
        #endregion
    }
}