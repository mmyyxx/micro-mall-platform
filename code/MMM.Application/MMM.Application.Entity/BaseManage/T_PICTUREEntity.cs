using System;
using MMM.Application.Code;

namespace MMM.Application.Entity.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-28 21:28
    /// 描 述：图片
    /// </summary>
    public class T_PICTUREEntity : BaseEntity
    {
        #region 实体成员
        /// <summary>
        /// 商品图片
        /// </summary>
        /// <returns></returns>
        public string GUID { get; set; }
        /// <summary>
        /// 与商品关联或是与活动商品关联
        /// </summary>
        /// <returns></returns>
        public string FK_GUID { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        /// <returns></returns>
        public int? Type { get; set; }
        /// <summary>
        /// 不用
        /// </summary>
        /// <returns></returns>
        public string Image { get; set; }
        /// <summary>
        /// ImageName
        /// </summary>
        /// <returns></returns>
        public string ImageName { get; set; }
        /// <summary>
        /// ImageURL
        /// </summary>
        /// <returns></returns>
        public string ImageURL { get; set; }
        /// <summary>
        /// IsDel
        /// </summary>
        /// <returns></returns>
        public int? IsDel { get; set; }
        /// <summary>
        /// Creater
        /// </summary>
        /// <returns></returns>
        public string Creater { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        /// <returns></returns>
        public DateTime? CreateTime { get; set; }
        /// <summary>
        /// Deleter
        /// </summary>
        /// <returns></returns>
        public string Deleter { get; set; }
        /// <summary>
        /// DeleteTime
        /// </summary>
        /// <returns></returns>
        public DateTime? DeleteTime { get; set; }
        #endregion

        #region 扩展操作
        /// <summary>
        /// 新增调用
        /// </summary>
        public override void Create()
        {
            this.GUID = Guid.NewGuid().ToString();
            this.CreateTime = DateTime.Now;
            this.Creater = OperatorProvider.Provider.Current().Account;
            this.IsDel = 0;
            //this.organizeid = OperatorProvider.Provider.Current().CompanyId;
                    }
        /// <summary>
        /// 编辑调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Modify(string keyValue)
        {
            this.GUID = keyValue;
                                            }
        /// <summary>
        /// 删除调用
        /// </summary>
        /// <param name="keyValue"></param>
        public override void Remove(string keyValue)
        {
            this.GUID = keyValue;
            this.Deleter = OperatorProvider.Provider.Current().Account;
            this.DeleteTime = DateTime.Now;
            this.IsDel = 1;
        }
        #endregion
    }
}