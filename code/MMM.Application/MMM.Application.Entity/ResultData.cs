﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Application.Entity
{
    [Serializable]
    public class ResultData
    {
        public bool IsOK { set; get; }

        public string Data { set; get; }

        public string msg { set; get; }

        /// <summary>
        /// 公司id
        /// </summary>
        public String organizeid { get; set; }

        public ResultData()
        {
            IsOK = false;
        }
    }
}
