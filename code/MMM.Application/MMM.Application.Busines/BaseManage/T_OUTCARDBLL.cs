using MMM.Application.Entity.BaseManage;
using MMM.Application.Service.BaseManage;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System;

namespace MMM.Application.Busines.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-18 22:59
    /// 描 述：T_OUTCARD
    /// </summary>
    public class T_OUTCARDBLL
    {
        private T_OUTCARDService service = new T_OUTCARDService();

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_OUTCARDEntity> GetList(string queryJson)
        {
            return service.GetList(queryJson);
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_OUTCARDEntity GetEntity(string keyValue)
        {
            return service.GetEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            service.RemoveForm(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, Int32 CardCount, String FK_AppId)
        {
            service.SaveForm(keyValue, CardCount, FK_AppId);
        }

        /// <summary>
        /// 生成卡券
        /// </summary>
        /// <param name="keyValue">T_Card主键</param>
        /// <returns>路径</returns>
        public IList<T_OUTCARDEntity> ExportCard(string keyValue)
        {
            return service.ExportCard(keyValue);
        }
        #endregion
    }
}
