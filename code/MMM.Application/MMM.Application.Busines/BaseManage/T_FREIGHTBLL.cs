using MMM.Application.Entity.BaseManage;
using MMM.Application.Service.BaseManage;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;

namespace MMM.Application.Busines.BaseManage
{
    /// <summary>
    /// 创 建：MMM
    /// 日 期：2019-05-19 11:23
    /// 描 述：T_FREIGHT
    /// </summary>
    public class T_FREIGHTBLL
    {
        private T_FREIGHTService service = new T_FREIGHTService();

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_FREIGHTEntity> GetList(string queryJson)
        {
            return service.GetList(queryJson);
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_FREIGHTEntity GetEntity(string keyValue)
        {
            return service.GetEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
                service.RemoveForm(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_FREIGHTEntity entity)
        {
                service.SaveForm(keyValue, entity);
        }

        public IEnumerable<T_FREIGHTEntity> GetPageList(Pagination pagination, JObject queryJson, String organizeList)
        {
            return service.GetPageList(pagination, queryJson, organizeList);
        }
        #endregion
    }
}
