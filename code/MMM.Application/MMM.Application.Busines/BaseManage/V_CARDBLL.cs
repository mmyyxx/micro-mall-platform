using MMM.Application.Entity.BaseManage;
using MMM.Application.Service.BaseManage;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;

namespace MMM.Application.Busines.BaseManage
{
    /// <summary>
    /// 创 建：超级管理员
    /// 日 期：2019-05-17 13:46
    /// 描 述：T_CARD
    /// </summary>
    public class V_CARDBLL
    {
        private V_CARDService service = new V_CARDService();

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<V_CARDEntity> GetPageList(Pagination pagination, JObject queryJson, String organizeList)
        {
            return service.GetPageList(pagination, queryJson, organizeList);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<V_CARDEntity> GetList(string queryJson)
        {
            return service.GetList(queryJson);
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public V_CARDEntity GetEntity(string keyValue)
        {
            return service.GetEntity(keyValue);
        }
        #endregion
        
    }
}
