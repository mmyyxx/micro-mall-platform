using MMM.Application.Entity.BaseManage;
using MMM.Application.Service.BaseManage;
using MMM.Util.WebControl;
using System.Collections.Generic;
using System;
using Newtonsoft.Json.Linq;

namespace MMM.Application.Busines.BaseManage
{
    /// <summary>
    /// 描 述：T_CARD
    /// </summary>
    public class T_CARDBLL
    {
        private T_CARDService service = new T_CARDService();

        #region 获取数据
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="pagination">分页</param>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回分页列表</returns>
        public IEnumerable<T_CARDEntity> GetPageList(Pagination pagination, JObject queryJson, IList<OrganizeEntity> organizeList)
        {
            return service.GetPageList(pagination, queryJson, organizeList);
        }
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="queryJson">查询参数</param>
        /// <returns>返回列表</returns>
        public IList<T_CARDEntity> GetList(string queryJson)
        {
            return service.GetList(queryJson);
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <returns></returns>
        public T_CARDEntity GetEntity(string keyValue)
        {
            return service.GetEntity(keyValue);
        }
        #endregion

        #region 提交数据
        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="keyValue">主键</param>
        public void RemoveForm(string keyValue)
        {
            service.RemoveForm(keyValue);
        }
        /// <summary>
        /// 保存表单（新增、修改）
        /// </summary>
        /// <param name="keyValue">主键值</param>
        /// <param name="entity">实体对象</param>
        /// <returns></returns>
        public void SaveForm(string keyValue, T_CARDEntity entity)
        {
            service.SaveForm(keyValue, entity);
        }
        #endregion
    }
}
