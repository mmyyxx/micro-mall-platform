﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MMM.Util
{
    public class JSONHelper
    {
        public static string ObjectToJson<T>(T obj)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, obj);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }

        public static T DecodeObject<T>(string data)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(data));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static string WXObjectToJson<T>(T obj)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(obj);
        }

        public static T WXDecodeObject<T>(string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Deserialize<T>(data);
        }
    }
}
