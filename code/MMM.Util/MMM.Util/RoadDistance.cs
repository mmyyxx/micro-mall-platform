﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Util
{
    public class RoadDistance
    {
        public static decimal algorithm(decimal longitude1, decimal latitude1, decimal longitude2, decimal latitude2)
        {
            double Lat1 = rad(Convert.ToDouble(latitude1));
            double Lat2 = rad(Convert.ToDouble(latitude2));
            double a = Lat1 - Lat2;
            double b = rad(Convert.ToDouble(longitude1)) - rad(Convert.ToDouble(longitude2));
            double s = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(a / 2), 2) + Math.Cos(Lat1) * Math.Cos(Lat2) * Math.Pow(Math.Sin(b / 2), 2)));
            s = s * 6378137.0;
            s = Math.Round(s * 10000d) / 10000d;
            return Convert.ToDecimal(s / 1000);
        }

        private static double rad(double d)
        {
            return d * Math.PI / 180.00;
        }
    }
}
