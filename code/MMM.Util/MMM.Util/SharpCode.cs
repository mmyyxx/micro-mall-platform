﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMM.Util
{
    public class SharpCode
    {
        /// <summary>
        /// 创建压缩文件
        /// </summary>
        /// <param name="filesPath">需要压缩的文件路径</param>
        /// <param name="zipFilePath">压缩之后的路径</param>
        public static void CreateZipFile(string filesPath, string zipFilePath)
        {
            if (!Directory.Exists(filesPath))
            {
                Console.WriteLine("Cannot find directory '{0}'", filesPath);
                return;
            }
            try
            {
                string[] filenames = Directory.GetFiles(filesPath);
                using (ZipOutputStream s = new ZipOutputStream(File.Create(zipFilePath)))
                {

                    s.SetLevel(9); // 压缩级别 0-9
                    //s.Password = "123"; //Zip压缩文件密码
                    byte[] buffer = new byte[4096]; //缓冲区大小
                    foreach (string file in filenames)
                    {
                        ZipEntry entry = new ZipEntry(Path.GetFileName(file));
                        entry.DateTime = DateTime.Now;
                        s.PutNextEntry(entry);
                        using (FileStream fs = File.OpenRead(file))
                        {
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                s.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                    }
                    s.Finish();
                    s.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during processing {0}", ex);
            }
        }

        /// <summary>
        /// 创建压缩文件
        /// </summary>
        /// <param name="filesPath">需要压缩的文件路径</param>
        /// <param name="zipFilePath">压缩之后的路径</param>
        public static void CreateZipFile2(string filesPath, string zipFilePath)
        {
            if (!Directory.Exists(filesPath))
            {
                Console.WriteLine("Cannot find directory '{0}'", filesPath);
                return;
            }
            try
            {
                string[] filenames = Directory.GetFiles(filesPath);
                String[] directorynames = Directory.GetDirectories(filesPath);
                using (ZipFile zip = ZipFile.Create(zipFilePath))
                {
                    zip.BeginUpdate();
                    foreach(String s in filenames)
                    {
                        zip.Add(s);
                    }
                    foreach (String s in directorynames)
                    {
                        zip.AddDirectory(s);
                    }
                    zip.CommitUpdate();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception during processing {0}", ex);
            }
        }

        /// <summary>
        /// 压缩多个文件目录
        /// </summary>
        /// <param name="dirname">需要压缩的目录</param>
        /// <param name="zipFile">压缩后的文件名</param>
        /// <param name="level">压缩等级</param>
        /// <param name="password">密码</param>
        public static void ZipDir(string dirname, string zipFile, int level = 9)
        {
            ZipOutputStream zos = new ZipOutputStream(File.Create(zipFile));
            zos.SetLevel(level);
            addZipEntry(dirname, zos, dirname);
            zos.Finish();
            zos.Close();
        }
        /// <summary>
        /// 往压缩文件里面添加Entry
        /// </summary>
        /// <param name="PathStr">文件路径</param>
        /// <param name="zos">ZipOutputStream</param>
        /// <param name="BaseDirName">基础目录</param>
        private static void addZipEntry(string PathStr, ZipOutputStream zos, string BaseDirName)
        {
            DirectoryInfo dir = new DirectoryInfo(PathStr);
            foreach (FileSystemInfo item in dir.GetFileSystemInfos())
            {
                if ((item.Attributes & FileAttributes.Directory) == FileAttributes.Directory)//如果是文件夹继续递归
                {
                    addZipEntry(item.FullName, zos, BaseDirName);
                }
                else
                {
                    FileInfo f_item = (FileInfo)item;
                    using (FileStream fs = f_item.OpenRead())
                    {
                        byte[] buffer = new byte[(int)fs.Length];
                        fs.Position = 0;
                        fs.Read(buffer, 0, buffer.Length);
                        fs.Close();
                        ZipEntry z_entry = new ZipEntry(item.FullName.Replace(BaseDirName, ""));
                        zos.PutNextEntry(z_entry);
                        zos.Write(buffer, 0, buffer.Length);
                    }
                }
            }
        }

    }
}
