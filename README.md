# 超市微商城平台

基于c#、EF6开发开发的超市平台包含微商城(包含团购、新人福利、组合商品等功能)，仓库管理，收银平台，客户管理等功能，微商城支持自定义模块样式。

## 内置功能

1. 小程序商城
2. 超值拼团
3. 新人福利
4. 订单管理
5. 客户关系管理
6. 仓库管理（包含小程序库存管理）
7. 财务管理
8. 收银台

## 功能截图
![输入图片说明](https://foruda.gitee.com/images/1659794973130998754/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659795017876076683/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659795017972108134/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659795017875778431/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659795017960710584/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659795018339953079/屏幕截图.png "屏幕截图.png")

![输入图片说明](https://foruda.gitee.com/images/1659795032463418314/屏幕截图.png "屏幕截图.png")![输入图片说明](https://foruda.gitee.com/images/1659795032764429962/屏幕截图.png "屏幕截图.png")





![输入图片说明](https://foruda.gitee.com/images/1659795380961029958/分类页面.jpeg "分类页面.jpg")

![输入图片说明](https://foruda.gitee.com/images/1659795539198143111/拼团2.jpeg "拼团2.jpg")

![输入图片说明](https://foruda.gitee.com/images/1659795826363010689/屏幕截图.png "屏幕截图.png")