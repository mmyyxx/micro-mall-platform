// pages/Setting/OrderList.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    OrderList: [],
    ListHeight: wx.getSystemInfoSync().windowHeight,
    openId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (app.globalData.userId == null) {
      wx.navigateTo({
        url: '/pages/index/index'
      })
      return
    }
    if (options.code) {
      this.setData({
        openId: options.code
      })
    }
    this.GetOrderList();

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //获取订单数据
  GetOrderList: function() {
    var that = this;
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    util.postWscMethods('GetStoreOrderList', {
      wxopenId: that.data.openId
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          OrderList: JSON.parse(data.Data)
        })
      }
      wx.hideLoading()
    })
  },
  //确认完成
  CompleteOrder: function(e) {
    var that = this
    //取货
    wx.showModal({
      content: '是否确认完成订单？',
      confirmColor: "#26D1FF",
      confirmText: "确定",
      success: function(res) {
        if (res.confirm) {
          wx.showLoading({
            title: '确认中',
            mask: true
          })
          util.postWscMethods('KFConfirmOrder', {
            userId: app.globalData.userId,
            guid: e.currentTarget.id,
            warehouseNo: app.globalData.warehouseNo
          }, function(data) {
            wx.hideLoading()
            if (data.IsOK) {
              wx.showModal({
                content: '确认成功！',
                showCancel: false,
                confirmColor: "#26D1FF",
                confirmText: "确定",
                success: function() {
                  that.GetOrderList();
                }
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#26D1FF",
                confirmText: "确定"
              })
            }
          })
        }
      }
    })
  }
})