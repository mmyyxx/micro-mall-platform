// pages/Setting/UserCard.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //列表高度
    ScrollHeight: null,
    //卡券
    CardList: null,
    //用户标识
    wxOpenId: ''
  },
  //刷新列表
  DataBind: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    util.postWscMethods('GetUserCard', {
      openId: that.data.wxOpenId
    }, function(data) {
      wx.hideLoading()
      if (data.IsOK) {
        that.setData({
          CardList: JSON.parse(data.Data)
        })
      } else {
        wx.showModal({
          content: '用户卡券信息获取失败！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var systemInfo = wx.getSystemInfoSync()
    this.setData({
      ScrollHeight: systemInfo.windowHeight,
      wxOpenId: options.code
    })
    this.DataBind()
  },
  //选择卡券
  ChooseCard: function(e) {
    var that = this
    if (e.currentTarget.dataset.isuse == 2) {
      wx.showModal({
        content: '该卡券已过期！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
    } else {
      wx.showModal({
        content: '是否确定核销该卡券？',
        confirmColor: "#26D1FF",
        confirmText: "确定",
        success: function(res) {
          if (res.confirm) {
            wx.showLoading({
              title: '核销中',
              mask: true
            })
            util.postWscMethods('UseCard', {
              card: e.currentTarget.id,
              userId: app.globalData.userId
            }, function(data) {
              wx.hideLoading()
              wx.showModal({
                content: data.IsOK ? "卡券已核销！" : "卡券核销失败！",
                showCancel: false,
                confirmColor: "#26D1FF",
                confirmText: "确定"
              })
              that.DataBind()
            })
          }
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})