// pages/StockManage/ProductStock.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: [],
    orderguid: '',
    sku: ''
  },
  //刷新数据
  DataBind: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods('GetProductStock', {
      sku: that.data.sku,
      warehouseNo: app.globalData.warehouseNo
    }, function(data) {
      wx.hideLoading()
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
      } else {
        that.setData({
          List: []
        })
        wx.showModal({
          content: '商品SKU' + that.data.sku + '的库存不存在！',
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定",
          complete: function() {
            wx.navigateBack({
              delta: 1
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.code) {
      this.setData({
        orderguid: options.code
      })
    }

    this.setData({
      sku: options.sku
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }

    this.DataBind();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //行点击事件
  ItemClick: function(e) {
    console.log(e.detail)
  }
})