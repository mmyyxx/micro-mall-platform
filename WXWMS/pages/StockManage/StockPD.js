// pages/StockManage/StockPD.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    SKU: '',
    date: '请选择生产日期',
    Stock: '',
    PdtName: '',
    PdtStock: '',
    QualityDate: ''
  },
  //条码输入
  bindKeyInput(e) {
    this.setData({
      SKU: e.detail.value
    })
  },
  //日期选择
  bindDateChange(e) {
    this.setData({
      date: e.detail.value
    })
  },
  //库存输入
  bindValueInput(e) {
    this.setData({
      Stock: e.detail.value
    })
  },
  //保质期输入
  bindDateInput(e) {
    this.setData({
      QualityDate: e.detail.value
    })
  },
  //条码扫码
  ProductScan() {
    var that = this
    // 只允许从相机扫码
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        that.setData({
          SKU: res.result
        })
        that.BindData()
      }
    })
  },
  //根据条码查询商品信息
  BindData() {
    var that = this
    util.postMethods('GetPDProductStock', {
      sku: that.data.SKU,
      warehouseNo: app.globalData.warehouseNo
    }, function(data) {
      if (data.IsOK) {
        var resultData = JSON.parse(data.Data)
        that.setData({
          PdtName: resultData.PdtName,
          PdtStock: resultData.Stock,
          QualityDate: resultData.QualityDate == 0 ? '' : resultData.QualityDate
        })
      } else {
        wx.showModal({
          content: '商品信息获取失败！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      }
    })
  },
  //保存信息
  BtnSave: function(e) {
    if (!this.data.SKU) {
      wx.showModal({
        content: '请输入或扫描条码！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    if (this.data.date == '请选择生产日期') {
      wx.showModal({
        content: '请选择生产日期！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    if (this.data.Stock == 0 || !this.data.Stock) {
      wx.showModal({
        content: '请输入库存！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    if (this.data.QualityDate == 0 || !this.data.QualityDate) {
      wx.showModal({
        content: '请输入保质期！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    //提交信息
    var that = this
    util.postMethods('SaveStockPD', {
      sku: that.data.SKU,
      warehouseNo: app.globalData.warehouseNo,
      pdtDate: that.data.date,
      pdtStock: that.data.Stock * e.currentTarget.id,
      qualityDate: that.data.QualityDate,
      operatorId: app.globalData.userId
    }, function(data) {
      if (data.IsOK) {
        that.BindData()
        that.setData({
          date: '请选择生产日期',
          Stock: ''
        })
      } else {
        wx.showModal({
          content: '保存失败！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  }
})