// pages/StockManage/StockDB.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //商品SKU
    SKU: '',
    //商品数量
    PdtCount: '',
    //商品名称
    PdtName: '',
    //数量
    Count: '',
    //成本单价
    CostPrice: '',
    //仓库信息
    WarehouseList: null,
    //仓库选择
    WarehouseIndex: 0
  },
  //条码输入
  bindKeyInput(e) {
    this.setData({
      SKU: e.detail.value
    })
  },
  //仓库变更
  bindPickerChange: function(e) {
    this.setData({
      WarehouseIndex: e.detail.value
    })
  },
  //条码扫码
  ProductScan() {
    var that = this
    // 只允许从相机扫码
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        that.setData({
          SKU: res.result
        })
        that.BindData()
      }
    })
  },
  //根据条码查询商品信息
  BindData() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    util.postMethods('GetProductInfoForDB', {
      sku: that.data.SKU,
      warehouseNo: app.globalData.warehouseNo
    }, function(data) {
      if (data.IsOK) {
        var resultData = JSON.parse(data.Data)

        that.setData({
          //成本单价
          CostPrice: resultData.CostPrice,
          //商品名称
          PdtName: resultData.PackageName ? resultData.PackageName : resultData.PdtName,
          //商品数量
          PdtCount: resultData.PackageStockCount ? resultData.PackageStockCount : resultData.StockCount
        })
        wx.hideLoading()
      } else {
        wx.hideLoading()
        wx.showModal({
          content: '商品信息获取失败！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库列表
    util.postMethods('GetWarehouseList', {}, function(data) {
      if (data.IsOK) {
        that.setData({
          WarehouseList: JSON.parse(data.Data)
        })
        wx.hideLoading()
      } else {
        wx.hideLoading()
        wx.showModal({
          content: '仓库信息获取失败！',
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  //提交表单
  formSubmit: function(e) {
    var formdata = {
      SKU: e.detail.value.sku,
      FK_WarehouseFrom: app.globalData.warehouseNo,
      FK_WarehouseTo: this.data.WarehouseList[this.data.WarehouseIndex].Code,
      AllocationCount: e.detail.value.count,
      Price: e.detail.value.price,
      Creater: app.globalData.userId
    }
    if (formdata.SKU.length == 0 || this.data.PdtName.length == 0 || this.data.PdtCount.length == 0) {
      wx.showModal({
        content: '请扫描商品信息！',
        showCancel: false,
        confirmColor: "#e4b584",
        confirmText: "确定"
      })
    } else if (formdata.Price.length == 0) {
      wx.showModal({
        content: '请输入成本信息！',
        showCancel: false,
        confirmColor: "#e4b584",
        confirmText: "确定"
      })
    } else if (formdata.AllocationCount.length == 0) {
      wx.showModal({
        content: '请输入调拨数量！',
        showCancel: false,
        confirmColor: "#e4b584",
        confirmText: "确定"
      })
    } else if (parseFloat(formdata.AllocationCount) > parseFloat(this.data.PdtCount)) {
      wx.showModal({
        content: '库存不足，无法调拨！',
        showCancel: false,
        confirmColor: "#e4b584",
        confirmText: "确定"
      })
    } else if (this.data.WarehouseList[this.data.WarehouseIndex].Code == app.globalData.warehouseNo) {
      wx.showModal({
        content: '不能调拨至同一仓库！',
        showCancel: false,
        confirmColor: "#e4b584",
        confirmText: "确定"
      })
    } else {
      var that = this
      wx.showLoading({
        title: '提交中',
        mask: true
      })
      //提交表单
      util.postMethods('CompleteStockAllocation', {
        data: JSON.stringify(formdata)
      }, function(data) {
        if (data.IsOK) {
          that.setData({
            //商品SKU
            SKU: '',
            //商品数量
            PdtCount: '',
            //商品名称
            PdtName: '',
            //数量
            Count: '',
            //成本单价
            CostPrice: ''
          })
          wx.hideLoading()
          wx.showModal({
            content: '调拨成功！',
            showCancel: false,
            confirmColor: "#e4b584",
            confirmText: "确定"
          })
        } else {
          wx.hideLoading()
          wx.showModal({
            content: data.msg + ',调拨失败！',
            showCancel: false,
            confirmColor: "#e4b584",
            confirmText: "确定"
          })
        }
      })
    }
  }
})