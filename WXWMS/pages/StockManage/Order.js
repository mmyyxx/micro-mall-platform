// pages/StockManage/Order.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: []
  },
  //刷新数据
  DataBind: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods('GetPHOrderInfo', {
      storeId: app.globalData.warehouseNo,
      operatorId: app.globalData.userId
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
      } else {
        that.setData({
          List: []
        })
      }
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }

    this.DataBind();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //行点击事件
  ItemClick: function(e) {
    var that = this
    wx.showActionSheet({
      itemList: ['订单明细', '订单出库'],
      success: function(res) {
        if (res.tapIndex == 0) {
          //查看明细
          wx.navigateTo({
            url: '/pages/StockManage/OrderDetail?code=' + e.detail
          })
        } else {
          //订单出库
          wx.showLoading({
            title: '出库中',
            mask: true
          })
          util.postMethods('OrderProductOutStock', {
            orderguid: e.detail,
            warehouseNo: app.globalData.warehouseNo,
            operatorId: app.globalData.userId
          }, function(data) {
            wx.hideLoading()
            wx.showModal({
              content: data.IsOK ? '订单出库成功！' : data.msg,
              showCancel: false,
              confirmColor: "#e4b584",
              confirmText: "确定"
            })
            that.DataBind()
          })
        }
      }
    })
  }
})