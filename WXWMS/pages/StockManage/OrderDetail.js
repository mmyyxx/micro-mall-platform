// pages/StockManage/OrderDetail.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: [],
    orderguid: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      orderguid: options.code
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods('GetOrderDetail', {
      orderguid: that.data.orderguid,
      operatorId: app.globalData.userId
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
        wx.hideLoading()
      } else {
        that.setData({
          List: []
        })
        wx.hideLoading()
        wx.showModal({
          content: data.msg,
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //行点击事件
  ItemClick: function(e) {
    wx.navigateTo({
      url: '/pages/StockManage/ProductStock?sku=' + e.detail + '&code=' + this.data.orderguid
    })
  }
})