// pages/Component/List.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    DataList: {
      type: Object,
      value: null,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    //行点击事件
    ItemClick: function (e) {
      this.triggerEvent("ItemClick", e.currentTarget.id);
      this.triggerEvent("ItemRowClick", e.currentTarget);
    }
  }
})
