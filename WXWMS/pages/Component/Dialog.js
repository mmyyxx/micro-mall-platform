// pages/Component/Dialog.js
const util = require('../../utils/util.js')

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    IsShow: {
      type: Boolean,
      value: false,
    },
    OutGoingGUID: {
      type: String,
      value: '',
    },
    StockGUID: {
      type: String,
      value: '',
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    DataInfo: null,
    PdtCount: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    //数据加载
    DataInit: function() {
      var that = this
      wx.showLoading({
        title: '加载中',
        mask: true
      })
      //获取库存提出信息
      util.postMethods("GetOutGoingStockInfo", {
        outgoingguid: that.data.OutGoingGUID,
        stockguid: that.data.StockGUID
      }, function(data) {
        if (data.IsOK) {
          that.setData({
            DataInfo: JSON.parse(data.Data)
          })
        }
        wx.hideLoading()
      })
    },
    //提出数量
    CountInput: function (e) {
      this.setData({
        PdtCount: e.detail.value
      })
    },
    //取消
    BtnClose: function(e) {
      this.triggerEvent("BtnClose")
    },
    BtnOK: function(e) {
      this.triggerEvent("BtnOK", this.data.PdtCount)
    }
  }
})