// pages/Component/Menu.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    IsShow: {
      type: Boolean,
      value: false,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    //关闭窗口
    BtnClose: function() {
      this.setData({
        IsShow: false
      })
    },
    //点击事件
    BtnClick: function(e) {
      if (e.currentTarget.id != 'Order' && e.currentTarget.id != 'StockDB') {
        wx.showModal({
          content: '功能暂未开通！',
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
        return
      }
      wx.navigateTo({
        url: '/pages/StockManage/' + e.currentTarget.id
      })
      this.setData({
        IsShow: false
      })
    },
    //卡券核销
    NavigateToCard: function(e) {
      var that = this
      // 只允许从相机扫码
      wx.scanCode({
        onlyFromCamera: true,
        success: (res) => {
          wx.navigateTo({
            url: '/pages/StockManage/UserCard?code=' + res.result
          })
        }
      })
    },
    //用户取货
    UserGet: function() {
      // 只允许从相机扫码
      wx.scanCode({
        onlyFromCamera: true,
        success: (res) => {
          wx.navigateTo({
            url: '/pages/StockManage/OrderList?code=' + res.result
          })
        }
      })
    }
  }
})