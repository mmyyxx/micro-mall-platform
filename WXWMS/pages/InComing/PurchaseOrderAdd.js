// pages/InComing/PurchaseOrderAdd.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //供应商
    Supplier: [],
    //供应商选择
    SupplierIndex: 0,
    //订单类型
    OrdertypeList: ['进货入库', '退货入库', '调拨入库'],
    //订单类型选择
    OrderTypeIndex: 0
  },
  //供应商选择
  bindSupplierChange(e) {
    this.setData({
      SupplierIndex: e.detail.value
    })
  },
  //的类型选择
  bindOrderTypeChange(e) {
    this.setData({
      OrderTypeIndex: e.detail.value
    })
  },
  //提交表单保存
  formSubmit: function(e) {
    var that = this
    wx.showLoading({
      title: '保存中',
      mask: true
    })
    //生成入库单
    util.postMethods('SavePurchaseOrderByXCX', {
      warehouseNo: app.globalData.warehouseNo,
      userId: app.globalData.userId,
      ordertype: e.detail.value.ordertype,
      supplierguid: that.data.Supplier[e.detail.value.supplier].Code,
      remark: e.detail.value.remark
    }, function(data) {
      wx.hideLoading()
      wx.showModal({
        content: data.IsOK ? '保存成功！' : '保存失败！',
        showCancel: false,
        confirmColor: "#e4b584",
        confirmText: "确定",
        complete: function() {
          if (data.IsOK) {
            wx.navigateBack({
              delta: 1
            })
          }
        }
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取供应商列表
    util.postMethods('GetSupplierList', {}, function(data) {
      if (data.IsOK) {
        that.setData({
          Supplier: JSON.parse(data.Data)
        })
        wx.hideLoading()
      } else {
        wx.hideLoading()
        wx.showModal({
          content: '供应商信息获取失败！',
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  }
})