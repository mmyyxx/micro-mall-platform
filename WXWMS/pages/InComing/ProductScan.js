// pages/InComing/ProductScan.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //入库单主键
    PurchaseOrderGUID: '',
    //商品SKU
    SKU: '',
    //生产日期
    date: '请选择生产日期',
    //商品数量
    PdtCount: '',
    //商品名称
    PdtName: '',
    //商品保质期
    QualityDate: '',
    //明显信息
    DetailInfo: null,
    //数量
    Count: ''
  },
  //条码输入
  bindKeyInput(e) {
    this.setData({
      SKU: e.detail.value
    })
  },
  //日期选择
  bindDateChange(e) {
    this.setData({
      date: e.detail.value
    })
  },
  //条码扫码
  ProductScan() {
    var that = this
    // 只允许从相机扫码
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        that.setData({
          SKU: res.result
        })
        that.BindData()
      }
    })
  },
  //根据条码查询商品信息
  BindData() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    util.postMethods('GetProductInfo', {
      sku: that.data.SKU,
      guid: that.data.PurchaseOrderGUID
    }, function(data) {
      if (data.IsOK) {
        var resultData = JSON.parse(data.Data)
        var pdtcount = resultData.Count
        if (that.data.SKU == resultData.AuxiliaryUnit) {
          pdtcount = resultData.UnitCount
        }
        that.setData({
          DetailInfo: resultData,
          PdtName: resultData.ProductName,
          PdtCount: pdtcount,
          QualityDate: resultData.Remark
        })
        wx.hideLoading()
      } else {
        wx.hideLoading()
        wx.showModal({
          content: '商品信息获取失败！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      }
    })
  },
  //提交表单
  formSubmit: function(e) {
    if (!this.data.SKU) {
      wx.showModal({
        content: '请输入或扫描条码！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    if (e.detail.value.qualitydate == 0 || !e.detail.value.qualitydate) {
      wx.showModal({
        content: '请输入保质期！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    if (this.data.date == '请选择生产日期') {
      wx.showModal({
        content: '请选择生产日期！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    if (e.detail.value.count == 0 || !e.detail.value.count) {
      wx.showModal({
        content: '请输入商品数量！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定"
      })
      return
    }
    var that = this
    wx.showLoading({
      title: '提交中',
      mask: true
    })
    var detailInfo = that.data.DetailInfo
    //操作人
    detailInfo.Creater = app.globalData.userId
    //生产日期
    detailInfo.ProductionDate = that.data.date
    //提交保存
    util.postMethods('SavePurchaseDetail', {
      //保质期
      qualityDate: e.detail.value.qualitydate,
      //数量
      pdtCount: e.detail.value.count,
      //明显信息
      detailInfo: JSON.stringify(detailInfo)
    }, function(data) {
      wx.hideLoading()
      if (data.IsOK) {
        that.setData({
          //商品SKU
          SKU: '',
          //生产日期
          date: '请选择生产日期',
          //商品数量
          PdtCount: '',
          //商品名称
          PdtName: '',
          //商品保质期
          QualityDate: '',
          //数量
          Count: ''
        })
        wx.showModal({
          content: '保存成功！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      } else {
        wx.showModal({
          content: '保存失败！',
          showCancel: false,
          confirmColor: "#26D1FF",
          confirmText: "确定"
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    if (options.code) {
      this.setData({
        PurchaseOrderGUID: options.code
      })
    } else {
      wx.showModal({
        content: '入库单信息获取失败！',
        showCancel: false,
        confirmColor: "#26D1FF",
        confirmText: "确定",
        complete: function() {
          wx.navigateBack({
            delta: 1
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  }
})