// pages/InComing/PutAway.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ProductGUID: '',
    List: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods('GetPutAwayDetailList', {
      productguid: options.code
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          ProductGUID: options.code,
          List: JSON.parse(data.Data)
        })
      }
      wx.hideLoading()
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //行点击事件
  ItemClick: function(e) {
    var that = this
    var putawayguid = e.detail
    wx.showActionSheet({
      itemList: ['默认储位上架', '实际储位上架'],
      success: function(e) {
        var locationNo = ''
        if (e.tapIndex == 1) {
          // 只允许从相机扫码
          wx.scanCode({
            onlyFromCamera: true,
            success: (res) => {
              locationNo = res.result
              wx.showModal({
                content: '上架至储位' + res.result + '?',
                confirmColor: "#E4B584",
                confirmText: "确定",
                success: function() {
                  wx.showLoading({
                    title: '上架中',
                    mask: true
                  })
                  //完成上架
                  util.postMethods('CompletePutAwayByGUID', {
                    guid: putawayguid,
                    operatorId: app.globalData.userId,
                    warehouseNo: app.globalData.warehouseNo,
                    actualLocationNo: locationNo
                  }, function(data) {
                    wx.hideLoading()
                    if (data.IsOK) {
                      wx.showModal({
                        content: "上架成功！",
                        showCancel: false,
                        confirmColor: "#E4B584",
                        confirmText: "确定"
                      })
                    } else {
                      wx.showModal({
                        content: data.msg,
                        showCancel: false,
                        confirmColor: "#E4B584",
                        confirmText: "确定"
                      })
                    }
                    //获取仓库中需要上架的商品
                    util.postMethods('GetPutAwayDetailList', {
                      productguid: that.data.ProductGUID
                    }, function(data) {
                      if (data.IsOK) {
                        that.setData({
                          List: JSON.parse(data.Data)
                        })
                      } else {
                        that.setData({
                          List: []
                        })
                      }
                    })
                  })
                }
              })
            }
          })
        } else {
          wx.showLoading({
            title: '上架中',
            mask: true
          })
          //完成上架
          util.postMethods('CompletePutAwayByGUID', {
            guid: putawayguid,
            operatorId: app.globalData.userId,
            warehouseNo: app.globalData.warehouseNo,
            actualLocationNo: locationNo
          }, function(data) {
            wx.hideLoading()
            if (data.IsOK) {
              wx.showModal({
                content: "上架成功！",
                showCancel: false,
                confirmColor: "#E4B584",
                confirmText: "确定"
              })
            } else {
              wx.showModal({
                content: data.msg,
                showCancel: false,
                confirmColor: "#E4B584",
                confirmText: "确定"
              })
            }
            //获取仓库中需要上架的商品
            util.postMethods('GetPutAwayDetailList', {
              productguid: that.data.ProductGUID
            }, function(data) {
              if (data.IsOK) {
                that.setData({
                  List: JSON.parse(data.Data)
                })
              } else {
                that.setData({
                  List: []
                })
              }
            })
          })
        }
      }
    })
  }
})