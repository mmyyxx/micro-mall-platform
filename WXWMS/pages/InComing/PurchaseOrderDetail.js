// pages/InComing/PurchaseOrderDetail.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: [],
    OrderGUID: ''
  },
  //数据加载
  BindData() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods('GetNoReadyPurchaseOrderDetail', {
      guid: that.data.OrderGUID
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
      } else {
        that.setData({
          List: []
        })
      }
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.code) {
      this.setData({
        OrderGUID: options.code
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    } else {
      this.BindData()
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  //行点击事件
  ItemRowClick: function(res) {

  }
})