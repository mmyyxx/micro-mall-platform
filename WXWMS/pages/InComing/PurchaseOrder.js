// pages/InComing/PurchaseOrder.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  //数据加载
  BindData() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods('GetNoReadyPurchaseOrders', {
      warehouseNo: app.globalData.warehouseNo
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
      } else {
        that.setData({
          List: []
        })
      }
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
    this.BindData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  //行点击事件
  ItemRowClick: function(res) {
    var that = this
    if (res.detail.dataset.tag != app.globalData.userId) {
      wx.showActionSheet({
        itemList: ['商品扫描', '入库单明细'],
        success: function(e) {
          if (e.tapIndex == 0) {
            wx.navigateTo({
              url: '/pages/InComing/ProductScan?code=' + res.detail.id
            })
          } else {
            wx.navigateTo({
              url: '/pages/InComing/PurchaseOrderDetail?code=' + res.detail.id
            })
          }
        }
      })
    } else {
      wx.showActionSheet({
        itemList: ['商品扫描', '入库单明细', '生成入库单'],
        success: function(e) {
          if (e.tapIndex == 0) {
            wx.navigateTo({
              url: '/pages/InComing/ProductScan?code=' + res.detail.id
            })
          } else if (e.tapIndex == 1) {
            wx.navigateTo({
              url: '/pages/InComing/PurchaseOrderDetail?code=' + res.detail.id
            })
          } else {
            wx.showLoading({
              title: '生成中',
              mask: true
            })
            //生成入库单
            util.postMethods('UpdatePurchaseOrder', {
              orderguid: res.detail.id
            }, function(data) {
              wx.hideLoading()
              wx.showModal({
                content: data.IsOK ? '生成成功！' : '生成失败！',
                showCancel: false,
                confirmColor: "#e4b584",
                confirmText: "确定",
                complete: function() {
                  if (data.IsOK) {
                    that.BindData()
                  }
                }
              })
            })
          }
        }
      })
    }
  },
  //新增入库单
  BtnAdd: function() {
    wx.navigateTo({
      url: '/pages/InComing/PurchaseOrderAdd',
    })
  }
})