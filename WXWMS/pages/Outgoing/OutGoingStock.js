// pages/Outgoing/OutGoingStock.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    OutGoingGUID: '',
    List: []
  },
  //数据查询
  BindList: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库中需要上架的商品
    util.postMethods("GetOutStockOrderInfo", {
      warehouseNo: app.globalData.warehouseNo,
      outgoingguid: that.data.OutGoingGUID,
      operatorId: app.globalData.userId
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
      } else {
        that.setData({
          List: []
        })
      }
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      OutGoingGUID: options.code
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
    this.BindList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //行点击事件
  ItemClick: function(e) {
    var that = this
    wx.showModal({
      content: "是否确认出库？",
      confirmColor: "#e4b584",
      confirmText: "确定",
      success: function (res) {
        if (res.confirm) {
          wx.showLoading({
            title: '出库中',
            mask: true
          })
          util.postMethods('OutStockDetailComplete', {
            detailguid: e.detail,
            outgoingguid: that.data.OutGoingGUID
          }, function (data) {
            wx.hideLoading()
            if (data.IsOK) {
              wx.showModal({
                content: "出库成功！",
                showCancel: false,
                confirmColor: "#e4b584",
                confirmText: "确定",
                success: function (res) {
                  that.BindList()
                }
              })
            }
          })
        }
      }
    })
  }
})