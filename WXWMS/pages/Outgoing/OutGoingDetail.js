// pages/Outgoing/OutGoingDetail.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    IsShow: false,
    OutGoingGUID: '',
    StockGUID: '',
    List: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      OutGoingGUID: options.code
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  BindList: function() {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取商品
    util.postMethods("GetOutGoingDetail", {
      warehouseNo: app.globalData.warehouseNo,
      outgoingguid: that.data.OutGoingGUID
    }, function(data) {
      if (data.IsOK) {
        that.setData({
          List: JSON.parse(data.Data)
        })
      } else {
        that.setData({
          List: []
        })
      }
      wx.hideLoading()
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
    this.BindList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //行点击事件
  ItemClick: function(e) {
    this.setData({
      IsShow: true,
      StockGUID: e.detail
    })
    this.selectComponent("#Dialog").DataInit()
  },
  //关闭
  BtnClose: function() {
    this.setData({
      IsShow: false
    })
    this.BindList()
  },
  //提交
  BtnOK: function(e) {
    var that = this
    wx.showLoading({
      title: '提出中',
      mask: true
    })
    //提出数量
    util.postMethods("OutStockInfo", {
      outgoingguid: that.data.OutGoingGUID,
      stockguid: that.data.StockGUID,
      operatorId: app.globalData.userId,
      stock: e.detail
    }, function(data) {
      wx.hideLoading()
      if (data.IsOK) {
        wx.showModal({
          content: '提出成功！',
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
        that.setData({
          IsShow: false
        })
        that.BindList()
      } else {
        wx.showModal({
          content: data.msg,
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
      }
    })
  }
})