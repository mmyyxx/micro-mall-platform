//index.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({
  data: {
    UserName: '',
    WarehouseName: '',
    IsShow: false
  },
  //页面加载
  onLoad: function() {
    this.setData({
      UserName: app.globalData.userName,
      WarehouseName: app.globalData.warehouseName
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (!app.globalData.userId) {
      wx.redirectTo({
        url: '/pages/index/Welcome'
      })
    }
  },
  //条码扫码
  BtnScan: function() {
    // 只允许从相机扫码
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        wx.navigateTo({
          url: '/pages/index/product?sku=' + res.result
        })
      }
    })
  },
  //入库管理
  BtnInComing: function() {
    wx.showActionSheet({
      itemList: ['入库扫描', '入库上架'],
      success: function(e) {
        if (e.tapIndex == 0) {
          wx.navigateTo({
            url: '/pages/InComing/PurchaseOrder'
          })
        } else {
          wx.navigateTo({
            url: '/pages/InComing/InComing'
          })
        }
      }
    })
  },
  //出库管理
  BtnOutGoing: function() {
    wx.showActionSheet({
      itemList: ['物料提出', '物料出库'],
      success: function(e) {
        wx.navigateTo({
          url: '/pages/Outgoing/Outgoing?code=' + e.tapIndex
        })
      }
    })
  },
  //库内管理
  BtnSetStock: function() {
    this.setData({
      IsShow: true
    })
  },
  //系统设置
  BtnSystem: function() {
    wx.showActionSheet({
      itemList: ['注销'],
      success: function (e) {
        app.globalData.userId = null
        app.globalData.warehouseNo = null
        app.globalData.userName = null
        app.globalData.warehouseName = null
        wx.redirectTo({
          url: '/pages/index/Welcome'
        })
      }
    })
  }
})