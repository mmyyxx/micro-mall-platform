// pages/manage/product.js
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ViewHeight: wx.getSystemInfoSync().windowHeight,
    ProductInfo: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    if (options.sku) {
      util.postWscMethods('GetProductInfoByCode', {
        sku: options.sku
      }, function(data) {
        wx.hideLoading()
        if (data.IsOK) {
          that.setData({
            ProductInfo: JSON.parse(data.Data)
          })
        } else {
          wx.showModal({
            content: '该商品不存在！',
            showCancel: false,
            confirmColor: "#26D1FF",
            confirmText: "确定"
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    if (app.globalData.userId == null) {
      wx.navigateTo({
        url: '/pages/index/index'
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})