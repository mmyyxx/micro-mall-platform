// pages/index/Welcome.js
//获取应用实例
const app = getApp()
const util = require('../../utils/util.js')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ViewHeight: wx.getSystemInfoSync().windowHeight,
    userId: '',
    password: '',
    WarehouseList: null,
    index: 0
  },
  //用户名输入
  UserIdInput: function(e) {
    this.setData({
      userId: e.detail.value
    })
  },
  //密码输入
  PassWordInput: function(e) {
    this.setData({
      password: e.detail.value
    })
  },
  //仓库变更
  bindPickerChange: function(e) {
    this.setData({
      index: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    //获取仓库列表
    util.postMethods('GetWarehouseList', {}, function(data) {
      if (data.IsOK) {
        that.setData({
          WarehouseList: JSON.parse(data.Data)
        })
        wx.hideLoading()
      }
      else{
        wx.hideLoading()
        wx.showModal({
          content: '仓库信息获取失败！',
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
      }
    })
    //获取缓存信息
    wx.getStorage({
      key: 'userInfo',
      success: function(res) {
        that.setData({
          userId: res.data.userId,
          password: res.data.password,
          index: res.data.index
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  BtnLogin: function() {
    var that = this
    wx.showLoading({
      title: '登录中',
      mask: true
    })
    util.postMethods('WMSUserLogin', {
      userId: this.data.userId,
      password: this.data.password
    }, function(data) {
      if (data.IsOK) {
        wx.setStorage({
          key: 'userInfo',
          data: {
            userId: that.data.userId,
            password: that.data.password,
            index: that.data.index
          },
        })
        wx.hideLoading()
        app.globalData.userId = that.data.userId
        app.globalData.userName = data.Data
        app.globalData.warehouseNo = that.data.WarehouseList[that.data.index].Code
        app.globalData.warehouseName = that.data.WarehouseList[that.data.index].Name
        wx.redirectTo({
          url: '/pages/index/index'
        })
      } else {
        wx.hideLoading()
        wx.showModal({
          content: "登录失败！",
          showCancel: false,
          confirmColor: "#e4b584",
          confirmText: "确定"
        })
      }
    })
  }
})