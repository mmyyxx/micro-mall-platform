const requestUrl = require('../config.js').requestUrl
const wscrequestUrl = require('../config.js').wscrequestUrl

const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function postMethods(menthod, newdata, callback, errorback) {
  wx.request({
    url: requestUrl + menthod,
    data: newdata,
    success: function(result) {
      if (result.data.length > 0) {
        try {
          callback(JSON.parse(result.data))
        } catch (e) {
          callback(result.data)
        }
      } else {
        callback(result.data)
      }
    },
    fail: function(result) {
      if (errorback) {
        errorback(result)
      }
    }
  })
}

function postWscMethods(menthod, newdata, callback, errorback) {
  wx.request({
    url: wscrequestUrl + menthod,
    data: newdata,
    success: function (result) {
      if (result.data.length > 0) {
        try {
          callback(JSON.parse(result.data))
        } catch (e) {
          callback(result.data)
        }
      } else {
        callback(result.data)
      }
    },
    fail: function (result) {
      if (errorback) {
        errorback(result)
      }
    }
  })
}

module.exports = {
  formatTime: formatTime,
  postMethods: postMethods,
  postWscMethods: postWscMethods
}