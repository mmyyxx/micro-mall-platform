/**
 * 小程序配置文件
 */

// 此处主机域名是腾讯云解决方案分配的域名
// 小程序后台服务解决方案：https://www.qcloud.com/solution/la

var config = {
  requestUrl: `http://localhost:4066/wsc/WMS/`,
  wscrequestUrl: `http://localhost:4066/wsc/Hmcys/`
  //requestUrl: `https://test1.hmcys.cn/wsc/WMS/`,
  //wscrequestUrl: `https://test1.hmcys.cn/wsc/Hmcys/`
};

module.exports = config